<div class="l-side-office">
    <div class="p-side-office-title">
        <h3>営業拠点</h3>
    </div>
    <div class="p-side-office-photo">
        <p><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_side_photo.png" alt="営業拠点のイメージ写真"></p>
    </div>
    <div class="p-side-office-list">
        <p><a href="<?php echo home_url();?>/access/#tokyo">&gt;&nbsp;東京本社</a></p>
        <p><a href="<?php echo home_url();?>/access/#kansai">&gt;&nbsp;関西支店</a></p>
        <p><a href="<?php echo home_url();?>/access/#nagoya">&gt;&nbsp;名古屋支店</a></p>
        <p><a href="<?php echo home_url();?>/access/#fukuoka">&gt;&nbsp;福岡オフィス</a></p>
    </div>
</div>