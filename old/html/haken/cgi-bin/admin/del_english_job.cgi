#!/usr/bin/perl

#各種設定開始
#ディレクトリ情報ここから
my $TextFileDir = "../secret/english_job";

my $TemplateDir = "../template/english_job";
my $LiblaryDir = "../perl_lib";
#ディレクトリ情報ここまで

#unix上への実装時のパス
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#テンプレートファイルリストここから
my $CompHtml = "delcomplete.html";
my $DeleteCheck = "delfaq.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#各種設定終了

print ("Content-type:text/html\n\n");

#削除対象の情報取得
my $DeleteFaqFile = $ENV{'QUERY_STRING'};

my @Tmp = ();
my $Question = "";
my $Answer = "";
my $Respondent = "";

open FAQFILE, "$TextFileDir/$DeleteFaqFile" or die "Error!!";
while (<FAQFILE>){
	@Tmp = <FAQFILE>;
}
close FAQFILE;

$Question = $Tmp[1];
$Answer = $Tmp[2];
$Respondent = $Tmp[3];


#フォームからデータ取得
&ReadParse(*in);
my $DeleteMode = $in{'DeleteMode'};#動作モードに関する情報取得（"1"で削除動作開始）


#以下、動作モードに応じた処理

#削除動作
if($DeleteMode == 1){
	unlink ("$TextFileDir/$DeleteFaqFile");
	
	my $OutputHtml = "";
	open COMPLETE, "$TemplateDir/$CompHtml";
	while (<COMPLETE>){
		$OutputHtml .= $_;
	}
	close COMPLETE;
	
	print ("$OutputHtml");
}
elsif(length ($DeleteFaqFile) == 0){
	print ('<html><head>');
	print ('<meta http-equiv="refresh" content="0;URL=list_english_job.cgi">');
	print ('</head><body></body></html>');
}
else{
	my $ConfirmForm = "";
	open CONFIRM, "$TemplateDir/$DeleteCheck";
	while (<CONFIRM>){
		$ConfirmForm .= $_;
	}
	close CONFIRM;
	
	$ConfirmForm =~ s/__QUESTION__TEXT__/$Question/;
	$ConfirmForm =~ s/__ANSWER__TEXT__/$Answer/;
	$ConfirmForm =~ s/__RESPONDENT__NAME__/$Respondent/;
	$ConfirmForm =~ s/__DELETE__FILE__NAME__/$DeleteFaqFile/;
	
	print ("$ConfirmForm");
}






