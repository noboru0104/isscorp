<?php get_header(); ?>

<?php
if (have_posts()) : while (have_posts()) : the_post();
	if (in_category('1')) { ?>
		<div>
	<?php }
	else{ ?>
		<div>
	<?php } ?>
			<div id="interpretation" class="l-wrapper">
                <div class="l-pankuzu">
                    <div class="l-inner">
                        <p><a href="<?php echo home_url();?>/">ホーム</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<a href="<?php echo home_url();?>/news">News&nbsp;&amp;&nbsp;Topics</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<?php the_title(); ?></p>
                    </div>
                </div>
                
                <section>
                    <div class="l-mainVisual">
                        <div class="l-mainVisual-inner">
                            <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/about/img_mv_pc.png" alt="ISSについて about">
                        </div>
                    </div>
                </section>
                
                <div class="l-container"> 
                    <div>   
                        
                        
                        <div class="l-aboutBlock">
                            <h3><p>ISSについて</p></h3>
                        </div>            
                        <div class="l-aboutBlock01">
                            <p>
                                ISSは1965年の創業以来、通訳・コンベンション・人材サービスの総合語学プロフェッショナルサービスを提供し、半世紀以上にわたり日本の国際化をともに歩んでまいりました。また、1966年に開設した日本初の同時通訳者養成校では、国際舞台の第一線で活躍する数多くのプロ通訳者・翻訳者、語学スペシャリストを輩出しています。
                            </p>
                            <p>
                                ISSグループでは、長年の実績と総合力を活かし、「質の高いサービスの提供」「人の成長の支援」「社会への貢献」をミッションとして、ISSに関わるすべての人々を豊かにするビジネスを目指しています。
                            </p>  
                            <div class="l-aboutBlock01_2">
                                <div>
                                    <a href=""><img class="is-imgChange2" src="../images/about/btn_main_01_pc.png" alt=""></a>
                                    <a href=""><p class="p-title">トップメッセージ</p></a>
                                </div>
                                <div>
                                    <a href=""><img class="is-imgChange2" src="../images/about/btn_main_02_pc.png" alt=""></a>
                                    <a href=""><p class="p-title">会社概要</p></a>
                                    <a href=""><p>営業拠点</p></a>
                                    <a href=""><p>グループ会社</p></a>
                                </div>
                                <div>
                                    <a href=""><img class="is-imgChange2" src="../images/about/btn_main_03_pc.png" alt=""></a>
                                    <a href=""><p class="p-title">沿革</p></a>
                                </div>                    
                            </div>
                            <div class="l-aboutBlock01_2">
                                <div>
                                    <a href=""><img class="is-imgChange2" src="../images/about/btn_main_04_pc.png" alt=""></a>
                                    <a href=""><p class="p-title">企業理念</p></a>
                                    <a href=""><p>ISSグループ「Vision&Mission」</p></a>
                                    <a href=""><p>ロゴマークの由来</p></a>
                                </div>
                                <div>
                                    <a href=""><img class="is-imgChange2" src="../images/about/btn_main_05_pc.png" alt=""></a>
                                    <a href=""><p class="p-title">CSR</p></a>
                                </div>
                                <div>
                                    <a href=""><img class="is-imgChange2" src="../images/about/btn_main_06_pc.png" alt=""></a>
                                    <a href=""><p class="p-title">創業50周年記念インタビュー</p></a>
                                </div>                    
                            </div>                  
                        </div>
                    </div>
                    <div class="l-side">
                        <div class="l-side-menu">
                            <div class="p-side-menu-title">
                                <h3>ISSについて</h3>
                            </div>
                            <div class="p-side-menu-list">
                                <p><a href="">トップメッセージ</a></p>
                                <p><a href="">会社概要</a></p>
                                <p><a href="">沿革</a></p>
                                <p><a href="">Vision&Mission　企業理念　ロゴマークの由来</a></p>
                                <p><a href="">CSR</a></p>
                                <p><a href="">創業50周年記念インタビュー</a></p>
                            </div>
                        </div>
                        <div class="l-side-office">
                            <div class="p-side-office-title">
                                <h3>営業拠点</h3>
                            </div>
                            <div class="p-side-office-photo">
                                <p><img src="../images/common/img_side_photo.png" alt=""></p>
                            </div>
                            <div class="p-side-office-list">
                                <p><a href="">&gt;&nbsp;東京本社</a></p>
                                <p><a href="">&gt;&nbsp;関西支店</a></p>
                                <p><a href="">&gt;&nbsp;名古屋支店</a></p>
                                <p><a href="">&gt;&nbsp;福岡支店</a></p>
                            </div>
                        </div>
                        <div class="l-side-banner">
                            <div class="p-side-banner-list">
                                <p><a href=""><img src="../images/common/img_side_registration.png" alt=""></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
		</div>
<?php
	endwhile; else: ?>
	<p>記事が見つかりませんでした。</p>
<?php endif; ?>
	<div class="p-singletop"><a href="link/news/">News＆Topics一覧へ戻る</a></div>
	</div>
    
</div>

<?php get_footer(); ?>
