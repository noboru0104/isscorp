#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから

my $TextFileDir = "../secret/english_job";
my $TemplateDir = "../template/english_job";
my $LiblaryDir = "../perl_lib";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "";
my $HtmlTable = "faqtable.html";
my @SrcFile = ("english_job_01.html","english_job_02.html","english_job_03.html","english_job_04.html");
#テンプレートファイルリストここまで

#読み込みファイル名用変数（これを変更することで保存ファイル名が変更される。複数のfaqを設けるときに使用。デフォルトは"faq"）
my $LoadFileHead = "english_job";

#表示の昇順・降順の設定
my $ListOrder = "0";#0が古いもの順で1が新しいもの順。デフォルトは古いもの順

#最終出力文字列
my $HtmlSrc = "";

#テンプレート一時保存用文字列
my $TempStr = "";

#テーブル加工用文字列
my $TableData = "";

#置換用データ
my $TableSrc = "";

#ここまで設定

#表示すべきカテゴリの情報を取得
my $CategoryNo = $ENV{'QUERY_STRING'};

if(length ($CategoryNo) == 0){

}
else{
my $HtmlBody = $SrcFile[$CategoryNo - 1];

#ファイルリスト取得ここから
opendir NEWDIR, "$TextFileDir" or die "Directory open error!!";
my @list = ();
while (my $v = readdir(NEWDIR)){
	if($v =~/^($LoadFileHead.+)\.txt$/){push(@list,$v);}
}
closedir NEWDIR;

@list = sort @list;

if($ListOrder == 0){
}
else{
	@list = reverse @list;
}
#ファイルリスト取得(unix環境下実装用)ここまで

print("Content-type:text/html\n\n");

#ファイル内容一時保存用リスト宣言
my @Tmp = ();
my @Question = ();
my @Answer = ();
my @Respondent = ();
my @Category = ();
my @Detail = ();

#ファイルからデータ取得ここから
foreach my $filename (@list){
	open FAQFILE, "$TextFileDir/$filename" or die "Error!!";
	@Tmp = <FAQFILE>;
	close FAQFILE;
	push(@Detail,$Tmp[0]);
	push(@Category,$Tmp[1]);
	push(@Question,$Tmp[2]);
	push(@Answer,$Tmp[3]);
	push(@Respondent,$Tmp[4]);
}

#ファイルからデータ取得ここまで

#テンプレートファイル読み込み（Html本体（unix用））
open BODYTEMP, "$TemplateDir/$HtmlBody";
while (<BODYTEMP>){
	$HtmlSrc .= $_;
}
close BODYTEMP;

#テンプレートファイル読み込み（加工用テーブル（unix用））
open TABLETEMP, "$TemplateDir/$HtmlTable";
while (<TABLETEMP>){
	$TempStr .= $_;
}
close TABLETEMP;

#テーブルデータ生成開始
for(my $count = 0;$count < @Question;$count++){
	if($Category[$count] == $CategoryNo){
		$TableData = $TempStr;
		$TableData =~ s/__QUESTION__TEXT__/$Question[$count]/;
		$TableData =~ s/__ANSWER__TEXT__/$Answer[$count]/;
		$TableSrc .= $TableData;
	}
	else{}
}

#テーブルデータ生成終了

#最終出力置換
$HtmlSrc =~ s/__ENGLISH__FAQ__/$TableSrc/;

#Htmlソース出力
print $HtmlSrc;
}

