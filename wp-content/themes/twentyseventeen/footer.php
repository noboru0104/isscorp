<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div id="page-top">
    <div><a href="#header"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_top.png" alt="TOPに戻る"></a></div>
</div>
<footer>
	<div class="l-footerBlock">
    	<div class="l-inner">
            <div class="l-footerBlock01">
            	<a href="<?php echo home_url();?>">HOME</a>|<a href="<?php echo home_url();?>/about/">ISSについて</a>|<a href="<?php echo home_url();?>/tsuyaku/">通訳</a>|<a href="<?php echo home_url();?>/kokusai/">コンベンション</a>|<a href="<?php echo home_url();?>/service/">人材サービス</a>|<a href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a>
            </div>
            <div class="l-footerBlock02">
            	<div class="l-footerBlock02-01">
            		<a href="<?php echo home_url();?>">サイトマップ</a><a href="<?php echo home_url();?>">サイトポリシー</a><a href="<?php echo home_url();?>">個人情報保護方針</a><a href="https://privacymark.jp/" target="_blank"><img class="p-image04 footer_privacy" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_privacy_pc.png" alt=""></a><span><a href="<?php echo home_url();?>/english/">ENGLISH</a></span>
                </div>
                <div class="l-footerBlock02-02">
            		<a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_inquiry_pc.png" alt=""></a>
                </div>
            </div>
            <div class="l-footerBlock03">
            	<p>グループサイト</p>
            	<a href="http://haken.issjp.com/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_haken_pc.png" alt=""></a><a href="https://www.issnet.co.jp/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_institute_pc.png" alt=""></a><a class="p-footer-honyaku" href="http://www.honyakuctr.com/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_honyaku_pc.png" alt=""></a>
            </div>
        </div>
    </div>
	<p>Copyright(C) ISS, INC. All Rights Reserved.</p>
</footer>

</body>
</html>
