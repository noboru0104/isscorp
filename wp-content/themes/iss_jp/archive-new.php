
<?php get_header(); ?>
	<div class="mv">
    	<img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/interview/img_mv_01_pc.png" alt="インタビュー一覧">
    </div>
    <div class="container">
    	<div class="breadcrumbs inner-96 clear">
            <ul>
                <li><a href="<?php echo home_url();?>/">HOME</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;</li>
                <li class="p-pankuzu">News & Topics一覧</li>
            </ul>
        </div>
        <section class="new clear">
            <div class="inner-98">
                <div class="newBlock-table clearfix">
                	<?php if (have_posts()) : ?>
                    <?php while (have_posts() ) : the_post(); ?>
                    
                    <div>
                        <p>
                            <a href="<?php the_permalink(); ?>">
                            	
                            	<span class="p-thumbnail_title"><?php the_time('Y.m.d'); ?></span>
                                
                                <?php
								$values = CFS()->get('category');
								foreach ($values as $value => $label) {
									
								}
								?>
                                <?php if ($value === 'ISSインスティテュート'): ?>
                                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category01_pc.png" alt="ISSインスティテュート">
                                <?php elseif ($value === '採用情報'): ?>
                                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category04_pc.png" alt="採用情報">
                                <?php elseif ($value === '通訳者 募集'): ?>
                                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category03_pc.png" alt="通訳者募集">
								<?php elseif ($value === 'コンベンション'): ?>
                                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category02_pc.png" alt="コンベンション">
                                <?php elseif ($value === 'お知らせ'): ?>
                                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category05_pc.png" alt="お知らせ">
                                <?php endif; ?>
                                <span class="p-thumbnail_name"><?php the_title(); ?></span>
                            </a>
                        </p>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
                <!-- ナビゲーション -->
                <div class="activity-nav pconly">
				<?php global $wp_rewrite;
					$paginate_base = get_pagenum_link(1);
					if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
					$paginate_format = '';
					$paginate_base = add_query_arg('paged', '%#%');
					} else {
					$paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
					user_trailingslashit('page/%#%/', 'paged');;
					$paginate_base .= '%_%';
					}
					echo paginate_links( array(
					'base' => $paginate_base,
					'format' => $paginate_format,
					'total' => $wp_query->max_num_pages,
					//'mid_size' => 3,
					'end_size'    => 1,
					'mid_size'    => 3,
					'current' => ($paged ? $paged : 1),
                )); ?>
                </div>
                <div class="activity-nav sponly0">
				<?php global $wp_rewrite;
					$paginate_base = get_pagenum_link(1);
					if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
					$paginate_format = '';
					$paginate_base = add_query_arg('paged', '%#%');
					} else {
					$paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
					user_trailingslashit('page/%#%/', 'paged');;
					$paginate_base .= '%_%';
					}
					echo paginate_links( array(
					'base' => $paginate_base,
					'format' => $paginate_format,
					'total' => $wp_query->max_num_pages,
					//'mid_size' => 3,
					'end_size'    => 0,
					'mid_size'    => 0,
					'current' => ($paged ? $paged : 1),
                )); ?>
                </div>
                <?php wp_reset_postdata(); ?>
                
                
                     
            </div>
        </section>
        <!--/.new-->
    </div>
<?php get_footer(); ?>