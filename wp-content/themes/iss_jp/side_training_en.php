<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>Interpreter and<br>Translator Training</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="#institute">ISS Institute</a></p>
        <p><a href="#training">Language Training Services<br>for Companies</a></p>
    </div>
</div>