#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $TextFileDir = "../../secret/column";

my $TemplateDir = "../../template/column";
my $LiblaryDir = "../../perl_lib";
my $ImageDir = "image";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "listcolumn.html";
my $HtmlTable = "listcolumntable.html";
#テンプレートファイルリストここまで

#読み込みファイル名用変数（これを変更することで保存ファイル名が変更される。複数のfaqを設けるときに使用。デフォルトは"faq"）
my $LoadFileHead = "column";

#表示の昇順・降順の設定
$ListOrder = "1";#0が古いもの順で1が新しいもの順。デフォルトは古いもの順

#最終出力文字列
$HtmlSrc = "";

#テンプレート一時保存用文字列
$TempStr = "";

#テーブル加工用文字列
$TableData = "";

#置換用データ
$TableSrc = "";

#ここまで設定

#ファイルリスト取得(unix環境下実装用)ここから
opendir NEWDIR, "$TextFileDir" or die "Directory open error!!";
my @list = ();
while (my $v = readdir(NEWDIR)){
	if($v =~/^\.+$/){next;}
	push(@list,$v);
}
closedir NEWDIR;

@list = sort @list;
#ファイルリスト取得(unix環境下実装用)ここまで

print("Content-type:text/html\n\n");

#ファイル内容一時保存用リスト宣言
my @Tmp = ();
my @Question = ();
my @Answer = ();
my @Respondent = ();

#ファイル名記憶用リスト宣言
my @TargetName = ();

#ファイルからデータ取得ここから
#古い順に表示
if($ListOrder == 0){
	foreach my $filename (@list){
		if($filename =~/^($LoadFileHead.+)\.txt$/){
			open FAQFILE, "$TextFileDir/$filename" or die "Error!!";
			@Tmp = <FAQFILE>;
			close FAQFILE;
			push(@TargetName,$filename);
			push(@Question,$Tmp[2]);
			push(@Answer,$Tmp[3]);
			push(@Respondent,$Tmp[4]);
		}
	}
}
#新しい順に表示
elsif($ListOrder == 1){
	foreach my $filename (@list){
		if($filename =~/^($LoadFileHead.+)\.txt$/){
			open FAQFILE, "$TextFileDir/$filename" or die "Error!!";
			@Tmp = <FAQFILE>;
			close FAQFILE;
			unshift(@TargetName,$filename);
			unshift(@Question,$Tmp[2]);
			unshift(@Answer,$Tmp[3]);
			unshift(@Respondent,$Tmp[4]);
		}
	}
}
#ファイルからデータ取得ここまで

#テンプレートファイル読み込み（Html本体（unix用））
open BODYTEMP, "$TemplateDir/$HtmlBody";
while (<BODYTEMP>){
	$HtmlSrc .= $_;
}
close BODYTEMP;

#テンプレートファイル読み込み（加工用テーブル（unix用））
open TABLETEMP, "$TemplateDir/$HtmlTable";
while (<TABLETEMP>){
	$TempStr .= $_;
}
close TABLETEMP;

#テーブルデータ生成開始
for(my $count = 0;$count < @Question;$count++){
	$TableData = $TempStr;
	$TableData =~ s/__EDIT__FILE__/$TargetName[$count]/;
	$TableData =~ s/__QUESTION__TEXT__/$Question[$count]/;
	$TableData =~ s/__ANSWER__TEXT__/$Answer[$count]/;
	$TableData =~ s/__RESPONDENT__NAME__/$Respondent[$count]/;
	$TableData =~ s/__DELETE__FILE__/$TargetName[$count]/;
	$TableSrc .= $TableData;
}

#テーブルデータ生成終了

#最終出力置換
$HtmlSrc =~ s/__FAQ__LIST__/$TableSrc/;

#Htmlソース出力
print $HtmlSrc;


