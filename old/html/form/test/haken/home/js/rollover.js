// JavaScript Document

jQuery(document).ready(function(){
// ----------------------------------------------------------------

	// rollover.
	jQuery("img[@class=rollover]").hover(
		function(){
			this.src = this.src.replace(/^(.+)(\.[a-z]+)$/, "$1on$2");
		},
		function(){
			this.src = this.src.replace(/^(.+)on(\.[a-z]+)$/, "$1$2");
		})
		.each(function(){
			this.preloaded = new Image;
			this.preloaded.src = this.src.replace(/^(.+)(\.[a-z]+)$/, "$1on$2");
		}
	);
		
	// rolloveron
	jQuery("img[@class=rolloveron]").each(function(i){
		this.src = this.src.replace(/^(.+)(\.[a-z]+)$/, "$1on$2");
	});

	// subnavi
	jQuery("li.ul01>ul").hide();	
	jQuery(".ul01").hover(
		function()
		{
			jQuery(this).children("ul").show();
		},
		function(){
			jQuery(this).children("ul").hide();
		}
	);

// ----------------------------------------------------------------
});
