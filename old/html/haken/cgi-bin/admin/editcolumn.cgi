#!/usr/bin/perl

#各種設定開始
#ディレクトリ情報ここから
my $TextFileDir = "../secret/trend";

my $TemplateDir = "../template/trend";
my $LiblaryDir = "../perl_lib";
my $ImageDir = "../../home/trend/image";
my $ImageDir2 = "/trend/image";
#ディレクトリ情報ここまで


require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#テンプレートファイルリストここから
my $CompHtml = "editcomplete.html";
my $EditCheck = "editcolumn.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#各種設定終了

print ("Content-type:text/html\n\n");

#編集対象の情報取得
my $EditColumnFile = $ENV{'QUERY_STRING'};

my $ErrorMessage = "";
my @Tmp = ();
my $Question = "";
my $Answer = "";
my $Respondent = "";
my $OldImageFile = "";

open COLUMNFILE, "$TextFileDir/$EditColumnFile" or die "Error!!";
while (<COLUMNFILE>){
	@Tmp = <COLUMNFILE>;
}
close COLUMNFILE;

$Question = $Tmp[1];
$Answer = $Tmp[2];
$Respondent = $Tmp[3];
$OldImageFile = $Tmp[4];

$Question =~ s/\n//g;
$Answer =~ s/\n//g;
$Respondent =~ s/\n//g;
$OldImageFile =~ s/\r\n//g;
$OldImageFile =~ s/\n//g;

$Question =~ s/<br>/\n/g;
$Answer =~ s/<br>/\n/g;
$Respondent =~ s/<br>/\n/g;


#フォームからデータ取得
&ReadParse(*in);
my $EditMode = $in{'EditMode'};#動作モードに関する情報取得（"1"で削除動作開始）
my $QCategory = $in{'Category'};
my $TextQuestion = $in{'TextQuestion'};
my $TextAnswer = $in{'TextAnswer'};
my $RespondentName = $in{'Respondent'};
my $file = $in{'image'};
my $ChangeImage = $in{'ChangeImage'};

#動作エラーチェック
if(length ($TextQuestion) == 0 and $EditMode == 1){
	$ErrorMessage = "全ての情報が記入されていません。もう一度フォームの入力情報をやり直して下さい。";
	$EditMode = 0;
}
else{}
if(length ($TextAnswer) == 0 and $EditMode == 1){
	$ErrorMessage = "全ての情報が記入されていません。もう一度フォームの入力情報をやり直して下さい。";
	$EditMode = 0;
}
else{}
if(length ($RespondentName) == 0 and $EditMode == 1){
	$ErrorMessage = "全ての情報が記入されていません。もう一度フォームの入力情報をやり直して下さい。";
	$EditMode = 0;
}
else{}


#以下、動作モードに応じた処理

if($EditMode == 1){
	#取得したデータから改行コードを削除
	#受け取ったデータの中に改行コードがあると、リスト表示に問題が発生するため
	$TextQuestion =~ s/\r\n/<br>/g;
	$TextAnswer =~ s/\r\n/<br>/g;
	$RespondentName =~ s/\r\n/<br>/g;

	$TextQuestion =~ s/\n/<br>/g;
	$TextAnswer =~ s/\n/<br>/g;
	$RespondentName =~ s/\n/<br>/g;
	
	#画像データアップロードと、旧データの削除
	my $ImageFile = "";
	
	if($ChangeImage == 1){
		if(length($file) == 0){
		}
		else{
		$ImageFile = sprintf("%10d",time());
		$ImageFile = "$ImageFile" . "\.jpeg";
		
		open OUTPUT, ">$ImageDir/$ImageFile";
		binmode(OUTPUT);

		print OUTPUT $file;
		close OUTPUT;
		chmod 0750,"$ImageDir/$ImageFile";
		}
		
		if(length($ImageFile) == 0){
			unlink ("$ImageDir/$ImageFile");
		}
		else{}
	}
	else{
		$ImageFile = "$OldImageFile";
	}
	
	#ファイル保存
	open COLUMN, "> $TextFileDir/$EditColumnFile" or die "";
	print COLUMN ("$EditColumnFile\n");
	print COLUMN ("$QCategory\n");
	print COLUMN ("$TextQuestion\n");
	print COLUMN ("$TextAnswer\n");
	print COLUMN ("$RespondentName\n");
	print COLUMN ("$ImageFile\n");
	close COLUMN;
	
	my $OutputHtml = "";
	open COMPLETE, "$TemplateDir/$CompHtml";
	while (<COMPLETE>){
		$OutputHtml .= $_;
	}
	close COMPLETE;
	
	print ("$OutputHtml");
}
elsif(length ($EditColumnFile) == 0){
	print ('<html><head>');
	print ('<meta http-equiv="refresh" content="0;URL=columnfulllist.cgi">');
	print ('</head><body></body></html>');
}
else{
	my $ImageTag = '<img src="' . "$ImageDir2/$OldImageFile" . '">';
	
	my $EditForm = "";
	open EDIT, "$TemplateDir/$EditCheck";
	while (<EDIT>){
		$EditForm .= $_;
	}
	close EDIT;
	
	$EditForm =~ s/__MESSAGE__TO__USER__/$ErrorMessage/;
	$EditForm =~ s/__QUESTION__MESSAGE__/$Question/;
	$EditForm =~ s/__ANSWER__MESSAGE__/$Answer/;
	$EditForm =~ s/__RESPONDENT__/$Respondent/;
	$EditForm =~ s/__COLUMN__IMAGE__/$ImageTag/;
	$EditForm =~ s/__EDIT__FILE__NAME__/$EditColumnFile/;
	
	print ("$EditForm");
}


