<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<!--<meta name="format-detection" content="telephone=no">-->
<title>ISSコーポレート</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/reset.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/common.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/jquery.bxslider.css">
<!-- ▲共通CSS▲ -->
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon.ico">
<!-- ▼個別CSS▼ -->
<?php if(is_page('kokusai') || is_parent_slug() === 'kokusai' || is_parent_slug() === 'conference'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/kokusai/convention.css">
<?php elseif(is_page('about') || is_parent_slug() === 'about' || is_parent_slug() === 'interview'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/about/about.css">
<?php elseif(is_page('tsuyaku') || is_parent_slug() === 'tsuyaku'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/tsuyaku/interpretation.css">
<?php elseif(is_page('recruit') || is_parent_slug() === 'recruit'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/recruit/recruit.css">
<?php elseif(is_page('service') || is_parent_slug() === 'service'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/service/service.css">
<?php elseif(is_page('training') || is_parent_slug() === 'training'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/training/training.css">
<?php else: ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/top.css">
<?php endif; ?>
<!-- ▲個別CSS▲ -->
<!-- ▼共通JS▼ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/script.js"></script>
<!-- ▲共通JS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/jquery.matchHeight.js"></script>
<?php if(is_page('kokusai') || is_parent_slug() === 'kokusai' || is_parent_slug() === 'conference'): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/kokusai/convention.js"></script>
<?php elseif(is_page('about') || is_parent_slug() === 'about' || is_parent_slug() === 'interview'): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/about/about.js"></script>
<?php elseif(is_page('recruit') || is_parent_slug() === 'recruit'): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/recruit/recruit.js"></script>
<?php else: ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/bxslider.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/top.js"></script>
<?php endif; ?>
<!-- ▲個別JS▲ -->
</head>
<body>
<header id="header">
	<div class="l-headerMenu">
        <div class="l-headerMenu-inner">
            <div class="l-headerMenu-left">
            	<div class="l-headerMenu-left-table">
                	<div class="l-headerMenu-left-table-cell">
                        <h1><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_header_logo_pc.png" alt="株式会社 アイ・エス・エス 通訳・翻訳・国際会議・人材サービス"></a></h1>
                    </div>
                    <div class="l-headerMenu-left-table-cell">
                        <a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_inquiry.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="l-headerMenu-right">
            	<p class="l-headerMenu-right-top pconly"><a class="p-inquiry" href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_inquiry_pc.png" alt="お問い合わせ"></a></p>
                <p class="l-headerMenu-right-bottom pconly"><a class="p-access" href="<?php echo home_url();?>/access/">アクセスマップ</a><a class="p-recruit" href="<?php echo home_url();?>/recruit/">採用情報</a><a class="p-english" href="<?php echo home_url();?>/english/">ENGLISH</a></p>
                
                <div id="toggle_block" class="sponly0">
                	
                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span>
                        </div>
                    </div><!--/#toggle-->
                    <nav>
                        <div id="global-nav-area">
                            <ul id="global-nav">
                                <li class="title"><p><a href="<?php echo home_url();?>">HOME</a></p></li>
                                <li class="title js-underlayer"><p>ISSについて</p><p class="js-menu">＋</p></li>
                                <ul class="js-global-nav-detail">
                                	<li><a href="<?php echo home_url();?>/about/">サービス概要</a></li>
                                    <li><a href="<?php echo home_url();?>/about/">トップメッセージ</a></li>
                                    <li><a href="<?php echo home_url();?>/about/company">会社概要</a></li>
                                    <li><a href="<?php echo home_url();?>/about/history">沿革</a></li>
                                    <li><a href="<?php echo home_url();?>/about/vision">企業理念</a></li>
                                    <li><a href="<?php echo home_url();?>/about/csr">CSR</a></li>
                                    <li><a href="<?php echo home_url();?>/about/interview">創業50周年記念インタビュー</a></li>
                                </ul>
                                <li class="title js-underlayer"><p>通訳</p><p class="js-menu">＋</p></li>
                                <ul class="js-global-nav-detail">
                                	<li><a href="<?php echo home_url();?>/tsuyaku/">サービス概要</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/">ご利用の流れ</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/">サービス内容詳細</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/">実績</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/">FAQ</a></li>
                                </ul>
                                <li class="title js-underlayer">
                                	<p>コンベンション</p>
                                    <p class="js-menu">＋</p>
                                </li>
                                <ul class="js-global-nav-detail">
                                    <li><a href="<?php echo home_url();?>/kokusai/">サービス概要</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/conference">国際会議および学術会議企画・運営</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/event1">企業ミーティング・イベント</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/event2">式典・展示会・見本市・博覧会・スポーツイベント・その他</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/mice">MICEプロモーション・コンサルティング</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/organization">学会・団体事務局受託事業</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/facility">施設運営管理、施設運営サポート業務</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/plans">開催予定と実績一覧</a></li>
                                </ul>
                                <li class="title"><p><a href="<?php echo home_url();?>/service/">人材サービス</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/recruit/">採用情報</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/access/">アクセスマップ</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/english/">お問い合わせ</a></p></li>
                            </ul>
                        </div>
                    </nav>
                </div><!--/#toggle_block-->
            </div>
        </div>        
    </div>
	<div class="l-globalMenu">
        <div class="l-globalMenu-inner">
            <div><a href="<?php echo home_url();?>">HOME</a></div>
            <div>
            	<a class="js-globalMenu" href="<?php echo home_url();?>/about/">ISSについて</a>
            	<div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/about/">サービス概要</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/">トップメッセージ</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/company">会社概要</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/history">沿革</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/vision">企業理念</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/csr">CSR</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/interview">創業50周年記念インタビュー</a></td></tr>
                            </table>
                        </div>
                    </div>
                </div>   
            </div>
            <div>
            	<a class="js-globalMenu" href="<?php echo home_url();?>/tsuyaku/">通訳</a>
            	<div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/">サービス概要</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/">ご利用の流れ</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/">サービス内容詳細</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/">実績</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/">FAQ</a></td></tr>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
            <div>
            	<a class="js-globalMenu" href="<?php echo home_url();?>/kokusai/">コンベンション</a>
            	<div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01 p-long">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/">サービス概要</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/conference">国際会議および学術会議企画・運営</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/event1">企業ミーティング・イベント</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/event2">式典・展示会・見本市・博覧会・スポーツイベント・その他</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/mice">MICEプロモーション・コンサルティング</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/organization">学会・団体事務局受託事業</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/facility">施設運営管理、施設運営サポート業務</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/plans">開催予定と実績一覧</a></td></tr>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
            <div><a href="<?php echo home_url();?>/service/">人材サービス</a></div>
            <div><a href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a></div>
        </div>        
    </div>
</header>
