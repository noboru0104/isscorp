<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>ウェブ会議/ウェビナー支援</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="#webinar">サービス概要</a></p>
        <p><a href="#studio">ISS麹町スタジオ</a></p>
        <p><a href="#achievement">実績</a></p>
        <p><a href="#plan">パッケージプラン</a></p>
    </div>
</div>
