<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>採用情報</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="<?php echo home_url();?>/recruit/registration/">通訳者・翻訳者の登録について</a></p>
        <p class="p-conference">社員採用</p>
        <div class="p-conference-list close">
            <p><a href="<?php echo home_url();?>/recruit/career/">募集職種</a></p>
<!--            <p><a href="<?php echo home_url();?>/recruit/career/interview/">転職者インタビュー</a></p>-->
            <p><a href="<?php echo home_url();?>/recruit/career/program/">人材育成プログラム</a></p>
        </div>
    </div>
</div>