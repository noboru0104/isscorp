#!/usr/bin/perl

#ディレクトリ情報(unix環境下実装用)ここから
my $TextFileDir = "../secret/tips";

my $TemplateDir = "../template/tips";
my $ImageDir = "/tips/image";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "detailcolumn.html";
#テンプレートファイルリストここまで

#最終出力文字列
my $HtmlSrc = "";

#ここまで設定

print("Content-type:text/html\n\n");

opendir NEWDIR, "$TextFileDir" or die "Directory open error!!";
my @list = ();
while (my $v = readdir(NEWDIR)){
	if($v =~/^($LoadFileHead.+)\.txt$/){push(@list,$v);}
}
closedir NEWDIR;

@list = sort @list;

@list = reverse @list;

#ファイルオープン、情報取得
my @FaqInfo = ();
open COLUMNFILE,("$TextFileDir/$list[0]");
@FaqInfo = <COLUMNFILE>;
close COLUMNFILE;

my $FileName = $FaqInfo[0];
my $Category = $FaqInfo[1];
my $Question = $FaqInfo[2];
my $Answer = $FaqInfo[3];
my $Respondent = $FaqInfo[4];
my $ImageFile = $FaqInfo[5];
$ImageFile =~ s/\r\n//;
$ImageFile =~ s/\n//;

my $ImageTag = "";

if(length($ImageFile) == 0){}
else{
	$ImageTag = '<img src="' . "$ImageDir/$ImageFile" . '" alt="">';
}

#テンプレート読み込み
open TEMPLATE,("$TemplateDir/$HtmlBody");
while(<TEMPLATE>){
	$HtmlSrc .= $_;
}
close TEMPLATE;

#置換処理
$HtmlSrc =~ s/__QUESTION__TEXT__/$Question/;
$HtmlSrc =~ s/__ANSWER__TEXT__/$Answer/;
$HtmlSrc =~ s/__RESPONDENT__NAME__/$Respondent/;
$HtmlSrc =~ s/__COLUMN__IMAGE__/$ImageTag/;

#HTML出力


print ("$HtmlSrc");