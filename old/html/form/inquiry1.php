<?

$pagecheck = "0";
if(getenv("HTTP_REFERER") == "https://www.issjp.com/form/inquiry.php"){$pagecheck = "1";}
if(getenv("HTTP_REFERER") == "https://202.212.213.210/form/inquiry.php"){$pagecheck = "1";}

if(getenv("HTTP_REFERER") == "https://www.issjp.com/form_test/inquiry.php"){$pagecheck = "1";}

if($pagecheck == "0"){
	header("Location: https://www.issjp.com/form/inquiry.php");
	exit;
}

$agree = $_POST['agree'];

if($agree == ""){
	header("Location: https://www.issjp.com/form/inquiry.php");
	exit;
}

$checkbox1 = $_POST['checkbox1'];
$checkbox2 = $_POST['checkbox2'];
$checkbox3 = $_POST['checkbox3'];
$checkbox4 = $_POST['checkbox4'];
//$checkbox5 = $_POST['checkbox5'];
$checkbox6 = $_POST['checkbox6'];

$toiawase = mb_convert_kana(htmlspecialchars($_POST['toiawase']),"KV","EUC-JP");
$company = mb_convert_kana(htmlspecialchars($_POST['company']),"KV","EUC-JP");
$depart = mb_convert_kana(htmlspecialchars($_POST['depart']),"KV","EUC-JP");

$gyoukai = $_POST['gyoukai'];

$name = mb_convert_kana(htmlspecialchars($_POST['name']),"KV","EUC-JP");
$name2 = mb_convert_kana(htmlspecialchars($_POST['name2']),"KV","EUC-JP");
$tel = mb_convert_kana(htmlspecialchars($_POST['tel']),"KV","EUC-JP");
$mail = mb_convert_kana(htmlspecialchars($_POST['mail']),"KV","EUC-JP");
$zip21 = mb_convert_kana(htmlspecialchars($_POST['zip21']),"KV","EUC-JP");
$zip22 = mb_convert_kana(htmlspecialchars($_POST['zip22']),"KV","EUC-JP");
$pref21 = mb_convert_kana(htmlspecialchars($_POST['pref21']),"KV","EUC-JP");
$addr21 = mb_convert_kana(htmlspecialchars($_POST['addr21']),"KV","EUC-JP");
$strt21 = mb_convert_kana(htmlspecialchars($_POST['strt21']),"KV","EUC-JP");

$kikkake = $_POST['kikkake'];
if ($kikkake=="その他"){
	$sonota = mb_convert_kana(htmlspecialchars($_POST['sonota']),"KV","EUC-JP");
}else{
	$sonota = "";
}

$err = "";

if ($checkbox1 == "" and $checkbox2 == "" and $checkbox3 == "" and $checkbox4 == "" and $checkbox5 == "" and $checkbox6 == ""){$err .= "・お問い合わせサービス<br>";}
if ($toiawase == ""){$err .= "・お問い合わせ内容<br>";}
if ($company == ""){$err .= "・会社（団体）名<br>";}
if ($name == ""){$err .= "・お名前<br>";}
if ($name2 == ""){$err .= "・お名前（フリガナ）<br>";}
if ($tel == ""){$err .= "・お電話番号<br>";}
if ($mail == "" or !ereg("^[^@]+@[^.]+\..+", $mail)){$err .= "・e-mailアドレス<br>";}

if ($err != ""){$err = "以下の項目の入力が正しくありません。<br>" . $err . "<br>戻るボタンでお戻りのうえ、お手数ですがもう一度入力しなおしてください。 ";}


$services = "";
$mailto = "";

if($checkbox1 != ""){
	$services .= "通訳\r\n";
	$mailto .= "ask_conv@issjp.com,";
}
if($checkbox2 != ""){
	$services .= "国際会議企画・運営\r\n";
	$mailto .= "ask_conference@issjp.com,";
}
if($checkbox3 != ""){
	$services .= "翻訳\r\n";
	$mailto .= "ask_honyaku@issjp.com,";
}
if($checkbox4 != ""){
	$services .= "人材派遣・紹介予定派遣\r\n";
	$mailto .= "haken@issjp.com,";
}

//if($checkbox5 != ""){
//	$services .= "法人向け語学研修\r\n";
//	$mailto .= "kensyu@issjp.com,";
//}

if($checkbox6 != ""){
	$services .= "その他\r\n";
//	$mailto .= "info@issjp.com,";
}

$services = substr($services,0,strlen($services)-2);
$mailto .= "info@issjp.com";



if($zip21 != "" and $zip22 != ""){
	$zipcode = "〒" . $zip21 . "-" . $zip22;
}else{
	$zipcode = "";
}

$address = $pref21 . $addr21 . $strt21; 



?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="keywords" content="ISS,アイ・エス・エス,お問い合わせ" />
<title>確認 | ISS総合お問い合わせフォーム</title>
<link href="common/form_style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>

<!-- Header Begin -->
<div id="header">
<div class="contentform"><a href="../index.html"><img src="../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../index.html"><img src="../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi"></div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<div id="formcont">
<h2><img src="images/ind_h2.jpg" alt="お問い合わせフォーム" width="700" height="78" /></h2><form method="POST" action="inquiry2.php">

<input type="hidden" name="services" value="<? echo $services; ?>" />
<input type="hidden" name="mailto" value="<? echo $mailto; ?>" />
<input type="hidden" name="toiawase" value="<? echo $toiawase; ?>" />
<input type="hidden" name="company" value="<? echo $company; ?>" />
<input type="hidden" name="depart" value="<? echo $depart; ?>" />
<input type="hidden" name="gyoukai" value="<? echo $gyoukai; ?>" />
<input type="hidden" name="name" value="<? echo $name; ?>" />
<input type="hidden" name="name2" value="<? echo $name2; ?>" />
<input type="hidden" name="tel" value="<? echo $tel; ?>" />
<input type="hidden" name="mail" value="<? echo $mail; ?>" />
<input type="hidden" name="zipcode" value="<? echo $zipcode; ?>" />
<input type="hidden" name="address" value="<? echo $address; ?>" />
<input type="hidden" name="kikkake" value="<? echo $kikkake; ?>" />
<input type="hidden" name="sonota" value="<? echo $sonota; ?>" />

<table id="confirm">
  <tr>
    <td colspan="2" class="t1">入力内容確認</td>
  </tr>

<? if ($err != "") { ?>
		<tr>
			<td colspan="2" class="error">
				<br>
				<? echo $err;?>			</td>
		</tr>
<? } ?>


<tr>
<th width="200"><nobr>お問い合わせサービス</nobr></th>
<td class="hisu">
<? echo ereg_replace("\r\n","<br>",$services); ?>
</td>
</tr>

<tr>
<th>お問い合わせ内容</th>
<td class="hisu">
<?php echo  ereg_replace("\r\n","<br>",$toiawase); ?>
</td>
</tr>

<tr>
<th>会社（団体）名</th>
<td class="hisu">
<? echo $company; ?></td>
</tr>

<tr>
<th>部署名</th>
<td class="hisu">
<? echo $depart; ?></td>
</tr>

<tr>
<th>業界</th>
<td class="hisu">
<? echo $gyoukai; ?></td>
</tr>

<tr>
<th>お名前</th>
<td class="hisu">
<? echo $name; ?></td>
</tr>

<tr>
<th>お名前（フリガナ）</th>
<td class="hisu">
<? echo $name2; ?></td>
</tr>

<tr>
<th>お電話番号</th>
<td class="hisu">
<? echo $tel; ?></td>
</tr>

<tr>
<th>e-maiアドレス</th>
<td class="hisu">
<? echo $mail ?></td>
</tr>

<tr>
<th>ご住所</th>
<td class="hisu">
<? echo $zipcode; ?><br />
<? echo $address; ?>
</td>
</tr>

<tr>
<th>当社をどのように<br>
お知りになりましたか</th>
<td class="hisu"><? echo $kikkake; ?>  <? echo $sonota; ?></td>
</tr>


<tr>
<td class="btnForm" colspan="2"><input type="submit" name="btn_send" style="font-size:11pt;" value="　送　信　" <? if($err != ""){echo " disabled";} ?>></td>
</tr>
<tr>
<td class="btnForm" colspan="2"><input type="button" name="btn_back" style="font-size:11pt;" value="　戻　る　" onClick="history.back()"></td>
</tr>
</table>

</form>
</div>
    <!-- コンテンツ -->

	
<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../common/images/pixel_trans.gif" width="816" height="15" alt="" class="spacer" />
<div class="menuList">
<ul>
  <li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>
</div>
</div>
</div>
<!-- Footer End -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body></html>
