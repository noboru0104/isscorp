#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $DataFileDir = "../secret/job";

my $TemplateDir = "../template/job";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "workfulllist.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#データファイルリストここから
my $WorkInfo = "workinfo.csv";
#データファイルリストここまで

#モジュール読み込み
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#出力HTMLソース一時格納用変数
my $HtmlSrc = "";

#テンプレート一時保存用文字列
my $TempStr = "";

#埋め込みテーブル用文字列
my $TableSrc = "";

#各種設定ここまで

print("Content-type:text/html\n\n");;

open DATA, "$DataFileDir/$WorkInfo" or die "file not exist!";

while (<DATA>){
	my @TmpWorkInfo = split(/,/,$_);
	$TmpWorkInfo[7] = &ConvertDay($TmpWorkInfo[7],$TmpWorkInfo[15]);
	$TmpWorkInfo[3] = &ConvertJob($TmpWorkInfo[3]);
	$TmpWorkInfo[6] = &ConvertPlace($TmpWorkInfo[6],$TmpWorkInfo[13]);
my $WorkDatas = << "EOM";
<tr>
<td> <a href="editworkinfo.cgi?$TmpWorkInfo[14]">編集</a> </td>
<td> $TmpWorkInfo[1] </td>
<td> $TmpWorkInfo[2] </td>
<td> $TmpWorkInfo[3] </td>
<td> $TmpWorkInfo[0] </td>
<td> $TmpWorkInfo[4] </td>
<td> $TmpWorkInfo[6] </td>
<td> $TmpWorkInfo[7] </td>
<td> $TmpWorkInfo[8] </td>
<td> $TmpWorkInfo[9] 〜 $TmpWorkInfo[10]</td>
<td> $TmpWorkInfo[11] </td>
<td> <a href="delworkinfo.cgi?$TmpWorkInfo[14]">削除</a> </td>
</tr>
<tr>
<td colspan="12" height="3" bgcolor="#cc66bb"></td>
</tr>
EOM

$TableSrc = "$WorkDatas" . "$TableSrc";
}
close DATA;

#HTML本体読み込み
open BODYTEMP, "$TemplateDir/$HtmlBody";
while (<BODYTEMP>){
	$HtmlSrc .= $_;
}
close BODYTEMP;

#テーブル情報埋め込み処理
$HtmlSrc =~ s/__WORK__FULL__INFORMATION__/$TableSrc/;

#ソース出力
print ("$HtmlSrc");

#以下サブルーチン
sub ConvertJob{
	my $ConvertedJob = "";
	my @GetJobData = @_;
	#変換用仕事データ
	my @JobList = ('','通訳・翻訳・翻訳チェッカー','秘書・グループセクレタリー','英文事務・アシスタント・英文経理・貿易事務・人事・総務など','金融業界でのお仕事','IT・技術関連');
	$ConvertedJob = $JobList[$GetJobData[0]];
	return $ConvertedJob;
}

sub ConvertPlace{
	my $ConvertedPlace = "";
	my @GetPlaceData = @_;
	#変換用勤務場所データ
	my @PlaceList = ('','神谷町','赤坂見附','大手町','青山一丁目','茅場町','九段下','新宿','溜池山王','表参道','みなとみらい');
	if ($GetPlaceData[0] == -1){
		$ConvertedPlace = $GetPlaceData[1];
	}
	else{
		$ConvertedPlace = $PlaceList[$GetPlaceData[0]];
	}
	return $ConvertedPlace;
}

sub ConvertDay{
	my $ConvertedDay = "";
	my @GetDayData = @_;
	if ($GetDayData[0] == 1){
		$ConvertedDay = "平日";
	}
	elsif ($GetDayData[0] == 0){
		$ConvertedDay = "土・日";
	}
	elsif ($GetDayData[0] == -1){
		$ConvertedDay = $GetDayData[1];
	}
	else{
		if(($GetDayData[0] % 2) == 0){$ConvertedDay .= "月・";}
		if(($GetDayData[0] % 3) == 0){$ConvertedDay .= "火・";}
		if(($GetDayData[0] % 5) == 0){$ConvertedDay .= "水・";}
		if(($GetDayData[0] % 7) == 0){$ConvertedDay .= "木・";}
		if(($GetDayData[0] % 11) == 0){$ConvertedDay .= "金・";}
		if(($GetDayData[0] % 13) == 0){$ConvertedDay .= "土・";}
		if(($GetDayData[0] % 17) == 0){$ConvertedDay .= "日・";}
		chop ($ConvertedDay);
		chop ($ConvertedDay);
	}
	return $ConvertedDay;

}