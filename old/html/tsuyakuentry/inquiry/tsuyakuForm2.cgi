#!/usr/bin/perl
############################################################
#
#    ISS 通訳者の登録フォーム
#
############################################################
require './cgi-lib.pl';
require './jcode.pl';
use CGI::Carp qw(fatalsToBrowser);

############################################################
#    設定項目
############################################################

## 送信先メールアドレス
$toAdd = 'reg_honyaku@issjp.com';

## CCメールアドレス
$ccAdd = '';

## BCCメールアドレス
$bccAdd = '';

## 送信者メールアドレス
$fromAdd = 'trans3@issjp.com';

## メールタイトル
$subjectUser = 'ISS：通訳者登録へのご応募ありがとうございます';
$subjectAdmin = '通訳者登録の応募がありました ';

## 確認画面HTML
$confirmHtmlFile = './tsuyaku_confirm.html';

## エラー画面HTML
$errorHtmlFile = './tsuyaku_error.html';

## 完了画面HTML
$endHtmlFile = './tsuyaku_complete.html';

## sendmailパス
$sendmail = "/usr/sbin/sendmail -t -f$fromAdd";

############################################################
#    プログラム開始
############################################################
&decode();
&setTime();
&checkForm();

if($g_html{'f_conf'}){
	&outHtml($confirmHtmlFile);
}elsif($g_html{'f_exe'}){
	&sendMailUser();
	&sendMailAdmin();
	&outHtml($endHtmlFile);
}

############################################################
#    フォーム処理
############################################################
sub decode{
	my ($key,$value);
	
	&ReadParse();
	while(($key,$value) = each %in){
		&jcode::convert(\$key,'euc');
		&jcode::convert(\$value,'euc','','z');
		$value =~ s/&/&amp;/gi;
		$value =~ s/\"/&quot;/gi;
		$value =~ s/\'/&#39;/gi;
		$value =~ s/</&lt;/gi;
		$value =~ s/>/&gt;/gi;
		$value =~ s/&lt;br&gt;/<br>/gi;
		$value =~ s/\r\n?/\n/g;
		$value =~ s/\n/<br>/g;
		$value =~ s/\0/ /g;
		
		if($key !~ /^f_/){
			$g_hidden .= qq(<input type="hidden" name="$key" value="$value">\n);
		}
		$g_html{$key} = $value;
	}
	$g_html{'hidden'} = $g_hidden;
	
}

############################################################
#    日時取得
############################################################
sub setTime{
	my ($sec,$min,$hour,$day,$mon,$year,$wday) = localtime();
	
	$g_time{'sec'} = sprintf("%02d",$sec);
	$g_time{'min'} = sprintf("%02d",$min);
	$g_time{'hour'} = sprintf("%02d",$hour);
	$g_time{'day'} = sprintf("%02d",$day);
	$g_time{'mon'} = sprintf("%02d",$mon+1);
	$g_time{'year'} = sprintf("%04d",$year+1900);
	$g_time{'all'} = "$g_time{'year'}/$g_time{'mon'}/$g_time{'day'} $g_time{'hour'}:$g_time{'min'}:$g_time{'sec'}";
}

############################################################
#    エラーチェック
############################################################
sub checkForm{
	my $flag = 1;
	
	if($g_html{'name1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'name2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・名」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'kana1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓・カタカナ」が入力されていません。</li>);
		$flag = 0;
	}elsif($g_html{'kana1'} !~ /^(\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc])+$/){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓・カタカナ」にカタカナ以外が入力されています。</li>);
		$flag = 0;
	}
	if($g_html{'kana2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・名・カタカナ」が入力されていません。</li>);
		$flag = 0;
	}elsif($g_html{'kana2'} !~ /^(\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc])+$/){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・名・カタカナ」にカタカナ以外が入力されています。</li>);
		$flag = 0;
	}
#	if($g_html{'birth_year'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「生年月日・年」が入力されていません。</li>);
#		$flag = 0;
#	}
#	if($g_html{'birth_month'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「生年月日・月」が入力されていません。</li>);
#		$flag = 0;
#	}
#	if($g_html{'birth_day'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「生年月日・日」が入力されていません。</li>);
#		$flag = 0;
#	}
	if($g_html{'sex'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「性別」が選択されていません。</li>);
		$flag = 0;
	}
	if($g_html{'email'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「メールアドレス」が入力されていません。</li>);
		$flag = 0;
	}else{
		if($g_html{'email'} !~ /^[\w\-\~\.]+@[\w\-\~].[\w\-\~\.]+$/){
			$g_html{'error'} .= qq(<li class="mtx">「メールアドレス」の形式が不正です。</li>);
			$flag = 0;
		}
	}
	$zip = $g_html{'zip1'} . $g_html{'zip2'};
	if($g_html{'zip1'} eq '' or $g_html{'zip2'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「郵便番号」が入力されていません。</li>);
#		$flag = 0;
	}elsif($zip !~ /^\d{7}$/){
		$g_html{'error'} .= qq(<li class="mtx">「郵便番号」が正しく入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'pref'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「都道府県」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'address1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「郡市区町村 」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'address2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「番地」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'tel1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「TEL・市外局番」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'tel2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「TEL・市内局番」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'tel3'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「TEL・下4桁」が入力されていません。</li>);
		$flag = 0;
	}
	$tel = $g_html{'tel1'} . $g_html{'tel2'} . $g_html{'tel3'};
	if($tel !~ /[0-9]/){
##	if($tel !~ /^\d{10}$/){
		$g_html{'error'} .= qq(<li class="mtx">「TEL」が正しく入力されていません。</li>);
		$flag = 0;
	}
	$fax = $g_html{'fax1'} . $g_html{'fax2'} . $g_html{'fax3'};
	if($fax ne ''){
		if($fax !~ /^\d{10}$/){
			$g_html{'error'} .= qq(<li class="mtx">「FAX」が正しく入力されていません。</li>);
			$flag = 0;
		}
	}
#	if($g_html{'agree'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「サービスの利用規約」にチェックが入っていません。</li>);
#		$flag = 0;
#	}
	
	if($flag == 0){
#		$g_html{'error'} .= qq(<li class="mtx">ブラウザの戻るボタンでお戻りのうえ、お手数ですがもう一度入力しなおしてください。 </li>);
		&outHtml($errorHtmlFile);
	}
	
	if($g_html{'lang1'} ne ''){
		$g_html{'lang1'} .= qq(，　);
	}
	if($g_html{'lang2'} ne ''){
		$g_html{'lang2'} .= qq(，　);
	}
	if($g_html{'lang3'} ne ''){
		$g_html{'lang3'} .= ' ' . qq(（言語： ) . $g_html{'lang3_name'} . qq(），　);
	}
	if($g_html{'lang4'} ne ''){
		$g_html{'lang4'} .= ' ' . qq(（言語： ) . $g_html{'lang4_name'} . qq(）);
	}

#	for($i=1;$i<=4;$i++){
#		$name = 'lang' . $i;
#	 if($g_html{$name}){
#		$g_html{'lang'} .= $g_html{$name} . ' ';
#	}


	}


#}

############################################################
#    HTML出力
############################################################
sub outHtml{
	my $file = shift;
	my ($html,@htmlList,$htmlAll);
	my @tmp;
	
	open(HTML,"<$file") or die();
	read(HTML,$html,-s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	foreach (@htmlList){
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_."\n";
		}else{
			$htmlAll .= $_."\n";
		}
	}
	
#	&jcode::convert(\$htmlAll,'sjis','euc');
	print "Content-type:text/html; charset=euc-jp\n\n";
	print $htmlAll;
	
	exit;
}

############################################################
#    エンドユーザメール送信
############################################################
sub sendMailUser{
	my $mailBody;

	foreach $key(keys %g_html){
		$value = $g_html{$key};
		$value =~ s/&amp;/&/gi;
		$value =~ s/&lt;/</gi;
		$value =~ s/&gt;/>/gi;
		$g_html{$key} = $value;
	}
	
	$mailBody = <<MAIL;
To: $g_html{'email'}
From: $fromAdd
Subject: $subjectUser
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

$g_html{'name1'} $g_html{'name2'} 様

通訳者登録にご応募いただき誠にありがとうございます。 
このメールはシステムより自動的に送信されております。 

ご応募の内容を拝見し、今後お仕事でご一緒できる可能性のある方には、 
弊社よりご連絡させていただきます。 

万が一このメールに心当たりがない場合には、大変お手数ですが 
下記アドレスまでお知らせいただきますようお願いいたします。 
通訳グループアドレス：trans3\@issjp.com 

今後ともよろしくお願い申し上げます。 

------------------------------------- 
株式会社アイ・エス・エス 
営業統括部　通訳グループ 
TEL: 
------------------------------------- 

MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}

############################################################
#    管理者宛メール送信
############################################################
sub sendMailAdmin{
	my $mailBody;
	
	$g_html{'career'} =~ s/<br>/\n/g;
	$g_html{'qualification'} =~ s/<br>/\n/g;
	$g_html{'achievement'} =~ s/<br>/\n/g;
	$g_html{'salespoint'} =~ s/<br>/\n/g;
	
	$mailBody = <<MAIL;
To: $toAdd
From: $g_html{'email'}
Cc: $ccAdd
Bcc: $bccAdd
Subject: $subjectAdmin
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

通訳者登録の応募がありました。
■氏名
$g_html{'name1'} $g_html{'name2'}
■フリガナ
$g_html{'kana1'} $g_html{'kana2'}
■生年月日
$g_html{'birth_year'}年$g_html{'birth_month'}月$g_html{'birth_day'}日
■性別
$g_html{'sex'}
■E-mailアドレス
$g_html{'email'}
■郵便番号
$g_html{'zip1'}-$g_html{'zip2'}
■都道府県
$g_html{'pref'}
■郡市区町村
$g_html{'address1'}
■番地
$g_html{'address2'}
■TEL
$g_html{'tel1'}-$g_html{'tel2'}-$g_html{'tel3'}
■FAX
$g_html{'fax1'}-$g_html{'fax2'}-$g_html{'fax3'}
■一日の翻訳量
$g_html{'performance_word'}ワード　$g_html{'performance_page'}ページ
■言語の種類
$g_html{'lang1'}
$g_html{'lang2'}
$g_html{'lang3'}
$g_html{'lang4'}
■最終学歴、職業
$g_html{'career'}
■資格、取得技術
$g_html{'qualification'}
■直近5年間の主要な実績
$g_html{'achievement'}
■アイ・エス・エスを知った経緯
$g_html{'salespoint'}
MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}
