#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $DataFileDir = "../secret/job";

my $TemplateDir = "../template/job";
my $LiblaryDir = "../perl_lib";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "workinfo.html";
my $HtmlTable = "workinfotable.html";
#テンプレートファイルリストここまで

#データファイルリストここから
my $WorkInfo = "newworkinfo.csv";
#データファイルリストここまで

#モジュール読み込み
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
&ReadParse;

#出力HTMLソース一時格納用変数
my $HtmlSrc = "";

#埋め込みテーブル用一時格納変数
my $TableSrc = "";

#テンプレート一時保存用文字列
my $TempStr = "";

#テーブル加工用文字列
my $TableData = "";

#表示件数
my $DisplayDataNumber = 5;

#各種設定ここまで

print("Content-type:text/html\n\n");

if( length($in{'start'})==0){
	$start=0;
}else{
	$start=$in{'start'};
}

#HTMLソース読み込み
open BODYTEMP, "$TemplateDir/$HtmlBody";
while (<BODYTEMP>){
	$HtmlSrc .= $_;
}
close BODYTEMP;

#テーブルテンプレート読み込み
open TABLETEMP, "$TemplateDir/$HtmlTable";
while (<TABLETEMP>){
	$TempStr .= $_;
}
close TABLETEMP;


#データファイル読み込み
my @AskNo = (); 
my @JobNo = ();
my @Catch = ();
my @JobKind = ();
my @Wage = ();
my @WorkPlace = ();
my @PlaceData = ();
my @WorkDay = ();
my @AddWork = ();
my @StartTime = (); 
my @EndTime = ();
my @WorkPeriod = ();
my @Bikou = ();
my @UniqueNo = ();

open DATA, "$DataFileDir/$WorkInfo" or die "file not exist!";
$record_count=0;
while (<DATA>){
	my @TmpWorkInfo = split(/,/,$_);
	push(@AskNo,$TmpWorkInfo[0]); 
	push(@JobNo,$TmpWorkInfo[1]);
	push(@Catch,$TmpWorkInfo[2]);
	push(@JobKind,&ConvertJob($TmpWorkInfo[3])); 
	push(@Wage,$TmpWorkInfo[4]);
	push(@WorkPlace,&ConvertPlace($TmpWorkInfo[6],$TmpWorkInfo[13]));
	push(@PlaceData,$TmpWorkInfo[13]); 
	push(@WorkDay,&ConvertDay($TmpWorkInfo[7],$TmpWorkInfo[15]));
	push(@AddWork,$TmpWorkInfo[8]);
	push(@StartTime,$TmpWorkInfo[9]); 
	push(@EndTime,$TmpWorkInfo[10]);
	push(@WorkPeriod,$TmpWorkInfo[11]);
	push(@Bikou,$TmpWorkInfo[12]);
	push(@DayData,$TmpWorkInfo[15]);
	push(@UniqueNo,$TmpWorkInfo[14]);
	$record_count++;
}
close DATA;
#printf $record_count;
#テーブルデータ生成

for(my $count = $start ;$count < ($start + $DisplayDataNumber);$count++){
	$TableData = $TempStr;
	$TableData =~ s/__CATCH__/$Catch[$count]/;
	$TableData =~ s/__WORK__NO__/$JobNo[$count]/;
	$TableData =~ s/__DETAIL__NO__/$UniqueNo[$count]/;
	$TableData =~ s/__JOB__TYPE__/$AskNo[$count]/;
	if(length($AskNo[$count]) ==0 ){
		$TableData =~ s/__JOB__NAME__/　/;
	}else{
		$TableData =~ s/__JOB__NAME__/$AskNo[$count]/;
	}
	$TableData =~ s/__WAGE__/$Wage[$count]/;
	$TableData =~ s/__WORK__PLACE__/$WorkPlace[$count]/;
	$TableData =~ s/__WORK__DAY__/$WorkDay[$count]/;
	$TableData =~ s/__START__TIME__/$StartTime[$count]/;
	$TableData =~ s/__END__TIME__/$EndTime[$count]/;
	$TableSrc .= $TableData;
}

#置換処理
$HtmlSrc =~ s/__NEW__INFORMATION__/$TableSrc/g;

if($start <= $DisplayDataNumber){
	$prev=0;
}else{
	$prev=$start-$DisplayDataNumber;
}

if($start > 0){
	$HtmlSrc =~ s/__PREV_LINK__/<a href=.\/jobinfo.cgi?start=$prev><img src=\/job\/images\/job_prev_button.gif width=49 height=11 border=0><\/a>/g;
}else{
	$HtmlSrc =~ s/__PREV_LINK__//g;
}


if($start > $record_count-$DisplayDataNumber){
	$next=$record_count-$DisplayDataNumber;
}else{
	$next=$start+$DisplayDataNumber;
}

if($start < ($record_count-$DisplayDataNumber)){
	$HtmlSrc =~ s/__NEXT_LINK__/<a href=.\/jobinfo.cgi?start=$next><img src=\/job\/images\/job_next_button.gif width=49 height=11 border=0><\/a>/g;
}else{
	$HtmlSrc =~ s/__NEXT_LINK__//g;
}

#HTML出力

print ("$HtmlSrc");

#以下サブルーチン
sub ConvertPlace{
	my $ConvertedPlace = "";
	my @GetPlaceData = @_;
	#変換用勤務場所データ
	my @PlaceList = ("","神谷町","赤坂見附","大手町","青山一丁目","茅場町","九段下","新宿","溜池山王","表参道","みなとみらい");
	if ($GetPlaceData[0] == -1){
		$ConvertedPlace = "$GetPlaceData[1]";
	}
	else{
		$ConvertedPlace = $PlaceList[$GetPlaceData[0]];
	}
	return $ConvertedPlace;
}

sub ConvertJob{
	my $ConvertedJob = "";
	my @GetJobData = @_;
	#変換用仕事データ
	my @JobList = ("","通訳・翻訳・翻訳チェッカー","秘書・グループセクレタリー","英文事務・アシスタント・英文経理・貿易事務・人事・総務など","金融業界でのお仕事","IT・技術関連");
	$ConvertedJob = $JobList[$GetJobData[0]];
	return $ConvertedJob;
}

sub ConvertDay{
	my $ConvertedDay = "";
	my @GetDayData = @_;
	if ($GetDayData[0] == 1){
		$ConvertedDay = "平日";
	}
	elsif ($GetDayData[0] == 0){
		$ConvertedDay = "土・日";
	}
	elsif ($GetDayData[0] == -1){
		$ConvertedDay = $GetDayData[1];
	}
	else{
		if(($GetDayData[0] % 2) == 0){$ConvertedDay .= "月・";}
		if(($GetDayData[0] % 3) == 0){$ConvertedDay .= "火・";}
		if(($GetDayData[0] % 5) == 0){$ConvertedDay .= "水・";}
		if(($GetDayData[0] % 7) == 0){$ConvertedDay .= "木・";}
		if(($GetDayData[0] % 11) == 0){$ConvertedDay .= "金・";}
		if(($GetDayData[0] % 13) == 0){$ConvertedDay .= "土・";}
		if(($GetDayData[0] % 17) == 0){$ConvertedDay .= "日・";}
		chop ($ConvertedDay);
		chop ($ConvertedDay);
	}
	return $ConvertedDay;

}