
<?php get_header(); ?>
<section>
    <div class="l-slider">
        <div class="l-slider-inner">
            <ul class="bxslider">
                <li><a href="<?php echo home_url();?>/tsuyaku/"><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_01_pc.jpg" alt="通訳"></a</li>
                <li><a href="<?php echo home_url();?>/kokusai/"><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_02_pc.jpg" alt="会議・イベント運営"></a></li>
                <li><a href="<?php echo home_url();?>/personnel/"><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_03_pc.jpg" alt="人材"></a></li>
                <li><a href="<?php echo home_url();?>/training/"><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_04_pc.jpg" alt="スクール"></a></li>
            </ul>
        </div>
    </div>
</section>
<!-- <section>
    <div class="l-slider">
        <div class="l-slider-inner">
            <ul class="bxslider">
                <li><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_01_pc.png" alt="円滑な「グローバルコミュニケーション」を提供します"></li>
                <li><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_02_pc.png" alt="国際会議会場風景写真1"></a></li>
                <li><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_03_pc.png" alt="国際会議会場風景写真2"></li>
                <li><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_04_pc.png" alt="国際会議会場風景写真3"></li>
            </ul>
        </div>
    </div>
</section> -->
<!-- <section>
    <div class="l-mainVisual">
        <div class="l-mainVisual-inner">
            <div>
                <a href="<?php echo home_url();?>/inquiry/online"><img src="<?php echo get_stylesheet_directory_uri();?>/images/top/top_banner_pc.png" alt=""></a>
                <a href="<?php echo home_url();?>/inquiry/tsuyaku"><img src="<?php echo get_stylesheet_directory_uri();?>/images/top/top_banner_olt_pc.jpg" alt="オンライン通訳サービス"></a>
                <a href="<?php echo home_url();?>/inquiry/kokusai"><img src="<?php echo get_stylesheet_directory_uri();?>/images/top/top_banner_online_pc.jpg" alt="ISS Online Conference Services"></a>
            </div>
        </div>
    </div>
</section> -->
<!-- <section>
    <div class="l-mainVisual">
        <div class="l-mainVisual-inner">
            <div class="bxslider_2">
                <a href="<?php echo home_url();?>/tsuyaku/online"><img src="<?php echo get_stylesheet_directory_uri();?>/images/top/top_banner_olt_pc.jpg" alt="オンライン通訳サービス"></a>
                <a href="<?php echo home_url();?>/kokusai/online"><img src="<?php echo get_stylesheet_directory_uri();?>/images/top/top_banner_online_pc.jpg" alt="ISS Online Conference Services"></a>
                <a href="<?php echo home_url();?>/webinar"><img src="<?php echo get_stylesheet_directory_uri();?>/images/top/top_banner_web.jpg" alt="ウェブ会議/ウェビナー支援"></a>
            </div>
        </div>
    </div>
</section> -->




<div class="l-wrapper">

    <div class="l-interpretationBlock">
        <div class="l-inner">
            <div class="l-interpretationBlock02">
                <div class="l-interpretationBlock02-table">
                    <div class="l-interpretationBlock02-cell">
                        <div>
                            <p class="p-title pconly"><a href="<?php echo home_url();?>/tsuyaku/">通訳</a></p>
                            <p class="p-photo"><a href="<?php echo home_url();?>/tsuyaku/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_01_pc.png" alt="メディカル、金融、IT通信、製造業を中心とした幅広い業界、政府官公庁や自治体等において、各分野の豊富な実績を持つ質の高い通訳者を手配いたします。"></a></p>
                            <p class="p-message">
                                <span class="pconly">メディカル、金融、IT通信、製造業を中心とした幅広い業界、政府官公庁や自治体等において、各分野の豊富な実績を持つ質の高い通訳者を手配いたします。</span>
                                <span class="sponly0">メディカル、金融、IT通信、製造業を中心に幅広い業界や官公庁等において質の高い通訳者を手配いたします。</span>
                            </p>
                            <p class="p-all"><a class="p-more" href="<?php echo home_url();?>/tsuyaku/">詳しく見る</a></p>
                        </div>
                    </div>
                    <div class="l-interpretationBlock02-cell">
                        <div>
                            <p class="p-title pconly"><a href="<?php echo home_url();?>/kokusai/">会議・イベント運営<span></span></a></p>
                            <p class="p-photo"><a href="<?php echo home_url();?>/kokusai/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_02_pc.png" alt="お客様の会議やイベント運営に要する一連の実務を、一つ一つ積み上げてまいります。会議に参加された方々がつながり、共感が生まれる場の創出を目指します。"></a></p>
                            <p class="p-message">
                            お客様の会議やイベント運営に要する一連の実務を、一つ一つ積み上げてまいります。会議に参加された方々がつながり、共感が生まれる場の創出を目指します。
                            </p>
                            <p class="p-all"><a class="p-more" href="<?php echo home_url();?>/kokusai/">詳しく見る</a></p>
                        </div>
                    </div>
                    <div class="l-interpretationBlock02-cell">
                        <div>
                            <p class="p-title pconly"><a href="<?php echo home_url();?>/personnel/">人材サービス</a></p>
                            <p class="p-photo"><a href="<?php echo home_url();?>/personnel/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_03_pc.png" alt="通訳、翻訳、バイリンガル秘書など、語学に特化した人材サービス（人材派遣・紹介予定派遣・人材紹介）を提供しています。"></a></p>
                            <p class="p-message">
                                <span class="pconly">通訳、翻訳、バイリンガル秘書など、語学に特化した人材サービス（人材派遣・紹介予定派遣・人材紹介）を提供しています。</span>
                                <span class="sponly0">通訳翻訳、バイリンガル秘書など語学に特化した人材派遣・紹介予定派遣・人材紹介サービスを提供しています。</span>
                            </p>
                            <p class="p-all"><a class="p-more" href="<?php echo home_url();?>/personnel/">詳しく見る</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="l-interpretationBlock02">
                <div class="l-interpretationBlock02-table">

                    <div class="l-interpretationBlock02-cell">
                        <div>
                            <p class="p-title pconly"><a href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a></p>
                            <p class="p-photo"><a href="<?php echo home_url();?>/training/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_05_pc.png" alt="アイ・エス・エス・インスティテュートでは、英語・中国語の通訳者・翻訳者の養成、および、法人向け語学研修サービスを提供しています。"></a></p>
                            <p class="p-message">
                                <span class="pconly">アイ・エス・エス・インスティテュートでは、英語・中国語の通訳者・翻訳者の養成、および、法人向け語学研修サービスを提供しています。</span>
                                <span class="sponly0">ISSインスティテュートでは、英語・中国語の通訳・翻訳者養成、法人向け語学研修サービスを提供しています。</span>
                            </p>
                            <p class="p-all"><a class="p-more" href="<?php echo home_url();?>/training/">詳しく見る</a></p>
                        </div>
                    </div>

                    <div class="l-interpretationBlock02-cell">
                        <div>
                            <p class="p-title pconly"><a href="<?php echo home_url();?>/honyaku/">翻訳</a></p>
                            <p class="p-photo"><a href="<?php echo home_url();?>/honyaku/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_04_pc.png" alt="特許、医薬、工業・ローカライゼーション、金融・法務の4分野を軸にした専門性の高い翻訳サービスを、グループ会社の翻訳センターがご提供いたします。"></a></p>
                            <p class="p-message">
                                <span class="pconly">医薬、特許、工業・ローカライゼーション、金融・法務の4分野を軸にした専門性の高い翻訳サービスを、グループ会社の翻訳センターがご提供いたします。</span>
                                <span class="sponly0">医薬、特許、工業、金融・法務を軸に専門性の高い翻訳を、グループ会社の翻訳センターがご提供いたします。</span>
                            </p>
                            <p class="p-all"><a class="p-more" href="<?php echo home_url();?>/honyaku/">詳しく見る</a></p>
                        </div>
                    </div>
                    <div class="l-interpretationBlock02-cell"></div>
                    
                    <!-- <div class="l-interpretationBlock02-cell p-middle">
                        <p class="p-honyaku pconly"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_honyaku_pc.png" alt="ISSは、産業翻訳業界のリーディングカンパニーである翻訳センターグループの一員です。"></p>
                    </div> -->
                </div>
            </div>

        </div>
    </div>

    <div class="l-newsBlock">
        <div class="l-inner">
            <div class="l-newsBlock01">
                <div class="l-newsBlock01-table">
                    <p>News&nbsp;&amp;&nbsp;Topics</p>
                    <p><a href="<?php echo home_url();?>/news/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_news_all_pc.png" alt="すべてを見る"></a></p>
                </div>
            </div>

            <?php
                //args
                $args = array(
                    'post_type' => 'news',
                    'posts_per_page' => '5',
                    'order'=>'DESC',
                    'orderby'=>'post_date',
                );
                // the query
                $the_query = new WP_Query($args);
            ?>

            <div class="l-newsBlock02">
                <?php if ($the_query->have_posts()) : ?>
                <?php while ($the_query-> have_posts() ) : $the_query->the_post(); ?>
                <?php
                    //「カテゴリ」項目を取得
                    $values = CFS()->get('category');
                    foreach ($values as $value => $label) {

                    }
                ?>
                <?php
                //「外部リンク」項目が未入力の場合
                if (empty(CFS()->get('link'))) :
                ?>
                    <?php
                    //かつ「カテゴリ」が「採用情報」の場合採用ページに遷移
                    if ($value === '採用情報'):
                    ?>
                    <a href="<?php echo home_url();?>/recruit/career">
                    <?php
                    //ニュース記事ページに遷移
                    else:
                    ?>
                    <a href="<?php the_permalink(); ?>">
                    <?php endif; ?>
                <?php
                //入力されたURLに別タブで遷移
                else:
                ?>
                <a href="<?php echo CFS()->get('link') ?>" target="_blank">
                <?php endif; ?>
                    <div class="l-newsBlock02-table">
                        <div class="p-date">
                            <p><?php the_time('Y.m.d'); ?></p>
                        </div>
                        <div class="p-category">
                            <?php if ($value === 'ISSインスティテュート'): ?>
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category01_pc.png" alt="ISSインスティテュート"></p>
                            <?php elseif ($value === '採用情報'): ?>
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category04_pc.png" alt="採用情報"></p>
                            <?php elseif ($value === '通訳者 募集'): ?>
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category03_pc.png" alt="通訳者募集"></p>
                            <?php elseif ($value === 'コンベンション'): ?>
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category02_pc.png" alt="コンベンション"></p>
                            <?php elseif ($value === 'お知らせ'): ?>
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category05_pc.png" alt="お知らせ"></p>
                            <?php endif; ?>
                        </div>
                        <div class="p-message">
                            <p><?php the_title(); ?></p>
                        </div>
                    </div>
                </a>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>

    <div class="l-inquiryBlock">
        <div class="l-inner">
            <div class="l-inquiryBlock-table">
                <div class="p-inquiry">
                    <div class="p-photo"><a href="<?php echo home_url();?>/inquiry/form"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/bg_inquiry_02_pc.png" alt="アイ・エス・エスのサービスに関するお問い合わせはWebフォームよりお気軽にご相談ください。"></a></div>
                    <!--<div class="p-button"><a href="<?php echo home_url();?>/inquiry"><img class="js-btn_inquiry is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_inquiry_01_pc.png" alt="お問い合わせ"></a></div>-->
                </div>
                <div class="p-registration">
                    <div class="p-photo"><a href="<?php echo home_url();?>/recruit/registration"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/bg_registration_01_pc.png" alt="高水準・高品質なサービスに対応できる通訳者・翻訳者を求めています。"></a></div>
                    <!--<div class="p-button"><a href="<?php echo home_url();?>/recruit/registration"><img class="js-btn_registration is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_registration_01_pc.png" alt="通訳者・翻訳者の登録について"></a></div>-->
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="l-inquiryBlock" style="padding-bottom: 0px;">
        <div class="l-inner">
            <p class="p-honyaku sponly0"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_honyaku_pc.png" alt="ISSは、産業翻訳業界のリーディングカンパニーである翻訳センターグループの一員です。"></p>
        </div>
    </div> -->

    <div class="l-inquiryBlock" style="padding-top: 0;">
        <div class="l-inner">
            <div class="l-inquiryBlock-table">
                <div class="p-inquiry">
                    <div class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_bnr_honyaku_pc.jpg" alt="アイ・エス・エスは、産業翻訳業界のリーディングカンパニーである翻訳センターグループの一員です"></div>
                </div>
            </div>
        </div>
    </div>




    

    <div class="l-bottomBlock">
        <div class="l-inner">
            <div class="l-bottomBlock01">
                <p class="p-bottomTitle">加盟団体</p>
            </div>
            <ul>
                <!-- <li><a href="http://www.iccaworld.org/" target="_blank"><img class="p-icca" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_icca.png" alt="International Congress and Convention Association"></a></li> -->
                <!-- <li><a href="http://www.jccb.or.jp/" target="_blank"><img class="p-jccb" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jccb.png" alt="日本コングレス・コンベンション・ビューロー"></a></li> -->
                <li><a href="http://www.jp-cma.org/" target="_blank"><img class="p-jcma" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jcma.png" alt="一般社団法人　日本コンベンション協会"></a></li>
                <li><a href="https://www.jata-net.or.jp/" target="_blank"><img class="p-jea" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jata.png" alt="一般社団法人 日本旅行業協会"></a></li>
                <li><a href="http://www.jtf.jp/" target="_blank"><img class="p-jtf" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jtf.png" alt="一般社団法人　日本翻訳連盟"></a></li>
                <li><a href="http://www.jassa.jp/" target="_blank"><img class="p-jsa" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jsa.png" alt="一般社団法人　日本人材派遣協会"></a></li>
                <li><a href="http://www.jesra.or.jp/" target="_blank"><img class="p-jinzai" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jinzaikyo.png" alt="人材協"></a></li>
            </ul>
            <!-- <div class="l-bottomBlock02">
                <a href="http://www.tcvb.or.jp/jp/" target="_blank">公益財団法人 東京観光財団</a><a href="http://www.ccb.or.jp/mice/" target="_blank">公益財団法人 ちば国際コンベンションビューロー</a>
            </div> -->
        </div>
    </div>

</div>
<?php get_footer(); ?>
