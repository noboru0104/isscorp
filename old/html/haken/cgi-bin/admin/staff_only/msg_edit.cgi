#!/usr/bin/perl

#ディレクトリ情報設定ここから
my $StaffHtmlRoot = "../../../home/staff_only";

my $StaffCgiRoot = "../../../cgi-bin";
my $TemplateDir = "../../template/staff_only";
my $DataFileDir = "../../secret/staff_only";
#ディレクトリ情報設定ここまで

#ライブラリ設定開始
require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
#ライブラリ設定終了

#テンプレートファイルリスト
my $EditForm = "msg_edit.html";
my $DeleteConfirm = "msg_delete.html";
my $Complete = "edit_complete.html";

#データファイル
my $DataFile = "msg.csv";
my $NewDataFile = "new_msg.csv";

#エラーメッセージ変数宣言
my $ErrorMessage = "";
my $DateError = "";
my $TitleError = "";
my $StatementError = "";

#出力文字列格納用変数
my $HtmlSrc = "";

print("Content-type:text/html\n\n");

#URLから情報取得
my $Conf = $ENV{'QUERY_STRING'};

#デバッグ用変数設定
#$Conf = "mode=add&data=1000";

my @Datas = split /&/,$Conf;
my @EditMode = split /=/,$Datas[0];#動作モード情報
my @DataNumber = split /=/,$Datas[1];#削除・編集対象情報

#フォームから情報入手
&ReadParse(*in);

my $Date = $in{'Date'};
my $Title = $in{'Title'};
my $Statement = $in{'Statement'};
my $Mode = $in{'Mode'};#Modeが1でないと実際の動作は行わない

#動作モードの確認
if($EditMode[1] eq "delete"){#削除モード
	if($Mode == 1){
	#削除の実行
	my $DeletedData = "";
	open DELETEFILE, "$DataFileDir/$DataFile";
	while (<DELETEFILE>){
		my @DelCheck = split /,/,$_;
		if ($DelCheck[3] == $DataNumber[1]){}
		else{
			$DeletedData .= $_;
		}
	}
	close DELETEFILE;
	open DELETEFILE, ">$DataFileDir/$DataFile";
	print DELETEFILE "$DeletedData";
	close DELETEFILE;
	
	&CreateNewMsgList();
	&RenewTopPage();
	
	$HtmlSrc = &HtmlSrcLoad($Complete);
	}
	else{
	#削除確認画面の表示
	$HtmlSrc = &HtmlSrcLoad($DeleteConfirm);
	
	&GetMsgData($DataNumber[1]);
	
	&GetDataReplace();
	}
}
elsif($EditMode[1] eq "edit"){#編集モード
	if($Mode == 1 and &DataChecker == 1){
	#情報不足時処理
	$HtmlSrc = &HtmlSrcLoad($EditForm);
	
	&GetMsgData($DataNumber[1]);
	
	$ErrorMessage = "入力情報が不足しています。確認して情報の再入力を行ってください。";
	
	&GetDataReplace();
	&ReplaceErrorMessage();
	}
	elsif($Mode == 1){
	#編集作業の実行
	my $EditedData = "";
	open EDITFILE, "$DataFileDir/$DataFile";
	while (<EDITFILE>){
		my @EditCheck = split /,/,$_;
		if ($EditCheck[3] == $DataNumber[1]){
			$Data = &StrCorrect($Date);
			$Title = &StrCorrect($Title);
			$Statement = &StrCorrect($Statement);
			$EditedData .= "$Date" . "," . "$Title" . "," . "$Statement" . "," . "$DataNumber[1]" . "\n"
		}
		else{
			$EditedData .= $_;
		}
	}
	close EDITFILE;
	open EDITFILE, ">$DataFileDir/$DataFile";
	print EDITFILE "$EditedData";
	close EDITFILE;
	

	&CreateNewMsgList();
	&RenewTopPage();
	
	$HtmlSrc = &HtmlSrcLoad($Complete);
	}
	else{
	#編集フォームの表示
	$HtmlSrc = &HtmlSrcLoad($EditForm);
	
	&GetMsgData($DataNumber[1]);
	
	$ErrorMessage = "";
	$DateError = "";
	$TitleError = "";
	$StatementError = "";	
	
	&GetDataReplace();
	&ReplaceErrorMessage();
	}

}
elsif($EditMode[1] eq "add"){#追加モード
	if($Mode == 1 and &DataChecker == 1){
	#情報不足時処理
	$HtmlSrc = &HtmlSrcLoad($EditForm);
	
	&GetMsgData($DataNumber[1]);
	
	$ErrorMessage = "入力情報が不足しています。確認して情報の再入力を行ってください。";
	
	&GetDataReplace();
	&ReplaceErrorMessage();
	}
	elsif($Mode == 1){
	#追加作業の実行
	
	my $Number = time();
	
	$Data = &StrCorrect($Date);
	$Title = &StrCorrect($Title);
	$Statement = &StrCorrect($Statement);
	
	open DATAFILE, ">>$DataFileDir/$DataFile";
	print DATAFILE ("$Date" . "," . "$Title" . "," . "$Statement" . "," . "$Number" . "\n");
	close DATAFILE;
	
	&CreateNewMsgList();
	&RenewTopPage();
	
	$HtmlSrc = &HtmlSrcLoad($Complete);
	}
	else{
	#追加フォームの表示
	$HtmlSrc = &HtmlSrcLoad($EditForm);
	
	$Conf = "mode=add";
	$Date = "";
	$Title = "";
	$Statement = "";
	$ErrorMessage = "";
	$DateError = "";
	$TitleError = "";
	$StatementError = "";	
	
	&GetDataReplace();
	&ReplaceErrorMessage();
	}

}
else{
}

print("$HtmlSrc");

#以下サブルーチン
sub HtmlSrcLoad{
	my @UsingFile = @_;
	my $TempSrc = "";
	open BODY, "$TemplateDir/$UsingFile[0]" or die "Error!";
	while(<BODY>){
		$TempSrc .= $_;
	}
	close BODY;
	return $TempSrc;
}

sub ReplaceErrorMessage{
	$HtmlSrc =~ s/__ERROR__MESSAGE__/$ErrorMessage/;
	$HtmlSrc =~ s/__DATE__ERROR__/$DateError/;
	$HtmlSrc =~ s/__TITLE__ERROR__/$TitleError/;
	$HtmlSrc =~ s/__STATEMENT__ERROR__/$StatementError/;
	return 0;
}

sub GetDataReplace{
	$HtmlSrc =~ s/__EDIT__INFORMATION__/$Conf/;
	$HtmlSrc =~ s/__DATE__/$Date/;
	$HtmlSrc =~ s/__TITLE__/$Title/;
	$HtmlSrc =~ s/__STATEMENT__/$Statement/;
	return 0;
}

sub CreateNewMsgList{
	my $NumberOfNewInfo = 5;
	
	my @TempInfoList = ();
	open CONVINFO, "$DataFileDir/$DataFile";
	@TempInfoList = <CONVINFO>;
	close CONVINFO;
	
	open NEWINFO, ">$DataFileDir/$NewDataFile";
	for (my $count = 0;$count < $NumberOfNewInfo;$count++){
		print NEWINFO (pop(@TempInfoList));
	}
	close NEWINFO;	
	return 0;
}

sub GetMsgData{
	my @TargetNumber = @_;
	open GETDATA, "$DataFileDir/$DataFile";
	label1:while(<GETDATA>){
		my @TempMsgData = split /,/,$_;
		if($TempMsgData[3] == $TargetNumber[0]){
			$Date = &RevStrCorrect($TempMsgData[0]);
			$Title = &RevStrCorrect($TempMsgData[1]);
			$Statement = &RevStrCorrect($TempMsgData[2]);
			last label1;
		}
	}
	close GETDATA;
	return 0;
}

sub StrCorrect{
	my @ConvertedStr = @_;
	$ConvertedStr[0] =~ s/,/&\#044\;/;
	$ConvertedStr[0] =~ s/\r\n/<br>/;
	return $ConvertedStr[0];
}

sub RevStrCorrect{
	my @ConvertedStr = @_;
	$ConvertedStr[0] =~ s/&\#044\;/,/;
	$ConvertedStr[0] =~ s/<br>/\r\n/;
	return $ConvertedStr[0];
}

sub DataChecker{
	my $CheckFrag = 0;
	if(length ($Date) == 0){
		$DateError = "日付が入力されていません。";
		$CheckFrag = 1;
	}
	else{}
	if(length ($Title) == 0){
		$TitleError = "題名が入力されていません。";
		$CheckFrag = 1;
	}
	else{}
	if(length ($Statement) == 0){
		$StatementError = "本文が入力されていません。";
		$CheckFrag = 1;
	}
	else{}
	return $CheckFrag;
}

sub RenewTopPage{
	#ファイル設定等
	my $RenewalFileName = "index.html";
	my $ToppageTableFile = "index_table.html";
	
	my $ToppageSrc = "";
	my $ToppageTableSrc = "";
	my $ToppageTableTemplate = "";
	my $TopTableElement = "";
	
	#テーブルテンプレート呼び出し
	$ToppageTableTemplate = &HtmlSrcLoad($ToppageTableFile);
	
	#テーブル生成
	open TOPTABLE,"$DataFileDir/$NewDataFile";
	while(<TOPTABLE>){
		my @TempToppageData = split /,/,$_;
		$TopTableElement = $ToppageTableTemplate;
		$TopTableElement =~ s/__DATE__/$TempToppageData[0]/;
		$TopTableElement =~ s/__TITLE__/$TempToppageData[1]/;
		$ToppageTableSrc .= $TopTableElement;
	}
	close TOPTABLE;
	
	#トップページテンプレート呼び出し
	$ToppageSrc = &HtmlSrcLoad($RenewalFileName);
	
	#新着情報をテンプレートに移植
	$ToppageSrc =~ s/__NEW__INFORMATION__/$ToppageTableSrc/;
	
	#トップページ更新処理
	open TOPPAGE, ">$StaffHtmlRoot/$RenewalFileName";
	print TOPPAGE ("$ToppageSrc");
	close TOPPAGE;
	return 0;

}