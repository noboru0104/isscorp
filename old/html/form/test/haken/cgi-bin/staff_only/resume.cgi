#!/usr/bin/perl
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
$cgi_lib'maxdata    = 5000*1024*1024; 

&ReadParse;

$FROM		='touroku@isssc.co.jp';
#$TO		='touroku@isssc.co.jp';
$TO		='ichino@si-seed.com';
#$TO2		='yumiko@loveworkaholic.com';

$SubjectAdmin 		= "ISS ONLINE Staff Only Website Notification";
$SubjectUser		= "ISS ONLINE Staff Only Website";

$entry_cgi_path		="/cgi-bin/staff_only/resume.cgi";
$csv_dir		="../secret/staff_only/resume/";
$csv_path		=">>" . $csv_dir . "registered.csv";
$resume_dir		="../secret/staff_only/resume/";
$template_dir		="../template/staff_only/resume/";

$confirm_html_path	=$template_dir . "confirm.html";
$entry_html_path	=$template_dir . "entry.html";
$thanks_html_path	=$template_dir . "thanks.html";
$csv_txt_path		=$template_dir . "csv.txt";
$mail_admin_txt_path	=$template_dir . "mail_admin.txt";
$mail_user_txt_path	=$template_dir . "mail_user.txt";

$epoch=time;
$name=$in{'name'};	$name=~s/\r\n/ /g;	$name=~s/,/./g;
$memo=$in{'memo'};	$memo=~s/\r\n/ /g;	$memo=~s/,/./g;
$email=$in{'email'};	$email=~s/\r\n/ /g;	$email=~s/,/./g;
if(length $email==0){
	$err.="メールアドレスは必須です。<br>";
}
if(length $in{'file'}==0){
	$err.="レジュメの添付は必須です。<br>";
}

if($in{'cmd'} eq 'send'){

	if( length $err > 0){
		open(TEMPLATE,$confirm_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		&jcode::euc2sjis(\$err);
		$strHtml=~s/__err__/$err/g;
		print "Content-type: text/html\n\n";
		print $strHtml;
		exit;
	}

#ファイル保存
	if( length $in{'file'} >0){
		$tmp=$incfn{'file'};
		$tmp =~ /(.*)\.([^\.]+$)/;
		$ext=$2;
		if(length $ext == 0 ){
			$file2=time.$email.".doc";
		}else{
			$file2=time.$email.".".$ext;
		}
		open(ATTACH,">".$resume_dir.$file2);
		print ATTACH $in{file};
		close(ATTACH);
	}

	use lib "./";
	use Lite;
#	use MIME::Lite;

	open(TEMPLATE,$mail_admin_txt_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&jcode::sjis2jis(\$strHtml);

	$type = 'multipart/mixed';
	$msg = MIME::Lite->new( From    =>$FROM,To      =>$TO,Subject =>$SubjectAdmin,Type    =>$type);
	$msg->attach(Type     =>'TEXT',   Data     =>$strHtml);
	$msg->send;

	open(TEMPLATE,$mail_user_txt_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&jcode::sjis2jis(\$strHtml);

	$msg_user = MIME::Lite->new( From=>$FROM,To=>$email,Subject=>$SubjectUser,Type=>$type);
	$msg_user->attach(Type=>'TEXT',Data=>$strHtml);
	$msg_user->send;

	open(CSV_TEMPLATE,$csv_txt_path);
	@entry=<CSV_TEMPLATE>;
	close(CSV_TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	$strHtml.="\n";

	open(CSV,$csv_path);
	print CSV $strHtml;
	close(CSV);


	open(TEMPLATE,$thanks_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	print "Content-type: text/html\n\n";
	print $strHtml;

	exit;
}else{

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print $strHtml;
}

sub replace{
	$strHtml=~s/__name__/$name/g;
	$strHtml=~s/__memo__/$memo/g;
	$strHtml=~s/__email__/$email/g;
	$strHtml=~s/__epoch__/$epoch/g;
	$strHtml=~s/__file2__/$file2/g;
}

sub is_zenkaku{
	$tmp = $_[0];
	&jcode::sjis2euc(\$tmp);
	if($tmp !~ /[\x00-\x7f]/){
		return 1;
	}else{
		return 0;
	}
}

sub is_hankaku{
	$tmp = $_[0];
	if($tmp !~ /[\x80-\xff]/){
		return 1;
	}else{
		return 0;
	}
}

sub enq_radio{
	$temp=$_[0];
	$temp=~s/\n//g;
	if(length $temp > 0){
		if($_[3]==$_[2]){
			$enq.="<input type=radio name=".$_[1]." value=".$_[3]." checked>".$temp;
		}else{
			$enq.="<input type=radio name=".$_[1]." value=".$_[3].">".$temp;
		}
	};
}

sub wtime {
	($sec,$min,$hour,$mday,$mon,$year,$weekday) = localtime;
	
	$year += 1900;
	$mon++;
	$mon = sprintf("%.2d",$mon);
	$mday = sprintf("%.2d",$mday);
	$hour = sprintf("%.2d",$hour);
	$min = sprintf("%.2d",$min);
	$sec = sprintf("%.2d",$sec);
	
	@week = ("日","月","火","水","木","金","土");
	$weekday = $week[$wday];
	&jcode::euc2sjis(\$weekday);
}

