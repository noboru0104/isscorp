############################################################
#
#    ISS　設定ファイル
#
############################################################

## 一覧件数
$g_pageNum = 20;

## TOPページテンプレート
$g_topHtmlFile = './index.html';

## previewページテンプレート
$g_previewHtmlFile = './confirm.html';

## 記事編集用テンプレート
$g_editHtmlFile = './edit.html';

## 記事編集確認画面テンプレート
$g_editConfHtmlFile = './edit_confirm.html';

## 編集エラー画面テンプレート
$g_errorHtmlFile = './edit.html';

## 完了画面テンプレート
$g_endHtmlFile = './thanks.html';

## システムエラーページテンプレート
$g_sysErrorHtmlFile = './error.html';

## TOPページ
$g_topPageHtmlFile = '../index.html';
#$g_topPageHtmlFile = '../testindex.html';

## 記事用テンプレート
$g_templateFile = './news.html';

## 記事一覧用テンプレート
$g_indexTemplateFile = './news_index.html';

## HTML保存場所
$g_htmlDir = '../news/';

## 画像一時保存場所
$g_imageTmpDir = '../tmp_img/';
$g_imageTmpPath = '../tmp_img/';

## 画像保存場所
$g_imageDir = '../up_img/';

$g_imagePath = '../up_img/';

$g_adminImagePath = '../up_img/';

## PDF保存場所
$g_pdfDir = '../up_pdf/';

$g_pdfPath = '../up_pdf/';

$g_adminPdfPath = '../up_pdf/';

## 投稿データ保存ディレクトリ
$g_dataDir = './data/';

## 投稿データ保存CSV（一覧用）
$g_dataCsv = $g_dataDir . 'data.csv';

## 投稿記事No用CSV
$g_numberCsv = $g_dataDir . 'number.csv';

## 詳細記事作成テキスト情報
$g_detailText[1] = '作成しない';
$g_detailText[2] = '作成する';

## タイトルからのリンクテキスト情報
$g_titleLinkText[1] = 'なし';
$g_titleLinkText[2] = 'URL';
$g_titleLinkText[3] = 'PDF';

## 画像横幅
$g_width = 160;

## sendmailパス
$sendmail = '/usr/sbin/sendmail -t';

## lockファイル
$g_lockFile = $g_dataDir . 'lock/lockfile';

## base url設定
$g_html{'base_url'} = qq(<base href="http://www.issjp.com/");

## cgi path設定
$g_html{'cgi_path'} = 'http://www.issjp.com/';
