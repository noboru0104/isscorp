#!/usr/bin/perl

#require '../../../perl_lib/jcode.pl';
#require '../../../perl_lib/cgi-lib.pl';

require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
&ReadParse;

$entry_cgi_path		="/cgi-bin/admin/counseling/entry_admin.cgi";

$resume_dir		="../../secret/counseling/";

$csv_dir		="../../secret/counseling/";
$csv_path		=$csv_dir . "registered.csv";
$csv_tmp_path		=$csv_dir . "tmp.csv";
$csv_write_path		=">>".$csv_dir . "registered.csv";

$template_dir		="../../template/counseling/";
$index_html_path	=$template_dir . "entry_admin.html";
$detail_html_path	=$template_dir . "entry_detail.html";
$csv_head_txt_path	=$template_dir . "csv_head.txt";
$enq_edit_html_path	=$template_dir . "entry_enq_edit.html";
$enq_update_html_path	=$template_dir . "entry_enq_update.html";



if($in{'cmd'} eq 'enq_edit'){

	print "Content-type: text/html\n\n";
	$enq_path=$csv_dir."enq".$in{'id'}.".txt";
	open(ENQ,$enq_path);
	@entry=<ENQ>;
	$id=$in{'id'};
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;
	$ans02=shift @entry;
	$ans03=shift @entry;
	$ans04=shift @entry;
	$ans05=shift @entry;
	$ans06=shift @entry;
	$ans07=shift @entry;
	$ans08=shift @entry;
	$ans09=shift @entry;
	$ans10=shift @entry;
#	foreach $i (@entty){$qes.=$i;}
	do{$qes.=shift @entry;}while($#entry>0);
	close(ENQ);

	open(TEMPLATE,$enq_edit_html_path);
	$strHtml="";
	@entry=<TEMPLATE>;
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	close(TEMPLATE);
	print $strHtml;

}elsif($in{'cmd'} eq 'enq_update'){
	$id=$in{'id'};
	$sel_type=$in{sel_type};
	$req_type=$in{req_type};
	$ans01=$in{ans01};
	$ans02=$in{ans02};
	$ans03=$in{ans03};
	$ans04=$in{ans04};
	$ans05=$in{ans05};
	$ans06=$in{ans06};
	$ans07=$in{ans07};
	$ans08=$in{ans08};
	$ans09=$in{ans09};
	$ans10=$in{ans10};
	$qes=$in{'qes'};

	$enq_path=$csv_dir."enq".$in{'id'}.".txt";
	open(ENQ,">".$enq_path);
	print ENQ $sel_type."\n";
	print ENQ $req_type."\n";
	print ENQ $ans01."\n";
	print ENQ $ans02."\n";
	print ENQ $ans03."\n";
	print ENQ $ans04."\n";
	print ENQ $ans05."\n";
	print ENQ $ans06."\n";
	print ENQ $ans07."\n";
	print ENQ $ans08."\n";
	print ENQ $ans09."\n";
	print ENQ $ans10."\n";
	print ENQ $qes."\n";
	close(ENQ);

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$enq_update_html_path);
	$strHtml="";
	@entry=<TEMPLATE>;
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	close(TEMPLATE);
	print $strHtml;

}elsif($in{'cmd'} eq 'download'){
	$filename = 'entry.csv';

print <<"EOL";
Content-type: text/comma-separated-values
Content-Disposition: attachment; filename=$filename

EOL
	open (HEAD,$csv_head_txt_path);
	foreach $data (<HEAD>) {	print $data."\n";}
	close(HEAD);
	open (IN,$csv_path);
	foreach $data (<IN>) {		print $data;}
	close(IN);
}elsif($in{'cmd'} eq 'resume'){
	$resume_path = $resume_dir.$in{'id'};
	$in{'id'} =~ /(.*)\.([^\.]+$)/;
	if($2 eq "png"){
		print "Content-type: image/png\n";
	}elsif(($2 eq "jpg") || ($2 eq "jpeg")){
		print "Content-type: image/jpeg\n";
	}elsif($2 eq "pdf"){
		print "Content-type: application/pdf\n";
	}elsif($2 eq "sit"){
		print "Content-type: application/x-stuffit\n";
	}elsif($2 eq "zip"){
		print "Content-type: application/x-zip-compressed\n";
	}elsif($2 eq "doc"){
		print "Content-type: application/msword\n";
	}else{
		print "Content-type: application/msword\n";
	}

print <<"EOL";
Content-Disposition: attachment; filename=$filename

EOL
	open (IN,$resume_path);
	foreach $data (<IN>) {		print $data;}
	close(IN);
}elsif($in{'cmd'} eq 'detail'){
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="";
	foreach $i (@entry){
		$i=~/(.*?),(.*?),/;
		if(($1 eq $in{'id'}) && ($2 eq $in{'email'}) ){
			$strList=$i;
		}
	}
	if(length $strList > 0){
		
		print "Content-type: text/html\n\n";
		open(TEMPLATE,$detail_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
$strList=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
$job_id=$4;
$name_A=$5;
$name_B=$6;
$name_A2=$7;
$name_B2=$8;
$visa=$9;
$visa_term=$10;
$visa_type=$11;
$birthday=$12;
$age=$13;
$sex_kanji=$14;
$zip=$15;
$pref=$16;
$address1=$17;
$address2=$18;
$address3=$19;
$moyori1_rosen=$20;
$moyori1_eki=$21;
$moyori1_walk=$22;
$moyori1_bus=$23;
$moyori1_bike=$24;
$moyori1_other=$25;
$moyori2_rosen=$26;
$moyori2_eki=$27;
$moyori2_walk=$28;
$moyori2_bus=$29;
$moyori2_bike=$30;
$moyori2_other=$31;
$tel1=$32;
$tel1_rusu=$33;
$tel2=$34;
$tel2_rusu=$35;
$fax=$36;
$renraku_any=$37;
$renraku_home=$38;
$renraku_mob=$39;
$renraku_pcmail=$40;
$renraku_mobmail=$41;
$mail1_A=$42;
$mail2_A=$43;
$gakureki=$44;
$gakureki_school=$45;
$gakureki_year=$46;
$ryugaku1_kokumei=$47;
$ryugaku1_kokumei_other=$48;
$ryugaku1_school=$49;
$ryugaku1_start_yy=$50;
$ryugaku1_start_mm=$51;
$ryugaku1_end_yy=$52;
$ryugaku1_end_mm=$53;
$ryugaku2_kokumei=$54;
$ryugaku2_kokumei_other=$55;
$ryugaku2_school=$56;
$ryugaku2_start_yy=$57;
$ryugaku2_start_mm=$58;
$ryugaku2_end_yy=$59;
$ryugaku2_end_mm=$60;
$ryugaku3_kokumei=$61;
$ryugaku3_kokumei_other=$62;
$ryugaku3_school=$63;
$ryugaku3_start_yy=$64;
$ryugaku3_start_mm=$65;
$ryugaku3_end_yy=$66;
$ryugaku3_end_mm=$67;
$taizai1_kokumei=$68;
$taizai1_memo=$69;
$taizai1_term=$70;
$taizai2_kokumei=$71;
$taizai2_memo=$72;
$taizai2_term=$73;
$start_year=$74;
$start_month=$75;
$start_day=$76;
$nowstatus=$77;
$work_style1=$78;
$work_style2=$79;
$work_style3=$80;
$time_start=$81;
$time_end=$82;
$work_time=$83;
$wday_mon=$84;
$wday_tue=$85;
$wday_wed=$86;
$wday_thu=$87;
$wday_fri=$88;
$overtime=$89;
$overtime_hour=$90;
$sat_type=$91;
$smoke=$92;
$jikyu=$93;
$jikyu_any=$94;
$work_memo=$95;
$shokushu1=$96;
$shokushu2=$97;
$hakensaki_type=$98;
$hakensaki_kibo=$99;
$hakensaki_gyousyu=$100;
$saketai_gyousyu=$101;
$kinmuchi=$102;
$shoyouzikan=$103;
$soft_word=$104;
$soft_excel=$105;
$soft_excel1=$106;
$soft_excel2=$107;
$soft_excel3=$108;
$soft_excel4=$109;
$soft_excel5=$110;
$soft_powerpoint=$111;
$soft_access=$112;
$soft_outlook=$113;
$soft_lotus=$114;
$soft_other=$115;
$typespeed_j=$116;
$typespeed_e=$117;
$os_win=$118;
$os_mac=$119;
$os_unix=$120;
$os_other=$121;
$os_other_name=$122;
$boki_score=$123;
$boki_year=$124;
$hisho_score=$125;
$hisho_year=$126;
$eiken_score=$127;
$eiken_year=$128;
$toeic_score=$129;
$toeic_year=$130;
$toeic_ip=$131;
$toefl_score=$132;
$toefl_year=$133;
$tsuyaku_score=$134;
$tsuyaku_year=$135;
$honyaku_score=$136;
$honyaku_year=$137;
$kokuren_score=$138;
$kokuren_year=$139;
$cambridge1=$140;
$cambridge2=$141;
$cambridge3=$142;
$cambridge4=$143;
$cambridge5=$144;
$cambridge_year=$145;
$annai_lang=$146;
$annai_year=$147;
$other_gogaku1=$148;
$other_gogaku1_other=$149;
$other_gogaku1_lv=$150;
$other_gogaku2=$151;
$other_gogaku2_other=$152;
$other_gogaku2_lv=$153;
$other_gogaku3=$154;
$other_gogaku3_other=$155;
$other_gogaku3_lv=$156;
$other_gogaku_shikaku=$157;
$iss_school=$158;
$iss_school_name=$159;
$iss_school_course=$160;
$iss_school_class=$161;
$iss_school_term=$162;
$other_school1=$163;
$other_school2=$164;
$other_school3=$165;
$other_school4=$166;
$otoiawase=$167;
$enq01qes=$168;
$enq01sel=$169;
$enq01text=$170;
$enq02qes=$171;
$enq02sel=$172;
$enq02text=$173;
$enq03qes=$174;
$enq03sel=$175;
$enq03text=$176;
$enq04qes=$177;
$enq04sel=$178;
$enq04text=$179;
$strHtml=~s/__epoch__/$epoch/g;
$strHtml=~s/__mail1_A__/$mail1_A/g;
$strHtml=~s/__attach__/$attach/g;
$strHtml=~s/__job_id__/$job_id/g;
$strHtml=~s/__name_A__/$name_A/g;
$strHtml=~s/__name_B__/$name_B/g;
$strHtml=~s/__name_A2__/$name_A2/g;
$strHtml=~s/__name_B2__/$name_B2/g;
$strHtml=~s/__visa__/$visa/g;
$strHtml=~s/__visa_term__/$visa_term/g;
$strHtml=~s/__visa_type__/$visa_type/g;
$strHtml=~s/__birthday__/$birthday/g;
$strHtml=~s/__age__/$age/g;
$strHtml=~s/__sex_kanji__/$sex_kanji/g;
$strHtml=~s/__zip__/$zip/g;
$strHtml=~s/__pref__/$pref/g;
$strHtml=~s/__address1__/$address1/g;
$strHtml=~s/__address2__/$address2/g;
$strHtml=~s/__address3__/$address3/g;
$strHtml=~s/__moyori1_rosen__/$moyori1_rosen/g;
$strHtml=~s/__moyori1_eki__/$moyori1_eki/g;
$strHtml=~s/__moyori1_walk__/$moyori1_walk/g;
$strHtml=~s/__moyori1_bus__/$moyori1_bus/g;
$strHtml=~s/__moyori1_bike__/$moyori1_bike/g;
$strHtml=~s/__moyori1_other__/$moyori1_other/g;
$strHtml=~s/__moyori2_rosen__/$moyori2_rosen/g;
$strHtml=~s/__moyori2_eki__/$moyori2_eki/g;
$strHtml=~s/__moyori2_walk__/$moyori2_walk/g;
$strHtml=~s/__moyori2_bus__/$moyori2_bus/g;
$strHtml=~s/__moyori2_bike__/$moyori2_bike/g;
$strHtml=~s/__moyori2_other__/$moyori2_other/g;
$strHtml=~s/__tel1__/$tel1/g;
$strHtml=~s/__tel1_rusu__/$tel1_rusu/g;
$strHtml=~s/__tel2__/$tel2/g;
$strHtml=~s/__tel2_rusu__/$tel2_rusu/g;
$strHtml=~s/__fax__/$fax/g;
$strHtml=~s/__renraku_any__/$renraku_any/g;
$strHtml=~s/__renraku_home__/$renraku_home/g;
$strHtml=~s/__renraku_mob__/$renraku_mob/g;
$strHtml=~s/__renraku_pcmail__/$renraku_pcmail/g;
$strHtml=~s/__renraku_mobmail__/$renraku_mobmail/g;
$strHtml=~s/__mail1_A__/$mail1_A/g;
$strHtml=~s/__mail2_A__/$mail2_A/g;
$strHtml=~s/__gakureki__/$gakureki/g;
$strHtml=~s/__gakureki_school__/$gakureki_school/g;
$strHtml=~s/__gakureki_year__/$gakureki_year/g;
$strHtml=~s/__ryugaku1_kokumei__/$ryugaku1_kokumei/g;
$strHtml=~s/__ryugaku1_kokumei_other__/$ryugaku1_kokumei_other/g;
$strHtml=~s/__ryugaku1_school__/$ryugaku1_school/g;
$strHtml=~s/__ryugaku1_start_yy__/$ryugaku1_start_yy/g;
$strHtml=~s/__ryugaku1_start_mm__/$ryugaku1_start_mm/g;
$strHtml=~s/__ryugaku1_end_yy__/$ryugaku1_end_yy/g;
$strHtml=~s/__ryugaku1_end_mm__/$ryugaku1_end_mm/g;
$strHtml=~s/__ryugaku2_kokumei__/$ryugaku2_kokumei/g;
$strHtml=~s/__ryugaku2_kokumei_other__/$ryugaku2_kokumei_other/g;
$strHtml=~s/__ryugaku2_school__/$ryugaku2_school/g;
$strHtml=~s/__ryugaku2_start_yy__/$ryugaku2_start_yy/g;
$strHtml=~s/__ryugaku2_start_mm__/$ryugaku2_start_mm/g;
$strHtml=~s/__ryugaku2_end_yy__/$ryugaku2_end_yy/g;
$strHtml=~s/__ryugaku2_end_mm__/$ryugaku2_end_mm/g;
$strHtml=~s/__ryugaku3_kokumei__/$ryugaku3_kokumei/g;
$strHtml=~s/__ryugaku3_kokumei_other__/$ryugaku3_kokumei_other/g;
$strHtml=~s/__ryugaku3_school__/$ryugaku3_school/g;
$strHtml=~s/__ryugaku3_start_yy__/$ryugaku3_start_yy/g;
$strHtml=~s/__ryugaku3_start_mm__/$ryugaku3_start_mm/g;
$strHtml=~s/__ryugaku3_end_yy__/$ryugaku3_end_yy/g;
$strHtml=~s/__ryugaku3_end_mm__/$ryugaku3_end_mm/g;
$strHtml=~s/__taizai1_kokumei__/$taizai1_kokumei/g;
$strHtml=~s/__taizai1_memo__/$taizai1_memo/g;
$strHtml=~s/__taizai1_term__/$taizai1_term/g;
$strHtml=~s/__taizai2_kokumei__/$taizai2_kokumei/g;
$strHtml=~s/__taizai2_memo__/$taizai2_memo/g;
$strHtml=~s/__taizai2_term__/$taizai2_term/g;
$strHtml=~s/__start_year__/$start_year/g;
$strHtml=~s/__start_month__/$start_month/g;
$strHtml=~s/__start_day__/$start_day/g;
$strHtml=~s/__nowstatus__/$nowstatus/g;
$strHtml=~s/__work_style1__/$work_style1/g;
$strHtml=~s/__work_style2__/$work_style2/g;
$strHtml=~s/__work_style3__/$work_style3/g;
$strHtml=~s/__time_start__/$time_start/g;
$strHtml=~s/__time_end__/$time_end/g;
$strHtml=~s/__work_time__/$work_time/g;
$strHtml=~s/__wday_mon__/$wday_mon/g;
$strHtml=~s/__wday_tue__/$wday_tue/g;
$strHtml=~s/__wday_wed__/$wday_wed/g;
$strHtml=~s/__wday_thu__/$wday_thu/g;
$strHtml=~s/__wday_fri__/$wday_fri/g;
$strHtml=~s/__overtime__/$overtime/g;
$strHtml=~s/__overtime_hour__/$overtime_hour/g;
$strHtml=~s/__sat_type__/$sat_type/g;
$strHtml=~s/__smoke__/$smoke/g;
$strHtml=~s/__jikyu__/$jikyu/g;
$strHtml=~s/__jikyu_any__/$jikyu_any/g;
$strHtml=~s/__work_memo__/$work_memo/g;
$strHtml=~s/__shokushu1__/$shokushu1/g;
$strHtml=~s/__shokushu2__/$shokushu2/g;
$strHtml=~s/__hakensaki_type__/$hakensaki_type/g;
$strHtml=~s/__hakensaki_kibo__/$hakensaki_kibo/g;
$strHtml=~s/__hakensaki_gyousyu__/$hakensaki_gyousyu/g;
$strHtml=~s/__saketai_gyousyu__/$saketai_gyousyu/g;
$strHtml=~s/__kinmuchi__/$kinmuchi/g;
$strHtml=~s/__shoyouzikan__/$shoyouzikan/g;
$strHtml=~s/__soft_word__/$soft_word/g;
$strHtml=~s/__soft_excel__/$soft_excel/g;
$strHtml=~s/__soft_excel1__/$soft_excel1/g;
$strHtml=~s/__soft_excel2__/$soft_excel2/g;
$strHtml=~s/__soft_excel3__/$soft_excel3/g;
$strHtml=~s/__soft_excel4__/$soft_excel4/g;
$strHtml=~s/__soft_excel5__/$soft_excel5/g;
$strHtml=~s/__soft_powerpoint__/$soft_powerpoint/g;
$strHtml=~s/__soft_access__/$soft_access/g;
$strHtml=~s/__soft_outlook__/$soft_outlook/g;
$strHtml=~s/__soft_lotus__/$soft_lotus/g;
$strHtml=~s/__soft_other__/$soft_other/g;
$strHtml=~s/__typespeed_j__/$typespeed_j/g;
$strHtml=~s/__typespeed_e__/$typespeed_e/g;
$strHtml=~s/__os_win__/$os_win/g;
$strHtml=~s/__os_mac__/$os_mac/g;
$strHtml=~s/__os_unix__/$os_unix/g;
$strHtml=~s/__os_other__/$os_other/g;
$strHtml=~s/__os_other_name__/$os_other_name/g;
$strHtml=~s/__boki_score__/$boki_score/g;
$strHtml=~s/__boki_year__/$boki_year/g;
$strHtml=~s/__hisho_score__/$hisho_score/g;
$strHtml=~s/__hisho_year__/$hisho_year/g;
$strHtml=~s/__eiken_score__/$eiken_score/g;
$strHtml=~s/__eiken_year__/$eiken_year/g;
$strHtml=~s/__toeic_score__/$toeic_score/g;
$strHtml=~s/__toeic_year__/$toeic_year/g;
$strHtml=~s/__toeic_ip__/$toeic_ip/g;
$strHtml=~s/__toefl_score__/$toefl_score/g;
$strHtml=~s/__toefl_year__/$toefl_year/g;
$strHtml=~s/__tsuyaku_score__/$tsuyaku_score/g;
$strHtml=~s/__tsuyaku_year__/$tsuyaku_year/g;
$strHtml=~s/__honyaku_score__/$honyaku_score/g;
$strHtml=~s/__honyaku_year__/$honyaku_year/g;
$strHtml=~s/__kokuren_score__/$kokuren_score/g;
$strHtml=~s/__kokuren_year__/$kokuren_year/g;
$strHtml=~s/__cambridge1__/$cambridge1/g;
$strHtml=~s/__cambridge2__/$cambridge2/g;
$strHtml=~s/__cambridge3__/$cambridge3/g;
$strHtml=~s/__cambridge4__/$cambridge4/g;
$strHtml=~s/__cambridge5__/$cambridge5/g;
$strHtml=~s/__cambridge_year__/$cambridge_year/g;
$strHtml=~s/__annai_lang__/$annai_lang/g;
$strHtml=~s/__annai_year__/$annai_year/g;
$strHtml=~s/__other_gogaku1__/$other_gogaku1/g;
$strHtml=~s/__other_gogaku1_other__/$other_gogaku1_other/g;
$strHtml=~s/__other_gogaku1_lv__/$other_gogaku1_lv/g;
$strHtml=~s/__other_gogaku2__/$other_gogaku2/g;
$strHtml=~s/__other_gogaku2_other__/$other_gogaku2_other/g;
$strHtml=~s/__other_gogaku2_lv__/$other_gogaku2_lv/g;
$strHtml=~s/__other_gogaku3__/$other_gogaku3/g;
$strHtml=~s/__other_gogaku3_other__/$other_gogaku3_other/g;
$strHtml=~s/__other_gogaku3_lv__/$other_gogaku3_lv/g;
$strHtml=~s/__other_gogaku_shikaku__/$other_gogaku_shikaku/g;
$strHtml=~s/__iss_school__/$iss_school/g;
$strHtml=~s/__iss_school_name__/$iss_school_name/g;
$strHtml=~s/__iss_school_course__/$iss_school_course/g;
$strHtml=~s/__iss_school_class__/$iss_school_class/g;
$strHtml=~s/__iss_school_term__/$iss_school_term/g;
$strHtml=~s/__other_school1__/$other_school1/g;
$strHtml=~s/__other_school2__/$other_school2/g;
$strHtml=~s/__other_school3__/$other_school3/g;
$strHtml=~s/__other_school4__/$other_school4/g;
$strHtml=~s/__otoiawase__/$otoiawase/g;
$strHtml=~s/__enq01qes__/$enq01qes/g;
$strHtml=~s/__enq01sel__/$enq01sel/g;
$strHtml=~s/__enq01text__/$enq01text/g;
$strHtml=~s/__enq02qes__/$enq02qes/g;
$strHtml=~s/__enq02sel__/$enq02sel/g;
$strHtml=~s/__enq02text__/$enq02text/g;
$strHtml=~s/__enq03qes__/$enq03qes/g;
$strHtml=~s/__enq03sel__/$enq03sel/g;
$strHtml=~s/__enq03text__/$enq03text/g;
$strHtml=~s/__enq04qes__/$enq04qes/g;
$strHtml=~s/__enq04sel__/$enq04sel/g;
$strHtml=~s/__enq04text__/$enq04text/g;


		print $strHtml;
	}else{
		$strList="お問い合わせの情報は存在しません。削除された可能性があります。";
		print "Content-type: text/html\n\n";

		open(TEMPLATE,$index_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		$strHtml=~s/__INDEX__/$strList/g;
		&jcode::euc2sjis(\$strHtml);
		print $strHtml;
	}
}elsif($in{'cmd'} eq "del_confirm"){
	$strList="本当に削除してもよろしいですか？";
	$strList.="<a href=".$entry_cgi_path ."?cmd=del&id=".$in{'id'}."&email=".$in{'email'}.">[OK]</a>/<a href=".$entry_cgi_path .">戻る</a>";
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
	
}elsif($in{'cmd'} eq "del"){
	rename $csv_path,$csv_tmp_path;
	open(CSV_WRITE,$csv_write_path);
	open(CSV_TMP,$csv_tmp_path);
	@entry=<CSV_TMP>;
	close(CSV_TMP);
	foreach $i (@entry){
		$i=~/(.*?),(.*?),/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		if(($1 eq $in{'id'}) && ($2 eq $in{'email'}) ){
			unlink $resume_dir.$1.$2;
		}else{
			print CSV_WRITE $i;
		}
	}
	close(CSV_WRITE);
	
	$strList="削除しました。";
	$strList.="<a href=".$entry_cgi_path .">戻る</a>";
	
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}else{
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="[<a href=".$entry_cgi_path ."?cmd=download >CSVファイルのダウンロード</a>]<br><br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=1 >アンケート設定1</a>]<br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=2 >アンケート設定2</a>]<br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=3 >アンケート設定3</a>]<br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=4 >アンケート設定4</a>]<br>";
	$strList.="<table>";
	foreach $i (@entry){
		$strList.="<tr>";
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		$strList.="<td><a href=./entry_admin.cgi?cmd=detail&id=".$1. "&email=". $2 .">[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]". $5 . " " . $6 . "</a></td>";
		if(length $3 >0){
			$strList.="<td>[<a href=./entry_admin.cgi?cmd=resume&id=".$3.">レジュメ</a>]</td>";
		}else{
			$strList.="<td></td>";
		}
		$strList.="<td>[<a href=./entry_admin.cgi?cmd=del_confirm&id=".$1. "&email=". $2 .">削除</a>]</td></tr>";
	}
	$strList.="</table>";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}

sub replace{
	if($sel_type ==1){
		$sel_type_kanji="テキスト";
	}elsif($sel_type == 2){
		$sel_type_kanji="選択肢";
	}elsif($sel_type == 3){
		$sel_type_kanji="テキスト+選択肢";
	}else{
		$sel_type_kanji="使用しない";
		$sel_type=0;
	}
	&jcode::euc2sjis(\$sel_type_kanji);

	if($req_type == 1){
		$req_type_kanji="必須";
	}else{
		$req_type_kanji="任意";
		$req_type=0;
	}
	&jcode::euc2sjis(\$req_type_kanji);

	$strHtml=~s/__id__/$id/g;
	&replace_radio2('sel_type','0',$sel_type);
	&replace_radio2('sel_type','1',$sel_type);
	&replace_radio2('sel_type','2',$sel_type);
	&replace_radio2('sel_type','3',$sel_type);
	$strHtml=~s/__sel_type__/$sel_type/g;
	$strHtml=~s/__sel_type_kanji__/$sel_type_kanji/g;
	&replace_radio2('req_type','1',$req_type);
	$strHtml=~s/__req_type__/$req_type/g;
	$strHtml=~s/__req_type_kanji__/$req_type_kanji/g;
	$strHtml=~s/__qes__/$qes/g;
	$strHtml=~s/__ans01__/$ans01/g;
	$strHtml=~s/__ans02__/$ans02/g;
	$strHtml=~s/__ans03__/$ans03/g;
	$strHtml=~s/__ans04__/$ans04/g;
	$strHtml=~s/__ans05__/$ans05/g;
	$strHtml=~s/__ans06__/$ans06/g;
	$strHtml=~s/__ans07__/$ans07/g;
	$strHtml=~s/__ans08__/$ans08/g;
	$strHtml=~s/__ans09__/$ans09/g;
	$strHtml=~s/__ans10__/$ans10/g;
}

sub replace_radio2{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($_[2] == $tmp2){
		$strHtml=~s/$tmp/checked/g;
	}else{
		$strHtml=~s/$tmp//g;
	}
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp/$tmp/g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

