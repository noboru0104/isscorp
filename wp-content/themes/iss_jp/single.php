<?php get_header(); ?>

<?php
if (have_posts()) : while (have_posts()) : the_post();
	if (in_category('1')) { ?>
		<div>
	<?php }
	else{ ?>
		<div>
	<?php } ?>
			<div id="interpretation" class="l-wrapper">
                <div class="l-pankuzu">
                    <div class="l-inner">
                        <p><a href="<?php echo home_url();?>/">ホーム</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<a href="<?php echo home_url();?>/news">News&nbsp;&amp;&nbsp;Topics</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<?php the_title(); ?></p>
                    </div>
                </div>
                
                <div class="l-container"> 
                    <div>
                        <div class="l-detailBlock">
                            <h3><p><?php the_title(); ?></p></h3>
                        </div>            
                        <div class="l-detailBlock01">
                            <p>
                                <?php echo CFS()->get('honbun'); ?>
                            </p>
                            <p class="p-image">
                                <img src="<?php echo CFS()->get('image'); ?>">
                                <br><?php echo CFS()->get('hosoku'); ?>
                            </p>
                            <p class="p-image">
                                <img src="<?php echo CFS()->get('image2'); ?>">
                                <br><?php echo CFS()->get('hosoku2'); ?>
                            </p>
                            <?php
							//「募集タイプ」項目を取得
							$values2 = CFS()->get('recruit_type');
							foreach ((array)$values2 as $value2 => $label2) {
								
							}     
							?>
                            <?php
							if ($value2 === 'フリーランス'):
							?>
                            <p class="p-image">
                                <a href="<?php echo home_url();?>/recruit/registration/form"><img src="<?php echo get_stylesheet_directory_uri();?>/images/recruit/btn_recruit_11_pc.png" alt="フリーランス通訳者　応募フォーム"></a>
                            </p>
                            <?php
							elseif ($value2 === 'アルバイト'):
							?>
                            <p class="p-image">
                                <a href="<?php echo home_url();?>/recruit/staff/form"><img src="<?php echo get_stylesheet_directory_uri();?>/images/recruit/btn_recruit_09_pc.png" alt="アルバイトスタッフ　登録フォーム"></a>
                            </p>
                            <?php
							endif;
							?>
                        </div>
                    </div>
                </div>                
            </div>
		</div>
<?php
	endwhile; else: ?>
	<p>記事が見つかりませんでした。</p>
<?php endif; ?>
	<div class="l-container"> 
        <div class="l-conferenceBlock04">
            <div class="l-conferenceBlock04-table">
                <div class="l-conferenceBlock04-table-cell">
                    <a href="link/news/">
                        <div class="l-conferenceBlock04-outside">
                            <div class="l-conferenceBlock04-inside">
                                <p><span>News&nbsp;&amp;&nbsp;Topics一覧へ</span></p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
</div>

<?php get_footer(); ?>
