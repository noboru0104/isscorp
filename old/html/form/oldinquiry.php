<?
if ($_SERVER['HTTPS'] != 'on') {
	header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
	exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="follow,index,noydir">
<meta http-equiv="keywords" content="ISS,アイ・エス・エス,お問い合わせ" />
<title>入力 | ISS総合お問い合わせフォーム </title>
<link href="common/form_style.css" rel="stylesheet" type="text/css" media="screen" />
<script src="https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js" charset="UTF-8"></script>

<SCRIPT TYPE="text/javascript"> 
<!-- 
function check(){
	if(!document.getElementById('agree').checked){ // 同意事項の入力をチェック
		alert("「個人情報の取扱いについての同意」をご確認のうえ、「同意します」にチェックをお願いします。");
		return false;
	}
}

function check_agree()
{
	if(document.getElementById('agree').checked){

		document.getElementById('checkbox1').disabled = false;
		document.getElementById('checkbox2').disabled = false;
		document.getElementById('checkbox3').disabled = false;
		document.getElementById('checkbox4').disabled = false;
//		document.getElementById('checkbox5').disabled = false;
		document.getElementById('checkbox6').disabled = false;	
		document.getElementById('toiawase').disabled = false;
		document.getElementById('company').disabled = false;
		document.getElementById('depart').disabled = false;
		document.getElementById('gyoukai').disabled = false;
		document.getElementById('name').disabled = false;
		document.getElementById('name2').disabled = false;
		document.getElementById('tel').disabled = false;
		document.getElementById('mail').disabled = false;
		document.getElementById('zip21').disabled = false;
		document.getElementById('zip22').disabled = false;
		document.getElementById('pref21').disabled = false;
		document.getElementById('addr21').disabled = false;
		document.getElementById('strt21').disabled = false;
		document.getElementById('kikkake1').disabled = false;
		document.getElementById('kikkake2').disabled = false;
		document.getElementById('kikkake3').disabled = false;
		document.getElementById('kikkake4').disabled = false;
		document.getElementById('kikkake5').disabled = false;
		document.getElementById('kikkake6').disabled = false;
		document.getElementById('sonota').disabled = false;
		document.getElementById('btn_confirm').disabled = false;
		
	}
	else{

		document.getElementById('checkbox1').disabled = true;
		document.getElementById('checkbox2').disabled = true;
		document.getElementById('checkbox3').disabled = true;
		document.getElementById('checkbox4').disabled = true;
//		document.getElementById('checkbox5').disabled = true;
		document.getElementById('checkbox6').disabled = true;
		document.getElementById('toiawase').disabled = true;
		document.getElementById('company').disabled = true;
		document.getElementById('depart').disabled = true;
		document.getElementById('gyoukai').disabled = true;
		document.getElementById('name').disabled = true;
		document.getElementById('name2').disabled = true;
		document.getElementById('tel').disabled = true;
		document.getElementById('mail').disabled = true;
		document.getElementById('zip21').disabled = true;
		document.getElementById('zip22').disabled = true;
		document.getElementById('pref21').disabled = true;
		document.getElementById('addr21').disabled = true;
		document.getElementById('strt21').disabled = true;
		document.getElementById('kikkake1').disabled = true;
		document.getElementById('kikkake2').disabled = true;
		document.getElementById('kikkake3').disabled = true;
		document.getElementById('kikkake4').disabled = true;
		document.getElementById('kikkake5').disabled = true;
		document.getElementById('kikkake6').disabled = true;
		document.getElementById('sonota').disabled = true;
		document.getElementById('btn_confirm').disabled = true;
		
	}
}


// --> 
</SCRIPT>
</head>
<body onload="check_agree()">


<!-- Header Begin -->
<div id="header">
<div class="contentform"><a href="../index.html"><img src="../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../index.html"><img src="../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi"></div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<div id="formcont">
<h2><img src="images/ind_h2.jpg" alt="お問い合わせフォーム" width="700" height="78" /></h2>
<form method="POST" action="inquiry1.php" name="myForm" onSubmit="return check()">

<table cellspacing="2" id="confirm0">
  <tr>
    <td class="t1">個人情報の取扱いについての同意</td>
  </tr>
    <tr>
	<td align="center"><textarea name="kiyaku" rows="10" readonly="readonly" style="width:100%; color:#808080">
個人情報の利用目的および取扱いについて 

1.個人情報の利用目的 
お問い合わせ、ご相談の対応及び見積書作成、資料の送付の目的

2.個人情報の第三者への提供 
当社は、法令の定める場合を除き、あらかじめご本人の同意を得ずに、個人情報を第三者に提供することは致しません。 

3.個人情報の外部委託 
個人情報の処理について、外部委託をすることがあります。

4.個人情報提出の任意性 
個人情報のご提出は任意です。但し、必要な情報を提供頂かない場合は、適切な対応ができない場合があります。

5.個人情報の開示等の請求
ご提出頂いた個人情報に関する苦情の申し出並びに個人情報の開示等請求（1.利用目的の通知、2.開示、3.内容の訂正、4.追加、5.削除、6.利用の停止、7.消去、8.第三者への提供の停止のご請求）の権利があります。
上記権利の行使をご希望の場合、下記の「個人情報の取扱いに関するお問い合わせ先」宛、お電話あるいは書面でご請求ください。

　　　　　　　　　　　　　　　　　　　　　　　　　　株式会社アイ・エス・エス
　　　　　　　　　　　　　　　　　　　　　　　　　　個人情報保護管理者　山田 美野


■個人情報の取扱いに関するお問い合わせ先

　株式会社 アイ・エス・エス　個人情報保護管理者 山田 美野
　〒108-0073 東京都港区三田3-13-12 三田MTビル8階 
　電話 03-6369-9991　　e-mail：pv_info@issjp.com
　個人情報保護方針は、当社ホームページhttp://www.issjp.comをご参照下さい。</textarea><br><br>
                            <input type="checkbox" id="agree" name="agree" value="同意します" onclick="check_agree()" >　<span style="font-weight:bold">同意します</span></td>
    </tr>
	<tr>
	<td align="center">※「同意します」にチェックを入れると、フォームにご入力いただけます。</td>
	</tr>
</table>
<br />

<table cellspacing="2" id="confirm">
  <tr>
    <td colspan="2" class="t1">入力画面</td>
  </tr>
    <tr>
      <td colspan="2" class="hisu"><span class="importance">*</span>&nbsp;必須項目</td>
    </tr>
      
      <tr>
        <th><p align="left"><nobr>お問い合わせサービス <span class="importance">*</span></nobr></p>
（複数選択可）    </th>
    <td>
      <label>
        <input type="checkbox" id="checkbox1" name="checkbox1" disabled="disabled" value="・通訳"/> 
        通訳</label>
      <br />
      <label>
        <input type="checkbox" id="checkbox2" name="checkbox2" disabled="disabled" value="・コンベンション（会議企画・運営）"/> 
        コンベンション（会議企画・運営）</label>
      <br />
      <label>
	  	<input type="checkbox" id="checkbox3" name="checkbox3" disabled="disabled" value="・翻訳"/> 
      翻訳</label>
      <br />
      <label>
        <input type="checkbox" id="checkbox4" name="checkbox4" disabled="disabled" value="・人材派遣・紹介予定派遣"/> 
        人材派遣・紹介予定派遣</label>
      <br />

      <!-- label>
        <input type="checkbox" id="checkbox5" name="checkbox5" disabled="disabled" value="・法人向け語学研修"/> 
        法人向け語学研修</label>
      <br / -->

      <label>
        <input type="checkbox" id="checkbox6" name="checkbox6" disabled="disabled" value="・その他"/> 
        その他</label>    </td>
    </tr>
      <tr>
        <th>お問い合わせ内容&nbsp;<span class="importance">*</span></th>
    	<td><textarea id="toiawase" name="toiawase" cols="70" rows="10" style="ime-mode: active;" disabled="disabled"></textarea></td>
    </tr>
      
      <tr>
        <th><nobr>会社（団体）名 <span class="importance">*</span></nobr></th>
    <td><input type="text" id="company" name="company" style="ime-mode: active;" size="60" disabled="disabled">
<p><span class="selectRequiredMsg">例：株式会社アイ・エス・エス</span></p>      </td>
    </tr>      
      <tr>
        <th><nobr>部署名</nobr></th>
    <td><input type="text" id="depart" name="depart" style="ime-mode: active;" size="60" disabled="disabled">
	<p><span class="selectRequiredMsg">例：人事部</span></p></td>
    </tr>
      
      <tr>
        <th><nobr>業界</nobr></th>
    <td>
      <select id="gyoukai" name="gyoukai" disabled="disabled">
        <option value="" selected>選択してください</option>
        <option value="水産・農林業・鉱業">水産・農林業・鉱業</option>
        <option value="建設業">建設業</option>
        <option value="製造業(食品)">製造業(食品)</option>
        <option value="製造業(繊維・アパレル)">製造業(繊維・アパレル)</option>
        <option value="造業(化学・ガラス・セメント)">造業(化学・ガラス・セメント)</option>
        <option value="製造業(医薬品)">製造業(医薬品)</option>
        <option value="製造業(鉄鋼・非鉄・金属)">製造業(鉄鋼・非鉄・金属)</option>
        <option value="製造業(機械・電気機器・精密機器)">製造業(機械・電気機器・精密機器)</option>
        <option value="製造業(自動車・輸送用機器)">製造業(自動車・輸送用機器)</option>
        <option value="製造業(高級消費財)">製造業(高級消費財)</option>
        <option value="製造業(化粧品)">製造業(化粧品)</option>
        <option value="製造業(その他)">製造業(その他)</option>
        <option value="資源・エネルギー">資源・エネルギー</option>
        <option value="運輸業・郵便業">運輸業・郵便業</option>
        <option value="情報通信業(IT・通信)">情報通信業(IT・通信)</option>
        <option value="情報通信業(マスメディア・広告業)">情報通信業(マスメディア・広告業)</option>
        <option value="流通業">流通業</option>
        <option value="金融・保険・証券業">金融・保険・証券業</option>
        <option value="不動産業">不動産業</option>
        <option value="サービス(宿泊業、飲食サービス業)">サービス(宿泊業、飲食サービス業)</option>
        <option value="サービス(医療・福祉サービス)">サービス(医療・福祉サービス)</option>
        <option value="サービス(生活関連・娯楽・レジャーサービス)">サービス(生活関連・娯楽・レジャーサービス)</option>
        <option value="サービス(専門技術サービス業)">サービス(専門技術サービス業)</option>
        <option value="サービス(その他サービス業)">サービス(その他サービス業)</option>
        <option value="共(官公庁/日本政府機関/地方公共団体)">共(官公庁/日本政府機関/地方公共団体)</option>
        <option value="公共(国際機関・外国政府機関)">公共(国際機関・外国政府機関)</option>
        <option value="公共(学術・文化関係)">公共(学術・文化関係)</option>
        <option value="公共(公益法人)">公共(公益法人)</option>
        <option value="公共(教育機関)">公共(教育機関)業</option>
        <option value="公共(非営利法人/その他の団体)">公共(非営利法人/その他の団体)</option>
        </select></td>
    </tr>
      
      <tr>
        <th><nobr>お名前&nbsp;<span class="importance">*</span></nobr></th>
    <td><input type="text" id="name" name="name" style="ime-mode: active;" size="50" disabled="disabled"></td>
    </tr>
      
      <tr>
        <th><nobr>お名前（フリガナ）&nbsp;<span class="importance">*</span></nobr></th>
    <td><input type="text" id="name2" name="name2" style="ime-mode: active;" size="50" disabled="disabled"></td>
    </tr>
      
      <tr>
        <th><nobr>お電話番号&nbsp;<span class="importance">*</span></nobr></th>
    <td>
      <input type="text" id="tel" name="tel" style="ime-mode: disabled;" size="50" disabled="disabled"></td>
    </tr>
      
      <tr>
        <th><nobr>e-mail アドレス&nbsp;<span class="importance">*</span></nobr></th>
    <td><input type="text" id="mail" name="mail" style="ime-mode: disabled;" size="50" disabled="disabled"></td>
    </tr>
      
      <tr>
        <th><nobr>ご住所</nobr></th>
    <td>
      〒
      <input type="text" id="zip21" name="zip21" style="ime-mode: disabled;" size="4" maxlength="3" disabled="disabled">
      -
      <input type="text" id="zip22" name="zip22" style="ime-mode: disabled;" size="5" maxlength="4" onKeyUp="AjaxZip3.zip2addr('zip21','zip22','pref21','addr21','strt21');" disabled="disabled"><span class="supplement">（半角数字）　郵便番号を入力すると住所が自動入力されます</span>
      <br>
      <br>
	  <input type="text" id="pref21" name="pref21" style="ime-mode: active;" size="50" disabled="disabled">
      <span class="selectRequiredMsg"><nobr>(都道府県)</nobr></span>
      <br /><br />

      <input type="text" id="addr21" name="addr21" style="ime-mode: active;" size="50" disabled="disabled">
      <span class="selectRequiredMsg"><nobr>(市区町村)</nobr></span><br /><br />

      <input type="text" id="strt21" name="strt21" style="ime-mode: active;" size="50" disabled="disabled">
      <span class="selectRequiredMsg"><nobr>(番地以下)</nobr><br />
        ※ビル名まで詳しくご入力ください</span></td>
    </tr>
      
      <tr>
        <th><nobr>当社をどのように</nobr><br />
        <nobr>お知りになりましたか</nobr></th>
    <td><!--<br>
<input type="text" name="strt21" size="40">-->
      <label><input type="radio" id="kikkake1" name="kikkake" value="検索エンジン（Yahoo!）" disabled="disabled">
        検索エンジン（Yahoo!）</label><br />
      <label><input type="radio" id="kikkake2" name="kikkake" value="検索エンジン（Google）" disabled="disabled">
        検索エンジン（Google）</label><br />
      <label><input type="radio" id="kikkake3" name="kikkake" value="新聞・雑誌" disabled="disabled">
        新聞・雑誌</label><br />
      <label><input type="radio" id="kikkake4" name="kikkake" value="取引先や知人からの紹介" disabled="disabled">
        取引先や知人からの紹介</label><br />
      <label><input type="radio" id="kikkake5" name="kikkake" value="以前から知っていた" disabled="disabled">
        以前から知っていた</label><br />
      <label><input type="radio" id="kikkake6" name="kikkake" value="その他" disabled="disabled">
        その他</label>
      <input type="text" id="sonota" name="sonota" style="ime-mode: active;" size="50" disabled="disabled"></td>
    </tr>
	<tr>
		<td>　</td>
	</tr>      
      <tr>
        <td class="btnForm" colspan="2"><input type="submit" id="btn_confirm" name="btn_confirm" style="font-size:11pt;" value="確認画面へ" disabled="disabled"></td>
    </tr>
    </table>
</form>

<div align="center"><br />
<table width="600"  border="0" cellpadding="0" cellspacing="2" class="formTable mtx">
		<tr>
		  <td align="center" valign="top"><span id="ss_img_wrapper_100-50_flash_ja">
  <a href="http://jp.globalsign.com/" target=_blank>
  <img src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif" alt="SSL　グローバルサインのサイトシール" name="ss_img" border=0 id="ss_img"></a>
  </span>
  <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gs_flash_100-50_ja.js"></script></td>
			  <td width="2" align="left" valign="top">&nbsp;</td>
			  <td align="left" valign="top"><p class="mtx">当サイトでは、実在性の証明とプライバシー保護のため、グローバルサインのSSLサーバ証明書を使用し、SSL暗号化通信を実現しています。サイトシールのクリックにより、サーバ証明書の検証結果をご確認ください。</p></td>
	    </tr>
  </table>
</div>


</div>
<!-- コンテンツ -->

<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../common/images/pixel_trans.gif" width="816" height="15" alt="" class="spacer" />
<div class="menuList">
<ul>
  <li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>
</div>
</div>
</div>
<!-- Footer End -->

<!-- Google Code for JP&#21839;&#12356;&#21512;&#12431;&#12379;&#12501;&#12457;&#12540;&#12512;&#65288;&#21516;&#24847;&#65289; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 984295382;
var google_conversion_language = "ja";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "OcT5CJLrrQIQ1s-s1QM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/984295382/?label=OcT5CJLrrQIQ1s-s1QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body></html>