#!/usr/bin/perl


#各種設定開始

#トップページの更新の有無の設定（0は更新しない。１は更新する。デフォルトは更新しない）

my $RenewalTop = 0;

#ディレクトリ情報ここから
my $TextFileDir = "../secret/english_job";
my $TemplateDir = "../template/english_job";
#ディレクトリ情報ここまで

#unix上への実装時のパス
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#テンプレートファイルリストここから
my $CompHtml = "addcomplete.html";
my $CorrectForm = "correctinputdata.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#トップページ更新用データファイル
my $NewsInfo = "newsinfo.csv";

#保存ファイル名用変数（これを変更することで保存ファイル名が変更される。複数のfaqを設けるときに使用。デフォルトは"faq"）
my $SaveFileHead = "english_job";

#入力情報の過不足チェック用変数
my $ErrorCheck = "0";#0はエラーなし。その他はエラーあり。

#エラーメッセージ変数
my $ErrorMessage = "入力情報に不足があります。不足情報を入力し直して情報を再送信してください。";

#正常終了時Htmlソース出力用文字列(unix)
my $HtmlSrc = "";
open SAFEENDHTML, "$TemplateDir/$CompHtml";
while (<SAFEENDHTML>){
	$HtmlSrc .= $_;
}
close SAFEENDHTML;

#不正終了時Htmlソース出力用文字列(unix)
my $ErrorHtmlSrc = "";
open ERRORHTML, "$TemplateDir/$Failure";
while (<ERRORHTML>){
	$ErrorHtmlSrc .= $_;
}
close ERRORHTML;

#再入力用フォーム出力用文字列(unix)
my $ReInputForm = "";
open INPUTFORM, "$TemplateDir/$CorrectForm";
while (<INPUTFORM>){
	$ReInputForm .= $_;
}
close INPUTFORM;


#各種設定終了


#フォームからのデータ受け取り開始

&ReadParse(*in);

my $QCategory = $in{'Category'};
my $Question = $in{'TextQuestion'};
my $Answer = $in{'TextAnswer'};
my $Respondent = $in{'Respondent'};

#フォームからのデータ受け取り終了
print ("Content-type:text/html\n\n");

#受け取り情報が充分であるかどうかの判定
if(length ($Question) == 0){
	$ErrorCheck = 1;
}
else{}
if(length ($Answer) == 0){
	$ErrorCheck = 1;
}
else{}
if(length ($Respondent) == 0){
	$ErrorCheck = 1;
}
else{}

#正常時の書き込み作業
if($ErrorCheck == 0)
{
	#取得したデータから改行コードを削除
	#受け取ったデータの中に改行コードがあると、リスト表示に問題が発生するため
	$Question =~ s/\r\n/<br>/g;
	$Answer =~ s/\r\n/<br>/g;
	$Respondent =~ s/\r\n/<br>/g;
	
	$Question =~ s/\n/<br>/g;
	$Answer =~ s/\n/<br>/g;
	$Respondent =~ s/\n/<br>/g;
	
	#一意なファイル名を生成するためにエポック秒取得
	my $date = sprintf("%10d",time());
	
	#保存ファイル名生成
	my $SaveFileName = "$SaveFileHead" . "$date" . "\.txt";
	
	#ファイル保存
	open FAQ, ">$TextFileDir/$SaveFileName" or die "Error!";
	print FAQ ("$SaveFileName\n");
	print FAQ ("$QCategory\n");
	print FAQ ("$Question\n");
	print FAQ ("$Answer\n");
	print FAQ ("$Respondent\n");
	close FAQ;
	
	if($RenewalTop == 1){
		&CreateNewsInfo();
		&RenewTopPage();
	}
	else{}
	
	print ("$HtmlSrc");
}
#情報の不足時の再入力を促す処理
else
{
	$ReInputForm =~ s/__MESSAGE__TO__USER__/$ErrorMessage/;
	$ReInputForm =~ s/__QUESTION__MESSAGE__/$Question/;
	$ReInputForm =~ s/__ANSWER__MESSAGE__/$Answer/;
	$ReInputForm =~ s/__RESPONDENT__/$Respondent/;
	
	print ("$ReInputForm");
}

#以下サブルーチン
sub CreateNewsInfo{
	#各種設定開始
	my $NumberOfData = 3;#書き出しデータ数。デフォルトは3
	#各種設定終了
	my $OutputFormat = "";
	my @filelist = ();
	
	#ファイルリスト取得
	opendir NEWDIR, "$TextFileDir" or die "Directory open error!!";
	while (my $v = readdir(NEWDIR)){
		if($v =~/^($SaveFileHead.+)\.txt$/){push(@filelist,$v);}
	}
	
	@filelist = sort @filelist;
	@filelist = reverse @filelist;
	
	closedir NEWDIR;
	#ファイルからデータの読み込み
	for (my $count = 0;$count < $NumberOfData;$count++){
		my @TempOutput = ();
		open FAQFILE, "$TextFileDir/$filelist[$count]" or die "Error!!";
		@TempOutput = <FAQFILE>;
		close FAQFILE;
		
		chomp @TempOutput;
		$OutputFormat .= join(",",@TempOutput);
		$OutputFormat .= "\n";
	}
	
	#データの書き込み
	open NEWSINFO, ">$TextFileDir/$NewsInfo";
	print NEWSINFO ("$OutputFormat");
	close NEWSINFO;
	
	return 0;
}
sub RenewTopPage{
	#各種設定開始

	my $NumberOfWorkData = 5;#仕事情報書き出しデータ数。デフォルトは5。CreateNewWorkInfo内の同名の変数の値も変える必要あり。
	my $NumberOfNewsData = 3;#最新ニュース書き出しデータ数。デフォルトは3
	my $CatchLength = 26;#最大表示表示キャッチフレーズ長
	
	#ファイル、ディレクトリ情報
	my $TemplateDirectory = "../template/toppage";
	my $UsingWorkDataDirectory = "../secret/job";
	my $UsingNewsDataDirectory = "../secret/staff";
	my $TopPageDirectory = "../../../test";
	my $HtmlTemplateFile = "toppage.html";
	my $WorkTableTemplateFile = "toppagetable.html";
	my $WorkRowTemplateFile = "toppagetableelement.html";
	my $NewsTableTemplateFile = "toppagetable2.html";
	my $NewsRowTemplateFile = "toppagetableelement2.html";
	my $WorkDataFileName = "newworkinfo.csv";
	my $NewsDataFileName = "newsinfo.csv";
	my $OutputFileName = "index2.html";
	
	#各種設定終了
	
	#更新用情報生成開始
	my $OutputHtml = "";
	
	my $TempWorkRowSrc = "";
	my $TempWorkRow = "";
	my $OutputWorkRowSrc = "";
	my $OutputWorkRow = "";
	my $OutputWorkTable = "";
	
	my @Catch = ();
	my @DetailNo = ();
	
	my $TempNewsRowSrc = "";
	my $TempNewsRow = "";
	my $OutputNewsRowSrc = "";
	my $OutputNewsRow = "";
	my $OutputNewsTable = "";
	
	my @NewsTopic = ();
	my @NewsDetail = ();
	
	#仕事データ読み込み
	open WORKINFORMATION, "$UsingWorkDataDirectory/$WorkDataFileName";
	while (<WORKINFORMATION>){
		my @TempWorkOutputInfo = split(/,/,$_);
		push (@Catch,"$TempWorkOutputInfo[2]");
		push (@DetailNo,"$TempWorkOutputInfo[14]");
	}
	close WORKINFORMATION;
	
	#仕事情報の行要素の整形
	open WORKROWDATA, "$TemplateDirectory/$WorkRowTemplateFile";
	while (<WORKROWDATA>){
		$TempWorkRowSrc .= $_;
	}
	close WORKROWDATA;
	
	for (my $workcount = 0;$workcount < $NumberOfWorkData;$workcount++){
		$OutputWorkRowSrc = "$TempWorkRowSrc";
		my $TempCatchData = shift @Catch;
		my $TempDetailNoData = shift @DetailNo;
		$OutputWorkRowSrc =~ s/__CATCH__/$TempCatchData/;
		$OutputWorkRowSrc =~ s/__DETAIL__NO__/$TempDetailNoData/g;
		$OutputWorkRow .= $OutputWorkRowSrc;
	}
	
	#仕事情報のテーブルの整形
	open WORKTABLEDATA, "$TemplateDirectory/$WorkTableTemplateFile";
	while (<WORKTABLEDATA>){
		$OutputWorkTable .= $_;
	}
	close WORKTABLEDATA;
	$OutputWorkTable =~ s/__TABLE__ELEMENT__/$OutputWorkRow/;
	
	
	#ニュースデータ読み込み
	open NEWSINFORMATION, "$UsingNewsDataDirectory/$NewsDataFileName";
	while (<NEWSINFORMATION>){
		my @TempNewsOutputInfo = split(/,/,$_);
		push (@NewsTopic,"$TempNewsOutputInfo[2]");
		push (@NewsDetail,"$TempNewsOutputInfo[0]");
	}
	close NEWSINFORMATION;
	
	#ニュースの行要素の整形
	open NEWSROWDATA, "$TemplateDirectory/$NewsRowTemplateFile";
	while (<NEWSROWDATA>){
		$TempNewsRowSrc .= $_;
	}
	close NEWSROWDATA;
	
	for (my $newscount = 0;$newscount < $NumberOfNewsData;$newscount++){
		$OutputNewsRowSrc = $TempNewsRowSrc;
		my $TempNewsTopicData = shift @NewsTopic;
		my $TempNewsDetailData = shift @NewsDetail;
		$OutputNewsRowSrc =~ s/__NEWS__MESSAGE__/$TempNewsTopicData/;
		$OutputNewsRowSrc =~ s/__DETAIL__FILE__/$TempNewsDetailData/g;
		$OutputNewsRow .= $OutputNewsRowSrc;
	}
	
	#ニュースのテーブルの整形
	open NEWSTABLEDATA, "$TemplateDirectory/$NewsTableTemplateFile";
	while (<NEWSTABLEDATA>){
		$OutputNewsTable .= $_;
	}
	close NEWSTABLEDATA;
	$OutputNewsTable =~ s/__TABLE__ELEMENT__/$OutputNewsRow/;
	
	
	#最終整形
	open FINAL, "$TemplateDirectory/$HtmlTemplateFile";
	while (<FINAL>){
		$OutputHtml .= $_;
	}
	close FINAL;
	
	$OutputHtml =~ s/__NEW__WORK__INFORMATION__/$OutputWorkTable/;
	$OutputHtml =~ s/__TOPPAGE__NEWS__/$OutputNewsTable/;
	
	#トップページ書き換え
	open OUTPUT, ">$TopPageDirectory/$OutputFileName";
	print OUTPUT ("$OutputHtml");
	close OUTPUT;
	
	return 0;

}