#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $DataFileDir = "../secret/job";

my $TemplateDir = "../template/job";
my $LiblaryDir = "../perl_lib";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "searchresult.html";
my $HtmlTable = "workinfotable.html";
my $NotFound = "0hit.html";
#テンプレートファイルリストここまで

#データファイルリストここから
my $WorkInfo = "workinfo.csv";
#データファイルリストここまで

#モジュール読み込み
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#出力HTMLソース一時格納用変数
my $HtmlSrc = "";

#置換用文字列格納用変数
my $ReplaceStr = "";

#各種設定ここまで

#CGIから文字列受け取り
my $JobKind = $ENV{'QUERY_STRING'};

#フォームから情報受け取り
#&ReadParse(*in);

#my $JobKind = $in{'JobKind'};
#my $WageRange = $in{'WageRange'};

print("Content-type:text/html\n\n");

#出力用データ
my @Template = ('問い合わせNo','お仕事No','キャッチフレーズ','職種','時給','時給カテゴリ','場所','曜日','残業','勤務開始時間','勤務終了時間','仕事期間');

open DATA, "$DataFileDir/$WorkInfo" or die "file not exist!";

my $JobExistance = 0;
my $PlaceData = "";

#検索処理の実行
while(<DATA>){
	my @TmpInfo = split(/,/,$_);
#	if(($TmpInfo[3] == $JobKind and $TmpInfo[5] == $WageRange) or ($JobKind == 0 and $TmpInfo[5] == $WageRange) or ($JobKind == -1 and $TmpInfo[5] == $WageRange) or ($WageRange == 0 and $TmpInfo[3] == $JobKind) or ($JobKind == -1 and $WageRange == 0)){
	if(($TmpInfo[3] == $JobKind) or ($JobKind == -1)){
		my $TempTable = "";
		open NOHIT, "$TemplateDir/$HtmlTable";
		while (<NOHIT>){
			$TempTable .= $_;
		}
		close NOHIT;
		$PlaceData = $TmpInfo[13];
		$JobExistance++;
		$TmpInfo[3] = &ConvertJob ($TmpInfo[3]);
		$TmpInfo[6] = &ConvertPlace ($TmpInfo[6],$TmpInfo[13]);
		$TmpInfo[7] = &ConvertDay($TmpInfo[7],$TmpInfo[15]);
		$TempTable =~ s/__CATCH__/$TmpInfo[2]/;
		$TempTable =~ s/__WORK__NO__/$TmpInfo[1]/;
		$TempTable =~ s/__JOB__NAME__/$TmpInfo[0]/;
		$TempTable =~ s/__DETAIL__NO__/$TmpInfo[14]/;
#		$TempTable =~ s/__JOB__TYPE__/$TmpInfo[0]/;
		$TempTable =~ s/__WAGE__/$TmpInfo[4]/;
		$TempTable =~ s/__WORK__PLACE__/$TmpInfo[6]/;
		$TempTable =~ s/__WORK__DAY__/$TmpInfo[7]/;
		$TempTable =~ s/__START__TIME__/$TmpInfo[9]/;
		$TempTable =~ s/__END__TIME__/$TmpInfo[10]/;
		$ReplaceStr = "$TempTable" . "$ReplaceStr";

	}
}

#検索結果が0であった場合の処理
if($JobExistance == 0){
	open NOHIT, "$TemplateDir/$NotFound";
	while (<NOHIT>){
		$HtmlSrc .= $_;
	}
	close NOHIT;
}
else
{
	open BODYTEMP, "$TemplateDir/$HtmlBody";
	while (<BODYTEMP>){
		$HtmlSrc .= $_;
	}
	$HtmlSrc =~ s/__SEARCH__RESULT__/$ReplaceStr/;
}



print ("$HtmlSrc");

#以下サブルーチン
sub ConvertJob{
	my $ConvertedJob = "";
	my @GetJobData = @_;
	#変換用仕事データ
	my @JobList = ('','通訳・翻訳・翻訳チェッカー','秘書・グループセクレタリー','英文事務・アシスタント・英文経理・貿易事務・人事・総務など','金融業界でのお仕事','IT・技術関連');
	$ConvertedJob = $JobList[$GetJobData[0]];
	return $ConvertedJob;
}

sub ConvertPlace{
	my $ConvertedPlace = "";
	my @GetPlaceData = @_;
	#変換用勤務場所データ
	my @PlaceList = ('','神谷町','赤坂見附','大手町','青山一丁目','茅場町','九段下','新宿','溜池山王','表参道','みなとみらい');
	if ($GetPlaceData[0] == -1){
		$ConvertedPlace = $GetPlaceData[1];
	}
	else{
		$ConvertedPlace = $PlaceList[$GetPlaceData[0]];
	}
	return $ConvertedPlace;
}

sub ConvertDay{
	my $ConvertedDay = "";
	my @GetDayData = @_;
	if ($GetDayData[0] == 1){
		$ConvertedDay = "平日";
	}
	elsif ($GetDayData[0] == 0){
		$ConvertedDay = "土・日";
	}
	elsif ($GetDayData[0] == -1){
		$ConvertedDay = $GetDayData[1];
	}
	else{
		if(($GetDayData[0] % 2) == 0){$ConvertedDay .= "月・";}
		if(($GetDayData[0] % 3) == 0){$ConvertedDay .= "火・";}
		if(($GetDayData[0] % 5) == 0){$ConvertedDay .= "水・";}
		if(($GetDayData[0] % 7) == 0){$ConvertedDay .= "木・";}
		if(($GetDayData[0] % 11) == 0){$ConvertedDay .= "金・";}
		if(($GetDayData[0] % 13) == 0){$ConvertedDay .= "土・";}
		if(($GetDayData[0] % 17) == 0){$ConvertedDay .= "日・";}
		chop ($ConvertedDay);
		chop ($ConvertedDay);
	}
	return $ConvertedDay;

}