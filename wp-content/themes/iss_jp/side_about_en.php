<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>About ISS</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="<?php echo home_url();?>/en/about/message">Top Message</a></p>
        <p><a href="<?php echo home_url();?>/en/about/company">Corporate Profile</a></p>
        <p><a href="<?php echo home_url();?>/en/about/history">History</a></p>
        <p><a href="<?php echo home_url();?>/en/about/mission">ISS Corporate Philosophy</a></p>
    </div>
</div>