<?php

/********** 初期設定 **********/

require_once("./dbini.php");

/********** メイン処理 **********/

if (isset($_GET['uid'])) {

	$id=$_GET['uid'];

	//IDの最大値を取得
	$sql = "SELECT * FROM earth2007 WHERE id='$id'";

	//DB処理
	// DB選択
	$con = mysql_connect($DBSERVER,$DBUSER,$DBPASSWORD);
	if (!($con)) { error("データベース接続エラーです。"); }
	// DB接続

	$selectdb = mysql_select_db($DBNAME,$con);
	if (!($selectdb)) { error("データベース選択エラーです。"); }	
	
	// 結果セット取得
	$rst=mysql_query($sql,$con);
	
	if (!($rst)) {
		error("SQL発行エラーです。");
	}

	//取得データ格納
	$col=mysql_fetch_array($rst);

	// DB閉じる
	$con = mysql_close($con);
	if (!($con)) { error("データベース接続クローズエラーです。"); }

}

	//カード番号の処理
	$card_num = split("-", $col['card_num']);


?>

<html>
<head>
  <!--meta http-equiv="Content-Type" content="text/html; charset=UTF-8"-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>EARTH2007</title>
	<link rel="stylesheet" href="base.css" media="screen,print">
	<script language="JavaScript" src="form.js"></script>
	</head>
<SCRIPT>
history.forward();
</SCRIPT>

<body>
<!--form method="post" name="Organization"-->
<div id="wrapper">

<div id="Form_title">
<h1>The 9th International Symposium on East Asian Resources Recycling Technology<br>
October 29 - November 2, 2007 Sendai, Japan<br>
<span>On-line Registration Form</span></h1>
</div>

    <div id="Form_detail"> 
    	
	<p>This notification is <span style="font-weight:bold;">pre-confirmation. </span><br>
Your registration number is <span style="font-weight:bold;"><?printf("%04d", $col['id'])?>.</span><br>
Please print out this form, bring it with you to the symposium and present it at the reception desk.
</p>

		<h2>1. Personal Information: Regular Participant / Student</h2>


	    <table border="0" cellspacing="0" cellpadding="0">
	    <tr> 
	        <th>Title<span class="must">*</span></th>
			<td>
				<?=$col['title']?>
			</td>
	      </tr>
	        <tr> 
	        <th>Name<span class="must">*</span></th>
			<td>
				<?=$col['name']?>
			</td>
	      </tr>
	        <tr> 
	        <th>Affiliation<span class="must">*</span></th>
			<td>
				<?=$col['affiliation']?>
			</td>
	      </tr>
	      </tr>
	        <tr> 
	        <th>Department<span class="must">*</span></th>
			<td>
				<?=$col['department']?>
			</td>
	      </tr>
	      <tr> 
	          <th>Contact<span class="must">*</span></th>
	          <td>
				<?=$col['contact']?>
	      </tr>
	      <tr> 
	          <th>Address<span class="must">*</span></th>
	          <td>
				<?=$col['address']?>
	          </td>
	      </tr>
	      <tr> 
	          <th>Postal / Zip code<span class="must">*</span></th>
	          <td>
				<?=$col['postal']?>
	          </td>
	      </tr>
	      <tr> 
	          <th>Country<span class="must">*</span></th>
	          <td>
				<?=$col['country']?>
	          </td>
	      </tr>
	      <tr> 
	          <th>Telephone Number<span class="must">*</span></th>
	          <td>
				<?=$col['tel']?>
	          </td>
	      </tr>
	      <tr> 
	          <th>Fax Number<span class="must">*</span></th>
	          <td>
				<?=$col['fax']?>
	          </td>
	      </tr>
	      <tr> 
	          <th>Email Address<span class="must">*</span></th>
	        <td>
				<?=$col['email']?>
			</td>
	      </tr>
	      </table>


		<h2>2. Accompanying Person Information</h2>
	    <table border="0" cellspacing="0" cellpadding="0">
	        <th>
	        	Number of Accompanying Person
			</th>
			<td>
				<?=$col['number_of_accompanying_persons']?> Person
			</td>
		</tr>
		</table>
		
<? if ($col['number_of_accompanying_persons'] > 0) { ?>
		<table id="ap01">
	    <tr>
	        <th>Title<span class="must">*</span></th>
			<td>
				<?=$col['ap01_title']?>
			</td>
	    </tr>
	    <tr> 
	        <th>Name<span class="must">*</span></th>
			<td>
				<?=$col['ap01_name']?>			
			</td>
	      </tr>
	    <tr>
	        <th>Banquet<span class="must">*</span></th>
			<td>
				<?=$col['ap01_banquet']?>
			</td>
	    </tr>
	    <tr>
	        <th>Post Conference Tour 1<span class="must">*</span></th>
			<td>
				<?=$col['ap01_tour1']?>
			</td>
	    </tr>
	    <tr>
	        <th>Post Conference Tour 2<span class="must">*</span></th>
			<td>
				<?=$col['ap01_tour2']?>
			</td>
	    </tr>

	    </table>
<? }
if ($col['number_of_accompanying_persons'] > 1) { ?>
	    <table id="ap02">
	    <tr>
	        <th>Title</th>
			<td>
				<?=$col['ap02_title']?>
			</td>
	    </tr>
	    <tr> 
	        <th>Name</th>
			<td>
				<?=$col['ap02_name']?>			
			</td>
	      </tr>
	    <tr>
	        <th>Banquet</th>
			<td>
				<?=$col['ap02_banquet']?>
			</td>
	    </tr>
	    <tr>
	        <th>Post Conference Tour 1</th>
			<td>
				<?=$col['ap02_tour1']?>
			</td>
	    </tr>
	    <tr>
	        <th>Post Conference Tour 2</th>
			<td>
				<?=$col['ap02_tour2']?>
			</td>
	    </tr>

		</table>
<? } ?> 

		<h2>3. Registration Fees<span class="must">*</span></h2>
		<table border="0" cellspacing="1" cellpadding="0" id="tour">
		<tr> 
			<th style="text-align:center;vertical-align:middle;" rowspan="2">Category</th>
			<th  style="text-align:center;" colspan="2">Registration Fees</th>
		</tr>
		<tr style="text-align:center;">
			<th class="before" style="text-align:center;">
				Early Registration<br>By August 31, 2007
			</th>
			<th class="after" style="text-align:center;">
				Regular/On-Site Registration<br>After September 1, 2007
			</th>
		</tr>
		<tr> 
		<th>Regular participant</th>
			<td class="before">
				<?if($col['registration'] == "30000"){print "JPY 30,000";}?>
			</td>
			<td class="after">
				<?if($col['registration'] == "35000"){print "JPY 35,000";}?>
			</td>
		</tr>
		<th>Student</th>
			<td class="before">
				<?if($col['registration'] == "10000"){print "JPY 10,000";}?>
			</td>
			<td class="after">
				<?if($col['registration'] == "15000"){print "JPY 15,000";}?>
			</td>
		</tr>
		
		<tr>
			<th>
				Accompanying person
			</th>
			<td class="before">
			</td>
			<td class="after">
				<?if($col['accompanying_num'] != 0){ print $col['accompanying_num'] ." Person"; }else { print "Not Participate"; }?>

			</td>
		</tr>
		<tr>
			<th>
				Banquet<br>October 31 (Wed.) 
			</th>
			<td class="before">

			</td>
			<td class="after">
				<?if($col['banquet_num'] != 0){ print $col['banquet_num'] ." Person(s)"; }else { print "Not Attend"; }?>

			</td>
		</tr>

		<tr> 
			<th>Total payment amount</th>
			
			<td colspan="2">
				<span class="payment">JPY <?=$col['payment']?></span>
			</td>
		</tr>
		</table>


		<h2>4. Post conference tours and Excursion<span class="must">*</span><br><div style="font-weight:normal;color:#f00;display:inline;">(The fee will be collected <span style="font-weight:bold;">at the registration desk.</span> We will accept payment <span style="font-weight:bold;">by cash in JPY only.</span>)</div></h2>

		<table border="0" cellspacing="1" cellpadding="0" id="tour">
		<tr> 
			<th style="text-align:center;vertical-align:middle;" rowspan="2">Category</th>
			<th  style="text-align:center;">Fees</th>
		</tr>
		<tr style="text-align:center;">
			<th style="text-align:center;">
				Participate
			</th>
		</tr>
		
		<tr>
			<th>
				Post Conference Tour 1<br>November 1 (Thu.) 
			</th>
			<td>
				<?if($col['tour_1_num'] != 0){ print $col['tour_1_num'] ." Person(s)"; }else { print "Not Participate"; }?>
			</td>

		<tr>
			<th>
				Post Conference Tour 2<br>November 1 (Thu.) – 2 (Fri.) 
			</th>
			<td>
				<?if($col['tour_2_num'] != 0){ print $col['tour_2_num'] ." Person(s)"; }else { print "Not Participate"; }?>
			</td>
		</tr>

		<tr>
			<th>
				Excursion for accompanying person<br>October 30 (Tue.) 
			</th>
			<td>
				<?if($col['tour_3_num'] != 0){ print $col['tour_3_num'] ." Person(s)"; }else { print "Not Participate"; }?>
			</td>
		</tr>

		<tr> 
			<th>Total payment amount</th>
			<td>
				<span class="payment">JPY <?=$col['tour_payment']?></span>
			</td>
		</tr>
		</table>


	  <h2>5. Payment Method<span class="must">*</span></h2>
	  
	  <div id="payment">
	  <p>
	  <? if ($col['method'] == "credit card") { ?>
		  by credit card</p>
		  <div style="margin-left:2em;margin-bottom:30px;">	  	
		  <p>
	  	<?=$col['credit_card']?>
		</p>
		<p>Credit Card Number (16 digits):
				<?=$card_num[0] ."-". $card_num[1] ."-". $card_num[2] ."-****"?>
		</p>

		<p>Expiration Date:
				<?=$col['expiration_date']?>
		</p>

		<p>Cardholder's Name:
				<?=$col['card_name']?>
		</p>
		</div>


	  <? } else {
	  	print "by bank transfer</p>";
	  } ?>
	  <p style="margin-bottom:50px;font-weight:bold;text-align:center;text-decoration:underline;color:#f00;">If you have any problems or wish to change your registration,<br> please contact the registration office as soon as possible.</p>
	</div> <!-- payment -->
	  	  
		</div> <!-- note -->
</div><!-- Form_detail -->
</div><!-- wrapper -->
<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body>
</html>
