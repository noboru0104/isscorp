#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $DataFileDir = "../secret/job";

my $TemplateDir = "../template/job";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "addworkfinish.html";
my $ReInputHtml = "correctinputdata.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#データファイルリストここから
my $WorkInfo = "workinfo.csv";
my $NewWorkInfo = "newworkinfo.csv";
my $Counter = "workcount.dat";
#データファイルリストここまで

#モジュール読み込み
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#出力HTMLソース一時格納用変数
my $HtmlSrc = "";

#テンプレート一時保存用文字列
my $TempStr = "";

#入力情報の過不足チェック用変数
my $ErrorCheck = "0";#0はエラーなし。その他はエラーあり。

#エラーメッセージ変数各種
my $AskNoError = "";
my $JobNoError = "";
my $CatchError = "";
my $JobKindError = "";
my $WageError = "";
my $WorkPlaceError = "";
my $PlaceError = "";
my $AddWorkError = "";
my $WorkTimeError = "";
my $WorkPeriodError = "";
my $DayError = "";
my $WorkInformationError = "";
my $EntryConditionError = "";
my $BikouError = "";

#各種設定ここまで

print("Content-type:text/html\n\n");

#フォームからデータ取得
&ReadParse(*in);

my $WorkDayMode = $in{'WorkDayMode'};
my @DayChecker = ($in{'MonDayChecker'},$in{'TusDayChecker'},$in{'WedDayChecker'},$in{'TurDayChecker'},$in{'FriDayChecker'},$in{'SatDayChecker'},$in{'SunDayChecker'});

my $AskNo = $in{'AskNo'}; 
my $JobNo = $in{'JobNo'};
my $Catch = $in{'Catch'};
my $JobKind = $in{'JobKind'}; 
my $Wage = $in{'Wage'};
my $WorkPlace = $in{'WorkPlace'};
my $PlaceData = $in{'PlaceData'}; 
my $AddWork = $in{'AddWork'};
my $StartTime = $in{'StartTime'}; 
my $EndTime = $in{'EndTime'};
my $WorkPeriod = $in{'WorkPeriod'};
my $Bikou = $in{'Bikou'};
my $DayData = $in{'DayData'};

my $WorkInformation = $in{'WorkInfo'};
my $EntryCondition = $in{'EntryCondition'};

#時給のカテゴリを決定
my $WageRange = &WageRangeConv($Wage);

#WorkDayの値を決定
my $WorkDay = 1;
if ($WorkDayMode == 1){
	for (my $Count = 0;$Count < 7;$Count++){
		if($DayChecker[$Count] ==0 ){
		}
		else{
		$WorkDay = $WorkDay * $DayChecker[$Count];
		}
	}
}
elsif ($WorkDayMode == 2) {
	$WorkDay = 1;
}
elsif ($WorkDayMode == 3) {
	$WorkDay = 0;
}
elsif ($WorkDayMode == -1) {
	$WorkDay = -1;
}
else{
}
#入力データが揃っているかどうか確認
if(length ($AskNo) == 0){
	$ErrorCheck = 1;
	$AskNoError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($JobNo) == 0){
	$ErrorCheck = 1;
	$JobNoError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}

if(length ($Catch) == 0){
	$ErrorCheck = 1;
	$CatchError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if($JobKind == 0){
	$ErrorCheck = 1;
	$JobKindError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($Wage) == 0){
	$ErrorCheck = 1;
	$WageError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($WorkPlace) == 0){
	$ErrorCheck = 1;
	$PlaceError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(($WorkPlace == -1) and (length ($PlaceData) == 0)){
	$ErrorCheck = 1;
	$PlaceError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($AddWork) == 0){
	$ErrorCheck = 1;
	$AddWorkError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($StartTime) == 0){
	$ErrorCheck = 1;
	$WorkTimeError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($EndTime) == 0){
	$ErrorCheck = 1;
	$WorkTimeError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($WorkPeriod) == 0){
	$ErrorCheck = 1;
	$WorkPeriodError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($WorkDayMode) == 0){
	$ErrorCheck = 1;
	$DayError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(($WorkDay == -1) and (length ($DayData) == 0)){
	$ErrorCheck = 1;
	$DayError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($Bikou) == 0){
	$ErrorCheck = 1;
	$BikouError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($WorkInformation) == 0){
	$ErrorCheck = 1;
	$WorkInformationError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
if(length ($EntryCondition) == 0){
	$ErrorCheck = 1;
	$EntryConditionError = "値が入力されていないか、値が適切ではありません。再入力して下さい。";
}
else{}
#データに不備がない場合の書き込み作業
if($ErrorCheck == 0){
	open BODYTEMP, "$TemplateDir/$HtmlBody";
	while (<BODYTEMP>){
		$HtmlSrc .= $_;
	}
	close BODYTEMP;
	
	my $No = 0;
	
	open NUMBER, "$DataFileDir/$Counter" or die "Error";
	$No = <NUMBER>;
	close NUMBER;
	
	$No++;
	
	open NUMBER2, ">$DataFileDir/$Counter" or die "Error";
	print NUMBER2 $No;
	close NUMBER2;
	
	
	#一部データの修正（改行と半角カンマ）
	$AskNo = &StrCorrect($AskNo);
	$JobNo = &StrCorrect($JobNo);
	$Catch = &StrCorrect($Catch);
	$JobKind = &StrCorrect($JobKind);
	$Wage = &StrCorrect($Wage);
	$WageRange = &StrCorrect($WageRange);
	$WorkPlace = &StrCorrect($WorkPlace);
	$WorkDay = &StrCorrect($WorkDay);
	$AddWork = &StrCorrect($AddWork);
	$StartTime = &StrCorrect($StartTime);
	$EndTime = &StrCorrect($EndTime);
	$WorkPeriod = &StrCorrect($WorkPeriod);
	$Bikou = &StrCorrect($Bikou);
	$PlaceData = &StrCorrect($PlaceData);
	$No = &StrCorrect($No);
	$DayData = &StrCorrect($DayData);
	$WorkInformation = &StrCorrect($WorkInformation);
	$EntryCondition = &StrCorrect($EntryCondition);
	
	open DATA, ">>$DataFileDir/$WorkInfo" or die "Error";
	print DATA ($AskNo , ',' , $JobNo , ',' , $Catch  , ',' , $JobKind , ',' , $Wage , ',' , $WageRange , ',' , $WorkPlace , ',' , $WorkDay , ',' , $AddWork , ',' , $StartTime , ',' , $EndTime , ',' , $WorkPeriod , ',' , $Bikou , ',' , $PlaceData , ',' , $No , ',' , $DayData , ',' , $WorkInformation , ',' , $EntryCondition ,"\n");
	close DATA;
	
	&CreateNewWorkInfo();
	&RenewTopPage();
}

#データに不備があった場合の作業
else{
	open BODYTEMP, "$TemplateDir/$ReInputHtml";
	while (<BODYTEMP>){
		$HtmlSrc .= $_;
	}
	close BODYTEMP;
	
	my $ErrorMessage = "入力情報に不備があります。入力データをチェックして再入力を行ってください。";
	
	&CheckJobKind();
	&CheckPlace();
	&CheckDayMode();
	
	$HtmlSrc =~ s/__INPUT__ERROR__/$ErrorMessage/;
	$HtmlSrc =~ s/__ASK__NO__/$AskNo/;
	$HtmlSrc =~ s/__ASK__NO__ERROR__/$AskNoError/;
	$HtmlSrc =~ s/__WORK__NO__/$JobNo/;
	$HtmlSrc =~ s/__WORK__NO__ERROR__/$JobNoError/;

	$HtmlSrc =~ s/__CATCH__/$Catch/;
	$HtmlSrc =~ s/__CATCH__ERROR__/$CatchError/;
	$HtmlSrc =~ s/__JOB__TYPE__ERROR__/$JobKindError/;
	$HtmlSrc =~ s/__WAGE__/$Wage/;
	$HtmlSrc =~ s/__WAGE__ERROR__/$WageError/;
	$HtmlSrc =~ s/__PLACE__/$PlaceData/;
	$HtmlSrc =~ s/__PLACE__ERROR__/$PlaceError/;
	$HtmlSrc =~ s/__DAY__DATA__/$DayData/;
	$HtmlSrc =~ s/__DAY__ERROR__/$DayError/;
	$HtmlSrc =~ s/__ADDWORK__/$AddWork/;
	$HtmlSrc =~ s/__ADDWORK__ERROR__/$AddWorkError/;
	$HtmlSrc =~ s/__START__TIME__/$StartTime/;
	$HtmlSrc =~ s/__END__TIME__/$EndTime/;
	$HtmlSrc =~ s/__WORK__TIME__ERROR__/$WorkTimeError/;
	$HtmlSrc =~ s/__WORK__PERIOD__/$WorkPeriod/;
	$HtmlSrc =~ s/__WORK__PERIOD__ERROR__/$WorkPeriodError/;
	$HtmlSrc =~ s/__BIKOU__/$Bikou/;
	$HtmlSrc =~ s/__BIKOU__ERROR__/$BikouError/;
	$HtmlSrc =~ s/__WORK__INFO__/$WorkInformation/;
	$HtmlSrc =~ s/__WORK__INFO__ERROR__/$WorkInformationError/;
	$HtmlSrc =~ s/__ENTRY__CONDITION__/$EntryCondition/;
	$HtmlSrc =~ s/__ENTRY__CONDITION__ERROR__/$EntryConditionError/;
}

#結果の出力

print ("$HtmlSrc");


#以下サブルーチン
sub StrCorrect{
	my @ConvertedStr = @_;
	$ConvertedStr[0] =~ s/,/&\#044\;/g;
	$ConvertedStr[0] =~ s/\r\n/<br>/g;
	$ConvertedStr[0] =~ s/\n/<br>/g;
	return $ConvertedStr[0];
}

sub WageRangeConv{
	my @GetWageData = @_;
	my $ConvertedData = 0;
	my @WageRangeStd = ("1500","2000","2500","3000","3500","4000","4500","5000");
	label1:for (my $count = 0;$count < @WageRangeStd;$count++){
		if($GetWageData[0] > $WageRangeStd[$count]){
			$ConvertedData = $count + 1;
			last label1;
		}
	}
	return $ConvertedData;
}

sub CreateNewWorkInfo{
	#各種設定開始
	my $NumberOfData = 5;#書き出しデータ数。デフォルトは5
	#各種設定終了
	
	#データファイル読み込み
	my @TempWorkList = ();
	open CONVINFO, "$DataFileDir/$WorkInfo";
	@TempWorkList = <CONVINFO>;
	close CONVINFO;
	
	#データ書き込み
	open NEWINFO, ">$DataFileDir/$NewWorkInfo";
	for (my $count = 0;$count < $NumberOfData;$count++){
		print NEWINFO (pop(@TempWorkList));
	}
	close NEWINFO;
}

sub RenewTopPage{
	#各種設定開始
	my $NumberOfWorkData = 5;#仕事情報書き出しデータ数。デフォルトは5。CreateNewWorkInfo内の同名の変数の値も変える必要あり。
	my $NumberOfNewsData = 3;#最新ニュース書き出しデータ数。デフォルトは3
	my $CatchLength = 26;#最大表示表示キャッチフレーズ長
	
	#ファイル、ディレクトリ情報
	my $TemplateDirectory = "../template/toppage";
	my $UsingWorkDataDirectory = "../secret/job";
	my $UsingNewsDataDirectory = "../secret/news";
	my $TopPageDirectory = "../../home";
	my $HtmlTemplateFile = "toppage.html";
	my $WorkTableTemplateFile = "toppagetable.html";
	my $WorkRowTemplateFile = "toppagetableelement.html";
	my $NewsTableTemplateFile = "toppagetable2.html";
	my $NewsRowTemplateFile = "toppagetableelement2.html";
	my $WorkDataFileName = "newworkinfo.csv";
	my $NewsDataFileName = "newsinfo.csv";
	my $OutputFileName = "index.html";
	
	#各種設定終了
	
	#更新用情報生成開始
	my $OutputHtml = "";
	
	my $TempWorkRowSrc = "";
	my $TempWorkRow = "";
	my $OutputWorkRowSrc = "";
	my $OutputWorkRow = "";
	my $OutputWorkTable = "";
	
	my @Catch = ();
	my @DetailNo = ();
	
	my $TempNewsRowSrc = "";
	my $TempNewsRow = "";
	my $OutputNewsRowSrc = "";
	my $OutputNewsRow = "";
	my $OutputNewsTable = "";
	
	my @NewsTopic = ();
	my @NewsDetail = ();
	
	#仕事データ読み込み
	open WORKINFORMATION, "$UsingWorkDataDirectory/$WorkDataFileName";
	while (<WORKINFORMATION>){
		my @TempOutputInfo = split(/,/,$_);
		push (@Catch,"$TempOutputInfo[2]");
		push (@DetailNo,"$TempOutputInfo[14]");
	}
	close WORKINFORMATION;
	
	#仕事情報の行要素の整形
	open WORKROWDATA, "$TemplateDirectory/$WorkRowTemplateFile";
	while (<WORKROWDATA>){
		$TempWorkRowSrc .= $_;
	}
	close WORKROWDATA;
	
	for (my $count = 0;$count < $NumberOfWorkData;$count++){
		$OutputWorkRowSrc = "$TempWorkRowSrc";
		my $TempCatchData = shift @Catch;
		my $TempDetailNoData = shift @DetailNo;
		$OutputWorkRowSrc =~ s/__CATCH__/$TempCatchData/;
		$OutputWorkRowSrc =~ s/__DETAIL__NO__/$TempDetailNoData/g;
		$OutputWorkRow .= $OutputWorkRowSrc;
	}
	
	#仕事情報のテーブルの整形

	open WORKTABLEDATA, "$TemplateDirectory/$WorkTableTemplateFile";
	while (<WORKTABLEDATA>){
		$OutputWorkTable .= $_;
	}
	close WORKTABLEDATA;
	$OutputWorkTable =~ s/__TABLE__ELEMENT__/$OutputWorkRow/;
	
	
	#ニュースデータ読み込み
	open NEWSINFORMATION, "$UsingNewsDataDirectory/$NewsDataFileName";
	while (<NEWSINFORMATION>){
		my @TempOutputInfo = split(/,/,$_);
		push (@NewsTopic,"$TempOutputInfo[2]");
		push (@NewsDetail,"$TempOutputInfo[0]");
	}
	close NEWSINFORMATION;
	
	#ニュースの行要素の整形
	open NEWSROWDATA, "$TemplateDirectory/$NewsRowTemplateFile";
	while (<NEWSROWDATA>){
		$TempNewsRowSrc .= $_;
	}
	close NEWSROWDATA;
	
	for (my $count = 0;$count < $NumberOfNewsData;$count++){
		$OutputNewsRowSrc = "$TempNewsRowSrc";
		my $TempCatchData = shift @NewsTopic;
		my $TempDetailNoData = shift @NewsDetail;
		$OutputNewsRowSrc =~ s/__NEWS__MESSAGE__/$TempCatchData/;
		$OutputNewsRowSrc =~ s/__DETAIL__FILE__/$TempDetailNoData/g;
		$OutputNewsRow .= $OutputNewsRowSrc;
	}
	
	#ニュースのテーブルの整形
	open NEWSTABLEDATA, "$TemplateDirectory/$NewsTableTemplateFile";
	while (<NEWSTABLEDATA>){
		$OutputNewsTable .= $_;
	}
	close NEWSTABLEDATA;
	$OutputNewsTable =~ s/__TABLE__ELEMENT__/$OutputNewsRow/;
	
	
	#最終整形
	open FINAL, "$TemplateDirectory/$HtmlTemplateFile";
	while (<FINAL>){
		$OutputHtml .= $_;
	}
	close FINAL;
	

	$OutputHtml =~ s/__NEW__WORK__INFORMATION__/$OutputWorkTable/;
	$OutputHtml =~ s/__TOPPAGE__NEWS__/$OutputNewsTable/;
	
	#トップページ書き換え
	open OUTPUT, ">$TopPageDirectory/$OutputFileName";
	print OUTPUT ("$OutputHtml");
	close OUTPUT;
	
}

sub CheckJobKind{
	my $JobKindCheckStr = 'option value="' . "$JobKind" . '"';
	my $JobKindReplaceStr = "$JobKindCheckStr" . ' selected';
	$HtmlSrc =~ s/$JobKindCheckStr/$JobKindReplaceStr/;
}

sub CheckPlace{
	my $PlaceCheckStr = 'name="WorkPlace" value="' . "$WorkPlace" . '"';
	my $PlaceReplaceStr = "$PlaceCheckStr" . ' checked';
	$HtmlSrc =~ s/$PlaceCheckStr/$PlaceReplaceStr/;
}

sub CheckDayMode{
	my $ModeCheck = "";
	if($WorkDay == 1){$ModeCheck = "2";}
	elsif($WorkDay == 0){$ModeCheck = "3";}
	elsif($WorkDay == -1){$ModeCheck = "-1";}
	else{$ModeCheck = "1";}
	
	my $DayModeCheckStr = 'name="WorkDayMode" value="' . "$ModeCheck" . '"';
	my $DayModeReplaceStr = "$DayModeCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayModeCheckStr/$DayModeReplaceStr/;
	
	if($ModeCheck == 1){
		&CheckDay();
	}
}

sub CheckDay{
	my $DayCheckStr = "";
	my $DayReplaceStr = "";
	
	if(($WorkDay % 2) == 0){
	$DayCheckStr =  'name="MonDayChecker"';
	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}
	if(($WorkDay % 3) == 0){
	$DayCheckStr =  'name="TusDayChecker"';

	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}
	if(($WorkDay % 5) == 0){
	$DayCheckStr =  'name="WedDayChecker"';
	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}
	if(($WorkDay % 7) == 0){
	$DayCheckStr =  'name="TurDayChecker"';
	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}
	if(($WorkDay % 11) == 0){
	$DayCheckStr =  'name="FriDayChecker"';
	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}
	if(($WorkDay % 13) == 0){
	$DayCheckStr =  'name="SatDayChecker"';
	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}
	if(($WorkDay % 17) == 0){
	$DayCheckStr =  'name="SunDayChecker"';
	$DayReplaceStr = "$DayCheckStr" . ' checked';
	$HtmlSrc =~ s/$DayCheckStr/$DayReplaceStr/;
	}

}