<?php
error_reporting( 0 );

//$adminmail = 'info@issjp.com';
$adminmail = 'info@issjp.com';

$p = $_POST;

$list = array(
'toiawase','tel', 'company', 'company2', 'depart', 'gyoukai', 'name', 'name2', 'mail', 'zip21','zip22','pref21','addr21', 'strt21',  'checkbox1', 'checkbox1mail', 'checkbox2', 'checkbox2mail', 'checkbox3', 'checkbox3mail', 'checkbox4', 'checkbox4mail', 'checkbox5', 'checkbox5mail', 'checkbox6', 'checkbox6mail', 'kikkake', 'sonota', 
);
foreach ($list as $row) {
	$p[$row] = isset($p[$row]) ? $p[$row] : '';
}



$e = array();
$page = 'form.php';

if (isset($p['btn_confirm'])) {
	$e = validate($p);
	if (empty($e)) {
		$page = 'confirm.php';
	}
} else
if (isset($p['btn_send'])) {
	$e = validate($p);
	if (empty($e)) {
		send_mail($p);
		$page = 'finish.php';
	}
}

include($page);

exit();

/**
 * functions
 */

function validate(& $p) {

	$e = array();

	if (empty($p['name'])) {
		$e['name'] = 1;
	}

	if (empty($p['name2'])) {
		$e['name2'] = 1;
	}
	
	if (empty($p['tel'])) {
		$e['tel'] = 1;
	}
	
	if (!preg_match('/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$p['mail'])) {
		$e['mail'] = 1;
	}

	
	if (empty($p['company'])) {
		$e['company'] = 1;
	}
	
	if (empty($p['company2'])) {
		$e['company2'] = 1;
	}
	
	if (empty($p['toiawase'])) {
		$e['toiawase'] = 1;
	}

	if (empty($p['checkbox1']) && empty($p['checkbox2']) && empty($p['checkbox3']) && empty($p['checkbox4']) && empty($p['checkbox5']) && empty($p['checkbox6'])) {
		$e['checkbox'] = 1;
	}

	return $e;
}


function send_mail($p) {

	global $adminmail;

	$inputdate = date("Y/m/d/H:i:s", time());
//お申込者へメール送信

	$subject = "ISS：お問い合わせありがとうございます";
	$subject = mb_convert_encoding( $subject, 'JIS','UTF8');

	$mail_to = $p['mail'];

        $body = <<<_EOD_REPLY_MAIL_

{$p['company']}
{$p['name']}様


この度は、弊社にお問い合わせいただき誠にありがとうございます。
お問い合わせを受付いたしました。
このメールはシステムより自動的に送信されております。

お問い合わせ内容を確認の上、弊社担当よりご連絡をさせていただきます。

なるべく早めにご連絡させていただきますが、
万が一、数日過ぎても連絡がない場合には、
大変恐縮ですが下記の窓口までお問い合わせくださいますよう
お願い申し上げます。


お問い合わせ日時： {$inputdate} 

----------------------------------------------------------
株式会社アイ・エス・エス

〒102-0083 東京都千代田区麹町3-1-1 麹町311ビル9階

Email：info@issjp.com

URL：http://www.issjp.com

＊営業時間：土日祝日を除く9時～18時
----------------------------------------------------------


_EOD_REPLY_MAIL_;

//お申込者宛内容
                $addhead =
                        "From: {$adminmail} \n" .
                        "Reply-To: {$adminmail} \n" .
                        "Return-Path: <{$adminmail}> \n" .
                        "X-Mailer: PHP/" . phpversion();
                        "Content-Type: text/plain; charset=\"iso-2022-jp\"\n" .
                        "Content-Transfer-Encoding: 8bit;\n";

    		$subject_head = "";
    		$subject_head = "=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($subject,"ISO-2022-JP"))."?=";
				$cbody = mb_convert_encoding($body,'JIS', 'UTF8');
                mail( $mail_to, $subject_head, $cbody, $addhead );


//	管理者へメール送信
		$subject = "お問い合わせフォーム【受付】";
		$subject = mb_convert_encoding( $subject, 'JIS', 'UTF8');

//		$mail_to = "テストアドレス@yahoo.co.jp";
		$mail_to = "{$p['checkbox1mail']},{$p['checkbox2mail']},{$p['checkbox3mail']},{$p['checkbox4mail']},{$p['checkbox5mail']},{$p['checkbox6mail']},{$adminmail}";
        $body = <<<_EOD_REPLY_MAIL_

WEBでのお問い合わせフォームにて受付完了しました。
下記にて内容をご確認ください。

---------------------------------------------
＊なるべく早めにお客様にご連絡ください。
＊「問い合わせサービス」が複数部門の場合には
　該当する部門間で必ず調整してください。
---------------------------------------------

お申し込み日時： {$inputdate}

【お問い合わせサービス】
{$p['checkbox1']},{$p['checkbox2']},{$p['checkbox3']},{$p['checkbox4']},{$p['checkbox5']},{$p['checkbox6']}

【お問い合わせ内容】
{$p['toiawase']}

【会社(団体)名】　　{$p['company']}
【会社名(フリガナ)】{$p['company2']}

【部署名】　　　　　{$p['depart']}
【業界】　　　　　　{$p['gyoukai']}
【お名前】　　　　　{$p['name']}
【お名前(フリガナ)】{$p['name2']}
【電話番号】　　　　{$p['tel']}
【e-mail】　　　　　{$p['mail']}
【ご住所】　　　　　〒{$p['zip21']}-{$p['zip22']} {$p['pref21']}{$p['addr21']}{$p['str21']}
【当社をどのようにお知りになりましたか】
                    {$p['kikkake']} {$p['sonota']}


このメールに返信はできません。


以上、ご対応をよろしくお願いいたします。


_EOD_REPLY_MAIL_;

	//管理者様宛内容
	$addhead =
                        "From: {$adminmail} \n" .
                        "Reply-To: {$adminmail} \n" .
                        "Return-Path: <{$adminmail}> \n" .
                        "X-Mailer: PHP/" . phpversion();
                        "Content-Type: text/plain; charset=\"iso-2022-jp\"\n" .
                        "Content-Transfer-Encoding: 8bit;\n";


    	$subject_head = "";
    	$subject_head = "=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($subject,"ISO-2022-JP"))."?=";
	$cbody = mb_convert_encoding($body,'JIS', 'UTF8');
	mail( $mail_to, $subject_head, $cbody, $addhead );

	return ;
}

function hs($str) {
	return htmlspecialchars($str, ENT_QUOTES);
}

