<?php

//カスタムヘッダー
add_theme_support(
'custom-header',
array(
	'width' =>1200,
	'height' =>300,
	'header_text' =>false,
	'default-image' => '%s/images/top/main_image.png',
	)
	);
	
	//カスタムメニュー
	register_nav_menus(
		array(
			'place_global' => 'グローバル',
			'place_utility' => 'ユーティリティ',
			)
		);
//headerにmagin-topが入った場合、これをなくすwp_head()
//add_filter( 'show_admin_bar', '__return_false' );

//ページが存在しない場合の処理（例外を除きサイトトップを表示する）
function is404_redirect_home() {
	if( is_404() ){
		$url = $_SERVER['REQUEST_URI'];
		//旧サイトのhttp://www.issjp.com/company...の場合は新ページの会社概要に遷移させる。
		if(strstr($url,'/company/')==true){
			wp_safe_redirect( home_url( '/about/company' ) );
		} else {
			wp_safe_redirect( home_url( '/' ) );
		}		
		exit();
	}
}
add_action( 'template_redirect', 'is404_redirect_home' );

function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}
		
//画像パスの置換（固定ページ用）
function imgpass_short($arg) {
	$content = str_replace('"images/', '"' . get_stylesheet_directory_uri() . '/images/',$arg);
	return $content;
}
add_action('the_content','imgpass_short');

//aリンクの置換（固定ページ用）
function linkpass_short($arg) {
	$content = str_replace('"link/', '"' . home_url() . '/',$arg);
	return $content;
}
add_action('the_content','linkpass_short');

function modify_read_more_link() {
    echo '<a class="more-link" href="' . get_permalink() . '">More＞</a>';
}
//add_filter( 'the_content', 'modify_read_more_link' );

// タイトルのセパレータ
function change_separator() {
  return "|"; // ここに変更したい区切り文字を書く
}
add_filter('document_title_separator', 'change_separator');

//ショートコードを使ったphpファイルの呼び出し方法
function my_php_Include($params = array()) {
 extract(shortcode_atts(array('file' => 'default'), $params));
 ob_start();
 include(STYLESHEETPATH . "/$file.php");
 return ob_get_clean();
}
add_shortcode('myphp', 'my_php_Include');

/*
 * 親テーマのfunctions.phpを書き換える
 */
function mytheme_setup() {
     
    /* ここにfunctionを記述すると親テーマのfunctionsの後に実行される */
 
    /*********************************************
    *   トップページの抜粋表示のあとに、
    *   自動的に「続きを読む」が入るのを防ぐ
    **********************************************/
    function new_excerpt_more($post) {
        return '...';  
    }   
    add_filter('excerpt_more', 'new_excerpt_more');

}
add_action( 'after_setup_theme', 'mytheme_setup', 20 );

//親テーマがtwentyfifteenの時に不要css削除
function set_up_origin_theme() {
	function deregister_parent_css() {
		// Add custom fonts, used in the main stylesheet.
		wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );
	
		// Load our main stylesheet.
		wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );
	
		// Load the Internet Explorer specific stylesheet.
		/*wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
		wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );*/
	
		// Load the Internet Explorer 7 specific stylesheet.
		/*wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
		wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );*/
		
		wp_dequeue_style( 'twentyfifteen-fonts' );
		wp_dequeue_style( 'twentyfifteen-style' );
		/*wp_dequeue_style( 'twentyfifteen-ie' );
		wp_dequeue_style( 'twentyfifteen-ie7' );*/
	}
	remove_action('wp_enqueue_scripts', 'twentyfifteen_scripts');
	add_action('wp_enqueue_scripts', 'deregister_parent_css');
}
add_action('after_setup_theme', 'set_up_origin_theme');

//pタグ削除

add_action('init', function() {
	remove_filter('the_excerpt', 'wpautop');
	remove_filter('the_content', 'wpautop');
});
 
add_filter('tiny_mce_before_init', function($init) {
	$init['wpautop'] = false;
	$init['apply_source_formatting'] = false;
	return $init;
});

// パンくずリスト
function breadcrumb(){
    global $post;
    $str ='';
    if(!is_home()&&!is_admin()){
        $str.= '<div id="breadcrumb" class="pankuzu"><div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
        $str.= '<a href="'. home_url() .'" itemprop="url"><span itemprop="title">TOP</span></a> &gt;</div>';
 
        if(is_category()) {
            $cat = get_queried_object();
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($ancestor) .'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor) .'</span></a> &gt;</div>';
                }
            }
        $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($cat -> term_id). '" itemprop="url"><span itemprop="title">'. $cat-> cat_name . '</span></a> &gt;</div>';
        } elseif(is_page(123)){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            /*if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_archives_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor)"新着情報". '</span></a> &gt;</div>';
                }
            }*/
            $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a><span itemprop="title">'. /*$cat-> cat_name*/"新着情報" . '</span></a></div>';
        } elseif(is_page()){
            if($post -> post_parent != 0 ){
                $ancestors = array_reverse(get_post_ancestors( $post->ID ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_permalink($ancestor).'" itemprop="url"><span itemprop="title">'. get_the_title($ancestor) .'</span></a> &gt;</div>';
                }
            }
        } elseif(is_single() || is_page(123)){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            /*if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_archives_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor)"新着情報". '</span></a> &gt;</div>';
                }
            }*/
            $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. /*get_category_link($cat -> term_id)*/get_page_link( 123 ). '" itemprop="url"><span itemprop="title">'. /*$cat-> cat_name*/"新着情報" . '</span></a> &gt;</div>';
        } else{
            $str.='<div>'. wp_title('', false) .'</div>';
        }
        $str.='</div>';
    }
    echo $str;


//バージョンアップ通知を非表示
add_filter('pre_site_transient_update_core', '__return_zero'); //WP本体
add_filter('site_option__site_transient_update_plugins', '__return_zero'); //プラグイン
remove_action ('wp_version_check','wp_version_check');
remove_action ('admin_init','_maybe_update_core');

//管理バーから更新を非表示
//function remove_bar_menus( $wp_admin_bar ) {
//    $wp_admin_bar->remove_menu( 'updates' ); // 更新
//}
//add_action('admin_bar_menu', 'remove_bar_menus', 201);
//

}

