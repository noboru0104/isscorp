#!/usr/bin/perl

#各種設定開始

#ディレクトリ情報設定ここから
my $StaffHtmlRoot = "../../home/staff_only";

my $StaffCgiRoot = "../../cgi-bin";
my $TemplateDir = "../template/staff_only";
my $DataFileDir = "../secret/staff_only";
#ディレクトリ情報設定ここまで

#ライブラリ設定開始
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
#ライブラリ設定終了

#テンプレートファイルリスト
my $HtmlBody = "msg.html";

#データファイル
my $DataFile = "msg.csv";

#出力文字列格納用変数
my $HtmlSrc = "";

#詳細情報格納用変数
my $Date = "";
my $Title = "";
my $Statement = "";

#参照対象情報取得
my $TargetInfo = $ENV{'QUERY_STRING'};

#各種設定終了

print("Content-type:text/html\n\n");


#テンプレート読み込み
$HtmlSrc = &HtmlSrcLoad($HtmlBody);

#対象情報取得
&GetMsgData($TargetInfo);

#テンプレート内に情報埋め込み
$HtmlSrc =~ s/__DATE__/$Date/;
$HtmlSrc =~ s/__TITLE__/$Title/;
$HtmlSrc =~ s/__STATEMENT__/$Statement/;

#Html出力
print("$HtmlSrc");

#以下サブルーチン
sub HtmlSrcLoad{
	my @UsingFile = @_;
	my $TempSrc = "";
	open BODY, "$TemplateDir/$UsingFile[0]" or die "Error!";
	while(<BODY>){
		$TempSrc .= $_;
	}
	close BODY;
	return $TempSrc;
}

sub GetMsgData{
	my @TargetNumber = @_;
	open GETDATA, "$DataFileDir/$DataFile";
	label1:while(<GETDATA>){
		my @TempMsgData = split /,/,$_;
		if($TempMsgData[3] == $TargetNumber[0]){
			$Date = $TempMsgData[0];
			$Title = $TempMsgData[1];
			$Statement = $TempMsgData[2];
			last label1;
		}
	}
	close GETDATA;
	return 0;

}