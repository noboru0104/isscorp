------------------------------------------------------------------
この度はISSへのお問い合わせありがとうございます。
このメールは自動配信です。
------------------------------------------------------------------

後日、担当者からご連絡差し上げますので、今しばらくお待ち下さい。

------------------------------------------------------------------
このメールにお心当たりのないお客様は、大変お手数ですが
【お問い合わせ先】まで　ご連絡頂きますようお願い申し上げます。
------------------------------------------------------------------
【お問い合わせ先】
株式会社アイ・エス・エス・サービスセンター
フリーダイヤル：0120-598-155
haken@isssc.co.jp
http://www.isssc.co.jp
