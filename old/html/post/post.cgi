#!/usr/bin/perl
############################################################
#
#    ISS 更新管理ツール　登録フォーム
#
############################################################
use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use File::Copy;
require './jcode.pl';
require './init.pl';
require './post.pl';
umask 0070;

############################################################
#    プログラム開始
############################################################
&formDecode();
&setTime();
&lock();

if($g_html{'f_conf'}){
	&checkForm();
	&makeConfirmHtml();
	&outHtml($g_confirmHtmlFile);
}elsif($g_html{'f_exe'}){
#	&checkForm();
	&makeCsv();
	if($g_html{'detail'} eq '2'){
		&makeHtml();
	}
	&sendMailAdmin();
	&outHtml($g_endHtmlFile);
}else{
	$g_html{'n_year_select'} = &setYear($g_time{'year'});
	$g_html{'n_month_select'} = &setMonth($g_time{'mon'});
	$g_html{'n_day_select'} = &setDay($g_time{'day'});
	$g_html{'p_year_select'} = &setYear($g_time{'year'});
	$g_html{'p_month_select'} = &setMonth($g_time{'mon'});
	$g_html{'p_day_select'} = &setDay($g_time{'day'});
	&outHtml($g_formHtmlFile);
}