<?php
/*
Template Name: list
*/
?>
<?php get_header(); ?>
<div id="interpretation" class="l-wrapper">
    <div class="l-pankuzu">
        <div class="l-inner">
            <p><a href="<?php echo home_url(); ?>/">ホーム</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<a href="<?php echo home_url(); ?>/kokusai/">会議・イベント運営（ウェブ・ハイブリッド）</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;通訳・運営実績</p>
        </div>
    </div>

    <section>
        <div class="l-mainVisual">
            <div class="l-mainVisual-inner">
                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri(); ?>/images/kokusai/img_mv_pc.png" alt="会議・イベント運営（ウェブ・ハイブリッド）">
            </div>
        </div>
    </section>

    <div class="l-container">
        <div class="l-main">
            <div class="h2_block">
                <h2>
                    <p>通訳・運営実績</p>
                </h2>
            </div>

            <div class="l-planBlock">
                <p>通訳の技術的ノウハウのみならず、通訳者の得意分野や人柄も含めて信頼関係を築いてきたからこそ実現できる運営体制があります。リアル開催はもちろん、最適なウェブ会議システムを活用したハイブリッド開催でも、息の合ったチームワークが自慢です。</p>
                <!--入力されたデータを取得し、年の種類を表示する-->
                <?php
                $getfields0 = get_field('list');
                $getyears0 = array();
                foreach (array_reverse($getfields0) as $row0) {
                    array_push($getyears0, $row0['year']);
                }
                $length0 = count($getyears0);
                ?>
                <?php
                //$count = 0;
                $years0 = array();
                for ($i0 = 0; $i0 < $length0; $i0++) {
                    if ($i0 != 0) {
                        if ($getyears0[$i0 - 1] == $getyears0[$i0]) {
                        } else {
                            if ($getyears0[$i0] > intval(date('Y'))) {
                            } else {
                                array_push($years0, $getyears0[$i0]);
                            }
                        }
                    } else {
                        if ($getyears0[$i0] > intval(date('Y'))) {
                        } else {
                            array_push($years0, $getyears0[$i0]);
                        }
                    }
                }
                ?>
                <?php
                foreach ($years0 as $rowyear0) {
                ?>
                    <!-- <p>
                        <span class="p-brackets">【<?php echo $rowyear0; ?>年<?php if (intval($rowyear0) >= intval(date('Y'))) {
                                                                                echo '以降予定';
                                                                            } ?>】</span><br>
                        <a href="#<?php echo $rowyear0; ?>"><span class="p-arrow">▲</span><span class="p-brackets"><?php if (intval($rowyear0) >= intval(date('Y'))) {
                                                                                                                        echo '【開催予定】';
                                                                                                                    } else {
                                                                                                                        echo '【実績】';
                                                                                                                    } ?></span>
                        </a>
                    </p> -->
                <?php
                }
                ?>
                <!-- <p>
                	<span class="p-brackets">【2016年】</span><br>
                    <a href="#2016"><span class="p-arrow">▲</span><span class="p-brackets">【実績】</span></a>
                </p>
                <p>
                	<span class="p-brackets">【2015年】</span><br>
                    <a href="#2015"><span class="p-arrow">▲</span><span class="p-brackets">【実績】</span></a>
                </p> -->
                <p>
                    <a href="#2024"><span class="p-arrow">▲</span><span class="p-brackets">【2024年】</span></a>
                    <a href="#2023"><span class="p-arrow">▲</span><span class="p-brackets">【2023年】</span></a>
                    <a href="#2022"><span class="p-arrow">▲</span><span class="p-brackets">【2022年】</span></a>
                    <a href="#2021"><span class="p-arrow">▲</span><span class="p-brackets">【2021年】</span></a>
                </p>
                <p>
                    <a href="#2020"><span class="p-arrow">▲</span><span class="p-brackets">【2020年】</span></a>
                    <a href="#2019"><span class="p-arrow">▲</span><span class="p-brackets">【2019年】</span></a>
                    <a href="#2018"><span class="p-arrow">▲</span><span class="p-brackets">【2018年】</span></a>
                    <a href="#2017"><span class="p-arrow">▲</span><span class="p-brackets">【2017年】</span></a>
                    <a href="#2016"><span class="p-arrow">▲</span><span class="p-brackets">【2016年】</span></a>
                    <a href="#2015"><span class="p-arrow">▲</span><span class="p-brackets">【2015年】</span></a>
                </p>
                <p>
                    <!-- <span class="p-brackets">【2014年以前実績】</span><br> -->
                    <a href="#2014"><span class="p-arrow">▲</span><span class="p-brackets">【2014年】</span></a>
                    <a href="#2013"><span class="p-arrow">▲</span><span class="p-brackets">【2013年】</span></a>
                    <a href="#2012"><span class="p-arrow">▲</span><span class="p-brackets">【2012年】</span></a>
                    <a href="#2011"><span class="p-arrow">▲</span><span class="p-brackets">【2011年】</span></a>
                    <a href="#2010"><span class="p-arrow">▲</span><span class="p-brackets">【2010年～2006年】</span></a>
                </p>


                <p id="2024" class="p-year">
                    <span class="p-brackets">【2024年】</span>
                </p>
                <table class="table02">
                    <tr>
                        <th class="col02">お客様</th>
                        <th class="col03">会議・プロジェクト名</th>
                    </tr>
                    <tr>
                        <td>PCO・MICE</td>
                        <td>アニュアルイベント 2024同時通訳・通訳機材手配一式</td>
                    </tr>
                    <tr>
                        <td>自動車部品</td>
                        <td>同社国際会議（ハイブリッド開催）同時通訳、通訳ブース等機材⼿配⼀式</td>
                    </tr>
                    <tr>
                        <td>一般社団法人</td>
                        <td>経済団体様セミナー同時通訳、通訳機材手配一式</td>
                    </tr>
                    <tr>
                        <td>製薬</td>
                        <td>同社コミュニケーションシステム研修</td>
                    </tr>
                </table>

                <p id="2023" class="p-year">
                    <span class="p-brackets">【2023年】</span>
                </p>
                <table class="table02">
                    <tr>
                        <th class="col02">お客様</th>
                        <th class="col03">会議・プロジェクト名</th>
                    </tr>
                    <tr>
                        <td>監査法人系コンサル</td>
                        <td>学生アイデアソンの通訳手配</td>
                    </tr>
                    <tr>
                        <td>広告・PR</td>
                        <td>化学会社様の配信イベントでの通訳業務</td>
                    </tr>
                    <tr>
                        <td>医療機器</td>
                        <td>外国人講師による社員研修時の通訳業務</td>
                    </tr>
                    <tr>
                        <td>製薬</td>
                        <td>キックオフミーティングにかかる通訳</td>
                    </tr>
                    <tr>
                        <td>製薬</td>
                        <td>タウンホールミーティングにかかる通訳者と同時通訳機材手配</td>
                    </tr>
                    <tr>
                        <td>ゲーム</td>
                        <td>ゲーム業界向けの法律セミナーのイベントの通訳と機材手配</td>
                    </tr>
                    <tr>
                        <td>ゲーム</td>
                        <td>ITグローバル会議の通訳者手配と同時機材手配</td>
                    </tr>
                    <tr>
                        <td>商社</td>
                        <td>同社技術フォーラム2023における通訳業務</td>
                    </tr>
                    <tr>
                        <td>機械</td>
                        <td>社員総会での通訳業務</td>
                    </tr>
                    <tr>
                        <td>自動車部品</td>
                        <td>グローバル会議にかかる通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>自動車部品</td>
                        <td>グローバル自動車運営会議</td>
                    </tr>
                    <tr>
                        <td>電機</td>
                        <td>Product Lifecycle Managementワークショップ</td>
                    </tr>
                    <tr>
                        <td>電機</td>
                        <td>グローバルQMSストラテジーワークショップでの通訳業務</td>
                    </tr>
                    <tr>
                        <td>電機</td>
                        <td>トレーニング・セッションでの通訳業務</td>
                    </tr>
                    <tr>
                        <td>電機</td>
                        <td>海外子会社との報告会・討論会時の通訳業務</td>
                    </tr>
                    <tr>
                        <td>ラグジュアリー</td>
                        <td>エグゼクティブ来日イベントにかかる通訳</td>
                    </tr>
                    <tr>
                        <td>精密機器</td>
                        <td>カメラ・プロ市場の長期戦略に関する議論での同時通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>監査法人系コンサル</td>
                        <td>3社間でのワークショップ</td>
                    </tr>
                    <tr>
                        <td>監査法人系コンサル</td>
                        <td>ワークショップにかかる通訳</td>
                    </tr>
                    <tr>
                        <td>監査法人系コンサル</td>
                        <td>IT系ワークショップにかかる通訳業務</td>
                    </tr>
                    <tr>
                        <td>監査法人系コンサル</td>
                        <td>アジア企業との商談会（オンライン）での通訳業務</td>
                    </tr>
                    <tr>
                        <td>大手コンサル</td>
                        <td>適性検査員養成の研修にかかる英日通訳業務</td>
                    </tr>
                    <tr>
                        <td>印刷・出版</td>
                        <td>細胞及び遺伝子医薬討論会での通訳業務</td>
                    </tr>
                    <tr>
                        <td>印刷・出版</td>
                        <td>某学会様講演会での通訳業務</td>
                    </tr>
                    <tr>
                        <td>教育・学習支援</td>
                        <td>ベトナム語専門講義（介護・看護、社会文化等）での通訳業務</td>
                    </tr>
                    <tr>
                        <td>一般社団法人</td>
                        <td>法廷計量研修にかかる通訳業務</td>
                    </tr>
                    <tr>
                        <td>公益財団法人</td>
                        <td>ワークショップ基調講演</td>
                    </tr>
                    <tr>
                        <td>中央省庁・行政機関</td>
                        <td>作業部会等における同時通訳（日英・日中・日韓）及びオンライン会議への同時通訳音声配信等業務一式</td>
                    </tr>
                    <tr>
                        <td>IT・通信</td>
                        <td>アニュアルイベントでの同時通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>PCO・MICE</td>
                        <td>ゲームフェスティバルでの同時通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>PCO・MICE</td>
                        <td>自然エネルギー会議にかかる通訳業務</td>
                    </tr>
                    <tr>
                        <td>PCO・MICE</td>
                        <td>映画コンテンツマーケットについてのセミナー同時通訳</td>
                    </tr>
                    <tr>
                        <td>広告・PR</td>
                        <td>ASEAN関連会合での同時通訳業務</td>
                    </tr>
                    <tr>
                        <td>広告・PR</td>
                        <td>ASEAN関連フォーラムの同時通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>イベント会社</td>
                        <td>旅行会社様世界大会同時通訳機材</td>
                    </tr>
                    <tr>
                        <td>TV・映像</td>
                        <td>国立大学主催の学術会議での同時通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>一般社団法人</td>
                        <td>某商工会議所様主催セミナー通訳業務</td>
                    </tr>
                    <tr>
                        <td>一般社団法人</td>
                        <td>テレビ規格関連国際セミナーにおける通訳業務</td>
                    </tr>
                    <tr>
                        <td>公益財団法人</td>
                        <td>北米事業者とのサイバーセキュリティに関するディスカッションでの通訳業務</td>
                    </tr>
                    <tr>
                        <td>公益財団法人</td>
                        <td>アートイベント基調講演実施にかかる同時通訳手配</td>
                    </tr>
                    <tr>
                        <td>国際機関</td>
                        <td>Climate Summit</td>
                    </tr>
                </table>

                <p id="2022" class="p-year">
                    <span class="p-brackets">【2022年】</span>
                </p>
                <table class="table02">
                    <tr>
                        <th class="col02">お客様</th>
                        <th class="col03">会議・プロジェクト名</th>
                    </tr>
                    <tr>
                        <td>製薬</td>
                        <td>イスタンブールでの会議における日英逐次通訳業務</td>
                    </tr>
                    <tr>
                        <td>機械</td>
                        <td>社員総会での通訳業務</td>
                    </tr>
                    <tr>
                        <td>自動車部品</td>
                        <td>同社グループグローバル会議に関わる通訳業務</td>
                    </tr>
                    <tr>
                        <td>自動車部品</td>
                        <td>グローバルリーダー研修にかかる西日通訳業務</td>
                    </tr>
                    <tr>
                        <td>電機</td>
                        <td>エグゼクティブプレビューでの同時通訳機材費用</td>
                    </tr>
                    <tr>
                        <td>電機</td>
                        <td>IT関連会議での通訳業務</td>
                    </tr>
                    <tr>
                        <td>製薬</td>
                        <td>キックオフミーティングに係る英日通訳業務・同時通訳機材手配</td>
                    </tr>
                    <tr>
                        <td>独立行政法人</td>
                        <td>外国政府関係機関のための金融に係る研修の運営を伴う補助事務</td>
                    </tr>
                    <tr>
                        <td>独立行政法人</td>
                        <td>IAEA主催トレーニングワークショップ</td>
                    </tr>
                    <tr>
                        <td>生命保険</td>
                        <td>Japan Meeting―来日に伴うワークショップ通訳業務</td>
                    </tr>
                    <tr>
                        <td>損害保険</td>
                        <td>同社社内ワークショップ</td>
                    </tr>
                    <tr>
                        <td>自動車</td>
                        <td>某社・同業他社・コンサル会社間での調達に関する会議の英日同時通訳業務</td>
                    </tr>
                    <tr>
                        <td>PCO・MICE</td>
                        <td>自然エネルギー会議に係る通訳業務</td>
                    </tr>
                    <tr>
                        <td>通訳・翻訳</td>
                        <td>WCOフェローシップ・プログラム（スペイン語）に係る翻訳及び逐次通訳業務</td>
                    </tr>
                    <tr>
                        <td>TV・映像</td>
                        <td>国立大学主催の学術会議での同時通訳業務一式</td>
                    </tr>
                    <tr>
                        <td>一般社団法人</td>
                        <td>テレビ規格関連国際セミナーにおける通訳業務</td>
                    </tr>
                    <tr>
                        <td>一般社団法人</td>
                        <td>国際協力関連会議</td>
                    </tr>
                    <tr>
                        <td>公益財団法人</td>
                        <td>芸術系国際カンファレンスの通訳業務および同通機材手配</td>
                    </tr>
                    <tr>
                        <td>PCO・MICE</td>
                        <td>自然エネルギー会議に係る通訳業務</td>
                    </tr>
                    <tr>
                        <td>証券</td>
                        <td>銀行様IRロードショー通訳業務</td>
                    </tr>
                    <tr>
                        <td>証券</td>
                        <td>プロパティカンファレンス電話会議通訳業務</td>
                    </tr>
                </table>

                <!--入力されたデータを取得し、年の種類毎にテーブルを分けて表示する-->
                <?php
                $getfields = get_field('list');
                $getyears = array();
                foreach (array_reverse($getfields) as $row) {
                    array_push($getyears, $row['year']);
                }
                $length = count($getyears);
                //var_dump($length);
                ?>
                <?php
                //$count = 0;
                $years = array();
                for ($i = 0; $i < $length; $i++) {
                    if ($i != 0) {
                        if ($getyears[$i - 1] == $getyears[$i]) {
                        } else {
                            array_push($years, $getyears[$i]);
                        }
                    } else {
                        array_push($years, $getyears[$i]);
                    }
                }
                ?>
                <?php
                foreach ($years as $rowyear) {
                    //未来の場合表示しない（今年Tableにまとめて表示する）
                    if (intval($rowyear) > intval(date('Y'))) {
                    }
                    //今年の場合（未来分の内容もここににまとめて表示する）
                    elseif (intval($rowyear) === intval(date('Y'))) {

                ?>
                        <p id="<?php echo $rowyear; ?>" class="p-year">
                            <span class="p-brackets">【<?php echo $rowyear; ?>年以降予定】</span>
                        </p>
                        <table class="table01">
                            <tr>
                                <!-- <th class="col01">開催年</th> -->
                                <th class="col02">会議・プロジェクト名</th>
                                <th class="col03">開催都市</th>
                                <th class="col04">備考</th>
                            </tr>

                            <?php
                            $fields = get_field('list');      
                            $year = $rowyear;
                            foreach (array_reverse($fields) as $field) {
                                //foreach ($fields as $field) {
                                $values = $field['year'];
                                //if ($values == $year){
                                if ($values >= intval(date('Y'))) {
                            ?>
                                    <tr>
                                        <!-- <td><?php echo $field['year']; ?></td> -->
                                        <td><?php echo $field['name']; ?></td>
                                        <td><?php echo $field['city']; ?></td>
                                        <td><?php echo $field['bikou']; ?></td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </table>
                    <?php
                        //今年より過去の場合
                    } else {
                    ?>
                        <p id="<?php echo $rowyear; ?>" class="p-year">
                            <span class="p-brackets">【<?php echo $rowyear; ?>年】</span>
                        </p>
                        <table class="table01">
                            <tr>
                                <!-- <th class="col01">開催年</th> -->
                                <th class="col02">会議・プロジェクト名</th>
                                <th class="col03">開催都市</th>
                                <?php if (intval($rowyear) > 2019) : ?>
                                    <th class="col04">備考</th>
                                <?php endif; ?>
                            </tr>

                            <?php
                            $fields = get_field('list');       
                            $year = $rowyear;
                            foreach (array_reverse($fields) as $field) {
                                //foreach ($fields as $field) {
                                $values = $field['year'];
                                if ($values == $year) {
                            ?>
                                    <tr>
                                        <!-- <td><?php echo $field['year']; ?></td> -->
                                        <td><?php echo $field['name']; ?></td>
                                        <td><?php echo $field['city']; ?></td>
                                        <?php if (intval($rowyear) > 2019) : ?>
                                            <td><?php echo $field['bikou']; ?></td>
                                        <?php endif; ?>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </table>
                <?php
                    }
                }
                ?>


                <p id="2016" class="p-year">
                    <span class="p-brackets">【2016年】</span>
                </p>
                <table class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>メガバンク　イノベーション・フィールド2016</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社 木材活用フォーラム 2016</td>
                        <td>東京、大阪</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第32回国際生物学賞記念シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手システム系ファーム　全国フォーラム</td>
                        <td>大阪、東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>Brake Forum in Japan 2016</td>
                        <td>宇都宮</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>医療系メーカー 新製品発表記念セミナー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>カーボンナノチューブ発見25周記念シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>日本臨床眼科学会総会サテライトシンポジウム</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>5th World Centenarian Initiative International Symposium on Stroke</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社　金融ITイノベーションフォーラム2016</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>日本カナダワークショップ</td>
                        <td>ナイアガラ</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>医療系メーカー 第6回 Endoscopic Vitrectomy Seminar</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>経済産業省主催セミナー _平成28年度我が国におけるデータ駆動型社会に係る基盤整備</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>ICD-11 Revision Conference in Tokyo, Japan</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>WHO-FIC Network Annnual Meeting 2016 Tokyo</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第42回日本診療情報管理学会学術大会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>The 18th IFHIMA International Congress Tokyo 2016<br>診療情報管理協会国際連盟第18回国際大会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第40回国際外科学会世界総会(ICS2016)<br>40th Biennial World Congress of the International College of Surgeons</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>The 10th International Trends in Welding Research Conference</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社 BtoBセールス＆マーケティングSummit 2016 Autumn</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社 データ活用実践フォーラム 2016</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第28回汎太平洋不動産鑑定士・カウンセラー会議（PPC)</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>2016年度統計関連学会連合大会</td>
                        <td>金沢</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>医薬品開発のためのPPKPD研究会2016</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>FIB phDシンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>課題設定による先導的人文学・社会科学研究推進事業成果公開シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第45回国際学校図書館協会年次大会<br>2016 IASL Tokyo</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第13回日本中性子捕捉療法学会学術大会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>メガバンク　会員セミナー&amp;懇親会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社 IT Japan 2016</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第99回ライオンズクラブ世界大会</td>
                        <td>福岡</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>17th International Symposium on Olfaction and Taste (ISOT2016) <br>第17回国際味と匂い学会</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>オバマ米国大統領広島訪問接遇業務</td>
                        <td>広島</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>伊勢志摩サミット・NGOセンター運営業務</td>
                        <td>伊勢志摩</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>L2-tech 国際シンポジウム　ドイツと日本における低炭素技術</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社 アーキテクチュア40周年記念セミナー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第19回日本臨床救急医学会総会・学術集会</td>
                        <td>福島</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大手出版社 ITpro Activeセミナー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>日本形成外科学会イブニングセミナー</td>
                        <td>福岡</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>JRC2016 CTコロノグラフィートレーニングコース</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第1回福島第一廃炉国際フォーラム</td>
                        <td>いわき</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>IMF_The Seventh IMF-Japan High-Level Tax Conference</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>G7伊勢志摩サミット準備会合<br> (第3回および第4回シエルパ会合、第3回サブシェルパ会合）</td>
                        <td>非公開</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>サモア独立国日本名誉領事館　開設記念式典</td>
                        <td>いわき</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>一橋大学 Global Network Week</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>第89回日本薬理学会年会</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>RDA総会およびデーターシェアリングシンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>資源効率のためのG7アライアンスワークショップ</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>平成27年度日本医師会医療情報システム協議会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>平成27年度情報・システム研究機構シンポジウム　～オープンサイエンスにおける研究データのオープン化～</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>外資系医療メーカー　新商品発売記念イベント</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>外資系製薬会社　2016年度 Kick-off Meeting</td>
                        <td>神奈川</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>グローバル・アピール2016 ～ハンセン病シンポジウム～</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2016</td> -->
                        <td>大学のイノベーション形成システム確立推進フォーラム</td>
                        <td>東京</td>
                    </tr>
                </table>

                <p id="2015" class="p-year">
                    <span class="p-brackets">【2015年】</span>
                </p>
                <table class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第36回日本臨床薬理学会学術総会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第12回動力エネルギー国際会議（ICOPE 2015）</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第12回物理探査に関する国際シンポジウム(The 12th SEGJ International Symposium)</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>IWA LESAM 2015in Yokohama</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>International Cooperation on Harmonisation of Technical Requirements for Registration of Veterinary Medicinal Products （VICH5）</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>Microgen IV - The 4th International Conference on Microgeneration and Related Technologies -</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第3回東アジア環境史学会 (EAEH2015)</td>
                        <td>高松</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第4回アジア・スマートシティ会議</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>日米ワークショップ</td>
                        <td>アルバカーキ</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>アジア・オセアニアタックスコンサルタント協会（AOTCA）大阪大会2015</td>
                        <td>大阪</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>Asia Steel 2015</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>大手新聞社 地と学びのサミット</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第26回アジア文化賞授賞式</td>
                        <td>福岡</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>2015年度統計関連学会連合大会</td>
                        <td>岡山</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>医薬品開発のためのPPKPD研究会2015</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>メガバンク　会員セミナー&amp;懇親会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>大手医薬系コンサルティングファーム　Research Committee Meeting</td>
                        <td>成田</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第13回微量元素の生物地球化学に関する国際会議 (ICOBTE2015)</td>
                        <td>福岡</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>アルツハイマー病予防戦略国際シンポジウム -新たな地平を目指して-</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第2回ライブ＆イベント 産業展</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>MUOGRAPHERS 2015 -Muon, Optics, Geoneutrino, Radar, and Photonics for Earth Studies-</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第2回パシフィック・ビジョン21東京会合：新たな日米パートナーシップの構築に向けて</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>Bridging Biomedical Worlds (BBW) Japan 2015</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第25回ファインテック ジャパン ～フラットパネル ディスプレイ 技術展～</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>日本・太平洋諸島経済フォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第7回太平洋・島サミット<br>(The 7th Pacific Islands Leaders Meeting : PALM7)</td>
                        <td>いわき</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>一橋大学 GNAM Dean's and Director's Meeting</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>特許庁知財シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>JRS2015 CTコロノグラフィートレーニングコース</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>The Sixth IMF-Japan High-Level Tax Conference</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>Ras Al Khaimah Free Trade Zone Authority Seminar in Japan</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第1回 京都大学-ＵＣサンディエゴ 共同シンポジウム</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>一橋大学 Global Network Symposium</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第5回スポーツメディスンフォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第26回日本心エコー図学会学術集会</td>
                        <td>北九州</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>アジア経済研究所国際シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>DITTAワークショップⅠおよびⅡ</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>医療機器規制当局会議(IMDFR)第7回会合</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>COINS International Symposium</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>大手自動車メーカー ブランティングサミット</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>日本・カタール経済フォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>平成26年度日本医師会医療情報システム協議会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>「環境未来都市」構想推進国際フォーラムinマレーシア</td>
                        <td>マレーシア</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>第5回日韓知事会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>グローバルアピール2015・ハンセン病シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>外資系製薬会社　2015年度 Kick-off Meeting</td>
                        <td>鎌倉</td>
                    </tr>
                    <tr>
                        <!-- <td>2015</td> -->
                        <td>JCMプロジェクト中央大学国際ワークショップ</td>
                        <td>東京</td>
                    </tr>
                </table>

                <p id="2014" class="p-year">
                    <span class="p-brackets">【2014】</span>
                </p>
                <table class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第18回日本統合医療学会・第17回日本アロマセラピー学会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>原子力発電プラントの、伝熱流動、運転、安全関係の国際会議</td>
                        <td>沖縄</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>革新的先進複合材料活用国際フォーラム2014</td>
                        <td>高松</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第19回日本・韓国・台湾民間社会福祉代表者会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第3回日本プライマリ・ケア連合学会 関東甲信越ブロック地方会</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>The 3rd Annual Symposium for BEST Aliance</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>大手コンビニチェーンサミット2014</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第3回東アジア低炭素成長パートナーシップ対話 (ハイレベル・フォーラム)</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>国際エネルギー機関・地熱実施協定(IEA-GIA)第32回執行委員会(Exco)地熱技術開発コンファレンス</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>CID-UNESCO-TOKYO ユネスコ国際ダンスカウンシル東京大会</td>
                        <td>幕張</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>水化学国際会議（NPC2014）</td>
                        <td>札幌</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>放射線分解・電気化学・材料腐食特性に関する国際ワークショップ2014</td>
                        <td>札幌</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>原子力発電プラントの、伝熱流動、運転、安全関係の国際会議</td>
                        <td>沖縄</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第3回アジア・スマートシティ会議</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>Behavior, Energy and Climate Conference (BECC) JAPAN 2014</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>統計関連学会連合大会2014</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>International Symposium on Regulatory Peptides (REGPEP2014)</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>アジア輸出銀行会合トレーニングコミッティ</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>大手新聞社 シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>医薬品開発のためのPPKPD研究会</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第4回ヒューマンアカデミーキッズサイエンス全国大会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第26回国際鳥類学会議(IOC2014)</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第39回中東協力現地会議</td>
                        <td>イスタンブール</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第6回日中韓消費者政策協議会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>第31回TDM学会・学術大会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>The 10th International Conference of the Metabolomics Society (Metabolomics 2014)</td>
                        <td>山形</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>The Fifth IMF-Japan High-Level Tax Conference<br />
                            For Asian Countries in Tokyo</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>JRS2014 CTコロノグラフィー・トレーニングコース</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>Dow Jones Sustainability Index (DJSI) セミナー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>ブラジル自動車産業セミナー：サプライチェーンの現状と投資機会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>ターンアラウンド・マネジメント協会 アジアパシフィック カンファレンス 2014</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>コミュニティパワー国際会議2014 in 福島</td>
                        <td>福島</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>ジャパン・ハラール・フードプロジェクト</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2014</td> -->
                        <td>Council on Business and Society</td>
                        <td>東京</td>
                    </tr>
                </table>

                <p id="2013" class="p-year">
                    <span class="p-brackets">【2013】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第3回日本・アラブ経済フォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第17回日本統合医療学会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>Mega-ton Water System 国際シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>日中韓特許庁長官会合・日中韓知的財産シンポジウム</td>
                        <td>札幌</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>アジアの未来特別シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>環境リーダープログラム合同会議2013</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>国際セミナー<br />
                            『大学と非大学（１）―高等教育システムの機能的分化―』</td>
                        <td>福岡</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>大手新聞社主催教育フォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>Standardization of Surgical Treatment of Colon Cancer</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>アブダビ投資フォーラム2013・アブダビアルミセミナー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>パレスチナ開発のための東アジア協力促進会合</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第5回アフリカ開発会議 (TICAD V)</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>実践による日米医療機器規制調和（HBD）<br />
                            Japan-US HBD East 2013 Think Tank Meeting</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>MNR2013</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>大手新聞社主催 知と学びのサミット</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>日本心臓病学会 第2 回チーム医療のための症例検討会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第40回 国際福祉機器展（H.C.R.2013）</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>21世紀文明シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>2013年度統計関連学会連合大会</td>
                        <td>大阪</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第38回中東協力現地会議</td>
                        <td>ドバイ</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>WPI-iCeMS &amp; NEXT &amp; SMI合同シンポジウム</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>ジェトロ食品輸出商談会atアグリフードEXPO東京2013/<br />
                            ジャパン・インターナショナル・シーフードショー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>International Symposium on Genome Science</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第18回アジア・太平洋薬物取締会議　（ADEC)</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>5th GOSAT RA PI会議＆9th IWGGMS</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>JRS2013　CTコロノグラフィー・トレーニングコース</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>Tokyo PSAM 2013</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2013</td> -->
                        <td>第8回金属の歴史国際会議（BUMA8）</td>
                        <td>奈良</td>
                    </tr>
                </table>

                <p id="2012" class="p-year">
                    <span class="p-brackets">【2012】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>Brain Diseases and Molecular Machines Conference</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>アジア生命保険シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>The 67th Congress of the Liga Medicorum Homoeopathica Internationalis
                            (LMHI2012)</td>
                        <td>奈良</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>IFAC Workshop on Automation in the Mining, Mineral and Metal
                            Industries (IFAC MMM 2012)</td>
                        <td>岐阜</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>第38回日本整形外科スポーツ医学会学術集会</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>第31回関東甲信越ブロック理学療法士学会</td>
                        <td>さいたま</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>統計関連学会連合大会2012</td>
                        <td>札幌</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>第37回中東協力会議</td>
                        <td>ドーハ</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>The 1st International Aromatheraphy Congress<br />
                            第１回国際アロマセラピー会議</td>
                        <td>京都</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>10th International Symposium on Avian Endocrinology 2012<br />
                            第10 回国際鳥類内分泌学シンポジウム</td>
                        <td>岐阜</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>GPLC2012</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>『太平洋諸島展&amp;フェスタ2012』<br />
                            第6回太平洋・島サミット開催記念事業（展示会&amp;国際会議）</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>第4回日本・メコン地域諸国首脳会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>『モンゴル展2012』<br />
                            日本・モンゴル外交関係樹立40周年記念事業（展示会&amp;フォーラム）</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>IMF Seminar on Countercyclical Fiscal Policy in Asia</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>IMF Monetary Policy Workshop</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>5th International Supply Chain Management Symposium and Workshop</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>第1回日本アブダビ経済協議会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>アブダビ投資フォーラム2012</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>日本・サウジアラビア産業協力フォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>大手新聞社主催フォーラム, シンポジウム各種</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>IGES 世界に貢献する環境経済の政策研究公開シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>リサーチ・アドミニストレーター・シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2012</td> -->
                        <td>南相馬世界会議2012</td>
                        <td>福島</td>
                    </tr>
                </table>

                <p id="2011" class="p-year">
                    <span class="p-brackets">【2011】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>ASEAN+3 財務大臣・中央銀行総裁代理会議</td>
                        <td>仙台</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>第13回日経フォーラム世界経営者会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>東洋大学創立125周年記念国際シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>Satellite Symposium of The 20th Annual ISFN Meeting, GPCRs</td>
                        <td>エルサレム</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>MIRF2011 (The 3rd International Workshop on Mobile Information Retrieval for future)</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>第16回日本・韓国・台湾　民間社会福祉代表者会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>クウェート原油贈呈記念式典&amp;レセプション</td>
                        <td>横浜・東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>International Workshop on High Energy Geophysisc 2011</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>The 59th Meeting of ISO/TC45 Rubber and Rubber Products</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>世界建築雑誌編集長会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>世界建築家会議　サテライトイベント『Factor 4 city Design Workshop』</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>大手新聞社主催フォーラム, シンポジウム各種</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>統計関連学会連合大会2011</td>
                        <td>福岡</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>第36回中東協力会議</td>
                        <td>イスタンブール</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>第20回日本集中治療医学会関東甲信越地方会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>Japan-China Green Expo 2011<br />
                            日中グリーンエキスポ2011</td>
                        <td>北京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>The 3rd GOSAT RA PI Meeting<br />
                            第三回GOSAT RA PI 会議</td>
                        <td>エジンバラ</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>The 4th Internationaly Hypothermia Symposium<br />
                            （第4回国際低体温シンポジウム）</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2011</td> -->
                        <td>The 7th Academic Congress of the Asian Shoulder Association<br />
                            第7回アジア肩関節学会学術大会</td>
                        <td>沖縄</td>
                    </tr>
                </table>

                <p id="2010" class="p-year">
                    <span class="p-brackets">【2010】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2010</td> -->
                        <td>第2回日本・アラブ経済フォーラム</td>
                        <td>チュニス</td>
                    </tr>
                    <tr>
                        <!-- <td>2010</td> -->
                        <td>The World Congress on Adult Guardianship Law 2010 <br />
                            2010年成年後見法世界会議</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2010</td> -->
                        <td>International Synposium on Contamination Control 2010<br />
                            コンタミネーションコントロール国際会議</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2010</td> -->
                        <td>The Satellite Symposium of 11th International Congress on Obesity (ICO2010)<br />
                            （第11回国際肥満学会議サテライトシンポジウム）</td>
                        <td>ストックホルム</td>
                    </tr>
                    <tr>
                        <!-- <td>2010</td> -->
                        <td>第32回日本POS医療学会大会</td>
                        <td>東京</td>
                    </tr>
                </table>

                <p id="2009" class="p-year">
                    <span class="p-brackets">【2009】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2009</td> -->
                        <td>第1回日本・アラブ経済フォーラム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2009～</td> -->
                        <td>統計関連学会連合大会</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2009</td> -->
                        <td>高エネルギー地球科学シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2009</td> -->
                        <td>FEMTEC 2009 (General Assembly and International Thermalism /The 62nd Scientific Congress of The World Federation of Hydrotherapy and Climatotherapy) <br />
                            国際温泉会議ならびに第62期国際温泉気候連合大会</td>
                        <td>横浜</td>
                    </tr>
                    <tr>
                        <!-- <td>2009</td> -->
                        <td>The Satellite Symposium of 9th International Symposium on VIP, PACAP and Related Peptides<br />
                            第9回VIP/PACAP関連ペプチド国際シンポジウム サテライトシンポジウム</td>
                        <td>屋久島</td>
                    </tr>
                    <tr>
                        <!-- <td>2009</td> -->
                        <td>国際福祉機器展　国際セミナー</td>
                        <td>東京</td>
                    </tr>
                </table>

                <p id="2008" class="p-year">
                    <span class="p-brackets">【2008】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2008</td> -->
                        <td>アフリカにおける持続可能な開発のための環境とエネルギー</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2008</td> -->
                        <td>中東ビジネスフォーラム2008</td>
                        <td>東京・大阪</td>
                    </tr>
                    <tr>
                        <!-- <td>2008</td> -->
                        <td>The 4th International Congress on the Science and Technology of Steelmaking<br />
                            第4回世界製鋼会議</td>
                        <td>岐阜</td>
                    </tr>
                    <tr>
                        <!-- <td>2008</td> -->
                        <td>Joint Meeting of 4th World Conference on Computational Statistics &amp; Data Analysis of the IASC and 6th Conference of the Asian Regional Section of the IASC<br />
                            IASC 第4回世界大会・第6回アジア大会 合同国際会議</td>
                        <td>横浜</td>
                    </tr>
                </table>

                <p id="2007" class="p-year">
                    <span class="p-brackets">【2007】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2007</td> -->
                        <td>UIAA General Meeting <br />
                            世界山岳連盟総会</td>
                        <td>松本</td>
                    </tr>
                    <tr>
                        <!-- <td>2007</td> -->
                        <td>The 9th International Symposium on East Asian Resources Recycling Technology<br />
                            第9回東アジア資源リサイクリングシンポジウム</td>
                        <td>仙台</td>
                    </tr>
                </table>

                <p id="2006" class="p-year">
                    <span class="p-brackets">【2006】</span>
                </p>
                <table id="" class="table01">
                    <tr>
                        <!-- <th class="col01">開催年</th> -->
                        <th class="col02">会議・プロジェクト名</th>
                        <th class="col03">開催都市</th>
                        <!-- <th class="col04">備考</th> -->
                    </tr>
                    <tr>
                        <!-- <td>2006</td> -->
                        <td>The 16th Iketani Conference<br />
                            第16回池谷コンファレンス　増子シンポジウム</td>
                        <td>東京</td>
                    </tr>
                    <tr>
                        <!-- <td>2006</td> -->
                        <td>Workshop of the European Association for the Study of Diabetes Annual Meeting (42th EASD)<br />
                            欧州糖尿病学会年次大会サテライト座談会</td>
                        <td>コペンハーゲン</td>
                    </tr>
                </table>

            </div>
            <div class="l-interpretationBlock01">
                <h4>
                    <p class="p-interpretationTitle">まずはご相談ください</p>
                </h4>
            </div>
            <div class="l-interpretationBlock04 pconly">
                <div class="l-interpretationBlock04-table">
                    <div class="l-interpretationBlock04-left">
                        <p>
                            弊社サービスに関して、お気軽にお問い合わせ・ご相談ください。<br>
                            お客様のご要望に応じて、ご提案いたします。また、お見積りのご依頼も承ります。<br>
                            お電話、またはWebフォームより、お問い合わせください。
                        </p>
                        <p>
                            <span>&nbsp;</span>
                        </p>
                        <p>コンベンショングループ</p>
                        <p><img class="p-tel" src="<?php echo get_stylesheet_directory_uri(); ?>/images/common/icon_tel.png" alt="コンベンショングループ"><span>03-6369-9995</span><a class="p-btn_inquiry" href="<?php echo home_url(); ?>/inquiry/form"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/common/btn_inquiry.png" alt="お問い合わせ"></a></p>
                    </div>
                    <div class="l-interpretationBlock04-right">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/common/img_inquiry_type2.png" alt="弊社サービスに関して、お気軽にお問い合わせ・ご相談ください。お客様のご要望に応じて、ご提案いたします。また、お見積りのご依頼も承ります。お電話、またはWebフォームより、お問い合わせください。">
                    </div>
                </div>
            </div>
            <div class="l-inquiryBlock sponly0">
                <div class="l-inquiryBlock-table">
                    <div class="p-inquiry">
                        <div class="p-photo"><a href="tel:03-6369-9995"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri(); ?>/images/top/bg_inquiry_01_2_sp.png" alt="コンベンショングループ 03-6369-9995"></a></div>
                        <div class="p-button"><a href="<?php echo home_url(); ?>/inquiry/form"><img class="js-btn_inquiry is-imgChange" src="<?php echo get_stylesheet_directory_uri(); ?>/images/top/btn_inquiry_01_pc.png" alt="お問い合わせ"></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="l-side">
            <div class="l-side-menu">
                <div class="p-side-menu-title">
                    <h3>会議・イベント運営</h3>
                </div>
                <div class="p-side-menu-list">
                    <p><a href="<?php echo home_url();?>/kokusai/">サービス・運営概要</a></p>
                    <p><a href="<?php echo home_url();?>/kokusai/event">事例紹介</a></p>
                    <p><a href="<?php echo home_url();?>/kokusai/detail">ご提案から実施まで</a></p>
                    <p><a href="<?php echo home_url();?>/kokusai/list">通訳・運営実績</a></p>
                    <p><a href="<?php echo home_url();?>/kokusai/online">パッケージプラン</a></p>
                    <!-- <p><a href="<?php echo home_url();?>/kokusai/studio">ISSスタジオ</a></p> -->
                </div>
            </div>
            <div class="l-side-banner">
                <div class="p-side-banner-list">
                    <p><a href="<?php echo home_url(); ?>/recruit/registration/form"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/common/img_side_registration.png" alt="通訳者・翻訳者の登録について"></a></p>
                </div>
            </div>
        </div>
    </div>

</div>
<?php get_footer(); ?>