
// Roll Over
function initRollovers() {
	if (!document.getElementById) return
	
	var aPreLoad = new Array();
	var sTempSrc;
	var aImages = document.getElementsByTagName('img');

	for (var i = 0; i < aImages.length; i++) {		
		if (aImages[i].className == 'btn') {
			var src = aImages[i].getAttribute('src');
			var ftype = src.substring(src.lastIndexOf('.'), src.length);
			var hsrc = src.replace(ftype, '_current'+ftype);

			aImages[i].setAttribute('hsrc', hsrc);
			
			aPreLoad[i] = new Image();
			aPreLoad[i].src = hsrc;
			
			aImages[i].onmouseover = function() {
				sTempSrc = this.getAttribute('src');
				this.setAttribute('src', this.getAttribute('hsrc'));
			}	
			
			aImages[i].onmouseout = function() {
				if (!sTempSrc) sTempSrc = this.getAttribute('src').replace('_current'+ftype, ftype);
				this.setAttribute('src', sTempSrc);
			}
		}
	}
}

// New Window
function newWin( title, picURL, width, height ) {
var html = '<html><title>ISS | '+ title +'</title><body style="margin:0; padding:0;"><center><img src="' + picURL + '" alt="' + title + '" /></center><script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>
</body></html>';
var swin = window.open( '', 'subWin', 'width=' + width + ',height=' + height + ',resizable' );
swin.resizeTo( width, height );
swin.document.open();
swin.document.write( html );
swin.document.close();
swin.focus();
}


function viewBox(div) {
	div.style.display = "block";
}
function hideBox(div) {
	div.style.display = "none";
}
function showList(boxid) {
	var ref = "";
	var docref = "";
	ref = "ref_" + boxid;
	docref = document.getElementById(ref);
	if (docref.style.display == 'none' || docref.style.display == '') {
		viewBox(docref);
	} else {
		hideBox(docref);
	}
}

// CSS For FireFox H3
var vNum = navigator.appVersion.charAt(0);
document.writeln("<STYLE TYPE='text/css'><!--");
if(navigator.appVersion.indexOf("Mac") > -1) {
	document.writeln("#txtBody h3 strong {");
	document.writeln("	background-position:left 22px;");
	document.writeln("}");
	document.writeln("/* Safari Only \*/");
	document.writeln("html:\66irst-child #txtBody h3 strong {");
	document.writeln("	background-position:0 21px;	/* Safari Only */");
	document.writeln("}");
	document.writeln("/* end */");
}
document.writeln("--></STYLE>");
