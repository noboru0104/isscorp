<?php

//カスタムヘッダー
add_theme_support(
'custom-header',
array(
	'width' =>1200,
	'height' =>300,
	'header_text' =>false,
	'default-image' => '%s/images/top/main_image.png',
	)
	);
	
	//カスタムメニュー
	register_nav_menus(
		array(
			'place_global' => 'グローバル',
			'place_utility' => 'ユーティリティ',
			)
		);
//headerにmagin-topが入った場合、これをなくすwp_head()
//add_filter( 'show_admin_bar', '__return_false' );

//ショートコードを使ったphpファイルの呼び出し方法
function Include_my_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() . '/' . get_template() . "/$file.php");
    return ob_get_clean();
}
add_shortcode('myphp', 'Include_my_php');

function myPreGetPosts( $query ) {
  if ( is_admin() || ! $query->is_main_query() ){
	  return;
 }
	
	if ( $query->is_post_type_archive('news') ) {
		//記事一覧用
		$query->set('posts_per_page', 10);
		$query->set('order', 'DESC');
		$query->set('orderby', 'post_date');
		
		$query->set( 'tax_query', $taxquery );
		
	}
}
add_action('pre_get_posts','myPreGetPosts');

/**
 * my_error_message
 * @param string $error
 * @param string $key
 * @param string $rule（半角小文字）
 */
//お問い合わせ用（本番用）
function my_error_message( $error, $key, $rule ) {
    if ( $key === 'toiawaseservice' && $rule === 'required' ) {		
		return 'お問い合わせサービスは必須項目です。';
    }
	if ( $key === 'otoiawasenaiyou' && $rule === 'noempty' ) {
        return 'お問い合わせ内容は必須項目です。';
    }
	if ( $key === 'company' && $rule === 'noempty' ) {
        return '会社（団体名）は必須項目です。';
    }
	if ( $key === 'name' && $rule === 'noempty' ) {
        return 'お名前は必須項目です。';
    }
	if ( $key === 'furigana' && $rule === 'noempty' ) {
        return 'お名前（フリガナ）は必須項目です。';
    }
	if ( $key === 'tel1' && $rule === 'noempty' ) {
        return '電話番号は必須項目です。';
    }
	if ( $key === 'mail' && $rule === 'noempty' ) {
        return 'e-mailアドレスは必須項目です。';
    }
	if ( $key === 'consent' && $rule === 'required' ) {
        return '必ず「同意する」にチェックを入れてください。';
    }
    return $error;
}
add_filter( 'mwform_error_message_mw-wp-form-299', 'my_error_message', 10, 3 );

//お問い合わせ用管理者宛メール設定（本番用）
function my_mail_admin( $Mail, $values, $Data ) {
	
    $toiawaseservice = $Data->get('toiawaseservice');
	$adress = 'info@issjp.com'; //info@issjp.com
		
	if(strpos($toiawaseservice,'通訳') !== false){
		$adress .= ",ask_conv@issjp.com"; //ask_conv@issjp.com
	}
	if(strpos($toiawaseservice,'会議・イベント運営（ウェブ・ハイブリッド含む）') !== false){
		$adress .= ",ask_conv@issjp.com"; //ask_conv@issjp.com
	}
	if(strpos($toiawaseservice,'人材派遣・紹介予定派遣') !== false){
		$adress .= ',haken@issjp.com'; //haken@issjp.com
	}
	if(strpos($toiawaseservice,'人材紹介') !== false){
		if(strpos($adress,'haken@issjp.com') === false){
			$adress .= ',haken@issjp.com'; //haken@issjp.com
		}
	}
	if(strpos($toiawaseservice,'その他') !== false){
		if(strpos($adress,'info@issjp.com') === false){
			$adress .= ',info@issjp.com'; //info@issjp.com
		}
	}
	if(strpos($toiawaseservice,'法人向け語学研修') !== false){
		$adress .= ",kensyu@issnet.co.jp";
	}
	if(strpos($toiawaseservice,'ウェブ会議/ウェビナー支援') !== false){
		$adress .= ",webinar@issjp.com";
	}
	
	$Mail->to = $adress; // 送信先を変更
	//$Mail->to = "noboru_asano19810104@yahoo.co.jp,t.koyama@cif-inc.net"; // 送信先を変更
    // $Mail->send(); で送信もできます。
    return $Mail;	
}
add_filter( 'mwform_admin_mail_mw-wp-form-299', 'my_mail_admin', 10, 3 );

//お問い合わせ用（テスト用）
function my_error_message_test( $error, $key, $rule ) {
    if ( $key === 'toiawaseservice' && $rule === 'required' ) {		
		return 'お問い合わせサービスは必須項目です。';
    }
	if ( $key === 'otoiawasenaiyou' && $rule === 'noempty' ) {
        return 'お問い合わせ内容は必須項目です。';
    }
	if ( $key === 'company' && $rule === 'noempty' ) {
        return '会社（団体名）は必須項目です。';
    }
	if ( $key === 'name' && $rule === 'noempty' ) {
        return 'お名前は必須項目です。';
    }
	if ( $key === 'furigana' && $rule === 'noempty' ) {
        return 'お名前（フリガナ）は必須項目です。';
    }
	if ( $key === 'tel1' && $rule === 'noempty' ) {
        return '電話番号は必須項目です。';
    }
	if ( $key === 'mail' && $rule === 'noempty' ) {
        return 'e-mailアドレスは必須項目です。';
    }
	if ( $key === 'consent' && $rule === 'required' ) {
        return '必ず「同意する」にチェックを入れてください。';
    }
    return $error;
}
add_filter( 'mwform_error_message_mw-wp-form-10', 'my_error_message_test', 10, 3 );

//お問い合わせ用管理者宛メール設定（テスト用）
function my_mail_admin_test( $Mail, $values, $Data ) {
	
    $toiawaseservice = $Data->get('toiawaseservice');
//	$adress = 't.koyama@cif-inc.net'; //info@issjp.com
	$adress = 'info@issjp.com'; //info@issjp.com
		
	if(strpos($toiawaseservice,'通訳') !== false){
		$adress .= ",k.kishimizu@cif-inc.net"; //ask_conv@issjp.com
	}
	if(strpos($toiawaseservice,'会議・イベント運営（ウェブ・ハイブリッド含む）') !== false){
		$adress .= ",m.sugiyama@cif-inc.net"; //ask_conference@issjp.com
	}
	if(strpos($toiawaseservice,'人材派遣・紹介予定派遣') !== false){
		$adress .= ',t.koyama@cif-inc.net'; //haken@issjp.com
	}
	if(strpos($toiawaseservice,'人材紹介') !== false){
		if(strpos($adress,'t.koyama@cif-inc.net') === false){
			$adress .= ',t.koyama@cif-inc.net'; //haken@issjp.com
		}
	}
	if(strpos($toiawaseservice,'その他') !== false){
		if(strpos($adress,'noboru_asano19810104@yahoo.co.jp') === false){
			$adress .= ',noboru_asano19810104@yahoo.co.jp'; //info@issjp.com
		}
	}
	if(strpos($toiawaseservice,'法人向け語学研修') !== false){
		$adress .= ",kensyu@issnet.co.jp";
	}
	if(strpos($toiawaseservice,'ウェブ会議/ウェビナー支援') !== false){
		$adress = "iwashita1112@gmail.com";
	}
	
	$Mail->to = $adress; // 送信先を変更
	//$Mail->to = "noboru_asano19810104@yahoo.co.jp,t.koyama@cif-inc.net"; // 送信先を変更
    // $Mail->send(); で送信もできます。
    return $Mail;	
}
add_filter( 'mwform_admin_mail_mw-wp-form-10', 'my_mail_admin_test', 10, 3 );

//フリーランス通訳者用（本番用）
function my_error_message2( $error, $key, $rule ) {
    
	if ( $key === 'name1' && $rule === 'noempty' ) {
        return 'お名前-姓は必須項目です。';
    }
	if ( $key === 'name2' && $rule === 'noempty' ) {
        return 'お名前-名は必須項目です。';
    }
	if ( $key === 'furigana1' && $rule === 'noempty' ) {
        return 'お名前（ふりがな）-せいは必須項目です。';
    }
	if ( $key === 'furigana2' && $rule === 'noempty' ) {
        return 'お名前（ふりがな）-めいは必須項目です。';
    }
	if ( $key === 'email' && $rule === 'noempty' ) {
        return 'メールアドレスは必須項目です。';
    }
	if ( $key === 'gender' && $rule === 'required' ) {
        return '性別は必須項目です。';
    }
	if ( $key === 'zip31' && $rule === 'noempty' ) {
        return '郵便番号1は必須項目です。';
    }
	if ( $key === 'zip32' && $rule === 'noempty' ) {
        return '郵便番号2は必須項目です。';
    }
	if ( $key === 'pref31' && $rule === 'noempty' ) {
        return '都道府県は必須項目です。';
    }
	if ( $key === 'addr31' && $rule === 'noempty' ) {
        return '市区町村は必須項目です。';
    }
	if ( $key === 'banchi' && $rule === 'noempty' ) {
        return '番地以下は必須項目です。';
    }
	if ( $key === 'tel1' && $rule === 'noempty' ) {
        return '電話番号1は必須項目です。';
    }
	if ( $key === 'tel2' && $rule === 'noempty' ) {
        return '電話番号2は必須項目です。';
    }
	if ( $key === 'tel3' && $rule === 'noempty' ) {
        return '電話番号3は必須項目です。';
    }
	if ( $key === 'syurui4' && $rule === 'required' ) {
        return '応募職種は必須項目です。';
    }
	
	if ( $key === 'consent' && $rule === 'required' ) {
        return '必ず「同意する」にチェックを入れてください。';
    }
    return $error;
}
add_filter( 'mwform_error_message_mw-wp-form-418', 'my_error_message2', 10, 3 );

//フリーランス通訳者用（テスト用）
function my_error_message2_test( $error, $key, $rule ) {
    
	if ( $key === 'name1' && $rule === 'noempty' ) {
        return 'お名前-姓は必須項目です。';
    }
	if ( $key === 'name2' && $rule === 'noempty' ) {
        return 'お名前-名は必須項目です。';
    }
	if ( $key === 'furigana1' && $rule === 'noempty' ) {
        return 'お名前（ふりがな）-せいは必須項目です。';
    }
	if ( $key === 'furigana2' && $rule === 'noempty' ) {
        return 'お名前（ふりがな）-めいは必須項目です。';
    }
	if ( $key === 'email' && $rule === 'noempty' ) {
        return 'メールアドレスは必須項目です。';
    }
	if ( $key === 'gender' && $rule === 'required' ) {
        return '性別は必須項目です。';
    }
	if ( $key === 'zip31' && $rule === 'noempty' ) {
        return '郵便番号1は必須項目です。';
    }
	if ( $key === 'zip32' && $rule === 'noempty' ) {
        return '郵便番号2は必須項目です。';
    }
	if ( $key === 'pref31' && $rule === 'noempty' ) {
        return '都道府県は必須項目です。';
    }
	if ( $key === 'addr31' && $rule === 'noempty' ) {
        return '市区町村は必須項目です。';
    }
	if ( $key === 'banchi' && $rule === 'noempty' ) {
        return '番地以下は必須項目です。';
    }
	if ( $key === 'tel1' && $rule === 'noempty' ) {
        return '電話番号1は必須項目です。';
    }
	if ( $key === 'tel2' && $rule === 'noempty' ) {
        return '電話番号2は必須項目です。';
    }
	if ( $key === 'tel3' && $rule === 'noempty' ) {
        return '電話番号3は必須項目です。';
    }
	if ( $key === 'syurui4' && $rule === 'required' ) {
        return '応募職種は必須項目です。';
    }
	
	if ( $key === 'consent' && $rule === 'required' ) {
        return '必ず「同意する」にチェックを入れてください。';
    }
    return $error;
}
add_filter( 'mwform_error_message_mw-wp-form-147', 'my_error_message2_test', 10, 3 );

//コンベンションスタッフアルバイト用（本番用）
function my_error_message3( $error, $key, $rule ) {
    
	if ( $key === 'name1' && $rule === 'noempty' ) {
        return 'お名前-姓は必須項目です。';
    }
	if ( $key === 'name2' && $rule === 'noempty' ) {
        return 'お名前-名は必須項目です。';
    }
	if ( $key === 'furigana1' && $rule === 'noempty' ) {
        return 'お名前（フリガナ）-セイは必須項目です。';
    }
	if ( $key === 'furigana2' && $rule === 'noempty' ) {
        return 'お名前（フリガナ）-メイは必須項目です。';
    }
	if ( $key === 'roma1' && $rule === 'noempty' ) {
        return 'お名前（ローマ字）は必須項目です。';
    }
	if ( $key === 'gender' && $rule === 'required' ) {
        return '性別は必須項目です。';
    }
	if ( $key === 'tel1' && $rule === 'noempty' ) {
        return '電話番号は必須項目です。';
    }
	
	if ( $key === 'email' && $rule === 'noempty' ) {
        return 'メールアドレスは必須項目です。';
    }
	if ( $key === 'emailkakunin' && $rule === 'noempty' ) {
        return 'メールアドレス（確認用）は必須項目です。';
    }
	if ( $key === 'emailkakunin' && $rule === 'eq' ) {
        return 'メールアドレスと一致しません。';
    }
	if ( $key === 'renraku' && $rule === 'noempty' ) {
        return '日中連絡先は必須項目です。';
    }
	if ( $key === 'kanou' && $rule === 'noempty' ) {
        return '勤務可能エリアは必須項目です。';
    }
	if ( $key === 'jyoukyou' && $rule === 'noempty' ) {
        return '現在の就業状況は必須項目です。';
    }
	if ( $key === 'gogaku_j' && $rule === 'noempty' ) {
        return '語学（日本語）は必須項目です。';
    }
	if ( $key === 'gogaku_e' && $rule === 'noempty' ) {
        return '語学（英語）は必須項目です。';
    }
	
	
	if ( $key === 'consent' && $rule === 'required' ) {
        return '必ず「同意する」にチェックを入れてください。';
    }
    return $error;
}
add_filter( 'mwform_error_message_mw-wp-form-435', 'my_error_message3', 10, 3 );

//コンベンションスタッフアルバイト用（テスト用）
function my_error_message3_test( $error, $key, $rule ) {
    
	if ( $key === 'name1' && $rule === 'noempty' ) {
        return 'お名前-姓は必須項目です。';
    }
	if ( $key === 'name2' && $rule === 'noempty' ) {
        return 'お名前-名は必須項目です。';
    }
	if ( $key === 'furigana1' && $rule === 'noempty' ) {
        return 'お名前（フリガナ）-セイは必須項目です。';
    }
	if ( $key === 'furigana2' && $rule === 'noempty' ) {
        return 'お名前（フリガナ）-メイは必須項目です。';
    }
	if ( $key === 'roma1' && $rule === 'noempty' ) {
        return 'お名前（ローマ字）は必須項目です。';
    }
	if ( $key === 'gender' && $rule === 'required' ) {
        return '性別は必須項目です。';
    }
	if ( $key === 'tel1' && $rule === 'noempty' ) {
        return '電話番号は必須項目です。';
    }
	
	if ( $key === 'email' && $rule === 'noempty' ) {
        return 'メールアドレスは必須項目です。';
    }
	if ( $key === 'emailkakunin' && $rule === 'noempty' ) {
        return 'メールアドレス（確認用）は必須項目です。';
    }
	if ( $key === 'emailkakunin' && $rule === 'eq' ) {
        return 'メールアドレスと一致しません。';
    }
	if ( $key === 'renraku' && $rule === 'noempty' ) {
        return '日中連絡先は必須項目です。';
    }
	if ( $key === 'kanou' && $rule === 'noempty' ) {
        return '勤務可能エリアは必須項目です。';
    }
	if ( $key === 'jyoukyou' && $rule === 'noempty' ) {
        return '現在の就業状況は必須項目です。';
    }
	if ( $key === 'gogaku_j' && $rule === 'noempty' ) {
        return '語学（日本語）は必須項目です。';
    }
	if ( $key === 'gogaku_e' && $rule === 'noempty' ) {
        return '語学（英語）は必須項目です。';
    }
	
	
	if ( $key === 'consent' && $rule === 'required' ) {
        return '必ず「同意する」にチェックを入れてください。';
    }
    return $error;
}
add_filter( 'mwform_error_message_mw-wp-form-148', 'my_error_message3_test', 10, 3 );

/* mw wp form validate */
function my_validation_rule_test( $Validation, $data ) {
  //半角英字のみ許可
  if (!preg_match("/^[ 0-9\+\-]+$/", $data['tel1'])) {
	 $Validation->set_rule( 'tel1', 'tel', array(
		'message' => '半角数字で入力してください。'
	  ) ); 
  }
  return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-10', 'my_validation_rule_test', 10, 2 );

/* mw wp form validate */
function my_validation_rule2( $Validation, $data ) {
  //半角英字のみ許可
  if (!preg_match("/^[ 0-9\+\-]+$/", $data['tel1'])) {
	 $Validation->set_rule( 'tel1', 'tel', array(
		'message' => '半角数字で入力してください。'
	  ) ); 
  }
  return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-299', 'my_validation_rule2', 10, 2 );

function my_validation_rule3( $Validation, $data ) {
  //ひらがなと長音のみ許可
  if (!preg_match("/^[ぁ-ゞー]+$/u", $data['furigana1'])) {
	 $Validation->set_rule( 'furigana1', 'hiragana', array(
		'message' => 'ひらがなで入力してください。'
	  ) ); 
  }
  if (!preg_match("/^[ぁ-ゞー]+$/u", $data['furigana2'])) {
	 $Validation->set_rule( 'furigana2', 'hiragana', array(
		'message' => 'ひらがなで入力してください。'
	  ) ); 
  }
  return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-418', 'my_validation_rule3', 10, 2 );

/* mw wp form validate */
function my_validation_rule( $Validation, $data ) {
  //日中連絡先にその他を選択された場合必須にする
  if ( $data['renraku'] === 'その他' ) {
      $Validation->set_rule( 'renrakusonota', 'noEmpty', array(
        'message' => '日中連絡先にその他を選択された場合は必ずご記入下さい。'
      ) );
  }
  //半角スペースと半角英字のみ許可
  if (!preg_match("/^[ a-zA-Z]+$/", $data['roma1'])) {
	 $Validation->set_rule( 'roma1', 'alpha', array(
		'message' => '半角英字で入力してください。'
	  ) ); 
  }
  return $Validation;
}
add_filter( 'mwform_validation_mw-wp-form-435', 'my_validation_rule', 10, 2 );

//ページが存在しない場合の処理（例外を除きサイトトップを表示する）
function is404_redirect_home() {
	if( is_404() ){
		$url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		//旧サイトのhttp://www.issjp.com/company...の場合は新ページの会社概要に遷移させる。
		//if(strstr($url,'/tsuyakuentry/inquiry/')==true){
//			//var_dump($url);
//			wp_safe_redirect( home_url( '/recruit/registration/interpreters' ) );
//		} else {
			wp_safe_redirect( home_url( '/' ) );
		//}		
		exit();
	}
}
add_action( 'template_redirect', 'is404_redirect_home' );

function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}

//function ajaxzip3_scripts() {
//    wp_enqueue_script( 'ajaxzip3-script', 'https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js', array( 'jquery' ), '20140807', true );
//}
//add_action( 'wp_enqueue_scripts', 'ajaxzip3_scripts' );
		
//画像パスの置換（固定ページ用）
function imgpass_short($arg) {
	$content = str_replace('"images/', '"' . get_stylesheet_directory_uri() . '/images/',$arg);
	return $content;
}
add_action('the_content','imgpass_short');

//PDFパスの置換（固定ページ用）
function pdfpass_short($arg) {
	$content = str_replace('"pdf/', '"' . get_stylesheet_directory_uri() . '/pdf/',$arg);
	return $content;
}
add_action('the_content','pdfpass_short');

//aリンクの置換（固定ページ用）
function linkpass_short($arg) {
	$content = str_replace('"link/', '"' . home_url() . '/',$arg);
	return $content;
}
add_action('the_content','linkpass_short');

function modify_read_more_link() {
    echo '<a class="more-link" href="' . get_permalink() . '">More＞</a>';
}
//add_filter( 'the_content', 'modify_read_more_link' );

// タイトルのセパレータ
function change_separator() {
  return "|"; // ここに変更したい区切り文字を書く
}
add_filter('document_title_separator', 'change_separator');

//ショートコードを使ったphpファイルの呼び出し方法
function my_php_Include($params = array()) {
 extract(shortcode_atts(array('file' => 'default'), $params));
 ob_start();
 include(STYLESHEETPATH . "/$file.php");
 return ob_get_clean();
}
add_shortcode('myphp', 'my_php_Include');

/*
 * 親テーマのfunctions.phpを書き換える
 */
function mytheme_setup() {
     
    /* ここにfunctionを記述すると親テーマのfunctionsの後に実行される */
 
    /*********************************************
    *   トップページの抜粋表示のあとに、
    *   自動的に「続きを読む」が入るのを防ぐ
    **********************************************/
    function new_excerpt_more($post) {
        return '...';  
    }   
    add_filter('excerpt_more', 'new_excerpt_more');

}
add_action( 'after_setup_theme', 'mytheme_setup', 20 );

////親テーマがtwentyfifteenの時に不要css削除
//function set_up_origin_theme() {
//	function deregister_parent_css() {
//		// Add custom fonts, used in the main stylesheet.
//		wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );
//	
//		// Load our main stylesheet.
//		wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );
//	
//		// Load the Internet Explorer specific stylesheet.
//		/*wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
//		wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );*/
//	
//		// Load the Internet Explorer 7 specific stylesheet.
//		/*wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
//		wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );*/
//		
//		wp_dequeue_style( 'twentyfifteen-fonts' );
//		wp_dequeue_style( 'twentyfifteen-style' );
//		/*wp_dequeue_style( 'twentyfifteen-ie' );
//		wp_dequeue_style( 'twentyfifteen-ie7' );*/
//	}
//	remove_action('wp_enqueue_scripts', 'twentyfifteen_scripts');
//	add_action('wp_enqueue_scripts', 'deregister_parent_css');
//}
//add_action('after_setup_theme', 'set_up_origin_theme');

//pタグ削除

add_action('init', function() {
	remove_filter('the_excerpt', 'wpautop');
	remove_filter('the_content', 'wpautop');
});
 
add_filter('tiny_mce_before_init', function($init) {
	$init['wpautop'] = false;
	$init['apply_source_formatting'] = false;
	return $init;
});

// パンくずリスト
function breadcrumb(){
    global $post;
    $str ='';
    if(!is_home()&&!is_admin()){
        $str.= '<div id="breadcrumb" class="pankuzu"><div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
        $str.= '<a href="'. home_url() .'" itemprop="url"><span itemprop="title">TOP</span></a> &gt;</div>';
 
        if(is_category()) {
            $cat = get_queried_object();
            if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($ancestor) .'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor) .'</span></a> &gt;</div>';
                }
            }
        $str.='<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_category_link($cat -> term_id). '" itemprop="url"><span itemprop="title">'. $cat-> cat_name . '</span></a> &gt;</div>';
        } elseif(is_page(123)){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            /*if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_archives_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor)"新着情報". '</span></a> &gt;</div>';
                }
            }*/
            $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a><span itemprop="title">'. /*$cat-> cat_name*/"新着情報" . '</span></a></div>';
        } elseif(is_page()){
            if($post -> post_parent != 0 ){
                $ancestors = array_reverse(get_post_ancestors( $post->ID ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_permalink($ancestor).'" itemprop="url"><span itemprop="title">'. get_the_title($ancestor) .'</span></a> &gt;</div>';
                }
            }
        } elseif(is_single() || is_page(123)){
            $categories = get_the_category($post->ID);
            $cat = $categories[0];
            /*if($cat -> parent != 0){
                $ancestors = array_reverse(get_ancestors( $cat -> cat_ID, 'category' ));
                foreach($ancestors as $ancestor){
                    $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. get_archives_link($ancestor).'" itemprop="url"><span itemprop="title">'. get_cat_name($ancestor)"新着情報". '</span></a> &gt;</div>';
                }
            }*/
            $str.='<div class="pan_div" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'. /*get_category_link($cat -> term_id)*/get_page_link( 123 ). '" itemprop="url"><span itemprop="title">'. /*$cat-> cat_name*/"新着情報" . '</span></a> &gt;</div>';
        } else{
            $str.='<div>'. wp_title('', false) .'</div>';
        }
        $str.='</div>';
    }
    echo $str;


//バージョンアップ通知を非表示
add_filter('pre_site_transient_update_core', '__return_zero'); //WP本体
add_filter('site_option__site_transient_update_plugins', '__return_zero'); //プラグイン
remove_action ('wp_version_check','wp_version_check');
remove_action ('admin_init','_maybe_update_core');

//管理バーから更新を非表示
//function remove_bar_menus( $wp_admin_bar ) {
//    $wp_admin_bar->remove_menu( 'updates' ); // 更新
//}
//add_action('admin_bar_menu', 'remove_bar_menus', 201);
//

}


function delete_jquery() {
  if (!is_admin()) {
    wp_deregister_script('jquery');
  }
}
add_action('init', 'delete_jquery');