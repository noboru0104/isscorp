#!/usr/bin/perl

#各種設定開始

#ディレクトリ情報設定ここから
my $StaffHtmlRoot = "../../../home/staff_only";

my $StaffCgiRoot = "../../../cgi-bin";
my $TemplateDir = "../../template/staff_only";
my $DataFileDir = "../../secret/staff_only";
#ディレクトリ情報設定ここまで

#ライブラリ設定開始
require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
#ライブラリ設定終了

#テンプレートファイルリスト
my $HtmlBody = "msg_list.html";
my $DataTable = "msg_list_table.html";

#データファイル
my $DataFile = "msg.csv";
my $NewDataFile = "new_msg.csv";

#エラーメッセージ変数宣言
my $ErrorMessage = "";
my $DateError = "";
my $TitleError = "";
my $StatementError = "";

#出力文字列格納用変数
my $HtmlSrc = "";

#データテーブル用文字列格納変数
my $TableSrc = "";

#テーブルテンプレート保持用変数
my $TableTemplate = "";

#テーブルデータ加工用変数
my $TempTableData = "";

#各種設定終了

print("Content-type:text/html\n\n");

#テーブルテンプレート読み込み
$TableTemplate = &HtmlSrcLoad($DataTable);

#データテーブル生成（繰り返し処理。TableSrcに文字列を次々に追加）
open TABLEDATA, "$DataFileDir/$DataFile";
while(<TABLEDATA>){
	$TempTableData = $TableTemplate;
	my @TempFileData = split /,/,$_;
	$TempTableData =~ s/__DATE__/$TempFileData[0]/;
	$TempTableData =~ s/__TITLE__/$TempFileData[1]/;
	$TempTableData =~ s/__DATA__NO__/$TempFileData[3]/g;
	$TableSrc .= $TempTableData;
}
close TABLEDATA;

#Html本体読み込み
$HtmlSrc = &HtmlSrcLoad($HtmlBody);

#Html本体に置換処理
$HtmlSrc =~ s/__EDIT__LIST__/$TableSrc/;

#Html出力
print("$HtmlSrc");

#以下サブルーチン
sub HtmlSrcLoad{
	my @UsingFile = @_;
	my $TempSrc = "";
	open BODY, "$TemplateDir/$UsingFile[0]" or die "Error!";
	while(<BODY>){
		$TempSrc .= $_;
	}
	close BODY;
	return $TempSrc;
}

