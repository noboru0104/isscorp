$(function(){
	"use strict";
		
	$('.l-conventionlist-table-cell .l-inside').matchHeight();
	$('.l-conferenceBlock04-table-cell .l-conferenceBlock04-inside p').matchHeight();
	
	$(window).load(function () {
		
		TextChange();
		
	});
	
	$(window).resize(function () {
		
		$('.l-conventionlist-table-cell .l-inside').matchHeight();
		$('.l-conferenceBlock04-table-cell .l-conferenceBlock04-inside p').matchHeight();
		
	});
	
});

function  TextChange(){
	"use strict";
	
	if($('.l-planBlock').length){
		var widimage = window.innerWidth;
	
		if( widimage < 1001 ){
			$('.l-planBlock table tr th:nth-child(2)').text('開催年 - 会議・プロジェクト名 - 開催都市');
			
			$('.l-planBlock table tr').each( function(){
				var td_text1 = $(this).find('td:first-child').text();
				var td_text2 = $(this).find('td:nth-child(2)').text();
				var td_text3 = $(this).find('td:last-child').text();
				$(this).find('td:nth-child(2)').text(td_text1 + ' - ' + td_text2 + ' - ' + td_text3);
			});
		} else {
						
		}
	}
}

