
<?php get_header(); ?>
<div id="interpretation" class="l-wrapper">
	<div class="l-pankuzu">
    	<div class="l-inner">
    		<p><a href="../">ホーム</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;News&nbsp;&amp;&nbsp;Topics</p>
        </div>
    </div>
    
    <!--<section>
        <div class="l-mainVisual">
            <div class="l-mainVisual-inner">
                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_01_pc.png" alt="News & Topics">
            </div>
        </div>
    </section>-->    
    
    <div class="l-wrapper">
        <div class="l-newsBlock">
            <div class="l-inner">
                <div class="l-newsBlock01">
                    <div class="l-newsBlock01-table">
                        <p>News&nbsp;&amp;&nbsp;Topics</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
                <div class="l-newsBlock02">                    
                    <?php if (have_posts()) : ?>
                    <?php while (have_posts() ) : the_post(); ?>                    
                    	<?php
							//「カテゴリ」項目を取得
							$values = CFS()->get('category');
							foreach ($values as $value => $label) {
								
							}
						?>
                    	<?php
						//「外部リンク」項目が未入力の場合
                        if (empty(CFS()->get('link'))) :
						?>
							<?php
                            //かつ「カテゴリ」が「採用情報」の場合
                            if ($value === '採用情報'):
                            ?>
                            <a href="<?php echo home_url();?>/recruit/career">
                            <?php else: ?>
                            <a href="<?php the_permalink(); ?>">
                            <?php endif; ?>
                        <?php else: ?>
                        <a href="<?php echo CFS()->get('link') ?>" target="_blank">
                        <?php endif; ?>
                            <div class="l-newsBlock02-table">                        
                                <div class="p-date">
                                    <p><?php the_time('Y.m.d'); ?></p>
                                </div>
                                <div class="p-category">
                                    
                                    <?php if ($value === 'ISSインスティテュート'): ?>
                                    <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category01_pc.png" alt="ISSインスティテュート"></p>
                                    <?php elseif ($value === '採用情報'): ?>
                                    <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category04_pc.png" alt="採用情報"></p>
                                    <?php elseif ($value === '通訳者 募集'): ?>
                                    <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category03_pc.png" alt="通訳者募集"></p>
                                    <?php elseif ($value === 'コンベンション'): ?>
                                    <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category02_pc.png" alt="コンベンション"></p>
                                    <?php elseif ($value === 'お知らせ'): ?>
                                    <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category05_pc.png" alt="お知らせ"></p>
                                    <?php endif; ?>
                                </div>
                                <div class="p-message">
                                    <p><?php the_title(); ?></p>
                                </div>
                            </div>
                        </a>
                    <?php endwhile; endif; ?>                                     
                </div>
                <!-- ナビゲーション -->
                <div class="activity-nav pconly">
                <?php global $wp_rewrite;
                    $paginate_base = get_pagenum_link(1);
                    if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
                    $paginate_format = '';
                    $paginate_base = add_query_arg('paged', '%#%');
                    } else {
                    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
                    user_trailingslashit('page/%#%/', 'paged');;
                    $paginate_base .= '%_%';
                    }
                    echo paginate_links( array(
                    'base' => $paginate_base,
                    'format' => $paginate_format,
                    'total' => $wp_query->max_num_pages,
                    //'mid_size' => 3,
                    'end_size'    => 1,
                    'mid_size'    => 3,
                    'current' => ($paged ? $paged : 1),
                )); ?>
                </div>
                <div class="activity-nav sponly0">
                <?php global $wp_rewrite;
                    $paginate_base = get_pagenum_link(1);
                    if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
                    $paginate_format = '';
                    $paginate_base = add_query_arg('paged', '%#%');
                    } else {
                    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
                    user_trailingslashit('page/%#%/', 'paged');;
                    $paginate_base .= '%_%';
                    }
                    echo paginate_links( array(
                    'base' => $paginate_base,
                    'format' => $paginate_format,
                    'total' => $wp_query->max_num_pages,
                    //'mid_size' => 3,

                    'end_size'    => 0,
                    'mid_size'    => 0,
                    'current' => ($paged ? $paged : 1),
                )); ?>
                </div>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <!--/.new-->
    </div>
    
</div>
<?php get_footer(); ?>