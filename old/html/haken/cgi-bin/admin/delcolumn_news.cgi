#!/usr/bin/perl

#各種設定開始
#ディレクトリ情報ここから
my $TextFileDir = "../secret/news";

my $TemplateDir = "../template/news";
my $LiblaryDir = "../perl_lib";
my $ImageDir = "../../home/news/image";
my $ImageDir2 = "/news/image";
#ディレクトリ情報ここまで

#トップページの更新の有無の設定（0は更新しない。１は更新する。デフォルトは更新しない）
my $RenewalTop = 1;

#トップページ更新用データファイル
my $NewsInfo = "newsinfo.csv";

#windows上でのチェック用パス
#require 'cgi-lib.pl';
#require 'jcode.pl';

#unix上への実装時のパス
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#テンプレートファイルリストここから
my $CompHtml = "delcomplete.html";
my $DeleteCheck = "delcolumn.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#各種設定終了

print ("Content-type:text/html\n\n");

#削除対象の情報取得
my $DeleteFaqFile = $ENV{'QUERY_STRING'};

my @Tmp = ();
my $Question = "";
my $Answer = "";
my $Respondent = "";
my $ImageFile = "";

open FAQFILE, "$TextFileDir/$DeleteFaqFile" or die "Error!!";
while (<FAQFILE>){
	@Tmp = <FAQFILE>;
}
close FAQFILE;

$Question = $Tmp[1];
$Answer = $Tmp[2];
$Respondent = $Tmp[3];
$ImageFile = $Tmp[4];

$ImageFile =~ s/\r\n//;
$ImageFile =~ s/\n//;

my $ImageTag = '<img src="' . "$ImageDir2/$ImageFile" .'">';

#フォームからデータ取得
&ReadParse(*in);
my $DeleteMode = $in{'DeleteMode'};#動作モードに関する情報取得（"1"で削除動作開始）


#以下、動作モードに応じた処理

#削除動作
if($DeleteMode == 1){
	unlink ("$TextFileDir/$DeleteFaqFile");
	
	if(length($ImageFile) == 0){}
	else{unlink ("$ImageDir/$ImageFile");}
	
	my $OutputHtml = "";
	open COMPLETE, "$TemplateDir/$CompHtml";
	while (<COMPLETE>){
		$OutputHtml .= $_;
	}
	close COMPLETE;
	
	if($RenewalTop == 1){
		&CreateNewsInfo();
		&RenewTopPage();
	}
	else{}
	
	print ("$OutputHtml");
}
elsif(length ($DeleteFaqFile) == 0){
	print ('<html><head>');
	print ('<meta http-equiv="refresh" content="0;URL=listfaq_news.cgi">');
	print ('</head><body></body></html>');
}
else{
	my $ConfirmForm = "";
	open CONFIRM, "$TemplateDir/$DeleteCheck";
	while (<CONFIRM>){
		$ConfirmForm .= $_;
	}
	close CONFIRM;
	
	$ConfirmForm =~ s/__QUESTION__TEXT__/$Question/;
	$ConfirmForm =~ s/__ANSWER__TEXT__/$Answer/;
	$ConfirmForm =~ s/__RESPONDENT__NAME__/$Respondent/;
	$ConfirmForm =~ s/__COLUMN__IMAGE__/$ImageTag/;
	$ConfirmForm =~ s/__DELETE__FILE__NAME__/$DeleteFaqFile/;
	
	print ("$ConfirmForm");
}

#以下サブルーチン
sub CreateNewsInfo{
	#各種設定開始
	my $NumberOfData = 3;#書き出しデータ数。デフォルトは3
	#各種設定終了
	my $OutputFormat = "";
	my @filelist = ();
	
	#ファイルリスト取得

	opendir NEWDIR, "$TextFileDir" or die "Directory open error!!";
	while (my $v = readdir(NEWDIR)){
		if($v =~/^($SaveFileHead.+)\.txt$/){push(@filelist,$v);}
	}
	
	@filelist = sort @filelist;
	@filelist = reverse @filelist;
	
	closedir NEWDIR;
	#ファイルからデータの読み込み
	for (my $count = 0;$count < $NumberOfData;$count++){
		my @TempOutput = ();
		open FAQFILE, "$TextFileDir/$filelist[$count]" or die "Error!!";
		@TempOutput = <FAQFILE>;
		close FAQFILE;
		
		chomp @TempOutput;
		$OutputFormat .= join(",",@TempOutput);
		$OutputFormat .= "\n";
	}
	
	#データの書き込み
	open NEWSINFO, ">$TextFileDir/$NewsInfo";
	print NEWSINFO ("$OutputFormat");
	close NEWSINFO;
	
	return 0;
}
sub RenewTopPage{
	#各種設定開始
	my $NumberOfWorkData = 5;#仕事情報書き出しデータ数。デフォルトは5。CreateNewWorkInfo内の同名の変数の値も変える必要あり。
	my $NumberOfNewsData = 3;#最新ニュース書き出しデータ数。デフォルトは3
	my $CatchLength = 26;#最大表示表示キャッチフレーズ長
	
	#ファイル、ディレクトリ情報
	my $TemplateDirectory = "../template/toppage";
	my $UsingWorkDataDirectory = "../secret/job";
	my $UsingNewsDataDirectory = "../secret/news";
	my $TopPageDirectory = "../../home";
	my $HtmlTemplateFile = "toppage.html";
	my $WorkTableTemplateFile = "toppagetable.html";
	my $WorkRowTemplateFile = "toppagetableelement.html";
	my $NewsTableTemplateFile = "toppagetable2.html";
	my $NewsRowTemplateFile = "toppagetableelement2.html";
	my $WorkDataFileName = "newworkinfo.csv";
	my $NewsDataFileName = "newsinfo.csv";
	my $OutputFileName = "index.html";
	
	#各種設定終了
	
	#更新用情報生成開始
	my $OutputHtml = "";
	
	my $TempWorkRowSrc = "";
	my $TempWorkRow = "";
	my $OutputWorkRowSrc = "";
	my $OutputWorkRow = "";
	my $OutputWorkTable = "";
	
	my @Catch = ();
	my @DetailNo = ();
	
	my $TempNewsRowSrc = "";
	my $TempNewsRow = "";
	my $OutputNewsRowSrc = "";
	my $OutputNewsRow = "";
	my $OutputNewsTable = "";
	
	my @NewsTopic = ();
	my @NewsDetail = ();
	
	#仕事データ読み込み
	open WORKINFORMATION, "$UsingWorkDataDirectory/$WorkDataFileName";
	while (<WORKINFORMATION>){
		my @TempWorkOutputInfo = split(/,/,$_);
		push (@Catch,"$TempWorkOutputInfo[2]");
		push (@DetailNo,"$TempWorkOutputInfo[14]");
	}
	close WORKINFORMATION;
	
	#仕事情報の行要素の整形
	open WORKROWDATA, "$TemplateDirectory/$WorkRowTemplateFile";
	while (<WORKROWDATA>){
		$TempWorkRowSrc .= $_;
	}
	close WORKROWDATA;
	
	for (my $workcount = 0;$workcount < $NumberOfWorkData;$workcount++){
		$OutputWorkRowSrc = "$TempWorkRowSrc";
		my $TempCatchData = shift @Catch;
		my $TempDetailNoData = shift @DetailNo;
		$OutputWorkRowSrc =~ s/__CATCH__/$TempCatchData/;
		$OutputWorkRowSrc =~ s/__DETAIL__NO__/$TempDetailNoData/g;
		$OutputWorkRow .= $OutputWorkRowSrc;
	}
	
	#仕事情報のテーブルの整形
	open WORKTABLEDATA, "$TemplateDirectory/$WorkTableTemplateFile";
	while (<WORKTABLEDATA>){
		$OutputWorkTable .= $_;
	}
	close WORKTABLEDATA;
	$OutputWorkTable =~ s/__TABLE__ELEMENT__/$OutputWorkRow/;
	
	
	#ニュースデータ読み込み
	open NEWSINFORMATION, "$UsingNewsDataDirectory/$NewsDataFileName";
	while (<NEWSINFORMATION>){
		my @TempNewsOutputInfo = split(/,/,$_);
		push (@NewsTopic,"$TempNewsOutputInfo[2]");
		push (@NewsDetail,"$TempNewsOutputInfo[0]");
	}
	close NEWSINFORMATION;
	
	#ニュースの行要素の整形
	open NEWSROWDATA, "$TemplateDirectory/$NewsRowTemplateFile";
	while (<NEWSROWDATA>){
		$TempNewsRowSrc .= $_;
	}
	close NEWSROWDATA;
	
	for (my $newscount = 0;$newscount < $NumberOfNewsData;$newscount++){
		$OutputNewsRowSrc = $TempNewsRowSrc;
		my $TempNewsTopicData = shift @NewsTopic;
		my $TempNewsDetailData = shift @NewsDetail;
		$OutputNewsRowSrc =~ s/__NEWS__MESSAGE__/$TempNewsTopicData/;
		$OutputNewsRowSrc =~ s/__DETAIL__FILE__/$TempNewsDetailData/g;
		$OutputNewsRow .= $OutputNewsRowSrc;
	}
	
	#ニュースのテーブルの整形
	open NEWSTABLEDATA, "$TemplateDirectory/$NewsTableTemplateFile";
	while (<NEWSTABLEDATA>){
		$OutputNewsTable .= $_;
	}
	close NEWSTABLEDATA;
	$OutputNewsTable =~ s/__TABLE__ELEMENT__/$OutputNewsRow/;
	
	
	#最終整形
	open FINAL, "$TemplateDirectory/$HtmlTemplateFile";
	while (<FINAL>){
		$OutputHtml .= $_;
	}
	close FINAL;
	
	$OutputHtml =~ s/__NEW__WORK__INFORMATION__/$OutputWorkTable/;
	$OutputHtml =~ s/__TOPPAGE__NEWS__/$OutputNewsTable/;
	
	#トップページ書き換え
	open OUTPUT, ">$TopPageDirectory/$OutputFileName";
	print OUTPUT ("$OutputHtml");
	close OUTPUT;
	
	return 0;

}