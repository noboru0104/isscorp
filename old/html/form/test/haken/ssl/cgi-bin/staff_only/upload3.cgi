#!/usr/bin/perl
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
$cgi_lib'maxdata    = 5000*1024*1024; 
&ReadParse;

$entry_cgi_path		="/cgi-bin/staff_only/upload.cgi";
$csv_dir		="../secret/staff_only/upload/";
$csv_path		=">>" . $csv_dir . "registered.csv";
$resume_dir		="../secret/staff_only/upload/";
$template_dir		="../template/staff_only/upload/";

$entry_html_path	=$template_dir . "index.html";
$error_html_path	=$template_dir . "error.html";
$thanks_html_path	=$template_dir . "thanks.html";
$csv_txt_path		=$template_dir . "csv.txt";

$epoch=time;
$name=$in{'name'};	$name=~s/\r\n/ /g;	$name=~s/\n/ /g;	$name=~s/\r/ /g;	$name=~s/,/./g;
$memo=$in{'memo'};	$memo=~s/\r\n/ /g;	$memo=~s/\n/ /g;	$memo=~s/\r/ /g;	$memo=~s/,/./g;

use MIME::Base64 ();

if(length $in{'name'}==0){
	$err.="名前は必須項目です。<br>";
}else{
	$encoded = MIME::Base64::encode($in{'name'});
}

if($in{'type'} eq '0'){
	$err.="種類は必須項目です。<br>";
}else{
	if($in{'type'} eq '1'){
		$type="resume";
	}elsif($in{'type'} eq '2'){
		$type="timesheet";
	}
}
if(length $in{'file1'}==0){
	$err.="ファイル1の添付は必須です。<br>";
}

if($in{'cmd'} eq 'send'){

	if( length $err > 0){
		open(TEMPLATE,$error_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		&jcode::euc2sjis(\$err);
		$strHtml=~s/__err__/$err/g;
		print "Content-type: text/html\n\n";
		print $strHtml;
		exit;
	}

#ファイル保存
	if( length $in{'file1'} >0){
		$tmp=$incfn{'file1'};
		$tmp =~ /(.*)\.([^\.]+$)/;
		$ext=$2;
		if(length $ext == 0 ){
			if($ctype{'file1'} eq "image/png"){
				$ext="png";
			}elsif($ctype{'file1'} eq "image/jpeg"){
				$ext="jpg";
			}elsif($ctype{'file1'} eq "image/jpg"){
				$ext="jpg";
			}elsif($ctype{'file1'} eq "application/pdf"){
				$ext="pdf";
			}elsif($ctype{'file1'} eq "application/x-stuffit"){
				$ext="sit";
			}elsif($ctype{'file1'} eq "application/x-zip-compressed"){
				$ext="zip";
			}elsif($ctype{'file1'} eq "application/msword"){
				$ext="doc";
			}elsif($ctype{'file1'} eq "application/vnd.ms-excel"){
				$ext="xls";
			}elsif($ctype{'file1'} eq "application/x-msexcel"){
				$ext="xls";
			}elsif($ctype{'file1'} eq "application/ms-excel"){
				$ext="xls";
			}elsif($ctype{'file1'} eq "application/vnd.ms-powerpoint"){
				$ext="ppt";
			}elsif($ctype{'file1'} eq "application/ms-powerpoint"){
				$ext="ppt";
			}
		}
		$file1=$type."_1_".$epoch."_".$encoded.".".$ext;
		$file1=~s/\n//g;
		$file1=~s/\r//g;
		open(ATTACH,">".$resume_dir.$file1);
		print ATTACH $in{file1};
		close(ATTACH);
	}

#ファイル保存
	if( length $in{'file2'} >0){
		$tmp=$incfn{'file2'};
		$tmp =~ /(.*)\.([^\.]+$)/;
		$ext=$2;
		if(length $ext == 0 ){
			if($ctype{'file2'} eq "image/png"){
				$ext="png";
			}elsif($ctype{'file2'} eq "image/jpeg"){
				$ext="jpg";
			}elsif($ctype{'file2'} eq "image/jpg"){
				$ext="jpg";
			}elsif($ctype{'file2'} eq "application/pdf"){
				$ext="pdf";
			}elsif($ctype{'file2'} eq "application/x-stuffit"){
				$ext="sit";
			}elsif($ctype{'file2'} eq "application/x-zip-compressed"){
				$ext="zip";
			}elsif($ctype{'file2'} eq "application/msword"){
				$ext="doc";
			}elsif($ctype{'file2'} eq "application/vnd.ms-excel"){
				$ext="xls";
			}elsif($ctype{'file2'} eq "application/x-msexcel"){
				$ext="xls";
			}elsif($ctype{'file2'} eq "application/ms-excel"){
				$ext="xls";
			}elsif($ctype{'file2'} eq "application/vnd.ms-powerpoint"){
				$ext="ppt";
			}elsif($ctype{'file2'} eq "application/ms-powerpoint"){
				$ext="ppt";
			}
		}
		$file2=$type."_2_".$epoch."_".$encoded.".".$ext;
		$file2=~s/\n//g;
		$file2=~s/\r//g;
		open(ATTACH,">".$resume_dir.$file2);
		print ATTACH $in{file2};
		close(ATTACH);
	}

	open(CSV_TEMPLATE,$csv_txt_path);
	@entry=<CSV_TEMPLATE>;
	close(CSV_TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	$strHtml.="\n";

	open(CSV,$csv_path);
	print CSV $strHtml;
	close(CSV);

	open(TEMPLATE,$thanks_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	print "Content-type: text/html\n\n";
	print $strHtml;

	exit;
}else{

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print $strHtml;
}

sub replace{
	$strHtml=~s/__epoch__/$epoch/g;
	$strHtml=~s/__name__/$name/g;
	$strHtml=~s/__type__/$type/g;
	$strHtml=~s/__file1__/$file1/g;
	$strHtml=~s/__file2__/$file2/g;
	$strHtml=~s/__memo__/$memo/g;
}

sub is_zenkaku{
	$tmp = $_[0];
	&jcode::sjis2euc(\$tmp);
	if($tmp !~ /[\x00-\x7f]/){
		return 1;
	}else{
		return 0;
	}
}

sub is_hankaku{
	$tmp = $_[0];
	if($tmp !~ /[\x80-\xff]/){
		return 1;
	}else{
		return 0;
	}
}

sub enq_radio{
	$temp=$_[0];
	$temp=~s/\n//g;
	if(length $temp > 0){
		if($_[3]==$_[2]){
			$enq.="<input type=radio name=".$_[1]." value=".$_[3]." checked>".$temp;
		}else{
			$enq.="<input type=radio name=".$_[1]." value=".$_[3].">".$temp;
		}
	};
}

