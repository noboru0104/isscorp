#!/usr/bin/perl
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
#require '../../../../perl_lib/jcode.pl';

#require '../../../../perl_lib/cgi-lib.pl';
$cgi_lib'maxdata    = 5000*1024*1024; 
&ReadParse;

$FROM		='touroku@isssc.co.jp';
#$TO		='touroku@isssc.co.jp';
$TO		='ichino@si-seed.com';
#$TO2		='yumiko@loveworkaholic.com';

$Subject 		= "ISS ONLINE FORM(nikkei woman)";

#$entry_cgi_path		="/isssc/cgi-bin/entry/entry.cgi";
$entry_cgi_path		="/cgi-bin/entry/entry.cgi";

#$csv_dir		="../../../secret/entry/";
$csv_dir		="../secret/entry/";
$csv_path		=">>" . $csv_dir . "registered.csv";
$enq1_path		=$csv_dir . "enq1.txt";
$enq2_path		=$csv_dir . "enq2.txt";
$enq3_path		=$csv_dir . "enq3.txt";
$enq4_path		=$csv_dir . "enq4.txt";

#$resume_dir		="../../../secret/resume/";
$resume_dir		="../secret/resume/";

#$template_dir		="../../../template/entry/";
$template_dir		="../template/entry/";
$confirm_html_path	=$template_dir . "confirm.html";
$entry_html_path	=$template_dir . "entry.html";
$thanks_html_path	=$template_dir . "thanks.html";

$pref_txt_path		=$template_dir . "pref.txt";
$moyori_type_txt_path	=$template_dir . "moyori_type.txt";
$gakureki_txt_path	=$template_dir . "gakureki,txt";
$start_year_txt_path	=$template_dir . "start_year.txt";
$shokushu_txt_path	=$template_dir . "shokushu.txt";
$keitai_txt_path	=$template_dir . "keitai.txt";
$word_txt_path		=$template_dir . "word.txt";
$excel_txt_path		=$template_dir . "excel.txt";
$powerpoint_txt_path	=$template_dir . "powerpoint.txt";
$access_txt_path	=$template_dir . "access.txt";
$rusu_txt_path		=$template_dir . "rusu.txt";
$renraku_txt_path	=$template_dir . "renraku.txt";
$shoukai_txt_path	=$template_dir . "shoukai.txt";
$sat_type_txt_path	=$template_dir . "sat_type.txt";
$smoke_txt_path		=$template_dir . "smoke.txt";
$csv_txt_path		=$template_dir . "csv.txt";
$hakensaki_type_txt_path=$template_dir . "hakensaki_type.txt";
$hakensaki_kibo_txt_path=$template_dir . "hakensaki_kibo.txt";


$epoch=time;

$hakensaki_gyousyu_def	="例：金融・メディカル・IT通信・コンピュータ・システム・\nメーカー（自動車・アパレル・飲料/食品・化学）・\nエンターテイメント・PR・イベント・問わない";
&jcode::euc2sjis(\$hakensaki_gyousyu_def);
$tsuyaku_gakureki_def	=" 記入例\n2002.4?2004.3　●●通訳スクール　通訳コース　同時通訳科\n2004.4?現在　●●翻訳学校　翻訳コース　金融翻訳クラス";
&jcode::euc2sjis(\$tsuyaku_gakureki_def);

$job_id=$in{'job_id'};
$job_name=$in{'job_name'};


	$enq01ans=$in{'enq01ans'};	$enq01ans=~s/\r//g;	$enq01ans=~s/\n//g;	$enq01ans=~s/,/./g;
	$enq01text=$in{'enq01text'};	$enq01text=~s/\r//g;	$enq01text=~s/\n//g;	$enq01text=~s/,/./g;
	open(ENQ1,$enq1_path);		@entry=<ENQ1>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq01ans==1){$enq01sel=$ans01};
	$ans02=shift @entry;	if($enq01ans==2){$enq01sel=$ans02};
	$ans03=shift @entry;	if($enq01ans==3){$enq01sel=$ans03};
	$ans04=shift @entry;	if($enq01ans==4){$enq01sel=$ans04};
	$ans05=shift @entry;	if($enq01ans==5){$enq01sel=$ans05};
	$ans06=shift @entry;	if($enq01ans==6){$enq01sel=$ans06};
	$ans07=shift @entry;	if($enq01ans==7){$enq01sel=$ans07};
	$ans08=shift @entry;	if($enq01ans==8){$enq01sel=$ans08};
	$ans09=shift @entry;	if($enq01ans==9){$enq01sel=$ans09};
	$ans10=shift @entry;	if($enq01ans==10){$enq01sel=$ans10};
	$enq01sel=~s/\r//g;	$enq01sel=~s/\n//g;	$enq01sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\n//g;	$qes=~s/\r//g;	$qes=~s/,/./g;
	close(ENQ1);
	if($sel_type>0){
		$enq01qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq01text==0 ){						$err_enq01="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq01ans==0 )||($enq01ans==0) ){				$err_enq01="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq01ans==0 )||($enq01ans==0))&&(length $enq01text==0 ) ){	$err_enq01="<font color=red>←この設問は必須項目です。</font>";	}}
			&jcode::euc2sjis(\$err_enq01);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){

		enq_radio($ans01,'enq01ans',$enq01ans,'1');
		enq_radio($ans02,'enq01ans',$enq01ans,'2');
		enq_radio($ans03,'enq01ans',$enq01ans,'3');
		enq_radio($ans04,'enq01ans',$enq01ans,'4');
		enq_radio($ans05,'enq01ans',$enq01ans,'5');
		enq_radio($ans06,'enq01ans',$enq01ans,'6');
		enq_radio($ans07,'enq01ans',$enq01ans,'7');
		enq_radio($ans08,'enq01ans',$enq01ans,'8');
		enq_radio($ans09,'enq01ans',$enq01ans,'9');
		enq_radio($ans10,'enq01ans',$enq01ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq01text>".$enq01text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq=$tmp;
	}
	$enq01.=$enq;

	$enq02ans=$in{'enq02ans'};	$enq02ans=~s/\r//g;	$enq02ans=~s/\n//g;	$enq02ans=~s/,/./g;
	$enq02text=$in{'enq02text'};	$enq02text=~s/\r//g;	$enq02text=~s/\n//g;	$enq02text=~s/,/./g;
	open(ENQ2,$enq2_path);		@entry=<ENQ2>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq02ans==1){$enq02sel=$ans01};
	$ans02=shift @entry;	if($enq02ans==2){$enq02sel=$ans02};
	$ans03=shift @entry;	if($enq02ans==3){$enq02sel=$ans03};
	$ans04=shift @entry;	if($enq02ans==4){$enq02sel=$ans04};
	$ans05=shift @entry;	if($enq02ans==5){$enq02sel=$ans05};
	$ans06=shift @entry;	if($enq02ans==6){$enq02sel=$ans06};
	$ans07=shift @entry;	if($enq02ans==7){$enq02sel=$ans07};
	$ans08=shift @entry;	if($enq02ans==8){$enq02sel=$ans08};
	$ans09=shift @entry;	if($enq02ans==9){$enq02sel=$ans09};
	$ans10=shift @entry;	if($enq02ans==10){$enq02sel=$ans10};
	$enq02sel=~s/\r//g;	$enq02sel=~s/\n//g;	$enq02sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\r//g;	$qes=~s/\n//g;	$qes=~s/,/./g;
	close(ENQ2);
	if($sel_type>0){
		$enq02qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq02text==0 ){						$err_enq02="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq02ans==0 )||($enq02ans==0) ){				$err_enq02="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq02ans==0 )||($enq02ans==0))&&(length $enq02text==0 ) ){	$err_enq02="<font color=red>←この設問は必須項目です。</font>";	}}
		&jcode::euc2sjis(\$err_enq02);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq02ans',$enq02ans,'1');
		enq_radio($ans02,'enq02ans',$enq02ans,'2');
		enq_radio($ans03,'enq02ans',$enq02ans,'3');
		enq_radio($ans04,'enq02ans',$enq02ans,'4');
		enq_radio($ans05,'enq02ans',$enq02ans,'5');
		enq_radio($ans06,'enq02ans',$enq02ans,'6');
		enq_radio($ans07,'enq02ans',$enq02ans,'7');
		enq_radio($ans08,'enq02ans',$enq02ans,'8');
		enq_radio($ans09,'enq02ans',$enq02ans,'9');
		enq_radio($ans10,'enq02ans',$enq02ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq02text>".$enq02text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq.=$tmp;
	}
	$enq02.=$enq;

	$enq03ans=$in{'enq03ans'};	$enq03ans=~s/\r//g;	$enq03ans=~s/\n//g;	$enq03and=~s/,/./g;
	$enq03text=$in{'enq03text'};	$enq03text=~s/\r//g;	$enq03text=~s/\n//g;	$enq03text=~s/,/./g;
	open(ENQ3,$enq3_path);		@entry=<ENQ3>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq03ans==1){$enq03sel=$ans01};
	$ans02=shift @entry;	if($enq03ans==2){$enq03sel=$ans02};
	$ans03=shift @entry;	if($enq03ans==3){$enq03sel=$ans03};
	$ans04=shift @entry;	if($enq03ans==4){$enq03sel=$ans04};
	$ans05=shift @entry;	if($enq03ans==5){$enq03sel=$ans05};
	$ans06=shift @entry;	if($enq03ans==6){$enq03sel=$ans06};
	$ans07=shift @entry;	if($enq03ans==7){$enq03sel=$ans07};
	$ans08=shift @entry;	if($enq03ans==8){$enq03sel=$ans08};
	$ans09=shift @entry;	if($enq03ans==9){$enq03sel=$ans09};
	$ans10=shift @entry;	if($enq03ans==10){$enq03sel=$ans10};
	$enq03sel=~s/\r\n//g;	$enq03sel=~s/\n//g;	$enq03sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\n//g;	$qes=~s/\r//g;	$qes=~s/,/./g;
	close(ENQ3);
	if($sel_type>0){
		$enq03qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq03text==0 ){						$err_enq03="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq03ans==0 )||($enq03ans==0) ){				$err_enq03="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq03ans==0 )||($enq03ans==0))&&(length $enq03text==0 ) ){	$err_enq03="<font color=red>←この設問は必須項目です。</font>";	}}
			&jcode::euc2sjis(\$err_enq03);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq03ans',$enq03ans,'1');
		enq_radio($ans02,'enq03ans',$enq03ans,'2');
		enq_radio($ans03,'enq03ans',$enq03ans,'3');
		enq_radio($ans04,'enq03ans',$enq03ans,'4');
		enq_radio($ans05,'enq03ans',$enq03ans,'5');
		enq_radio($ans06,'enq03ans',$enq03ans,'6');
		enq_radio($ans07,'enq03ans',$enq03ans,'7');
		enq_radio($ans08,'enq03ans',$enq03ans,'8');
		enq_radio($ans09,'enq03ans',$enq03ans,'9');
		enq_radio($ans10,'enq03ans',$enq03ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq03text>".$enq03text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq.=$tmp;
	}
	$enq03=$enq;

	$enq04ans=$in{'enq04ans'};	$enq04ans=~s/\r//g;	$enq04ans=~s/\n//g;	$enq04ans=~s/,/./g;
	$enq04text=$in{'enq04text'};	$enq04text=~s/\r//g;	$enq04text=~s/\n//g;	$enq04text=~s/,/./g;
	open(ENQ4,$enq4_path);		@entry=<ENQ4>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq04ans==1){$enq04sel=$ans01};
	$ans02=shift @entry;	if($enq04ans==2){$enq04sel=$ans02};

	$ans03=shift @entry;	if($enq04ans==3){$enq04sel=$ans03};
	$ans04=shift @entry;	if($enq04ans==4){$enq04sel=$ans04};
	$ans05=shift @entry;	if($enq04ans==5){$enq04sel=$ans05};
	$ans06=shift @entry;	if($enq04ans==6){$enq04sel=$ans06};
	$ans07=shift @entry;	if($enq04ans==7){$enq04sel=$ans07};
	$ans08=shift @entry;	if($enq04ans==8){$enq04sel=$ans08};
	$ans09=shift @entry;	if($enq04ans==9){$enq04sel=$ans09};
	$ans10=shift @entry;	if($enq04ans==10){$enq04sel=$ans10};
	$enq04sel=~s/\n//g;$enq04sel=~s/\r\n//g;	$enq04sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\r//g;	$qes=~s/\n//g;		$qes=~s/,/./g;
	close(ENQ4);
	if($sel_type>0){
		$enq04qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq04text==0 ){						$err_enq04="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq04ans==0 )||($enq04ans==0) ){				$err_enq04="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq04ans==0 )||($enq04ans==0))&&(length $enq04text==0 ) ){	$err_enq04="<font color=red>←この設問は必須項目です。</font>";	}}
			&jcode::euc2sjis(\$err_enq04);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq04ans',$enq04ans,'1');
		enq_radio($ans02,'enq04ans',$enq04ans,'2');
		enq_radio($ans03,'enq04ans',$enq04ans,'3');
		enq_radio($ans04,'enq04ans',$enq04ans,'4');
		enq_radio($ans05,'enq04ans',$enq04ans,'5');
		enq_radio($ans06,'enq04ans',$enq04ans,'6');
		enq_radio($ans07,'enq04ans',$enq04ans,'7');
		enq_radio($ans08,'enq04ans',$enq04ans,'8');
		enq_radio($ans09,'enq04ans',$enq04ans,'9');
		enq_radio($ans10,'enq04ans',$enq04ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq04text>".$enq04text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq.=$tmp;
	}
	$enq04=$enq;



if(length $in{'cmd'} eq 0){
	$renraku='1';
	$hakensaki_gyousyu=$hakensaki_gyousyu_def;
	$tsuyaku_gakureki=$tsuyaku_gakureki_def;
}else{

	$name_A1=$in{'name_A1'};	$name_A1=~s/\r\n/ /g;	$name_A1=~s/,/./g;
	$name_B1=$in{'name_B1'};	$name_B1=~s/\r\n/ /g;	$name_B1=~s/,/./g;
	if(length $name_A1 eq 0)	{$err_name_1='<font color=red>←名前(姓)は必須項目です</font>' ;}
	elsif(!is_zenkaku($name_A1))	{$err_name_1='<font color=red>←名前(姓)は全角でお願いします</font>' ;}
	elsif(length $name_B1 eq 0)	{$err_name_1='<font color=red>←名前(名)は必須項目です</font>' ;}
	elsif(!is_zenkaku($name_B1))	{$err_name_1='<font color=red>←名前(名)は全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name_1);

	$name_A2=$in{'name_A2'};	$name_A2=~s/\r\n/ /g;	$name_A2=~s/,/./g;
	$name_B2=$in{'name_B2'};	$name_B2=~s/\r\n/ /g;	$name_B2=~s/,/./g;

	if(length $name_A2 eq 0)	{$err_name_2='<font color=red>←ふりがなは必須項目です</font>' ;}
	elsif(!is_zenkaku($name_A2))	{$err_name_2='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	elsif(length $name_B2 eq 0)	{$err_name_2='<font color=red>←ふりがなは必須項目です</font>' ;}
	elsif(!is_zenkaku($name_B2))	{$err_name_2='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name_2);

	$name_A3=$in{'name_A3'};	$name_A3=~s/\r\n/ /g;	$name_A3=~s/,/./g;
	$name_B3=$in{'name_B3'};	$name_B3=~s/\r\n/ /g;	$name_B3=~s/,/./g;
	if(!is_zenkaku($name_A3))	{$err_name_3='<font color=red>←旧姓(姓)は全角でお願いします</font>' ;}
	elsif(!is_zenkaku($name_B3))	{$err_name_3='<font color=red>←旧姓(名)は全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name_3);

	$name_A4=$in{'name_A4'};	$name_A4=~s/\r\n/ /g;	$name_A4=~s/,/./g;
	$name_B4=$in{'name_B4'};	$name_B4=~s/\r\n/ /g;	$name_B4=~s/,/./g;
	if(!is_zenkaku($name_A4))	{$err_name_4='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	elsif(!is_zenkaku($name_B4))	{$err_name_4='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name_4);

	$name_A5=$in{'name_A5'};	$name_A5=~s/\r\n/ /g;	$name_A5=~s/,/./g;
	$name_B5=$in{'name_B5'};	$name_B5=~s/\r\n/ /g;	$name_B5=~s/,/./g;
	if(length $name_A5 eq 0)	{$err_name_5='<font color=red>←戸籍名(姓)は必須項目です</font>' ;}
	elsif(!is_zenkaku($name_A5))	{$err_name_5='<font color=red>←戸籍名(姓)は全角でお願いします</font>' ;}
	if(length $name_B5 eq 0)	{$err_name_5='<font color=red>←戸籍名(名)は必須項目です</font>' ;}
	elsif(!is_zenkaku($name_B5))	{$err_name_5='<font color=red>←戸籍名(名)は全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name_5);

	$name_A6=$in{'name_A6'};	$name_A6=~s/\r\n/ /g;	$name_A6=~s/,/./g;
	$name_B6=$in{'name_B6'};	$name_B6=~s/\r\n/ /g;	$name_B6=~s/,/./g;
	if(length $name_A6 eq 0)	{$err_name_6='<font color=red>←ふりがなは必須項目です</font>' ;}
	elsif(!is_zenkaku($name_A6))	{$err_name_6='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	elsif(length $name_B6 eq 0)	{$err_name_6='<font color=red>←ふりがなは必須項目です</font>' ;}
	elsif(!is_zenkaku($name_B6))	{$err_name_6='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name_6);

	$birth_day_yy=$in{'birth_day_yy'};	$birth_day_yy=~s/\r\n/ /g;	$birth_day_yy=~s/,/./g;
	$birth_day_mm=$in{'birth_day_mm'};	$birth_day_mm=~s/\r\n/ /g;	$birth_day_mm=~s/,/./g;
	$birth_day_dd=$in{'birth_day_dd'};	$birth_day_dd=~s/\r\n/ /g;	$birth_day_dd=~s/,/./g;
	if(length $birth_day_yy eq 0)		{$err_birth_day='<font color=red>←生年月日(年)は必須項目です</font>' ;}
	elsif(!is_hankaku($birth_day_yy))	{$err_birth_day='<font color=red>←生年月日(年)は半角でお願いします</font>' ;}
	elsif(length $birth_day_mm eq 0)	{$err_birth_day='<font color=red>←生年月日(月)は必須項目です</font>' ;}
	elsif(!is_hankaku($birth_day_mm))	{$err_birth_day='<font color=red>←生年月日(月)は半角でお願いします</font>' ;}
	elsif(length $birth_day_dd eq 0)	{$err_birth_day='<font color=red>←生年月日(日)は必須項目です</font>' ;}
	elsif(!is_hankaku($birth_day_dd))	{$err_birth_day='<font color=red>←生年月日(日)は半角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_birth_day);

	$age=$in{'age'};			$age=~s/\r\n/ /g;	$age=~s/,/./g;
	if(length $age eq 0)		{$err_age='<font color=red>←年齢は必須項目です</font>' ;}
	elsif(!is_hankaku($age))	{$err_age='<font color=red>←年齢は半角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_age);
	
	$sex=$in{'sex'};			$sex=~s/\r\n/ /g;	$sex=~s/,/./g;
	if	(length $sex eq 0)	{$err_sex='<font color=red>性別は必須項目です</font>';}
	if	($sex eq 'man')		{$sex_kanji='男性';$sex_eng='man';}
	elsif	($sex eq 'woman')	{$sex_kanji='女性';$sex_eng='woman';}
	&jcode::euc2sjis(\$sex_kanji);
	&jcode::euc2sjis(\$err_sex);

	$zip1=$in{'zip1'};			$zip1=~s/\r\n/ /g;	$zip1=~s/,/./g;
	$zip2=$in{'zip2'};			$zip2=~s/\r\n/ /g;	$zip2=~s/,/./g;
	if(length $zip1 eq 0)		{$err_zip='<font color=red>←郵便番号は必須項目です</font>' ;}
	elsif(length $zip2 eq 0)	{$err_zip='<font color=red>←郵便番号は必須項目です</font>' ;}
	elsif(!is_hankaku($zip1))	{$err_zip='<font color=red>←郵便番号は半角でお願いします</font>' ;}
	elsif(!is_hankaku($zip2))	{$err_zip='<font color=red>←郵便番号は半角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_zip);

	if	(length $in{'pref'}  eq 0)	{$err_pref='<font color=red>都道府県は必須項目です</font>';}
	open ( FH_PREF , $pref_txt_path ) ;for ( $a = 0 ; $a <= $in{'pref'} ; $a++ ){$pref=<FH_PREF>;$pref=~s/\n//g;}close(FH_PREF);

	$address1	=$in{'address1'};	$address1=~s/\r\n/ /g;	$address1=~s/,/./g;
	if(length $address1 == 0)	{$err_address1='<font color=red>←市区町村は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_address1);

	$address2	=$in{'address2'};	$address2=~s/\r\n/ /g;	$address2=~s/,/./g;
	if(length $address2 == 0)	{$err_address2='<font color=red>←番地は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_address2);

	$address3	=$in{'address3'};	$address3=~s/\r\n/ /g;	$address3=~s/,/./g;
	if(length $address3 == 0)	{$err_address3='<font color=red>←建物・部屋番号は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_address3);

	$moyori_rosen	=$in{'moyori_rosen'};	$moyori_rosen=~s/\r\n/ /g;	$moyori_rosen=~s/,/./g;
	$moyori_eki	=$in{'moyori_eki'};	$moyori_eki=~s/\r\n/ /g;		$moyori_eki=~s/,/./g;
	$moyori_min	=$in{'moyori_min'};	$moyori_min=~s/\r\n/ /g;		$moyori_min=~s/,/./g;
	if(length $moyori_rosen eq 0)	{$err_moyori='<font color=red>←最寄り駅（路線）は必須項目です</font>' ;}
	if(length $moyori_eki eq 0)	{$err_moyori='<font color=red>←最寄り駅（駅）は必須項目です</font>' ;}
	if(length $moyori_min eq 0)	{$err_moyori='<font color=red>←最寄り駅へのアクセス時間は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_moyori);

	if(length $in{'moyori_type'} eq 0)	{$err_moyori_type='<font color=red>←最寄り駅へのアクセス方法は必須項目です</font>' ;}
	open ( FH_MOYORI_TYPE, $moyori_type_txt_path ) ;for ( $a = 0 ; $a <= $in{'moyori_type'} ; $a++ ){$moyori_type=<FH_MOYORI_TYPE>;$moyori_type=~s/\n//g;}close(FH_MOYORI_TYPE);

	$tel1		=$in{'tel1'};	$tel1=~s/\r\n/ /g;		$tel1=~s/,/./g;
	$tel2		=$in{'tel2'};	$tel2=~s/\r\n/ /g;		$tel2=~s/,/./g;
	$tel3		=$in{'tel3'};	$tel3=~s/\r\n/ /g;		$tel3=~s/,/./g;
	open ( FH_TEL_RUSU, $rusu_txt_path ) ;for ( $a = 0 ; $a <= $in{'tel_rusu'} ; $a++ ){$tel_rusu=<FH_TEL_RUSU>;$tel_rusu=~s/\n//g;}close(FH_TEL_RUSU);	
#$tel_rusu	=$in{'tel_rusu'};
	if(length $tel1 eq 0)		{$err_tel='<font color=red>←市外局番は必須項目です</font>' ;}
	elsif(!is_hankaku($tel1))	{$err_tel='<font color=red>←市外局番は半角でお願いします</font>' ;}
	elsif(length $tel2 eq 0)	{$err_tel='<font color=red>←市内局番は必須項目です/font>' ;}
	elsif(!is_hankaku($tel2))	{$err_tel='<font color=red>←市内局番は半角でお願いします</font>' ;}
	elsif(length $tel3 eq 0)	{$err_tel='<font color=red>←自宅電話番号は必須項目です</font>' ;}
	elsif(!is_hankaku($tel3))	{$err_tel='<font color=red>←自宅電話番号は半角でお願いします</font>' ;}
	elsif(length $tel_rusu eq 0)	{$err_tel='<font color=red>←留守電の有無は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_tel);

	$mob1		=$in{'mob1'};	$mob1=~s/\r\n/ /g;		$mob1=~s/,/./g;
	$mob2		=$in{'mob2'};	$mob2=~s/\r\n/ /g;		$mob2=~s/,/./g;
	$mob3		=$in{'mob3'};	$mob3=~s/\r\n/ /g;		$mob3=~s/,/./g;
	open ( FH_MOB_RUSU, $rusu_txt_path ) ;for ( $a = 0 ; $a <= $in{'mob_rusu'} ; $a++ ){$tel_rusu=<FH_MOB_RUSU>;$tel_rusu=~s/\n//g;}close(FH_MOB_RUSU);	
#	$mob_rusu	=$in{'mob_rusu'};

	$fax1		=$in{'fax1'};	$fax1=~s/\r\n/ /g;		$fax1=~s/,/./g;
	$fax2		=$in{'fax2'};	$fax2=~s/\r\n/ /g;		$fax2=~s/,/./g;
	$fax3		=$in{'fax3'};	$fax3=~s/\r\n/ /g;		$fax3=~s/,/./g;


	$mail_A=$in{'mail_A'};		$mail_A=~s/\r\n/ /g;		$mail_A=~s/,/./g;
	$mail_B=$in{'mail_B'};		$mail_B=~s/\r\n/ /g;		$mail_B=~s/,/./g;
	if(length $mail_A eq 0)	{$err_mail_A='<font color=red>←メールアドレスは必須項目です</font>' ;}
	elsif(!is_hankaku($mail_A))	{$err_mail_A='<font color=red>←メールアドレスは半角でお願いします</font>' ;}
	elsif($mail_A ne $mail_B)	{$err_mail_A='<font color=red>←メールアドレスが一致しません<font>' ;	$err_mail_B='<font color=red>←メールアドレスが一致しません<font>' ;}
	&jcode::euc2sjis(\$err_mail_A);
	&jcode::euc2sjis(\$err_mail_B);
	$TO_user=$mail_A;

	open ( FH_RENRAKU , $renraku_txt_path ) ;for ( $a = 0 ; $a <= $in{'renraku'} ; $a++ ){$renraku=<FH_RENRAKU>;$renraku=~ s/\n//g;}close(FH_RENRAKU);
	if(length $in{'renraku'} eq 0)	{$err_tel_rusu='<font color=red>←連絡方法は必須項目です</font>' ;}

	open ( FH_STARTYEAR ,$start_year_txt_path ) ;for ( $a = 0 ; $a <= $in{'start_year'} ; $a++ ){$start_year=<FH_STARTYEAR>;$start_year=~ s/\n//g;}close(FH_STARTYEAR);

	$start_month=$in{'start_month'};
	$start_month='未定' if($start_month eq 13);
	&jcode::euc2sjis(\$start_month);
	$start_day=$in{'start_day'};
	$start_day='未定' if($start_day eq 32);
	&jcode::euc2sjis(\$start_day);

	if($in{'start_year'} eq 0)	{$err_start='<font color=red>←希望勤務開始日(年)は必須です。</font>' ;}
	elsif($in{'start_month'} eq 0)	{$err_start='<font color=red>←希望勤務開始日(月)は必須です。</font>' ;}
	elsif($in{'start_day'} eq 0)	{$err_start='<font color=red>←希望勤務開始日(日)は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_start);


	$style1=$in{'style1'};
	$style2=$in{'style2'};
	$style3=$in{'style3'};
	if( (length $style1 == 0 ) &&  (length $style2 ==0) && (length $style3==0 )	){
		$err_style='<font color=red>←就業形態は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_style);
	open ( FH_SHOUKAI , $shoukai_txt_path ) ;for ( $a = 0 ; $a <= $in{'shoukai'} ; $a++ ){$shoukai=<FH_SHOUKAI>;$shoukai=~ s/\n//g;}close(FH_SHOUKAI);

	$time1=$in{'time1'};	$time1=~s/\r\n/ /g;		$time1=~s/,/./g;
	$time2=$in{'time2'};	$time2=~s/\r\n/ /g;		$time2=~s/,/./g;
	$time3=$in{'time3'};	$time3=~s/\r\n/ /g;		$time3=~s/,/./g;
	$time4=$in{'time4'};	$time4=~s/\r\n/ /g;		$time4=~s/,/./g;
	$time_type=$in{'time_type'};
	if(length $time1 eq 0)		{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	elsif(length $time2 eq 0)	{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	elsif(length $time3 eq 0)	{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	elsif(length $time4 eq 0)	{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_time);

	$wday_mon=$in{'wday_mon'};
	$wday_tue=$in{'wday_tue'};
	$wday_wed=$in{'wday_wed'};
	$wday_thu=$in{'wday_thu'};
	$wday_fri=$in{'wday_fri'};
	$wday_sat=$in{'wday_sat'};
	$wday_sun=$in{'wday_sun'};
	if(	(length $wday_mon==0) && (length $wday_tue==0) && (length $wday_wed ==0) && (length $wday_thu==0) && 
		(length $wday_fri==0) && (length $wday_sat==0) && (length $wday_sun==0)		){
		$err_wday='<font color=red>←希望勤務曜日は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_wday);

	open ( FH_SATTYPE , $sat_type_txt_path ) ;for ( $a = 0 ; $a <= $in{'sat_type'} ; $a++ ){$sat_type=<FH_SATTYPE>;$sat_type=~ s/\n//g;}close(FH_SATTYPE);
	open ( FH_SMOKE , $smoke_txt_path ) ;for ( $a = 0 ; $a <= $in{'smoke'} ; $a++ ){$smoke=<FH_SMOKE>;$smoke=~ s/\n//g;}close(FH_SMOKE);

	$jikyu=$in{'jikyu'};		$jikyu=~s/\r\n/ /g;		$jikyu=~s/,/./g;

	if(length $jikyu eq 0 ) 	{$err_jikyu='<font color=red>←希望時給は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_jikyu);

	$koutsuhi=$in{'koutsuhi'};

	open ( FH_SHOKUSHU1 , $shokushu_txt_path ) ;for ( $a = 0 ; $a <= $in{'shokushu1'} ; $a++ ){$shokushu1=<FH_SHOKUSHU1>;$shokushu1=~ s/\n//g;}close(FH_SHOKUSHU1);
	if($in{'shokushu1'} eq 0 )	{$err_shokushu='<font color=red>←希望職種は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_shokushu);

	open ( FH_SHOKUSHU2 , $shokushu_txt_path ) ;for ( $a = 0 ; $a <= $in{'shokushu2'} ; $a++ ){$shokushu2=<FH_SHOKUSHU2>;$shokushu2=~ s/\n//g;}close(FH_SHOKUSHU2);

	open ( FH_HAKENSAKITYPE , $hakensaki_type_txt_path ) ;for ( $a = 0 ; $a <= $in{'hakensaki_type'} ; $a++ ){$hakensaki_type=<FH_SHOKUSHU2>;$hakensaki_type=~ s/\n//g;}close(FH_HAKENSAKITYPE);

	open ( FH_HAKENSAKIKIBO, $hakensaki_kibo_txt_path ) ;for ( $a = 0 ; $a <= $in{'hakensaki_kibo'} ; $a++ ){$hakensaki_kibo=<FH_GAKUREKI>;$hakensaki_kibo=~ s/\n//g;}close(FH_HAKENSAKIKIBO);

	$haken_gyousyu=$in{'haken_gyousyu'};
	$haken_gyousyu=~s/\r\n/ /g;
	$haken_gyousyu=~s/,/./g;

	$saketai_gyousyu=$in{'saketai_gyousyu'};
	$saketai_gyousyu=~s/\r\n/ /g;
	$saketai_gyousyu=~s/,/./g;

	$kinmuchi=$in{'kinmuchi'};			$kinmuchi=~s/\r\n/ /g;		$kinmuchi=~s/,/./g;
	$shoyouzikan=$in{'shoyouzikan'};		$shoyouzikan=~s/\r\n/ /g;		$shoyouzikan=~s/,/./g;

	open ( FH_GAKUREKI ,$gakureki_txt_path ) ;for ( $a = 0 ; $a <= $in{'gakureki'} ; $a++ ){$gakureki=<FH_GAKUREKI>;$gakureki=~ s/\n//g;}close(FH_GAKUREKI);
	
	$ryugaku1_kokumei=$in{'ryugaku1_kokumei'};	$ryugaku1_kokumei=~s/\r\n/ /g;	$ryugaku1_kokumei=~s/,/./g;
	$ryugaku1_school=$in{'ryugaku1_school'};	$ryugaku1_school=~s/\r\n/ /g;	$ryugaku1_school=~s/,/./g;
	$ryugaku1_start_yy=$in{'ryugaku1_start_yy'};	$ryugaku1_start_yy=~s/\r\n/ /g;	$ryugaku1_start_yy=~s/,/./g;
	$ryugaku1_start_mm=$in{'ryugaku1_start_mm'};	$ryugaku1_start_mm=~s/\r\n/ /g;	$ryugaku1_start_mm=~s/,/./g;
	$ryugaku1_end_yy=$in{'ryugaku1_end_yy'};	$ryugaku1_end_yy=~s/\r\n/ /g;	$ryugaku1_end_yy=~s/,/./g;
	$ryugaku1_end_mm=$in{'ryugaku1_end_mm'};	$ryugaku1_end_mm=~s/\r\n/ /g;	$ryugaku1_end_mm=~s/,/./g;

	$ryugaku2_kokumei=$in{'ryugaku1_kokumei'};	$ryugaku2_kokumei=~s/\r\n/ /g;	$ryugaku2_kokumei=~s/,/./g;
	$ryugaku2_school=$in{'ryugaku1_school'};	$ryugaku2_school=~s/\r\n/ /g;	$ryugaku2_school=~s/,/./g;
	$ryugaku2_start_yy=$in{'ryugaku1_start_yy'};	$ryugaku2_start_yy=~s/\r\n/ /g;	$ryugaku2_start_yy=~s/,/./g;
	$ryugaku2_start_mm=$in{'ryugaku1_start_mm'};	$ryugaku2_start_mm=~s/\r\n/ /g;	$ryugaku2_start_mm=~s/,/./g;
	$ryugaku2_end_yy=$in{'ryugaku1_end_yy'};	$ryugaku2_end_yy=~s/\r\n/ /g;	$ryugaku2_end_yy=~s/,/./g;
	$ryugaku2_end_mm=$in{'ryugaku1_end_mm'};	$ryugaku2_end_mm=~s/\r\n/ /g;	$ryugaku2_end_mm=~s/,/./g;

	$tsuyaku_gakureki=$in{'tsuyaku_gakureki'};	$tsuyaku_gakureki=~s/\r\n/ /g;	$tsuyaku_gakureki=~s/,/./g;


	open ( FH_WORD , $word_txt_path) ;for ( $a = 0 ; $a <= $in{'soft_word'} ; $a++ ){$soft_word=<FH_WORD>;$soft_word=~ s/\n//g;}close(FH_WORD);
	open ( FH_EXCEL , $excel_txt_path ) ;for ( $a = 0 ; $a <= $in{'soft_excel'} ; $a++ ){$soft_excel=<FH_EXCEL>;$soft_excel=~ s/\n//g;}close(FH_EXCEL);
	$soft_excel1=$in{'soft_excel1'};
	$soft_excel2=$in{'soft_excel2'};
	$soft_excel3=$in{'soft_excel3'};
	$soft_excel4=$in{'soft_excel4'};

	open ( FH_POWERPOINT , $powerpoint_txt_path ) ;for ( $a = 0 ; $a <= $in{'soft_powerpoint'} ; $a++ ){$soft_powerpoint=<FH_POWERPOINT>;$soft_powerpoint=~ s/\n//g;}close(FH_POWERPOINT);
	open ( FH_ACCESS , $access_txt_path ) ;for ( $a = 0 ; $a <= $in{'soft_access'} ; $a++ ){$soft_access=<FH_ACCESS>;$soft_access=~ s/\n//g;}close(FH_ACCESS);
	$soft_other=$in{'soft_other'};


	$os_win=$in{'os_win'};
	$os_mac=$in{'os_mac'};
	$os_unix=$in{'os_unix'};
	$os_other=$in{'os_other'};	
	if((length $os_win==0) &&(length $os_mac==0) &&(length $os_unix==0) &&(length $os_other==0) ){
		$err_os='<font color=red>←使用経験OSは必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_os);

	$eiken_score=$in{'eiken_score'};	$eiken_score=~s/\r\n/ /g;		$eiken_score=~s/,/./g;
	$eiken_shutoku=$in{'eiken_shutoku'};	$eiken_score=~s/\r\n/ /g;		$eiken_score=~s/,/./g;
	$toeic_score=$in{'toeic_score'};	$toeic_score=~s/\r\n/ /g;		$toeic_score=~s/,/./g;
	$toeic_shutoku=$in{'toeic_shutoku'};	$toeic_shutoku=~s/\r\n/ /g;	$toeic_shutoku=~s/,/./g;
	$toefl_score=$in{'toefl_score'};	$toefl_score=~s/\r\n/ /g;		$toefl_score=~s/,/./g;
	$toefl_shutoku=$in{'toefl_shutoku'};	$toefl_shutoku=~s/\r\n/ /g;	$toefl_shutoku=~s/,/./g;


	$tsuyaku_score=$in{'tsuyaku_score'};		$tsuyaku_score=~s/\r\n/ /g;	$tsuyaku_score=~s/,/./g;
	$tsuyaku_shutoku=$in{'tsuyaku_shutoku'};	$tsuyaku_shutoku=~s/\r\n/ /g;	$tsuyaku_shutoku=~s/,/./g;
	$kokuren_score=$in{'kokuren_score'};		$kokuren_score=~s/\r\n/ /g;	$kokuren_score=~s/,/./g;
	$kokuren_shutoku=$in{'kokuren_shutoku'};	$kokuren_shutoku=~s/\r\n/ /g;	$kokuren_shutoku=~s/,/./g;
	$menkyo_shutoku=$in{'menkyo_shutoku'};		$menkyo_shutoku=~s/\r\n/ /g;	$menkyo_shutoku=~s/,/./g;
	$boki_score=$in{'boki_score'};			$boki_score=~s/\r\n/ /g;		$boki_score=~s/,/./g;
	$boki_shutoku=$in{'boki_shutoku'};		$boki_shutoku=~s/\r\n/ /g;	$boki_shutoku=~s/,/./g;
	$hisho_score=$in{'hisho_score'};		$hisho_score=~s/\r\n/ /g;		$hisho_score=~s/,/./g;
	$hisho_shutoku=$in{'hisho_shutoku'};		$hisho_shutoku=~s/\r\n/ /g;	$hisho_shutoku=~s/,/./g;

	$other_gogaku=$in{'other_gogaku'};		$other_gogaku=~s/\r\n/ /g;	$other_gogaku=~s/,/./g;
	$other_gogaku=~ s/\r\n/\n/g;
	open ( FH_KAIWA , $kaiwa_txt_path ) ;for ( $a = 0 ; $a <= $in{'kaiwa'} ; $a++ ){$kaiwa=<FH_KAIWA>;$kaiwa=~ s/\n//g;}close(FH_KAIWA);

	$hoyuu_shikaku=$in{'hoyuu_shikaku'};		$hoyuu_shikaku=~s/\r\n/ /g;	$hoyuu_shikaku=~s/,/./g;
	$hoyuu_shikaku=~ s/\r\n/\n/g;
	$PR_kibou=$in{'PR_kibou'};			$PR_kibou=~s/\r\n/ /g;	$PR_kibou=~s/,/./g;
	$PR_kibou=~ s/\r\n/\n/g;
	$otoiawase=$in{'otoiawase'};			$otoiawase=~s/\r\n/ /g;	$otoiawase=~s/,/./g;
	$otoiawase=~ s/\r\n/\n/g;
	$file2=$in{'file2'};
}

if($in{'cmd'} eq 'send'){
	use lib "./";
	use Lite;
#	use MIME::Lite;

#	open(TEMPLATE,"../../../cgi-template/woman/mail.txt");
#	@entry=<TEMPLATE>;
#	close(TEMPLATE);
#	$strHtml="";
#	foreach $i (@entry){strHtml.=$i;}
#	&replace;

#	&jcode::sjis2jis(\$strHtml);

#	$type = 'multipart/mixed';
#	$msg = MIME::Lite->new( From    =>$FROM,To      =>$TO,Subject =>$Subject,Type    =>$type);
#	$msg->attach(Type     =>'TEXT',   Data     =>$strHtml);
#	$msg->send;

#ichino
#	$msg1 = MIME::Lite->new( From=>$FROM,To=>$TO1,Subject=>$Subject,Type=>$type);
#	$msg1->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg1->send;

#inoue
#	$msg2 = MIME::Lite->new( From=>$FROM,To=>$TO2,Subject=>$Subject,Type=>$type);
#	$msg2->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg2->send;

#	open(TEMPLATE,"../../../cgi-template/woman/mail_user.txt");
#	@entry=<TEMPLATE>;
#	close(TEMPLATE);
#	$strHtml="";
#	foreach $i (@entry){$strHtml.=$i;
#	&replace;

#	$msg_user = MIME::Lite->new( From=>$FROM,To=>$TO_user,Subject=>$Subject,Type=>$type);
#	$msg_user->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg_user->send;

	open(CSV_TEMPLATE,$csv_txt_path);
	@entry=<CSV_TEMPLATE>;
	close(CSV_TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	$strHtml.="\n";

	open(CSV,$csv_path);
	print CSV $strHtml;
	close(CSV);


	open(TEMPLATE,$thanks_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	print "Content-type: text/html\n\n";
	print $strHtml;

	exit;
}elsif($in{'cmd'} eq 'confirm'){
	if( length $in{'file'} >0){
		$tmp=$incfn{'file'};
		$tmp =~ /(.*)\.([^\.]+$)/;
		$ext=$2;
		if(length $ext == 0 ){
			$file2=time.".doc";
		}else{
			$file2=time.".".$ext;
		}
		open(ATTACH,">".$resume_dir.$file2);
		print ATTACH $in{file};
		close(ATTACH);
	}
	if((
(length	$err_name_1)+
(length	$err_name_2)+
(length	$err_name_3)+
(length	$err_name_4)+
(length	$err_name_5)+
(length	$err_name_6)+
(length $err_birth_day)+
(length	$err_age)+
(length	$err_sex)+
(length	$err_zip)+
(length	$err_pref)+
(length	$err_address1)+
(length	$err_address2)+
(length	$err_address3)+
(length	$err_moyori)+
(length	$err_tel)+
(length	$err_mail_A)+
(length	$err_mail_B)+
(length	$err_start)+
(length	$err_style)+
(length	$err_time)+
(length	$err_wday)+
(length	$err_jikyu)+
(length	$err_shokushu)+
(length	$err_os)+
(length	$err_enq01)+
(length	$err_enq02)+
(length	$err_enq03)+
(length	$err_enq04)
) == 0 ){
#確認画面へ
		open(TEMPLATE,$confirm_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){	$strHtml.=$i;}
		&replace;
		print "Content-type: text/html\n\n";
		print $strHtml;
	}else{
		print "Content-type: text/html\n\n";
		$err='<br><font color=red>入力事項に不十分な部分がございます。ご確認の上、再度エントリーしてください。</font>';
		&jcode::euc2sjis(\$err);
		open(TEMPLATE,$entry_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){
			$strHtml.=$i;
		}
		&replace;
		$strHtml=~s/__entry_cgi_path__/$entry_cgi_path/g;
		print $strHtml;
	}
}elsif($in{'cmd'} eq 'edit'){
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print "Content-type: text/html\n\n";
	print $strHtml;
}else{
#init open
	$err_name_1="";
	$err_name_2="";
	$err_name_3="";
	$err_name_4="";
	$err_name_5="";
	$err_name_6="";
	$err_birth_day="";
	$err_age="";
	$err_sex="";
	$err_zip="";
	$err_pref="";
	$err_address1="";
	$err_address2="";
	$err_address3="";
	$err_moyori="";
	$err_tel="";
	$err_mail_A="";
	$err_mail_B="";
	$err_start="";
	$err_style="";
	$err_time="";
	$err_wday="";
	$err_jikyu="";
	$err_shokushu="";
	$err_os="";
	$err_enq01="";
	$err_enq02="";
	$err_enq03="";
	$err_enq04="";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print $strHtml;
}

sub replace{
	$strHtml=~s/__job_id__/$job_id/g;
	$strHtml=~s/__job_name__/$job_name/g;
	$strHtml=~s/__epoch__/$epoch/g;
	$strHtml=~s/__entry_cgi_path__/$entry_cgi_path/g;
	$strHtml=~s/__err__/$err/g;
	$strHtml=~s/__name_A1__/$name_A1/g;
	$strHtml=~s/__name_B1__/$name_B1/g;
	$strHtml=~s/__name_A2__/$name_A2/g;
	$strHtml=~s/__name_B2__/$name_B2/g;
	$strHtml=~s/__name_A3__/$name_A3/g;
	$strHtml=~s/__name_B3__/$name_B3/g;
	$strHtml=~s/__name_A4__/$name_A4/g;
	$strHtml=~s/__name_B4__/$name_B4/g;
	$strHtml=~s/__name_A5__/$name_A5/g;
	$strHtml=~s/__name_B5__/$name_B5/g;
	$strHtml=~s/__name_A6__/$name_A6/g;
	$strHtml=~s/__name_B6__/$name_B6/g;
	$strHtml=~s/__birth_day_yy__/$birth_day_yy/g;
	$strHtml=~s/__birth_day_mm__/$birth_day_mm/g;
	$strHtml=~s/__birth_day_dd__/$birth_day_dd/g;

	&replace_radio('sex','man');
	&replace_radio('sex','woman');
	$strHtml=~s/__sex__/$sex/g;
	$strHtml=~s/__sex_kanji__/$sex_kanji/g;

	$strHtml=~s/__age__/$age/g;
	$strHtml=~s/__zip1__/$zip1/g;
	$strHtml=~s/__zip2__/$zip2/g;

	&replace_select02d(pref,47);
	$strHtml=~s/__pref__/$pref/g;
	$strHtml=~s/__pref_num__/$in{'pref'}/g;

	$strHtml=~s/__address1__/$address1/g;
	$strHtml=~s/__address2__/$address2/g;
	$strHtml=~s/__address3__/$address3/g;

	$strHtml=~s/__moyori_rosen__/$moyori_rosen/g;
	$strHtml=~s/__moyori_eki__/$moyori_eki/g;
	$strHtml=~s/__moyori_type__/$moyori_type/g;

	&replace_radio('moyori_type','1');
	&replace_radio('moyori_type','2');
	&replace_radio('moyori_type','3');
	&replace_radio('moyori_type','4');
	$strHtml=~s/__moyori_type__/$moyori_type/g;
	$strHtml=~s/__moyori_type_num__/$moyori_type_num/g;

	$strHtml=~s/__moyori_min__/$moyori_min/g;

	$strHtml=~s/__tel1__/$in{'tel1'}/g;
	$strHtml=~s/__tel2__/$in{'tel2'}/g;
	$strHtml=~s/__tel3__/$in{'tel3'}/g;

	&replace_radio('tel_rusu','1');
	&replace_radio('tel_rusu','2');
	$strHtml=~s/__tel_rusu__/$tel_rusu/g;
	$strHtml=~s/__tel_rusu_num__/$in{tel_rusu}/g;

	$strHtml=~s/__mob1__/$in{'mob1'}/g;
	$strHtml=~s/__mob2__/$in{'mob2'}/g;
	$strHtml=~s/__mob3__/$in{'mob3'}/g;

	&replace_radio('mob_rusu','1');
	&replace_radio('mob_rusu','2');
	$strHtml=~s/__mob_rusu__/$mob_rusu/g;
	$strHtml=~s/__mob_rusu_num__/$in{'mob_rusu'}/g;

	$strHtml=~s/__fax1__/$in{'fax1'}/g;
	$strHtml=~s/__fax2__/$in{'fax2'}/g;
	$strHtml=~s/__fax3__/$in{'fax3'}/g;

	$strHtml=~s/__mail_A__/$mail_A/g;
	$strHtml=~s/__mail_B__/$mail_B/g;

	&replace_radio('renraku','1');

	&replace_radio('renraku','2');
	&replace_radio('renraku','3');
	&replace_radio('renraku','4');
	$strHtml=~s/__renraku__/$renraku/g;
	$strHtml=~s/__renraku_num__/$in{renraku}/g;


	&replace_select02d(start_year,11);
	$strHtml=~s/__start_year__/$start_year/g;
	$strHtml=~s/__start_year_num__/$in{'start_year'}/g;
	&replace_select02d(start_month,13);
	$strHtml=~s/__start_month__/$start_month/g;
	$strHtml=~s/__start_month_num__/$in{'start_month'}/g;
	&replace_select02d(start_day,32);
	$strHtml=~s/__start_day__/$start_day/g;
	$strHtml=~s/__start_day_num__/$in{'start_day'}/g;

	&replace_radio('style1','長期（3ヶ月以上）');
	&replace_radio('style2','短期（3ヶ月以内）');
	&replace_radio('style3','単発（10日前後）');
	$strHtml=~s/__style1__/$style1/g;
	$strHtml=~s/__style2__/$style2/g;
	$strHtml=~s/__style3__/$style3/g;

	&replace_radio('shoukai','1');
	&replace_radio('shoukai','2');
	$strHtml=~s/__shoukai__/$shoukai/g;
	$strHtml=~s/__shoukai_num__/$in{shoukai}/g;

	$strHtml=~s/__time1__/$time1/g;
	$strHtml=~s/__time2__/$time2/g;
	$strHtml=~s/__time3__/$time3/g;
	$strHtml=~s/__time4__/$time4/g;
	&replace_radio('time_type','1');
	$strHtml=~s/__time_type__/$time_type/g;

	&replace_radio('wday_mon','月');
	&replace_radio('wday_tue','火');
	&replace_radio('wday_wed','水');
	&replace_radio('wday_thu','木');
	&replace_radio('wday_fri','金');
	&replace_radio('wday_sat','土');
	&replace_radio('wday_sun','日');

	$strHtml=~s/__wday_mon__/$wday_mon/g;
	$strHtml=~s/__wday_tue__/$wday_tue/g;
	$strHtml=~s/__wday_wed__/$wday_wed/g;
	$strHtml=~s/__wday_thu__/$wday_thu/g;
	$strHtml=~s/__wday_fri__/$wday_fri/g;
	$strHtml=~s/__wday_sat__/$wday_sat/g;
	$strHtml=~s/__wday_sun__/$wday_sun/g;

	&replace_radio('sat_type','1');
	&replace_radio('sat_type','2');
	&replace_radio('sat_type','3');
	$strHtml=~s/__sat_type__/$sat_type/g;
	$strHtml=~s/__sat_type_num__/$in{sat_type}/g;

	&replace_radio('smoke','1');
	&replace_radio('smoke','2');
	$strHtml=~s/__smoke__/$smoke/g;
	$strHtml=~s/__smoke_num__/$in{smoke}/g;

	$strHtml=~s/__jikyu__/$jikyu/g;
	&replace_radio('koutsuhi','交通費別支給');
	$strHtml=~s/__koutsuhi__/$koutsuhi/g;

	&replace_select01d(shokushu1,6);
	$strHtml=~s/__shokushu1__/$shokushu1/g;
	$strHtml=~s/__shokushu1_num__/$in{'shokushu1'}/g;
	&replace_select01d(shokushu2,6);
	$strHtml=~s/__shokushu2__/$shokushu2/g;
	$strHtml=~s/__shokushu2_num__/$in{'shokushu2'}/g;

	&replace_radio('hakensaki_type','1');
	&replace_radio('hakensaki_type','2');
	&replace_radio('hakensaki_type','3');
	$strHtml=~s/__hakensaki_type__/$hakensaki_type/g;
	$strHtml=~s/__hakensaki_type_num__/$in{hakensaki_type}/g;

	&replace_radio('hakensaki_kibo','1');
	&replace_radio('hakensaki_kibo','2');
	&replace_radio('hakensaki_kibo','3');
	$strHtml=~s/__hakensaki_kibo__/$hakensaki_kibo/g;
	$strHtml=~s/__hakensaki_kibo_num__/$in{hakensaki_kibo}/g;

	$strHtml=~s/__hakensaki_gyousyu__/$hakensaki_gyousyu/g;
	$strHtml=~s/__saketai_gyousyu__/$saketai_gyousyu/g;
	$strHtml=~s/__kinmuchi__/$kinmuchi/g;
	$strHtml=~s/__shoyouzikan__/$shoyouzikan/g;

	&replace_select01d(gakureki,9);
	$strHtml=~s/__gakureki__/$gakureki/g;
	$strHtml=~s/__gakureki_num__/$in{'gakureki'}/g;

	$strHtml=~s/__ryugaku1_kokumei__/$ryugaku1_kokumei/g;
	$strHtml=~s/__ryugaku1_school__/$ryugaku1_school/g;
	$strHtml=~s/__ryugaku1_start_yy__/$ryugaku1_start_yy/g;
	$strHtml=~s/__ryugaku1_start_mm__/$ryugaku1_start_mm/g;
	$strHtml=~s/__ryugaku1_end_yy__/$ryugaku1_end_yy/g;
	$strHtml=~s/__ryugaku1_end_mm__/$ryugaku1_end_mm/g;

	$strHtml=~s/__ryugaku2_kokumei__/$ryugaku2_kokumei/g;
	$strHtml=~s/__ryugaku2_school__/$ryugaku2_school/g;
	$strHtml=~s/__ryugaku2_start_yy__/$ryugaku2_start_yy/g;
	$strHtml=~s/__ryugaku2_start_mm__/$ryugaku2_start_mm/g;
	$strHtml=~s/__ryugaku2_end_yy__/$ryugaku2_end_yy/g;
	$strHtml=~s/__ryugaku2_end_mm__/$ryugaku2_end_mm/g;

	$strHtml=~s/__tsuyaku_gakureki__/$tsuyaku_gakureki/g;

	&replace_radio('os_win','win');
	$strHtml=~s/__os_win__/$os_win/g;
	&replace_radio('os_mac','mac');
	$strHtml=~s/__os_mac__/$os_mac/g;
	&replace_radio('os_unix','unix');
	$strHtml=~s/__os_unix__/$os_unix/g;
	&replace_radio('os_other','other');
	$strHtml=~s/__os_other__/$os_other/g;

	&replace_select01d(soft_word,3);
	&replace_select01d(soft_excel,3);
	&replace_select01d(soft_powerpoint,3);
	&replace_select01d(soft_access,3);
	$strHtml=~s/__soft_word__/$soft_word/g;
	$strHtml=~s/__soft_excel__/$soft_excel/g;
	$strHtml=~s/__soft_powerpoint__/$soft_powerpoint/g;
	$strHtml=~s/__soft_access__/$soft_access/g;
	$strHtml=~s/__soft_word_num__/$in{'soft_word'}/g;
	$strHtml=~s/__soft_excel_num__/$in{'soft_excel'}/g;
	$strHtml=~s/__soft_powerpoint_num__/$in{'soft_powerpoint'}/g;
	$strHtml=~s/__soft_access_num__/$in{'soft_access'}/g;

	&replace_radio('soft_excel1','SUM');
	$strHtml=~s/__soft_excel1__/$soft_excel1/g;
	&replace_radio('soft_excel2','AVERAGE');
	$strHtml=~s/__soft_excel2__/$soft_excel2/g;
	&replace_radio('soft_excel3','IF');
	$strHtml=~s/__soft_excel3__/$soft_excel3/g;
	&replace_radio('soft_excel4','VLOOKUP');
	$strHtml=~s/__soft_excel4__/$soft_excel4/g;

	$strHtml=~s/__soft_other__/$soft_other/g;

	$strHtml=~s/__toeic_score__/$toeic_score/g;
	$strHtml=~s/__toeic_shutoku__/$toeic_shutoku/g;
	$strHtml=~s/__toefl_score__/$toefl_score/g;
	$strHtml=~s/__toefl_shutoku__/$toefl_shutoku/g;
	$strHtml=~s/__eiken_score__/$eiken_score/g;
	$strHtml=~s/__eiken_shutoku__/$eiken_shutoku/g;
	$strHtml=~s/__tsuyaku_score__/$tsuyaku_score/g;
	$strHtml=~s/__tsuyaku_shutoku__/$tsuyaku_shutoku/g;
	$strHtml=~s/__kokuren_score__/$kokuren_score/g;
	$strHtml=~s/__kokuren_shutoku__/$kokuren_shutoku/g;
	$strHtml=~s/__menkyo_shutoku__/$menkyo_shutoku/g;
	$strHtml=~s/__boki_score__/$boki_score/g;
	$strHtml=~s/__boki_shutoku__/$boki_shutoku/g;
	$strHtml=~s/__hisho_score__/$hisho_score/g;
	$strHtml=~s/__hisho_shutoku__/$hisho_shutoku/g;

	$strHtml=~s/__other_gogaku__/$other_gogaku/g;
	&replace_radio('kaiwa','1');
	&replace_radio('kaiwa','2');
	&replace_radio('kaiwa','3');
	$strHtml=~s/__kaiwa__/$kaiwa/g;
	$strHtml=~s/__kaiwa_num__/$in{kaiwa}/g;

	$strHtml=~s/__hoyuu_shikaku__/$hoyuu_shikaku/g;
	$strHtml=~s/__PR_kibou__/$PR_kibou/g;
	$strHtml=~s/__otoiawase__/$otoiawase/g;
	$strHtml=~s/__file2__/$file2/g;

	$strHtml=~s/__enq01__/$enq01/g;
	$strHtml=~s/__enq01qes__/$enq01qes/g;
	$strHtml=~s/__enq01ans__/$enq01ans/g;
	$strHtml=~s/__enq01sel__/$enq01sel/g;
	$strHtml=~s/__enq01text__/$enq01text/g;

	$strHtml=~s/__enq02__/$enq02/g;
	$strHtml=~s/__enq02qes__/$enq02qes/g;
	$strHtml=~s/__enq02ans__/$enq02ans/g;
	$strHtml=~s/__enq02sel__/$enq02sel/g;
	$strHtml=~s/__enq02text__/$enq02text/g;

	$strHtml=~s/__enq03__/$enq03/g;
	$strHtml=~s/__enq03qes__/$enq03qes/g;
	$strHtml=~s/__enq03ans__/$enq03ans/g;
	$strHtml=~s/__enq03sel__/$enq03sel/g;
	$strHtml=~s/__enq03text__/$enq03text/g;

	$strHtml=~s/__enq04__/$enq04/g;
	$strHtml=~s/__enq04qes__/$enq04qes/g;

	$strHtml=~s/__enq04ans__/$enq04ans/g;
	$strHtml=~s/__enq04sel__/$enq04sel/g;
	$strHtml=~s/__enq04text__/$enq04text/g;

	$strHtml=~s/__err_name_1__/$err_name_1/g;
	$strHtml=~s/__err_name_2__/$err_name_2/g;
	$strHtml=~s/__err_name_3__/$err_name_3/g;
	$strHtml=~s/__err_name_4__/$err_name_4/g;
	$strHtml=~s/__err_name_5__/$err_name_5/g;
	$strHtml=~s/__err_name_6__/$err_name_6/g;
	$strHtml=~s/__err_birth_day__/$err_birth_day/g;
	$strHtml=~s/__err_age__/$err_age/g;
	$strHtml=~s/__err_sex__/$err_sex/g;
	$strHtml=~s/__err_zip__/$err_zip/g;
	$strHtml=~s/__err_pref__/$err_pref/g;
	$strHtml=~s/__err_address1__/$err_address1/g;
	$strHtml=~s/__err_address2__/$err_address2/g;
	$strHtml=~s/__err_address3__/$err_address3/g;
	$strHtml=~s/__err_moyori__/$err_moyori/g;
	$strHtml=~s/__err_tel__/$err_tel/g;
	$strHtml=~s/__err_mail_A__/$err_mail_A/g;
	$strHtml=~s/__err_mail_B__/$err_mail_B/g;
	$strHtml=~s/__err_start__/$err_start/g;

	$strHtml=~s/__err_style__/$err_style/g;
	$strHtml=~s/__err_time__/$err_time/g;
	$strHtml=~s/__err_wday__/$err_wday/g;
	$strHtml=~s/__err_jikyu__/$err_jikyu/g;
	$strHtml=~s/__err_shokushu__/$err_shokushu/g;
	$strHtml=~s/__err_os__/$err_os/g;
	$strHtml=~s/__err_enq01__/$err_enq01/g;
	$strHtml=~s/__err_enq02__/$err_enq02/g;
	$strHtml=~s/__err_enq03__/$err_enq03/g;
	$strHtml=~s/__err_enq04__/$err_enq04/g;
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

sub is_zenkaku{
	$tmp = $_[0];
	&jcode::sjis2euc(\$tmp);
	if($tmp !~ /[\x00-\x7f]/){
		return 1;
	}else{
		return 0;
	}
}

sub is_hankaku{
	$tmp = $_[0];
	if($tmp !~ /[\x80-\xff]/){
		return 1;
	}else{
		return 0;
	}
}

sub enq_radio{
	$temp=$_[0];
	$temp=~s/\n//g;
	if(length $temp > 0){
		if($_[3]==$_[2]){
			$enq.="<input type=radio name=".$_[1]." value=".$_[3]." checked>".$temp;
		}else{
			$enq.="<input type=radio name=".$_[1]." value=".$_[3].">".$temp;
		}
	};
}

sub wtime {
	($sec,$min,$hour,$mday,$mon,$year,$weekday) = localtime;
	
	$year += 1900;
	$mon++;
	$mon = sprintf("%.2d",$mon);
	$mday = sprintf("%.2d",$mday);
	$hour = sprintf("%.2d",$hour);
	$min = sprintf("%.2d",$min);
	$sec = sprintf("%.2d",$sec);
	
	@week = ("日","月","火","水","木","金","土");
	$weekday = $week[$wday];
	&jcode::euc2sjis(\$weekday);
}

