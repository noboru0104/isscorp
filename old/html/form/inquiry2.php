<?

$pagecheck = "0";
if(getenv("HTTP_REFERER") == "https://www.issjp.com/form/inquiry1.php"){$pagecheck = "1";}
if(getenv("HTTP_REFERER") == "https://202.212.213.210/form/inquiry1.php"){$pagecheck = "1";}

if(getenv("HTTP_REFERER") == "https://www.issjp.com/form_test/inquiry1.php"){$pagecheck = "1";}

if($pagecheck == "0"){
	header("Location: https://www.issjp.com/form/inquiry.php");
	exit;
}

$services = $_POST['services'];


$mailto = $_POST['mailto'];
//$mailto = $_POST['mail'];


$toiawase = $_POST['toiawase'];
$company = $_POST['company'];
$depart = $_POST['depart'];
$gyoukai = $_POST['gyoukai'];
$name = $_POST['name'];
$name2 = $_POST['name2'];
$tel = $_POST['tel'];
$mail = $_POST['mail'];
$zipcode = $_POST['zipcode'];
$address = $_POST['address'];
$kikkake = $_POST['kikkake'];
$sonota = $_POST['sonota'];

mb_language ("Japanese");
mb_internal_encoding('euc-jp');

$inputdate = date("Y/m/d/H:i:s", time());

$parameter = "-f info@issjp.com";

$body1 = "
WEBでのお問い合わせフォームにて受付完了しました。
下記にて内容をご確認ください。

---------------------------------------------
＊なるべく早めにお客様にご連絡ください。
＊「問い合わせサービス」が複数部門の場合には
　該当する部門間で必ず調整してください。
---------------------------------------------

お申し込み日時： {$inputdate}

【お問い合わせサービス】
{$services}

【お問い合わせ内容】
{$toiawase}

【会社(団体)名】　　{$company}
【部署名】　　　　　{$depart}
【業界】　　　　　　{$gyoukai}
【お名前】　　　　　{$name}
【お名前(フリガナ)】{$name2}
【電話番号】　　　　{$tel}
【e-mail】　　　　　{$mail}
【ご住所】　　　　　{$zipcode} {$address}
【当社をどのようにお知りになりましたか】
                    {$kikkake} {$sonota}


このメールに返信はできません。


以上、ご対応をよろしくお願いいたします。


REQUEST_METHOD=".getenv("REQUEST_METHOD")."
HTTP_REFERER=".getenv("HTTP_REFERER")."
REMOTE_ADDR=".getenv("REMOTE_ADDR")."
HTTP_USER_AGENT=".getenv("HTTP_USER_AGENT");

$subject1 = "お問い合わせフォーム【受付】";
$header1  = "From: info@issjp.com\n";

mb_send_mail($mailto, $subject1, $body1, $header1, $parameter); //管理者へメール送信



$body2 = " 
{$company}
{$name}様

この度は、弊社にお問い合わせいただき誠にありがとうございます。
お問い合わせを受付いたしました。
このメールはシステムより自動的に送信されております。

お問い合わせ内容を確認の上、弊社担当よりご連絡をさせていただきます。
万が一、3営業日を過ぎても連絡がない場合には、
大変恐縮ですが下記の窓口までお問い合わせくださいますよう
お願い申し上げます。


お問い合わせ日時： {$inputdate} 

----------------------------------------------------------
株式会社アイ・エス・エス
〒108-0073 東京都港区三田3-13-12 三田MTビル8階
Email：info@issjp.com   URL：http://www.issjp.com
TEL：  03-6369-9991（代）平日9:00 - 18:00
----------------------------------------------------------
";

$subject2 = "ISS：お問い合わせありがとうございます";
$header2  = "From: info@issjp.com\n";

mb_send_mail($mail, $subject2, $body2, $header2, $parameter); //お客様へ確認メール送信


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="follow,index,noydir">
<meta http-equiv="keywords" content="ISS,アイ・エス・エス,お問い合わせ" />
<title>完了 | ISS総合お問い合わせフォーム </title>
<link href="common/form_style.css" rel="stylesheet" type="text/css" media="screen" />
<style type="text/css">

<SCRIPT language="JavaScript" type="text/javascript">
<!-- Yahoo Japan Corporation.
window.ysm_customData = new Object();
window.ysm_customData.conversion = "transId=,currency=,amount=";
var ysm_accountid  = "1958DN51ACH77AD5GQ76BGHG8P8";
document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' " 
+ "SRC=//" + "srv3.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid 
+ "></SCR" + "IPT>");
// -->
</SCRIPT>

<!--
.style1 {
	color: #003399;
	font-weight: bold;
}
-->
</style>

</head>
<body>

<!-- Header Begin -->
<div id="header">
<div class="contentform"><a href="../index.html"><img src="../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../index.html"><img src="../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi"></div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<!-- コンテンツ -->
<div id="formcont" align="center">
<h2><img src="images/ind_h2.jpg" alt="お問い合わせフォーム" width="700" height="78" /></h2>

<table id="confirm">
          <tr>
            <td class="t1" colspan="2">お問い合わせフォーム送信完了</th>          
          </tr>
          <tr>
            <td colspan="2" align="center"><table border="0" cellpadding="0" cellspacing="0"><tr><td align="left">
<br />
<br />
<br />
お問い合わせフォームをご利用いただきありがとうございます。<br />
ご入力いただきましたメールアドレスにフォーム受付確認の自動返信メールを送信いたしました。<br /><br />
お問い合わせ内容を確認の上、ご連絡いたしますが、万が一3営業日を過ぎても<br />
連絡がない場合は下記の窓口までお問い合わせくださいますようお願い申し上げます。<br /><br /><br />
株式会社アイ・エス・エス<br />
Eメール：　info@issjp.com<br />
電話：　　 03-6369-9991（代）　平日9:00 - 18:00<br />
<br />
<br />
<br />
<br />
<br />
			</td></tr></table>
			</td>
          </tr>
        </table>

</div>
    <!-- コンテンツ -->
	
<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../common/images/pixel_trans.gif" width="816" height="15" alt="" class="spacer" />
<div class="menuList">
<ul>
  <li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>
</div>
</div>
</div>
<!-- Footer End -->
	
    <!-- サイドバー
    <a href="/_common/inc/news.html" class="codeexample"></a>
    サイドバー -->

<!-- Google Code for JP&#21839;&#12356;&#21512;&#12431;&#12379;&#12501;&#12457;&#12540;&#12512;&#65288;&#23436;&#20102;&#65289; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 984295382;
var google_conversion_language = "ja";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "QQvzCKqyoQIQ1s-s1QM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/984295382/?label=QQvzCKqyoQIQ1s-s1QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<!--↓20160826add↓-->
<!-- Google Code for &#12362;&#21839;&#12356;&#21512;&#12431;&#12379;&#23436;&#20102; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "RZvLCLWZ32kQms-coQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/874981274/?label=RZvLCLWZ32kQms-coQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!--↑20160826add↑-->

<!--↓20160826add↓-->
<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var yahoo_conversion_id = 1000318685;
    var yahoo_conversion_label = "3WS-CO3yxWkQ7d-ZoQM";
    var yahoo_conversion_value = 0;
    /* ]]> */
</script>
<script type="text/javascript" src="//s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//b91.yahoo.co.jp/pagead/conversion/1000318685/?value=0&label=3WS-CO3yxWkQ7d-ZoQM&guid=ON&script=0&disvt=true"/>
    </div>
</noscript>
<!--↑20160826add↑-->


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body></html>