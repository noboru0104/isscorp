$(function(){
	"use strict";
		
	$('.l-conventionlist-table-cell .l-inside').matchHeight();
	$('.l-conferenceBlock04-table-cell .l-conferenceBlock04-inside p').matchHeight();
	
	TextChange();
	
	$(window).load(function () {		
		
		
	});
	
	$(window).resize(function () {
		
		$('.l-conventionlist-table-cell .l-inside').matchHeight();
		$('.l-conferenceBlock04-table-cell .l-conferenceBlock04-inside p').matchHeight();
		
	});
	
});

function TextChange() {
	"use strict";

	if ($('.l-planBlock').length) {
		var widimage = window.innerWidth;

		if (widimage < 1001) {
			$('.l-planBlock table.table01 tr th:nth-child(2)').text('会議・プロジェクト名 - 開催都市');

			$('.l-planBlock table.table01 tr').each(function () {
				var td_text1 = $(this).find('td:first-child').text();
				var td_text2 = $(this).find('td:nth-child(2)').text();
				$(this).find('td:nth-child(2)').text(td_text1 + ' - ' + td_text2);
			});
		} else {

		}

	}

	// if ($('.l-planBlock').length) {
	// 	var widimage = window.innerWidth;

	// 	if (widimage < 1001) {
	// 		$('.l-planBlock table.table02 tr th:nth-child(2)').text('お客様 - 会議・プロジェクト名');

	// 		$('.l-planBlock table.table02 tr').each(function () {
	// 			var td_text1 = $(this).find('td:first-child').text();
	// 			var td_text2 = $(this).find('td:nth-child(2)').text();
	// 			$(this).find('td:nth-child(2)').text(td_text1 + ' - ' + td_text2);
	// 		});
	// 	} else {

	// 	}

	// }


}

