$(function(){
	"use strict";
		
	$('.l-interpretationBlock02-cell div').matchHeight();
	btnTopFlex();
	
	$(window).resize(function () {
		
		$('.l-interpretationBlock02-cell div').matchHeight();
		btnTopFlex();
		
	});
	
});

