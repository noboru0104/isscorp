#!/usr/bin/perl
# 20080807 MW 追加 メッセージ投稿
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
&ReadParse;

$entry_cgi_path     ="./message.cgi";
#$entry_cgi_path        ="/haken/ssl/cgi-bin/staff_only/upload.cgi";
$csv_dir        ="../secret/staff_only/upload/";
$csv_path       =">>" . $csv_dir . "message.csv";
$resume_dir     ="../secret/staff_only/upload/";
$template_dir       ="../template/staff_only/upload/";

$entry_html_path    =$template_dir . "message.html";
$error_html_path    =$template_dir . "mes_error.html";
$confirm_html_path  =$template_dir . "confirm.html";
$thanks_html_path   =$template_dir . "complete.html";
$csv_txt_path       =$template_dir . "mes_csv.txt";
$count_path         =$template_dir . "count_mes.txt";

$epoch=time;
$name=$in{'name'};  $name=~s/\r\n//g;   $name=~s/\n//g; $name=~s/\r//g; $name=~s/,/./g; $name=~s/ //g;
$title=$in{'title'};  $title=~s/\r\n//g;   $title=~s/\n//g; $title=~s/\r//g; $title=~s/,/./g;
$message=$in{'message'}; # $message=~s/\r\n//g;   $message=~s/\n//g; $message=~s/\r//g; $message=~s/,/./g;

use MIME::Base64 ();

if(length $in{'name'}==0){
    $name_err="<br><font color=\"red\">お名前を入力してください。</font>";
    &jcode::euc2sjis(\$name_err);
}else{
#   $encoded = MIME::Base64::encode($in{'name'});
    $encoded = encode_base64fs($in{'name'});
}

if(length $in{'title'}==0){
    $title_err="<br><font color=\"red\">タイトルを入力してください。</font>";
    &jcode::euc2sjis(\$title_err);
}
if(length $in{'message'}==0){
    $message_err="<br><font color=\"red\">メッセージを入力してください。</font>";
    &jcode::euc2sjis(\$message_err);
}

if(defined $in{'confirm'}){
    if( length $name_err > 0 || length $title_err > 0 || length $message_err > 0 ){
        print "Content-type: text/html\n\n";
        open(TEMPLATE,$entry_html_path);
        @entry=<TEMPLATE>;
        close(TEMPLATE);
        $strHtml="";
        foreach $i (@entry){
            $strHtml.=$i;
        }
        $strHtml=~s/__nameErr__/$name_err/g;
        $strHtml=~s/__titleErr__/$title_err/g;
        $strHtml=~s/__messageErr__/$message_err/g;
        &replace;
        print $strHtml;
        exit;
    }

    print "Content-type: text/html\n\n";
    open(TEMPLATE,$confirm_html_path);
    @entry=<TEMPLATE>;
    close(TEMPLATE);
    $strHtml="";
    foreach $i (@entry){
        $strHtml.=$i;
    }
    $message = &StrCorrect($message);
    &replace;
    print $strHtml;
    exit;

}elsif(defined $in{'send'}){

    open(CSV_TEMPLATE,$csv_txt_path);
    @entry=<CSV_TEMPLATE>;
    close(CSV_TEMPLATE);
    $strHtml="";
    foreach $i (@entry){$strHtml.=$i;}
    &replace;
    $strHtml.="\n";

    open(COUNT,"+<".$count_path);
    flock(COUNT,2);
    $uid=<COUNT>;
    $strHtml=~s/__uid__/$uid/g;
    $uid++;
    seek(COUNT,0,0);
    print COUNT $uid;
    close(COUNT);
    chmod(0666, $count_path);

    open(CSV,$csv_path);
    flock(CSV,2);
    seek(CSV,0,0);
    print CSV $strHtml;
    truncate(CSV, tell(CSV));
    close(CSV);

    open(TEMPLATE,$thanks_html_path);
    @entry=<TEMPLATE>;
    close(TEMPLATE);
    $strHtml="";
    foreach $i (@entry){$strHtml.=$i;}
    &replace;
    print "Content-type: text/html\n\n";
    print $strHtml;
    exit;
}else{

    print "Content-type: text/html\n\n";
    open(TEMPLATE,$entry_html_path);
    @entry=<TEMPLATE>;
    close(TEMPLATE);
    $strHtml="";
    foreach $i (@entry){
        $strHtml.=$i;
    }
    $message = &RevStrCorrect($message);
    &replace;
    print $strHtml;
}

sub replace{
    $strHtml=~s/__epoch__/$epoch/g;
    $strHtml=~s/__name__/$name/g;
    $strHtml=~s/__title__/$title/g;
    $strHtml=~s/__message__/$message/g;
    $strHtml=~s/__nameErr__//g;
    $strHtml=~s/__titleErr__//g;
    $strHtml=~s/__messageErr__//g;
}

sub is_zenkaku{
    $tmp = $_[0];
    &jcode::sjis2euc(\$tmp);
    if($tmp !~ /[\x00-\x7f]/){
        return 1;
    }else{
        return 0;
    }
}

sub is_hankaku{
    $tmp = $_[0];
    if($tmp !~ /[\x80-\xff]/){
        return 1;
    }else{
        return 0;
    }
}

sub enq_radio{
    $temp=$_[0];
    $temp=~s/\n//g;
    if(length $temp > 0){
        if($_[3]==$_[2]){
            $enq.="<input type=radio name=".$_[1]." value=".$_[3]." checked>".$temp;
        }else{
            $enq.="<input type=radio name=".$_[1]." value=".$_[3].">".$temp;
        }
    };
}

sub encode_base64fs ($;$)
{
    if ($] >= 5.006) {
    require bytes;
    if (bytes::length($_[0]) > length($_[0]) ||
        ($] >= 5.008 && $_[0] =~ /[^\0-\xFF]/))
    {
        require Carp;
        Carp::croak("The Base64 encoding is only defined for bytes");
    }
    }

    use integer;

    my $eol = $_[1];
    $eol = "\n" unless defined $eol;

    my $res = pack("u", $_[0]);
    # Remove first character of each line, remove newlines
    $res =~ s/^.//mg;
    $res =~ s/\n//g;

    $res =~ tr|` -_|AA-Za-z0-9\-_|;               # `# help emacs
    # fix padding at the end
    my $padding = (3 - length($_[0]) % 3) % 3;
    $res =~ s/.{$padding}$/'=' x $padding/e if $padding;
    # break encoded string into lines of no more than 76 characters each
    if (length $eol) {
    $res =~ s/(.{1,76})/$1$eol/g;
    }
    return $res;
}

sub StrCorrect{
    my @ConvertedStr = @_;
    $ConvertedStr[0] =~ s/,/&\#044\;/g;
    $ConvertedStr[0] =~ s/\r\n/<br>/g;
    $ConvertedStr[0] =~ s/\n/<br>/g;
    return $ConvertedStr[0];
}

sub RevStrCorrect{
    my @ConvertedStr = @_;
    $ConvertedStr[0] =~ s/&\#044\;/,/g;
    $ConvertedStr[0] =~ s/<br>/\r\n/g;
    return $ConvertedStr[0];
}

