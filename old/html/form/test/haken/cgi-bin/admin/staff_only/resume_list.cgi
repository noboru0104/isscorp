#!/usr/bin/perl

#require '../../../perl_lib/jcode.pl';
#require '../../../perl_lib/cgi-lib.pl';

require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
&ReadParse;

$entry_cgi_path		="/cgi-bin/admin/staff_only/resume_list.cgi";
$csv_dir		="../../secret/staff_only/resume/";
$csv_path		=$csv_dir . "registered.csv";
$resume_dir		="../../secret/staff_only/resume/";
$template_dir		="../../template/staff_only/resume/";

$csv_path		=$csv_dir . "registered.csv";
$csv_tmp_path		=$csv_dir . "tmp.csv";
$csv_write_path		=">>".$csv_dir . "registered.csv";

$index_html_path	=$template_dir . "resume_admin.html";
$detail_html_path	=$template_dir . "resume_detail.html";


if($in{'cmd'} eq 'resume'){
	$resume_path = $resume_dir.$in{'id'};
	$in{'id'} =~ /(.*)\.([^\.]+$)/;
	if($2 eq "png"){
		print "Content-type: image/png\n";
	}elsif(($2 eq "jpg") || ($2 eq "jpeg")){
		print "Content-type: image/jpeg\n";
	}elsif($2 eq "pdf"){
		print "Content-type: application/pdf\n";
	}elsif($2 eq "sit"){
		print "Content-type: application/x-stuffit\n";
	}elsif($2 eq "zip"){
		print "Content-type: application/x-zip-compressed\n";
	}elsif($2 eq "doc"){
		print "Content-type: application/msword\n";
	}else{
		print "Content-type: application/msword\n";
	}

print <<"EOL";
Content-Disposition: attachment; filename=$filename

EOL
	open (IN,$resume_path);
	foreach $data (<IN>) {		print $data;}
	close(IN);
}elsif($in{'cmd'} eq 'detail'){
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="";
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
		if(($1 eq $in{'id'}) && ($3 eq $in{'email'}) ){
			$strList=$i;
		}
	}
	if(length $strList > 0){
		
		print "Content-type: text/html\n\n";
		open(TEMPLATE,$detail_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
$strList=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
	$id=$1;
	$name=$2;
	$email=$3;
	$memo=$4;
	$file=$5;

	$strHtml=~s/__id__/$id/g;
	$strHtml=~s/__name__/$name/g;
	$strHtml=~s/__email__/$email/g;
	$strHtml=~s/__memo__/$memo/g;
	$strHtml=~s/__file__/$file/g;

		print $strHtml;
	}else{
		$strList="お問い合わせの情報は存在しません。削除された可能性があります。";
		print "Content-type: text/html\n\n";
		open(TEMPLATE,$index_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		$strHtml=~s/__INDEX__/$strList/g;
		&jcode::euc2sjis(\$strHtml);
		print $strHtml;
	}
}elsif($in{'cmd'} eq "del_confirm"){
	$strList="本当に削除してもよろしいですか？";
	$strList.="<a href=".$entry_cgi_path ."?cmd=del&id=".$in{'id'}."&email=".$in{'email'}.">[OK]</a>/<a href=".$entry_cgi_path .">戻る</a>";
	print "Content-type: text/html\n\n";

	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
	
}elsif($in{'cmd'} eq "del"){
	rename $csv_path,$csv_tmp_path;
	open(CSV_WRITE,$csv_write_path);
	open(CSV_TMP,$csv_tmp_path);
	@entry=<CSV_TMP>;
	close(CSV_TMP);
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		if(($1 eq $in{'id'}) && ($3 eq $in{'email'}) ){
			unlink $resume_dir.$5;
		}else{
			print CSV_WRITE $i;
		}
	}
	close(CSV_WRITE);
	
	$strList="削除しました。";
	$strList.="<a href=".$entry_cgi_path .">戻る</a>";
	
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}else{
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList.="<table>";
	foreach $i (@entry){
		$strList.="<tr>";
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		$strList.="<td><a href=".$entry_cgi_path ."?cmd=detail&id=".$1. "&email=". $3 .">[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]". $2 . "さん</a> (" . $4 . ")</td>";
		if(length $5 >0){
			$strList.="<td>[<a href=".$entry_cgi_path ."?cmd=resume&id=".$5.">レジュメ</a>]</td>";
		}else{
			$strList.="<td></td>";
		}
		$strList.="<td>[<a href=".$entry_cgi_path ."?cmd=del_confirm&id=".$1. "&email=". $3 .">削除</a>]</td></tr>";
	}
	$strList.="</table>";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}

sub replace{
}

sub replace_radio2{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($_[2] == $tmp2){
		$strHtml=~s/$tmp/checked/g;
	}else{
		$strHtml=~s/$tmp//g;
	}
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp/$tmp/g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

