
	
// jQuery(document).ready(function($){
// 	var slider = $('.bxslider').bxSlider({
// 		mode: 'fade',
// 		auto: true,
// 		pause: 5000,
// 		speed: 3000,
// 		responsive: true,
//         touchEnabled:false,
// 		onSlideAfter: function(){
//             slider.startAuto();
//         }
// 	});	
// });


$(function(){ 
	var firstSlider = $('.bxslider').bxSlider({ 
		  auto: true,
		  pause: 5000,
		  speed: 2800,
		  pager: false,
		  infiniteLoop: true,
		  mode: 'fade',
		  responsive: true,
          touchEnabled:false
	  }); 
	var secondSlider = $('.bxslider_2').bxSlider({ 
		  auto: true,
		  pause: 6000,
		  speed: 2500,
		  pager: false,
		  infiniteLoop: true,
		  mode: 'horizontal',
		  autoHover: true,
		  responsive: true,
          touchEnabled:false
	  });
  }); 
