��          �            x  X   y     �     �     �               2     @     H     [     k  &     O   �     �  #     L  (  �   u     �               1     A     ]     m     }  	   �     �  4   �  @   �     &  #   <         	                                     
                                 Adding CAPTCHA field on MW WP Form. This plugin needs MW WP Form version 2.2.0 or later. An unknown error occurred CAPTCHA Convert half alphanumeric Don't Convert. Don't display error. Dsiplay error Invalid MW WP Form CAPTCHA Number of lines Number of scratches Please input at least five characters. Please input the alphanumeric characters of five characters that are displayed. String to use http://plugins.2inc.org/mw-wp-form/ Project-Id-Version: MW WP Form CAPTCHA 1.2.0
Report-Msgid-Bugs-To: http://wordpress.org/tag/mw-wp-form-captcha
POT-Creation-Date: 2015-01-13 14:22:12+00:00
PO-Revision-Date: 2015-01-13 23:23+0900
Last-Translator: Takashi Kitajima <inc@2inc.org>
Language-Team: Takashi Kitajima <inc@2inc.org>
Language: ja_JP
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Generator: Poedit 1.7.3
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html__
X-Poedit-SearchPath-0: ..
 MW WP Form に CAPTCHA を追加するプラグインです。このプラグインは MW WP Form 2.2.0 以降が必要です。 An unknown error occurred CAPTCHA 半角英数字に変換 変換しない エラーを表示しない エラー表示 不正です。 MW WP Form CAPTCHA 線の数 スクラッチの数 最低でも5文字以上入力してください。 表示された5文字の英数字を入力してください。 使用する文字列 http://plugins.2inc.org/mw-wp-form/ 