	cmd	send
	media	__media__
お名前	__name_A__
ふりがな	__name_B__
メールアドレス	__mail_A__

メールアドレス（確認）	__mail_B__
年齢	__age__
性別	__sex__
現住所	__address__
自宅の最寄り駅
路線	__moyori_rosen__
駅	__moyori_eki__
連絡先
自宅	__renraku_jitaku__
携帯	__renraku_keitai__
最終学歴	__gakureki__
希望勤務開始日	__start_year__	__start_month__	__start_day__
勤務可能曜日	__day__
希望期間	__term__
希望職種　第１希望	__shokushu1__
希望職種　第2希望	__shokushu2__
希望勤務地	__kinmuchi__
職歴１（直近のご経験）
会社名	__shokureki_kaisha__
勤務期間	__shokureki_start__	__shokureki_end__
職種	__shokureki_shokushu__
雇用形態	__shokureki_keitai__
職務内容	__shokureki_naiyo__
職歴２（直近のご経験）
会社名	__shokureki2_kaisha__
勤務期間	__shokureki2_start__	__shokureki2_end__
職種	__shokureki2_shokushu__
雇用形態	__shokureki2_keitai__
職務内容	__shokureki2_naiyo__
使用ＯＳ	__os_win__ __os_mac__ __os_unix__ __os_other__
使用可能ソフト
Word	__soft_word__
Excel	__soft_excel__
PwerPoint	__soft_powerpoint__
Access	__soft_access__
その他	__soft_other__
英語資格
TOEIC	__toeic_score__	__toeic_shutoku__
TOEFL	__toefl_score__ __toefl_shutoku__
英検	__eiken_score__ __eiken_shutoku__
その他保有資格	__shikaku__
スキルに関する補足PR	__hosokuPR__
自己PR・ご希望など	__PR_kibou__
ご質問・お問い合わせ	__otoiawase__

