<div class="l-side-menu">
<div class="p-side-menu-title">
        <h3>会議・イベント運営</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="<?php echo home_url();?>/kokusai/">サービス・運営概要</a></p>
        <p><a href="<?php echo home_url();?>/kokusai/event">事例紹介</a></p>
        <p><a href="<?php echo home_url();?>/kokusai/detail">ご提案から実施まで</a></p>
        <p><a href="<?php echo home_url();?>/kokusai/list">運営実績</a></p>
        <p><a href="<?php echo home_url();?>/kokusai/online">パッケージプラン</a></p>
        <!-- <p><a href="<?php echo home_url();?>/kokusai/studio">ISSスタジオ</a></p> -->
    </div>
</div>