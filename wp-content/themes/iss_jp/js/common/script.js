$(function(){
	"use strict";
	
	//ブラウザサイズによる画像の切り替え
	ImgChange();
	SpMenuTopPosition();
	btnTopFlex();
	SideScroll();
	//LineClamp();
	//ContainerHeight();
	
	// TOGGLE NAV	
	$( "#global-nav-area" ).hide();
	var flg = 'default';
	$( "#toggle" ).click( function(){

		$( "#toggle-btn-icon" ).toggleClass( "close" );

		if( flg === "default" ){
			
			flg = "close";

			$( "#global-nav-area" ).addClass( "show" );
			//setTimeout(function(){
//				$('.global-list li a').matchHeight();
//			},10);

		}else{
			
			flg = "default";
			$( "#global-nav-area" ).removeClass( "show" );
			//$('.global-list li a').off();
		}

		$("#global-nav-area").slideToggle('slow');
	});
	
	
	$( ".js-global-nav-detail" ).hide();
	$( ".js-underlayer" ).click(
		function(){
			$(this).next().toggleClass( "show" );
	
			if( $(this).next().hasClass( "show" ) ){
				
				$(this).find(".js-menu").text( "―" );
				$(this).next().addClass( "show" );
	
			}else{
				
				$(this).find(".js-menu").text( "＋" );
				$(this).next().removeClass( "show" );
			}
	
			$(this).next().slideToggle('slow');
		}
	);	
	
	
	$(".js-globalMenu").hover(
		function(){
			$(this).next('.header_courses_menu').addClass('on_hover');
		},
		function () {
			$(this).next('.header_courses_menu').removeClass('on_hover');
		}
	);
	
	$(".header_courses_menu").hover(
		function(){
			$(this).addClass('on_hover');
		},
		function () {
			$(this).removeClass('on_hover');
		}
	);
	
	//$(document).on('mouseleave','.header_courses_menu',function(){
//		$(this).next('.header_courses_menu').removeClass('on_hover');
//	});
	
	//スムーススクロール
	$('a[href^=#]').click(function(){
		var speed = 700;
		var href= $(this).attr("href");
		var target = $(href === "#" || href === "" ? 'html' : href);
		var position = target.offset().top-100;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
	
	var topBtn = $('#page-top');    
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
		
		if ($(this).scrollTop() > 100) {
            topBtn.fadeIn(1000);
			
        } else {
			topBtn.fadeOut();
        }
    });	
	
	
	//画面読み込み時もしくはブラウザリサイズ時に行う処理
	$(window).on('load resize', function(){
		//ブラウザサイズによる画像の切り替え
		ImgChange();
		SpMenuTopPosition();
		btnTopFlex();
		SideScroll();
		ContainerHeight();
		
	});
	
	//画面読み込み時もしくはブラウザリサイズ時に行う処理
	$(window).on('load', function(){
				
		//PCサイドメニューコンベンション開閉
		if ($( ".p-conference-list" ).hasClass("close")){
			$( ".p-conference-list" ).hide();
			$( ".p-conference" ).addClass( "close" );
		} else {
			$( ".p-conference-list" ).show();
			$( ".p-conference" ).removeClass( "close" );
		}
		
		var flg3 = 'default';
		$( ".p-conference" ).click( function(){
	
			$( ".p-conference" ).toggleClass( "close" );
			$( ".p-conference-list" ).toggleClass( "close" );
	
			if( flg3 === "default" ){
				
				flg3 = "close";
	
				$( ".p-conference-list" ).addClass( "show" );
	
			}else{
				
				flg3 = "default";
				$( ".p-conference-list" ).removeClass( "show" );
			}
	
			$(".p-conference-list").slideToggle(0);
			//$(".p-conference-list").slideToggle('show');
		});
		
		LineClamp();
		
	});
	
	SlideCheck();
	
});


function  ImgChange(){
	"use strict";
	
	//ブラウザサイズによる画像の切り替え
	var widimage = window.innerWidth;
	
	if( widimage < 1001 ){
		$('.is-imgChange').each( function(){
			
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
		
		if( widimage < 481 ){
			$('.is-imgChange2').each( function(){
				$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
			});
		} else {
			$('.is-imgChange2').each( function(){
				$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
			});
		}
		
	} else {
		
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}

}

function  SpMenuTopPosition(){
	"use strict";
	
	//ブラウザサイズによる画像の切り替え
	var widimage = window.innerWidth;
	
	if( widimage < 1001 ){
		var Header_height = parseInt($('#header').height()) + 4;
		$('#global-nav-area').css('top', Header_height + 'px');
		
	} else {
		
		//$('.is-imgChange').each( function(){
//			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
//		});
	}

}

function  btnTopFlex(){
	$(function(){
		
		var min = 20;
		var max = 70;
		var mid = 16;
		
		var windowSize = window.innerWidth;
		var size = windowSize / mid;
		
		if (size < min) size = min;
		if (size > max) size = max;
		
		if( windowSize < 1001 ){
			$(".js-btn_inquiry").css('height', size + 'px');
			$(".js-btn_registration").css('height', size + 'px');
		}
		else {
			$(".js-btn_inquiry").css('height', '32px');
			$(".js-btn_registration").css('height', '32px');
		}
		
	});
}

function  SideScroll(){
	"use strict";
	
	var widimage = window.innerWidth;
	
	if( widimage < 1001 ){
		
	} else {
		//TOPページ以外に適用
		if($('.l-container').length){			
			var side = $(".l-side");
			// #sideのオブジェクトをsideへ
			var main = $(".l-container");
			// #mainのオブジェクトをmainへ
			var min_move = main.offset().top;
			// #side が動ける最初の地点（main要素のtopの位置）
			var max_move = main.offset().top + main.height() - side.height() - 1*parseInt(side.css("top") );
			// max_moveはトップから #side が動ける最終地点までの長さ → #main 要素の内側の高さ内
			// max_move ←（ #mainボックスのトップの位置の値　＋　#mainボックスの高さ　ー　#sideの高さ　ー　サイドのトップ値✕２）
			var margin_bottom = max_move - min_move    ;
			// side要素の一番下にいる時のmargin-top値の計算
			
			//l-containerが小さい場合は追従させない
			if (main.height() > 1100){
				// スクロールした時に以下の処理        
				$(window).bind("scroll", function() {
						 
					var wst =  $(window).scrollTop();
					// スクロール値が wst に入る
						 
					// スクロール値が main 要素の高さ内にいる時以下
					if( wst > min_move && wst < max_move ){
						var margin_top = wst - min_move - 50;
						// スクロールした値から min_move（#mainのtopの表示位置）を引いたのを margin_top へ
						/*$('.l-side').css('top', '0px');*/
						side.animate({"margin-top": margin_top},{duration:0,queue:false});
						//side.css('position','absolute');
						// サイド CSSの margin-top の値を、変数の margin_top にする
							 
					// スクロールした値が min_move（main要素の高さより小さい）以下の場合はCSSのマージントップ値を0にする
					}else if( wst < min_move ){
						side.animate({"margin-top":0},{duration:0,queue:false});
						//side.css('position','static');
					 
					// スクロールした値が max_move （main要素の高さより大きい）以上の場合以下
					}else if( wst > max_move ){
						side.animate({"margin-top":margin_bottom - 50},{duration:0,queue:false});
					}
				});
			}
		}
		
	}

}

function  ContainerHeight(){
	"use strict";
	
	var widimage = window.innerWidth;
	if( widimage < 1001 ){
		
		//var main_height = $(".l-main").height() + 50;
//		$(".l-container").css('height', main_height + 'px');
	} else {
		
		if( $(".l-main").height() < $(".l-side").height()){
			var container_height = $(".l-side").height() + 100;
			$(".l-container").css('height', container_height + 'px');
		}
		else {
			var container_height2 = $(".l-main").height() + 100;
			$(".l-container").css('height', container_height2 + 'px');
		}
		
	}
}

function  LineClamp(){
	"use strict";
	
	//$(document).on(
//		"click", ".p-clampclick", (
//			function(){
//				$(this).hide();
//				$(this).parent('.main').removeClass('onhidden');
//			}
//		)
	//	);
	$('.readmore').click(function(){
		//alert('a');
		$(this).prev('.p-clampmessage').removeClass('onhidden');
		$(this).hide();
		
	});
}

function  SlideCheck(){
	"use strict";
	
	//パララックス-ブラウザによって使うかどうか判断（IEのみ使用）
	var ua = navigator.userAgent.toLowerCase();
	var ver = navigator.appVersion.toLowerCase();	 
	// IE(11以外)
	var isMSIE = (ua.indexOf('msie') > -1) && (ua.indexOf('opera') == -1);
	// IE6
	var isIE6 = isMSIE && (ver.indexOf('msie 6.') > -1);
	// IE7
	var isIE7 = isMSIE && (ver.indexOf('msie 7.') > -1);
	// IE8
	var isIE8 = isMSIE && (ver.indexOf('msie 8.') > -1);
	// IE9
	var isIE9 = isMSIE && (ver.indexOf('msie 9.') > -1);
	// IE10
	var isIE10 = isMSIE && (ver.indexOf('msie 10.') > -1);
	// IE11
	var isIE11 = (ua.indexOf('trident/7') > -1);
	// IE
	var isIE = isMSIE || isIE11;
	// Edge
	var isEdge = (ua.indexOf('edge') > -1);	 
	// Google Chrome
	var isChrome = (ua.indexOf('chrome') > -1) && (ua.indexOf('edge') == -1);
	// Firefox
	var isFirefox = (ua.indexOf('firefox') > -1);
	// Safari
	var isSafari = (ua.indexOf('safari') > -1) && (ua.indexOf('chrome') == -1);
	// Opera
	var isOpera = (ua.indexOf('opera') > -1);	
	if(isIE) {
		easing();
	}
	if(isIE6) {
		easing();
	}
	if(isIE7) {
		easing();
	}
	if(isIE8) {
		easing();
	}
	if(isIE9) {
		easing();
	}
	if(isIE10) {
		easing();
	}
	if(isIE11) {
		easing();
	}
	if(isEdge) {
		easing();
	}	 
	if(isChrome) {
		return true;
	}
	if(isFirefox) {
		easing();
	}
	if(isSafari) {
		return true;
	}
	if(isOpera) {
		return true;
	}
}

//スクロール時にIEやFirefoxなどのカクカク解消
function  easing(){
	$(function(){
		scrLength = 200;
		scrSpeed = 400;
		scrEasing = 'easeOutCirc';
	 
		var mousewheelevent = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
		$(document).on(mousewheelevent,function(e){
			e.preventDefault();
			var delta = e.originalEvent.deltaY ? -(e.originalEvent.deltaY) : e.originalEvent.wheelDelta ? e.originalEvent.wheelDelta : -(e.originalEvent.detail);
			if (delta < 0){
				scrSet =  $(document).scrollTop()+scrLength;
			} else {
				scrSet =  $(document).scrollTop()-scrLength;
			}
			$('html,body').stop().animate({scrollTop:scrSet},scrSpeed,scrEasing);
			return false;
		});
	});
}




//問合せページのトグル
$(function(){
	$('.toggle_title').click(function(){
		$(this).toggleClass('selected');
		$(this).next().slideToggle();
	});
});

