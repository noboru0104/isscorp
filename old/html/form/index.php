<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis" />
<meta name="robots" content="follow,index,noydir">
<meta http-equiv="keywords" content="ISS,アイ・エス・エス,お問い合わせ" />
<title>ISS総合お問い合わせフォーム</title>
<link href="common/form_style.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript"><!--
window.onload = function(){
document.getElementById("Accept").onclick = function(){
document.getElementById("nextButton").disabled = false;
}
document.getElementById("NG").onclick = function(){
document.getElementById("nextButton").disabled = true;
}
}
--></script>

<style type="text/css">
<!--
#confirm td #agree{
	text-align: center;
}
#agree-box {
	background-color:#F9F9F9;
	width: 512px;
	height: 260px;
	overflow: auto;
	border: 1px solid #CCCCCC;
	text-align: left;
	margin-top: 0;
	margin-right: auto;
	margin-bottom: 10;
	margin-left: auto;
	padding-top: 16px;
	padding-right: 12px;
	padding-bottom: 16px;
	padding-left: 12px;
	}
.dc {
	list-style-type: decimal;
	list-style-position: inside;
}     /* 数字 */
-->
</style>

</head>
<body>

<!-- Header Begin -->
<div id="header">
<div class="contentform"><a href="../index.html"><img src="../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../index.html"><img src="../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣、法人研修／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi"></div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<div id="formcont">
<h2><img src="images/ind_h2.jpg" alt="お問い合わせフォーム" width="700" height="78" /></h2>

<div style="margin-left: 20px; margin-right: 20px; margin-top: 10px; margin-bottom: 5px;">
<p style="font-size:12px;">フォームにてサービスに関する総合お問い合わせを承っております。どうぞご利用ください。</p>
</div>
<table border="0" id="confirm">
  <tr>
    <td class="t1">お問い合わせフォームのご利用</td>
  </tr>
  <tr>
    <td>「個人情報の取扱いについて」の内容をご確認ください。同意いただける場合には「同意します」にチェック後、『上記「個人情報の取扱いについて」に同意の上、お問い合わせ』をクリックし、表示されるフォームに入力を行ってください。</td>
  </tr>
  <tr>
    <td id="agree" style="text-align: center;"><div id="agree-box">
      <div style="width:490px;">
        <p align="center" class="ltx"> <strong> 個人情報の利用目的および取扱いについて </strong></p>
        <br />
        <ol class="dc">
          <li>個人情報の利用目的 <br />
            お問い合わせ、ご相談の対応及び見積書作成、資料の送付の目的</li>
          <li>個人情報の第三者への提供 </li>
          <p>当社は、法令の定める場合を除き、あらかじめご本人の同意を得ずに、個人情報を第三者に提供することは致しません。 </p>
          <li>個人情報の外部委託 </li>
          <p>個人情報の処理について、外部委託をすることがあります。</p>
          <li>個人情報提出の任意性 </li>
          <p>個人情報のご提出は任意です。但し、必要な情報を提供頂かない場合は、適切な対応ができない場合があります。</p>
          <li>個人情報の開示等の請求</li>
          <p>ご提出頂いた個人情報に関する苦情の申し出並びに個人情報の開示等請求（1.開示のご請求、2.利用目的の通知のご請求、3.訂正のご請求、4.追加のご請求、5.消去のご請求、6.利用停止または第三者への提供停止のご請求）の権利があります。<br />上記権利の行使をご希望の場合、下記の「個人情報の取扱いに関するお問い合わせ先」宛、お電話あるいは書面でご請求ください。</p>
        </ol>
        <br />
        <p align="right">株式会社アイ・エス・エス<br />
        個人情報保護管理者　白木原 孝次</p>
        <br />
        <br />
        <p><strong>■個人情報の取扱いに関するお問い合わせ先</strong></p>
        <p>株式会社 アイ・エス・エス　個人情報保護管理者  白木原 孝次<br />
          〒102-0083 東京都千代田区麹町3-1-1 麹町311ビル9階 <br />
          電話 03-3230-7731　　e-mail：pv_info@issjp.com<br />
          個人情報保護方針は、当社ホームページ<a href="http://www.issjp.com/" target="_parent">http://www.issjp.com</a>をご参照下さい。</p>
      </div>
    </div>
      <form action="contact.php" method="get" id="mainForm" name="mainForm">
	<div>
  <label>
      <input name="confirm" type="radio" id="NG" checked="checked" />
      同意しません</label>
    <label>
      <input type="radio" name="confirm" id="Accept" />
      同意します</label>
    <br />
    <br />
    <input name="submit" type="button" disabled="disabled" id="nextButton" value="上記「個人情報の取り扱いについて」に同意の上、お問い合わせ" onClick="location = 'contact.php'"/>
    <br />
  </div>
</form>
</table>
</div>

<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../common/images/pixel_trans.gif" width="816" height="15" alt="" class="spacer" />
<div class="menuList">
<ul>
  <li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>
</div>
</div>
</div>
<!-- Footer End -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body>
</html>
