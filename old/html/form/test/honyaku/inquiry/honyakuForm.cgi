#!/usr/bin/perl
############################################################
#
#    ISS 翻訳者の登録フォーム
#
############################################################
require './cgi-lib.pl';
require './jcode.pl';
use CGI::Carp qw(fatalsToBrowser);

############################################################
#    設定項目
############################################################

## 送信先メールアドレス
$toAdd = 'reg_honyaku@issjp.com';

## CCメールアドレス
$ccAdd = '';

## BCCメールアドレス
$bccAdd = '';

## 送信者メールアドレス
$fromAdd = 'info@issjp.com';

## メールタイトル
$subjectUser = '【株式会社アイ・エス・エス】ご登録ありがとうございます';
$subjectAdmin = '【株式会社アイ・エス・エス】翻訳者の登録がありました';

## 確認画面HTML
$confirmHtmlFile = './honyaku_confirm.html';

## エラー画面HTML
$errorHtmlFile = './honyaku_error.html';

## 完了画面HTML
$endHtmlFile = './honyaku_complete.html';

## sendmailパス
$sendmail = "/usr/sbin/sendmail -t -f$fromAdd";

############################################################
#    プログラム開始
############################################################
&decode();
&setTime();
&checkForm();

if($g_html{'f_conf'}){
	&outHtml($confirmHtmlFile);
}elsif($g_html{'f_exe'}){
	&sendMailUser();
	&sendMailAdmin();
	&outHtml($endHtmlFile);
}

############################################################
#    フォーム処理
############################################################
sub decode{
	my ($key,$value);
	
	&ReadParse();
	while(($key,$value) = each %in){
		&jcode::convert(\$key,'euc');
		&jcode::convert(\$value,'euc','','z');
		$value =~ s/&/&amp;/gi;
		$value =~ s/\"/&quot;/gi;
		$value =~ s/\'/&#39;/gi;
		$value =~ s/</&lt;/gi;
		$value =~ s/>/&gt;/gi;
		$value =~ s/&lt;br&gt;/<br>/gi;
		$value =~ s/\r\n?/\n/g;
		$value =~ s/\n/<br>/g;
		$value =~ s/\0/ /g;
		
		if($key !~ /^f_/){
			$g_hidden .= qq(<input type="hidden" name="$key" value="$value">\n);
		}
		$g_html{$key} = $value;
	}
	$g_html{'hidden'} = $g_hidden;
	
}

############################################################
#    日時取得
############################################################
sub setTime{
	my ($sec,$min,$hour,$day,$mon,$year,$wday) = localtime();
	
	$g_time{'sec'} = sprintf("%02d",$sec);
	$g_time{'min'} = sprintf("%02d",$min);
	$g_time{'hour'} = sprintf("%02d",$hour);
	$g_time{'day'} = sprintf("%02d",$day);
	$g_time{'mon'} = sprintf("%02d",$mon+1);
	$g_time{'year'} = sprintf("%04d",$year+1900);
	$g_time{'all'} = "$g_time{'year'}/$g_time{'mon'}/$g_time{'day'} $g_time{'hour'}:$g_time{'min'}:$g_time{'sec'}";
}

############################################################
#    エラーチェック
############################################################
sub checkForm{
	my $flag = 1;
	
	if($g_html{'name1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'name2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・名」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'kana1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓・カタカナ」が入力されていません。</li>);
		$flag = 0;
	}elsif($g_html{'kana1'} !~ /^(\xa5[\xa0-\xff])+([\xa1][\xa0]*)*(\xa5[\xa0-\xff])+$/){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓・カタカナ」にカタカナ以外が入力されています。</li>);
		$flag = 0;
	}
	if($g_html{'kana2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・名・カタカナ」が入力されていません。</li>);
		$flag = 0;
	}elsif($g_html{'kana2'} !~ /^(\xa5[\xa0-\xff])+([\xa1][\xa0]*)*(\xa5[\xa0-\xff])+$/){
		$g_html{'error'} .= qq(<li class="mtx">「お名前・姓・カタカナ」にカタカナ以外が入力されています。</li>);
		$flag = 0;
	}
	if($g_html{'birth_year'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「生年月日・年」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'birth_month'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「生年月日・月」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'birth_day'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「生年月日・日」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'sex'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「性別」が選択されていません。</li>);
		$flag = 0;
	}
	if($g_html{'email'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「メールアドレス」が入力されていません。</li>);
		$flag = 0;
	}else{
		if($g_html{'email'} !~ /^[\w\-\~\.]+@[\w\-\~].[\w\-\~\.]+$/){
			$g_html{'error'} .= qq(<li class="mtx">「メールアドレス」の形式が不正です。</li>);
			$flag = 0;
		}
	}
	$zip = $g_html{'zip1'} . $g_html{'zip2'};
	if($g_html{'zip1'} eq '' or $g_html{'zip2'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「郵便番号」が入力されていません。</li>);
#		$flag = 0;
	}elsif($zip !~ /^\d{7}$/){
		$g_html{'error'} .= qq(<li class="mtx">「郵便番号」が正しく入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'pref'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「都道府県」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'address1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「郡市区町村 」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'address2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「番地」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'tel1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「TEL・市外局番」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'tel2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「TEL・市内局番」が入力されていません。</li>);
		$flag = 0;
	}
	if($g_html{'tel3'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">「TEL・下4桁」が入力されていません。</li>);
		$flag = 0;
	}
	$tel = $g_html{'tel1'} . $g_html{'tel2'} . $g_html{'tel3'};
	if($tel !~ /^\d{10}$/){
		$g_html{'error'} .= qq(<li class="mtx">「TEL」が正しく入力されていません。</li>);
		$flag = 0;
	}
	$fax = $g_html{'fax1'} . $g_html{'fax2'} . $g_html{'fax3'};
	if($fax ne ''){
		if($fax !~ /^\d{10}$/){
			$g_html{'error'} .= qq(<li class="mtx">「FAX」が正しく入力されていません。</li>);
			$flag = 0;
		}
	}
#	if($g_html{'agree'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">「サービスの利用規約」にチェックが入っていません。</li>);
#		$flag = 0;
#	}
	
	if($flag == 0){
#		$g_html{'error'} .= qq(<li class="mtx">ブラウザの戻るボタンでお戻りのうえ、お手数ですがもう一度入力しなおしてください。 </li>);
		&outHtml($errorHtmlFile);
	}
	
	for($i=1;$i<=4;$i++){
		$name = 'lang' . $i;
		if($g_html{$name}){
			$g_html{'lang'} .= $g_html{$name} . ' ';
		}
	}
}

############################################################
#    HTML出力
############################################################
sub outHtml{
	my $file = shift;
	my ($html,@htmlList,$htmlAll);
	my @tmp;
	
	open(HTML,"<$file") or die();
	read(HTML,$html,-s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	foreach (@htmlList){
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_."\n";
		}else{
			$htmlAll .= $_."\n";
		}
	}
	
#	&jcode::convert(\$htmlAll,'sjis','euc');
	print "Content-type:text/html; charset=euc-jp\n\n";
	print $htmlAll;
	
	exit;
}

############################################################
#    エンドユーザメール送信
############################################################
sub sendMailUser{
	my $mailBody;

	foreach $key(keys %g_html){
		$value = $g_html{$key};
		$value =~ s/&amp;/&/gi;
		$value =~ s/&lt;/</gi;
		$value =~ s/&gt;/>/gi;
		$g_html{$key} = $value;
	}
	
	$mailBody = <<MAIL;
To: $g_html{'email'}
From: $fromAdd
Subject: $subjectUser
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

$g_html{'name1'} $g_html{'name2'} 様

ご登録いただき誠にありがとうございます。
このメールはシステムより自動的に送信されております。

ご応募内容を考慮し、あなたに適した業務が発生する際に、
弊社よりご連絡を差し上げます。

万が一このメールに心当たりが無い場合は、お手数ですが
下記アドレスまでお知らせ頂きますようお願い致します。

代表メールアドレス：info\@issjp.com

今後ともよろしくお願いします。
-------------------
株式会社アイ・エス・エス
ランゲージ事業部　翻訳グループ
TEL: 03-3230-2521
-------------------

MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}

############################################################
#    管理者宛メール送信
############################################################
sub sendMailAdmin{
	my $mailBody;
	
	$g_html{'career'} =~ s/<br>/\n/g;
	$g_html{'qualification'} =~ s/<br>/\n/g;
	$g_html{'achievement'} =~ s/<br>/\n/g;
	$g_html{'salespoint'} =~ s/<br>/\n/g;
	
	$mailBody = <<MAIL;
To: $toAdd
From: $g_html{'email'}
Cc: $ccAdd
Bcc: $bccAdd
Subject: $subjectAdmin
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

翻訳者の登録がありました。
■氏名
$g_html{'name1'} $g_html{'name2'}
■フリガナ
$g_html{'kana1'} $g_html{'kana2'}
■生年月日
$g_html{'birth_year'}年$g_html{'birth_month'}月$g_html{'birth_day'}日
■性別
$g_html{'sex'}
■E-mailアドレス
$g_html{'email'}
■郵便番号
$g_html{'zip1'}-$g_html{'zip2'}
■都道府県
$g_html{'pref'}
■郡市区町村
$g_html{'address1'}
■番地
$g_html{'address2'}
■TEL
$g_html{'tel1'}-$g_html{'tel2'}-$g_html{'tel3'}
■FAX
$g_html{'fax1'}-$g_html{'fax2'}-$g_html{'fax3'}
■一日の翻訳量
$g_html{'performance_word'}ワード　$g_html{'performance_page'}ページ
■言語の種類
$g_html{'lang'}
■最終学歴、職業
$g_html{'career'}
■資格、取得技術
$g_html{'qualification'}
■これまでの主要な実績
$g_html{'achievement'}
■セールスポイント
$g_html{'salespoint'}
MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}
