############################################################
#    設定項目
############################################################

## 管理者宛メールアドレス
$toAdd = 'n-sakamoto@issjp.com';

## 送信者アドレス
$fromAdd = 'info@issjp.com';

## メールタイトル
$subject = '【新着情報】記事が投稿されました';

## フォームテンプレート
$g_formHtmlFile = './index.html';

## 確認画面テンプレート
$g_confirmHtmlFile = './confirm.html';

## エラー画面テンプレート
$g_errorHtmlFile = './error.html';

## 完了画面テンプレート
$g_endHtmlFile = './thanks.html';

## システムエラーテンプレート
$g_sysErrorHtmlFile = './sys_error.html';

## 画像一時保存場所
$g_imageTmpDir = '../tmp_img/';
$g_imageTmpPath = '../tmp_img/';

## 画像保存場所
$g_imageDir = '../up_img/';

$g_imagePath = '../up_img/';

## PDF保存場所
$g_pdfDir = '../up_pdf/';

$g_pdfPath = '../up_pdf/';

## 記事用HTMLテンプレート
$g_templateFile = './news.html';

## HTML保存ディレクトリ
$g_htmlDir = '../news/';

## 投稿データ保存ディレクトリ
$g_dataDir = '../admin/data/';

## 投稿データ保存CSV（一覧用）
$g_dataCsv = $g_dataDir . 'data.csv';

## 投稿記事No用CSV
$g_numberCsv = $g_dataDir . 'number.csv';

## 画像横幅
$g_width = 160;

## sendmailパス
$sendmail = '/usr/sbin/sendmail -t';

## lockファイル
$g_lockFile = $g_dataDir . 'lock/lockfile';

## 詳細記事作成テキスト情報
$g_detailText[1] = '作成しない';
$g_detailText[2] = '作成する';

## タイトルからのリンクテキスト情報
$g_titleLinkText[1] = 'なし';
$g_titleLinkText[2] = 'URL';
$g_titleLinkText[3] = 'PDF';

1;