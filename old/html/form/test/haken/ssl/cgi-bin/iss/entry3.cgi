#!/usr/bin/perl

require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

&ReadParse;


$FROM		='touroku@isssc.co.jp';
$TO		='touroku@isssc.co.jp';
#$TO		='ichino@si-seed.com';
#$TO2		='yumiko@loveworkaholic.com';

$Subject 		= "ISS ONLINE CLIENT FORM";

$entry_cgi		="/cgi-bin/iss/entry.cgi";

$cgi_dir		="..";

$csv_dir		=$cgi_dir . "/secret/iss/";

$csv_path		=">>" . $csv_dir . "registered.csv";

$template_dir		=$cgi_dir . "/template/iss/";

$confirm_html_path	=$template_dir . "confirm.html";
$entry_html_path	=$template_dir . "entry.html";
$thanks_html_path	=$template_dir . "thanks.html";
$pref_txt_path		=$template_dir . "pref.txt";
$csv_txt_path		=$template_dir . "csv.txt";
$mail_txt_path		=$template_dir . "mail.txt";
$mail_user_txt_path	=$template_dir . "mail_user.txt";

$epoch=time;

if(length $in{'cmd'} eq 0){

}else{

	$name=$in{'name'};	$name=~s/\r\n/ /g;	$name=~s/,/./g;
	if(length $name eq 0)	{$err_name='<font color=red>←お名前（御社名・ご担当者名）は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_name);

	$name2=$in{'name2'};	$name2=~s/\r\n/ /g;	$name2=~s/,/./g;
	if(length $name2 eq 0)	{$err_name2='<font color=red>←ふりがなは必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_name2);

	$zip1=$in{'zip1'};			$zip1=~s/\r\n/ /g;	$zip1=~s/,/./g;
	$zip2=$in{'zip2'};			$zip2=~s/\r\n/ /g;	$zip2=~s/,/./g;

	open ( FH_PREF , $pref_txt_path ) ;for ( $a = 0 ; $a <= $in{'pref'} ; $a++ ){$pref=<FH_PREF>;$pref=~s/\n//g;}close(FH_PREF);

	$address1	=$in{'address1'};	$address1=~s/\r\n/ /g;	$address1=~s/,/./g;
	$address2	=$in{'address2'};	$address2=~s/\r\n/ /g;	$address2=~s/,/./g;
	$address3	=$in{'address3'};	$address3=~s/\r\n/ /g;	$address3=~s/,/./g;

	$tel1		=$in{'tel1'};	$tel1=~s/\r\n/ /g;		$tel1=~s/,/./g;
	$tel2		=$in{'tel2'};	$tel2=~s/\r\n/ /g;		$tel2=~s/,/./g;
	$tel3		=$in{'tel3'};	$tel3=~s/\r\n/ /g;		$tel3=~s/,/./g;

	if(length $tel1 eq 0)		{$err_tel='<font color=red>←市外局番は必須項目です</font>' ;}
	elsif(!is_hankaku($tel1))	{$err_tel='<font color=red>←市外局番は半角でお願いします</font>' ;}
	elsif(length $tel2 eq 0)	{$err_tel='<font color=red>←市内局番は必須項目です</font>' ;}
	elsif(!is_hankaku($tel2))	{$err_tel='<font color=red>←市内局番は半角でお願いします</font>' ;}
	elsif(length $tel3 eq 0)	{$err_tel='<font color=red>←電話番号は必須項目です</font>' ;}
	elsif(!is_hankaku($tel3))	{$err_tel='<font color=red>←電話番号は半角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_tel);

	$fax1		=$in{'fax1'};	$fax1=~s/\r\n/ /g;		$fax1=~s/,/./g;
	$fax2		=$in{'fax2'};	$fax2=~s/\r\n/ /g;		$fax2=~s/,/./g;
	$fax3		=$in{'fax3'};	$fax3=~s/\r\n/ /g;		$fax3=~s/,/./g;

	$mail_A=$in{'mail_A'};		$mail_A=~s/\r\n/ /g;		$mail_A=~s/,/./g;
	$mail_B=$in{'mail_B'};		$mail_B=~s/\r\n/ /g;		$mail_B=~s/,/./g;
	if(length $mail_A eq 0)	{$err_mail_A='<font color=red>←メールアドレスは必須項目です</font>' ;}
	elsif(!is_hankaku($mail_A))	{$err_mail_A='<font color=red>←メールアドレスは半角でお願いします</font>' ;}
	elsif($mail_A ne $mail_B)	{$err_mail_A='<font color=red>←メールアドレスが一致しません<font>' ;	$err_mail_B='<font color=red>←メールアドレスが一致しません<font>' ;}
	&jcode::euc2sjis(\$err_mail_A);
	&jcode::euc2sjis(\$err_mail_B);

	$message=$in{'message'};

	$TO_user=$mail_A;

}

if($in{'cmd'} eq 'send'){

	use lib "./";
	use Lite;
#	use MIME::Lite;

	open(TEMPLATE,$mail_txt_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;

	&jcode::sjis2jis(\$strHtml);

	$type = 'multipart/mixed';
	$msg = MIME::Lite->new( From    =>$FROM,To      =>$TO,Subject =>$Subject,Type    =>$type);
	$msg->attach(Type     =>'TEXT',   Data     =>$strHtml);
	$msg->send;
#ichino
#	$msg1 = MIME::Lite->new( From=>$FROM,To=>$TO1,Subject=>$Subject,Type=>$type);
#	$msg1->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg1->send;

#inoue
#	$msg2 = MIME::Lite->new( From=>$FROM,To=>$TO2,Subject=>$Subject,Type=>$type);
#	$msg2->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg2->send;

	open(TEMPLATE,$mail_user_txt_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}

	&replace;

	$msg_user = MIME::Lite->new( From=>$FROM,To=>$TO_user,Subject=>$Subject,Type=>$type);
	$msg_user->attach(Type=>'TEXT',Data=>$strHtml);
	$msg_user->send;

	open(CSV_TEMPLATE,$csv_txt_path);
	@entry=<CSV_TEMPLATE>;
	close(CSV_TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	$strHtml.="\n";

	open(CSV,$csv_path);
	print CSV $strHtml;
	close(CSV);


	open(TEMPLATE,$thanks_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;

	print "Content-type: text/html\n\n";
	print $strHtml;

	exit;
}elsif($in{'cmd'} eq 'confirm'){
	if((
(length	$err_name)+
(length	$err_name2)+
(length	$err_tel)+
(length	$err_mail_A)+
(length	$err_mail_B)
) == 0 ){
#確認画面へ
		open(TEMPLATE,$confirm_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){	$strHtml.=$i;}
		&replace;
		print "Content-type: text/html\n\n";
		print $strHtml;
	}else{
		print "Content-type: text/html\n\n";
		$err='<br><font color=red>入力事項に不十分な部分がございます。ご確認の上、再度エントリーしてください。</font>';
		&jcode::euc2sjis(\$err);
		open(TEMPLATE,$entry_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){
			$strHtml.=$i;
		}
		&replace;
		$strHtml=~s/__entry_cgi_path__/$entry_cgi_path/g;
		print $strHtml;
	}
}elsif($in{'cmd'} eq 'edit'){
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print "Content-type: text/html\n\n";
	print $strHtml;
}else{
#init open
	$err_name="";
	$err_name2="";
	$err_company_name="";
	$err_company_name2="";
	$err_tel="";
	$err_mail_A="";
	$err_mail_B="";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print $strHtml;
}

sub replace{
	$strHtml=~s/__epoch__/$epoch/g;
	$strHtml=~s/__entry_cgi__/$entry_cgi/g;
	$strHtml=~s/__err__/$err/g;
	$strHtml=~s/__name__/$name/g;
	$strHtml=~s/__name2__/$name2/g;
	$strHtml=~s/__company_name__/$company_name/g;
	$strHtml=~s/__company_name2__/$company_name2/g;
	$strHtml=~s/__zip1__/$zip1/g;
	$strHtml=~s/__zip2__/$zip2/g;

	&replace_select02d('pref',47);
	$strHtml=~s/__pref__/$pref/g;
	$strHtml=~s/__pref_num__/$in{'pref'}/g;

	$strHtml=~s/__address1__/$address1/g;
	$strHtml=~s/__address2__/$address2/g;
	$strHtml=~s/__address3__/$address3/g;

	$strHtml=~s/__tel1__/$in{'tel1'}/g;
	$strHtml=~s/__tel2__/$in{'tel2'}/g;
	$strHtml=~s/__tel3__/$in{'tel3'}/g;

	$strHtml=~s/__fax1__/$in{'fax1'}/g;
	$strHtml=~s/__fax2__/$in{'fax2'}/g;
	$strHtml=~s/__fax3__/$in{'fax3'}/g;

	$strHtml=~s/__mail_A__/$mail_A/g;
	$strHtml=~s/__mail_B__/$mail_B/g;

	$strHtml=~s/__message__/$message/g;

	$strHtml=~s/__err_name__/$err_name/g;
	$strHtml=~s/__err_name2__/$err_name2/g;
	$strHtml=~s/__err_company_name__/$err_company_name/g;
	$strHtml=~s/__err_company_name2__/$err_company_name2/g;
	$strHtml=~s/__err_tel__/$err_tel/g;
	$strHtml=~s/__err_mail_A__/$err_mail_A/g;
	$strHtml=~s/__err_mail_B__/$err_mail_B/g;
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){

				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

sub is_zenkaku{
	$tmp = $_[0];
	&jcode::sjis2euc(\$tmp);
	if($tmp !~ /[\x00-\x7f]/){
		return 1;
	}else{
		return 0;
	}
}

sub is_hankaku{
	$tmp = $_[0];
	if($tmp !~ /[\x80-\xff]/){
		return 1;
	}else{
		return 0;
	}
}

sub enq_radio{
	$temp=$_[0];
	$temp=~s/\n//g;
	if(length $temp > 0){
		if($_[3]==$_[2]){
			$enq.="<input type=radio name=".$_[1]." value=".$_[3]." checked>".$temp;
		}else{
			$enq.="<input type=radio name=".$_[1]." value=".$_[3].">".$temp;
		}
	};
}

sub wtime {
	($sec,$min,$hour,$mday,$mon,$year,$weekday) = localtime;
	
	$year += 1900;
	$mon++;
	$mon = sprintf("%.2d",$mon);
	$mday = sprintf("%.2d",$mday);
	$hour = sprintf("%.2d",$hour);
	$min = sprintf("%.2d",$min);
	$sec = sprintf("%.2d",$sec);
	
	@week = ("日","月","火","水","木","金","土");
	$weekday = $week[$wday];
	&jcode::euc2sjis(\$weekday);
}

