<?

$pagecheck = "0";

if(getenv("HTTP_REFERER") == "https://www.issjp.com/honyaku/inquiry/visa_form2.php"){$pagecheck = "1";}
if(getenv("HTTP_REFERER") == "https://202.212.213.210/honyaku/inquiry/visa_form2.php"){$pagecheck = "1";}

//テスト用
if(getenv("HTTP_REFERER") == "https://www.issjp.com/honyaku_test/inquiry/visa_form2.php"){$pagecheck = "1";}

if($pagecheck == "0"){
	header("Location: https://www.issjp.com/honyaku/inquiry/visa_form.html");
	exit;
}

mb_language ("Japanese");
mb_internal_encoding('euc-jp');

$parameter = "-f visa_application@issjp.com";

$body1 = "
WEBでのビザ申請書類見積依頼フォームにて
受付完了しました。下記にて内容をご確認ください。
----------------------------------------
3営業日以内にお客様にご連絡ください。
----------------------------------------

";

if ($_POST['folder_name'] != ""){
	$body1 .= "●アップロードされたファイルがあります。\n";
	$body1 .= "　フォルダ名：" . $_POST['folder_name'] . "\n";
	
	if ($_POST['file_name1'] != ""){
		$body1 .= "　　ファイル名：" . mb_convert_kana($_POST['file_name1'],"K","EUC-JP") . "\n";
	}
	if ($_POST['file_name2'] != ""){
		$body1 .= "　　ファイル名：" . mb_convert_kana($_POST['file_name2'],"K","EUC-JP") . "\n";
	}
	if ($_POST['file_name3'] != ""){
		$body1 .= "　　ファイル名：" . mb_convert_kana($_POST['file_name3'],"K","EUC-JP") . "\n";
	}
	
} 
else{
	$body1 .= "●アップロードされたファイルはありません。\n";
}


$body1 .= "
●入力項目
＜お名前＞
" . $_POST['name_kanji'] . "

＜お名前（フリガナ）＞
" . $_POST['name_kana'] . "

＜お名前（ローマ字）＞
" . $_POST['name_rome'] . "

＜性別＞
" . $_POST['sex'] . "

＜E-mailアドレス＞
" . $_POST['email'] . "

＜ご住所＞
〒" . $_POST['zip1'] . "-" . $_POST['zip2'] . "
" . $_POST['pref'] . "
" . $_POST['address1'] . "
" . $_POST['address2'] . "

＜お電話番号＞
" . $_POST['tel1'] . "-" . $_POST['tel2'] . "-" . $_POST['tel3'] . "

＜ビザ申請先の国＞
" . $_POST['country'];

if ($_POST['country'] == "その他の国"){
	$body1 .= "　" . $_POST['country_s'];
}

$body1 .= "

＜翻訳言語＞
" . $_POST['language'];

if ($_POST['language'] == "その他"){
	$body1 .= "　" . $_POST['language_s1'] . "語→" . $_POST['language_s2'] . "語";
}

$body1 .= "

＜翻訳対象書類の種類と数量＞\n";

if ($_POST['suryo_tsucho'] != ""){
	$body1 .= "通帳 " . $_POST['suryo_tsucho'] . "件\n";
}

if ($_POST['suryo_koseki'] != ""){
	$body1 .= "戸籍謄本 " . $_POST['suryo_koseki'] . "件\n";
}

if ($_POST['suryo_zandaka'] != ""){
	$body1 .= "残高証明書 " . $_POST['suryo_zandaka'] . "件\n";
}

if ($_POST['suryo_shotoku'] != ""){
	$body1 .= "所得証明書 " . $_POST['suryo_shotoku'] . "件\n";
}

if ($_POST['suryo_zaishoku'] != ""){
	$body1 .= "在職証明書 " . $_POST['suryo_zaishoku'] . "件\n";
}

if ($_POST['suryo_kyuyo'] != ""){
	$body1 .= "給与証明書 " . $_POST['suryo_kyuyo'] . "ヶ月分\n";
}

if ($_POST['suryo_konin'] != ""){
	$body1 .= "婚姻届受理証明書 " . $_POST['suryo_konin'] . "部\n";
}

if ($_POST['suryo_gaikokujin'] != ""){
	$body1 .= "外国人登録証明書 " . $_POST['suryo_gaikokujin'] . "部\n";
}

if ($_POST['suryo_sonota'] != ""){
	$body1 .= "その他の書類 " . $_POST['suryo_sonota'] . "\n"; 
}

$body1 .= "
＜翻訳原稿の送付＞\n";

if ($_POST['sending'] == "フォーム"){
	$body1 .= "フォームに添付する";
}
elseif ($_POST['sending'] == "その他"){

	if ($_POST['sending_sonota'] == "FAX"){
		$body1 .= "FAXで送付する";
	}
	elseif ($_POST['sending_sonota'] == "郵送"){
		$body1 .= "郵送する";
	}
	elseif ($_POST['sending_sonota'] == "持参"){
		$body1 .= "持参する　（" . $_POST['raisha_month'] . "月" . $_POST['raisha_day'] . "日）";
	}
	elseif ($_POST['sending_sonota'] == "E-mail"){
		$body1 .= "E-mailに添付する"; 
	}
}

$body1 .= "

＜見積連絡方法のご希望＞\n";

if ($_POST['contact'] == "E-mail"){
	$body1 .= "E-mail";
}
elseif ($_POST['contact'] == "TEL"){
	$body1 .= "TEL";
}
elseif ($_POST['contact'] == "FAX"){
	$body1 .= "FAX　" . $_POST['contact_fax1'] . "-" . $_POST['contact_fax2'] . "-" . $_POST['contact_fax3'];
}

$body1 .= "

＜受取希望日＞
" . $_POST['uketori_month'] . "月 " . $_POST['uketori_day'] . "日

＜受取方法＞
" . $_POST['uketori_hoho'] . "

＜ご要望・ご連絡事項など＞
" . $_POST['renraku'] . "


以上、ご対応をよろしくお願いいたします。



REQUEST_METHOD=" . getenv("REQUEST_METHOD") . "
HTTP_REFERER=" . getenv("HTTP_REFERER") . "
REMOTE_ADDR=" . getenv("REMOTE_ADDR") . "
HTTP_USER_AGENT=" . getenv("HTTP_USER_AGENT");


$subject1 = "ビザ申請書類翻訳見積依頼フォーム【受付】";
$header1  = "From: form@issjp.com\n";

//$mailto = $_POST['email'];
$mailto = "visa_application@issjp.com";

mb_send_mail($mailto, $subject1, $body1, $header1, $parameter); //管理者へメール送信


$body2 = $_POST['name_kanji'] . "　様

この度は、弊社にビザ申請書類翻訳の見積もりをご依頼いただき
誠にありがとうございます。受け付け完了いたしました。
このメールはシステムより自動的に送信されております。

ご依頼内容を確認の上、弊社担当より3営業日以内に
ご連絡をさせていただきます。

なお、なるべく早めにご連絡させていただきますが、
万が一、3営業日を過ぎても連絡がない場合には、
大変恐縮ですが下記の窓口までお問い合わせくださいますよう
お願い申し上げます。


------------------------------------------------------
株式会社アイ・エス・エス
ランゲージ事業部　翻訳グループ

〒102-0082　東京都千代田区一番町23-3　日本生命一番町ビル4F 
TEL: 03-3230-2521　 Fax: 03-3262-6633
Email：visa_application@issjp.com

URL：http://www.issjp.com

＊営業時間：土日祝日を除く9時〜18時
------------------------------------------------------";

$header2  = "From: visa_application@issjp.com\n";
$subject2 = "ISS：見積依頼ありがとうございます";

mb_send_mail($_POST['email'], $subject2, $body2, $header2, $parameter); //お客様へ確認メール送信

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
<meta name="description" content="ビザ申請書類の翻訳見積依頼を受け付けております。ISSは英国ビザ申請センターの翻訳会社リスト「Translation Agents List」に掲載されています。" />
<meta name="keywords" content="ビザ,翻訳,見積,証明書,翻訳会社,英国ビザ,ISS" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>ビザ申請書類の翻訳見積依頼 | ISS | 翻訳サービス内容詳細</title>
<link href="../../common/default.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../common/print.css" rel="stylesheet" type="text/css" media="print" />
<script type="text/javascript" src="../../common/script.js"></script>
<script type="text/javascript" src="../../form/_js/jquery.js"></script>
<script type="text/javascript" src="../../form/_js/sb/adapter/shadowbox-jquery.js"></script>
<script type="text/javascript" src="../../form/_js/sb/shadowbox.js"></script>
<script type="text/javascript">
<!--
Shadowbox.loadSkin('classic', '../../form/_js/sb/skin'); Shadowbox.loadLanguage('en', '../../form/_js/sb/lang');
 Shadowbox.loadPlayer(['img', 'swf', 'flv', 'qt', 'wmp', 'iframe', 'html'], '../../form/_js/sb/player'); 
 $(document).ready(function() {
 	Shadowbox.init();
	});
//-->
</script></head>
<body onload="initRollovers();">
<a name="pageTop" id="pageTop"></a>

<!-- Header Begin -->
<div id="header">
<div class="content"><a href="../../index.html"><img src="../../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../../index.html"><img src="../../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣、法人研修／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi">
<div class="searchBox">
<script type="text/javascript" src="../../search_s.js"></script>
</div>
<div class="hMenuBtn">
  <table cellspacing="0" summary="HeaderNavigation">
    <tr>
      <td><a href="http://www.issjp.com/index.html"><img src="../../common/images/hNavi_btn_05.gif" alt="HOME" width="57" height="20" /></a></td>
						<td><a href="http://www.issjp.com/inquiry/index.html"><img src="../../common/images/hNavi_btn_01.gif" alt="お問い合わせ" width="81" height="20" /></a></td>
      <td><a href="http://www.issjp.com/access/index.html"><img src="../../common/images/hNavi_btn_02.gif" alt="アクセスマップ" width="91" height="20" /></a></td>
      <td><a href="http://www.issjp.com/sitemap/index.html"><img src="../../common/images/hNavi_btn_03.gif" alt="サイトマップ" width="81" height="20" /></a></td>
      <td><a href="http://www.issjp.com/en/index.html"><img src="../../common/images/hNavi_btn_04.gif" alt="ENGLISH" width="65" height="20" border="0" /></a></td>
    </tr>
  </table>
</div>
</div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<!-- Global Navi Begin -->
<div id="globalNavi">
<div class="content">
<div class="menuList">
<ul>
<li class="bt01"><a href="http://www.issjp.com/about/index.html"><img src="../../common/images/gNavi_btn_01.jpg" alt="ISSグループとは" width="163" height="47" class="btn" /></a></li><li class="bt02"><a href="http://www.issjp.com/company/index.html"><img src="../../common/images/gNavi_btn_02.jpg" alt="アイ・エス・エス企業情報" width="163" height="47" class="btn" /></a></li>
<li class="bt03"><a href="http://www.issjp.com/service/index.html"><img src="../../common/images/gNavi_btn_03_current.jpg" alt="サービス案内" width="163" height="47" /></a></li><li class="bt04"><a href="http://www.issjp.com/recruit/index.html"><img src="../../common/images/gNavi_btn_04.jpg" alt="採用情報" width="163" height="47" class="btn" /></a></li><li class="bt05"><a href="http://www.issjp.com/publishing/index.html"><img src="../../common/images/gNavi_btn_05.jpg" alt="出版物紹介" width="164" height="47" class="btn" /></a></li>
</ul>
</div>
<h1><img src="../../service/images/h1.jpg" alt="サービス案内" width="816" height="90" /></h1>
</div>
</div>
<!-- Global Navi End -->

<!-- Main Content Begin -->
<div id="colum2">
<div class="content">

<!-- BreadList Begin -->
<div id="breadList">
<table cellspacing="0" summary="breadList"><tr>
<td><img src="../../common/images/breadListLeft.jpg" alt="" width="211" height="30" /></td>
<td nowrap="nowrap"><p class="s"><a href="http://www.issjp.com/index.html">HOME</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/service/index.html">サービス案内</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/honyaku/index.html">翻訳</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/honyaku/service.html">サービス内容詳細</a><span>&nbsp; &gt; &nbsp;</span>ビザ申請書類の翻訳見積依頼</p></td>
</tr></table>
</div>
<!-- BreadList End -->

<!-- Colum Left Begin -->
<div id="columLeft">
<div class="sMenu">
<ul>
<li><a href="http://www.issjp.com/tsuyaku/index.html"><img src="../../service/images/sMenu_btn_02.jpg" alt="通訳" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/kokusai/index.html"><img src="../../service/images/sMenu_btn_01.jpg" alt="国際会議企画・運営" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/honyaku/index.html"><img src="../../service/images/sMenu_btn_03_current.jpg" alt="翻訳" width="211" height="28" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/service.html"><img src="../images/sMenu_btn_04_02.jpg" alt="サービス内容詳細" width="211" height="18" class="btn" /></a></li>

<li class="third"><a href="http://www.issjp.com/honyaku/visa.html"><img src="../images/sMenu_btn_04_02_01_current.jpg" alt="英国ビザ申請書類の翻訳" width="211" height="18" /></a></li>

<li class="second"><a href="http://www.issjp.com/honyaku/flow.html"><img src="../images/sMenu_btn_04_03.jpg" alt="サービスの流れ" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/outline.html"><img src="../images/sMenu_btn_04_01.jpg" alt="サービスの体制" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/faq.html"><img src="../images/sMenu_btn_04_04.jpg" alt="FAQ" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/inquiry/index.html"><img src="../images/sMenu_btn_04_05.jpg" alt="問い合わせ先、地図" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="http://www.issjp.com/honyaku/inquiry/juridical.html"><img src="../images/sMenu_btn_04_05_01.jpg" alt="法人のお客様" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="http://www.issjp.com/honyaku/inquiry/individual.html"><img src="../images/sMenu_btn_04_05_04.jpg" alt="個人のお客様" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="https://www.issjp.com/honyaku/inquiry/honyaku_form.html"><img src="../images/sMenu_btn_04_05_02.jpg" alt="翻訳者の登録について" width="211" height="18" border="0" /></a></li>
<li class="space"><img src="../../common/images/pixel_white.gif" alt="" width="211" height="8" /></li>
<!-- li><a href="http://www.issjp.com/honyaku/localization.html"><img src="../../service/images/sMenu_btn_04.jpg" alt="ローカライゼーション" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/haken/index.html"><img src="../../service/images/sMenu_btn_05.jpg" alt="人材派遣・紹介予定派遣" width="211" height="28" class="btn" /></a></li>
<!-- li><a href="http://www.issjp.com/haken/referral.html"><img src="../../service/images/sMenu_btn_06.jpg" alt="紹介予定派遣" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/corptre/index.html"><img src="../../service/images/sMenu_btn_07.jpg" alt="法人向け語学研修" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/chinese/index.html"><img src="../../chinese/images/sMenu_btn_12.jpg" alt="中国語サービス" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/service/resources.html"><img src="../../service/images/sMenu_btn_08.jpg" alt="人材紹介" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/service/training.html"><img src="../../service/images/sMenu_btn_09.jpg" alt="通訳者・翻訳者養成" width="211" height="28" class="btn" /></a></li>
<!-- li><a href="http://www.issjp.com/service/com_training.html"><img src="../../service/images/sMenu_btn_10.jpg" alt="法人向け語学研修" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/service/network.html"><img src="../../service/images/sMenu_btn_11.jpg" alt="在ロサンゼルス米国現地法人" width="211" height="28" class="btn" /></a></li>
<li><img src="../../common/images/sMenu_bottom.jpg" alt="" width="211" height="30" /></li>
</ul>
</div>


<img src="../../common/images/pixel_trans.gif" alt="" width="211" height="20" class="spacer" />

<div class="infoBox">
<img src="../../common/images/infoBoxTitle.jpg" alt="お問い合わせ情報" width="193" height="26" />
<table class="fix"><tr>
<td class="ac"><a href="http://www.issjp.com/inquiry/index.html"><img src="../../common/images/infoBtn_01.jpg" alt="お問い合わせ窓口一覧" width="177" height="33" border="0" /></a></td>
</tr><tr>
<td class="ac"><a href="http://www.issjp.com/access/index.html"><img src="../../common/images/infoBtn_02.jpg" alt="アクセスマップ" width="177" height="33" border="0" /></a></td>
</tr></table>
</div>

<img src="../../common/images/pixel_trans.gif" alt="" width="211" height="20" class="spacer" />

</div>
<!-- Colum Left End -->

<!-- Colum Right Begin -->
<div id="columRight">


<!--　○○○○○○○○○○　ここから　○○○○○○○○○○　-->


<div id="txtBody">

<h2><img src="images/visa_m__h2.jpg" alt="ビザ申請書類翻訳　見積依頼" width="525" height="60" /></h2>

<table border="0" cellpadding="0" cellspacing="0" width="525">

<tr>
<td>
<table border="0" cellpadding="0" cellpadding="0">
<tr>
<td colspan="2"><img src="../../common/images/h3_top.gif" alt="" width="525" height="5" /></td>
</tr>
<tr>
<td style="text-align:left; vertical-align:middle; height:40px; background-color:#DCF0F9; color:#326698"><strong>　ビザ申請書類翻訳　見積依頼フォーム</strong></td>
<td style="text-align:left; vertical-align:bottom; height:40px; background-color:#DCF0F9; color:#326698;"><strong>完了画面</strong></td>
</tr>
<tr>
<td colspan="2"><img src="../../common/images/h3_btm.gif" alt="" width="525" height="5" /></td>
</tr>
</table>
</td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr>

<tr>
<td class="mtx">ビザ申請書類見積依頼フォームをご利用いただきありがとうございます。<br>ご記入いただきましたメールアドレスにフォーム完了確認の自動返信メールをお送りしております。</td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr>


<tr>
<td class="mtx">ご依頼内容を確認の上、弊社担当より3営業日以内にご連絡をさせていただきます。</td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr>

<tr>
<td class="mtx">お問い合わせ先：<br />
株式会社アイ・エス・エス　ランゲージ事業部　翻訳グループ<br />
（営業時間　土日祝日を除く9時〜18時）<br />
TEL: 03-3230-2521　　Email: <a href="mailto:visa_application@issjp.com">visa_application@issjp.com</a></td>
</tr>


</table>

<img src="../../common/images/pixel_trans.gif" width="525" height="30" alt="" class="spacer" />
<a href="#pageTop"><img src="../../common/images/pageTop.gif" alt="このページの上へ" width="90" height="20" class="pageTop" /></a>
<img src="../../common/images/pixel_trans.gif" width="525" height="50" alt="" class="spacer" />

</div>

<!--　○○○○○○○○○○　ここまで　○○○○○○○○○○　-->

</div>
<!-- Colum Right End -->

<br class="clr" />
</div>
</div>
<!-- Main Content End -->

<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../../common/images/pixel_trans.gif" width="816" height="35" alt="" class="spacer" />
<div class="menuList">
<ul>
<li class="bt01"><a href="http://www.issjp.com/tsuyaku/index.html">通訳</a>　|　<a href="http://www.issjp.com/kokusai/index.html">国際会議企画・運営</a>　|　<a href="http://www.issjp.com/honyaku/index.html">翻訳</a>　|　<a href="http://www.issjp.com/haken/index.html">人材派遣・紹介予定派遣</a>　|　<a href="http://www.issjp.com/corptre/index.html">法人向け語学研修</a></li>
<li class="bt02"><a href="http://www.issjp.com/policy/index.html"><img src="../../common/images/fNavi_btn_01.gif" alt="個人情報保護方針" width="99" height="20" /></a></li>
<li class="bt03"><a href="http://www.issjp.com/site/index.html"><img src="../../common/images/fNavi_btn_03.gif" alt="サイトポリシー" width="90" height="20" /></a></li>
</ul>
</div>
<div class="menuList">
<ul>
<li class="bt05">【ISSグループサイト】　<a href="http://www.issnet.co.jp/" target="_blank">通訳・翻訳者養成学校のISS</a>　|　<a href="http://haken.issjp.com/" target="_blank">通訳・翻訳のISS人材派遣</a>　|　<a href="http://www.isssc.com/" target="_blank">外資系転職のISS</a>　|　<a href="http://www.issla.net/" target="_blank">通訳・翻訳のISSロサンゼルス</a></li>
<li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>

</div>
</div>
</div>
<!-- Footer End -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>
</body>
</html>
