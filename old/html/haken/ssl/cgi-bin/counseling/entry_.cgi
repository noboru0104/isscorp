#!/usr/bin/perl
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
#require '../../../../perl_lib/jcode.pl';

#require '../../../../perl_lib/cgi-lib.pl';
$cgi_lib'maxdata    = 5000*1024*1024; 
&ReadParse;

$FROM		='touroku@isssc.co.jp';
#$TO		='touroku@isssc.co.jp';
$TO		='ichino@si-seed.com';
#$TO2		='yumiko@loveworkaholic.com';

$Subject 		= "ISS ONLINE ENTRY FORM";

$entry_cgi_path		="/cgi-bin/counseling/entry.cgi";

$csv_dir		="../secret/counseling/";
$csv_path		=">>" . $csv_dir . "registered.csv";
$enq1_path		=$csv_dir . "enq1.txt";
$enq2_path		=$csv_dir . "enq2.txt";
$enq3_path		=$csv_dir . "enq3.txt";
$enq4_path		=$csv_dir . "enq4.txt";

$resume_dir		="../secret/counseling/";

$template_dir		="../template/counseling/";
$confirm_html_path	=$template_dir . "confirm.html";
$entry_html_path	=$template_dir . "entry.html";
$thanks_html_path	=$template_dir . "thanks.html";

$pref_txt_path		=$template_dir . "pref.txt";
$moyori_type_txt_path	=$template_dir . "moyori_type.txt";
$gakureki_txt_path	=$template_dir . "gakureki,txt";
$start_year_txt_path	=$template_dir . "start_year.txt";
$shokushu_txt_path	=$template_dir . "shokushu.txt";
$keitai_txt_path	=$template_dir . "keitai.txt";
$word_txt_path		=$template_dir . "word.txt";
$excel_txt_path		=$template_dir . "excel.txt";
$powerpoint_txt_path	=$template_dir . "powerpoint.txt";
$access_txt_path	=$template_dir . "access.txt";
$rusu_txt_path		=$template_dir . "rusu.txt";
$land_txt_path		=$template_dir . "land.txt";
$lang_lv_txt_path	=$template_dir . "lang_lv.txt";
$language_txt_path	=$template_dir . "language.txt";
$renraku_txt_path	=$template_dir . "renraku.txt";
$shoukai_txt_path	=$template_dir . "shoukai.txt";
$sat_type_txt_path	=$template_dir . "sat_type.txt";
$smoke_txt_path		=$template_dir . "smoke.txt";
$work_style_txt_path	=$template_dir . "work_style.txt";
$csv_txt_path		=$template_dir . "csv.txt";
$hakensaki_type_txt_path=$template_dir . "hakensaki_type.txt";
$hakensaki_kibo_txt_path=$template_dir . "hakensaki_kibo.txt";


$epoch=time;

$hakensaki_gyousyu_def	="例：金融・メディカル・IT通信・コンピュータ・システム・\nメーカー（自動車・アパレル・飲料/食品・化学）・\nエンターテイメント・PR・イベント・問わない";
&jcode::euc2sjis(\$hakensaki_gyousyu_def);
$other_school_def	="記入例：20XX.4〜現在　●●スクール　●●コース　●●クラス";
&jcode::euc2sjis(\$other_school_def);

	$enq01ans=$in{'enq01ans'};	$enq01ans=~s/\r//g;	$enq01ans=~s/\n//g;	$enq01ans=~s/,/./g;
	$enq01text=$in{'enq01text'};	$enq01text=~s/\r//g;	$enq01text=~s/\n//g;	$enq01text=~s/,/./g;
	open(ENQ1,$enq1_path);		@entry=<ENQ1>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq01ans==1){$enq01sel=$ans01};
	$ans02=shift @entry;	if($enq01ans==2){$enq01sel=$ans02};
	$ans03=shift @entry;	if($enq01ans==3){$enq01sel=$ans03};
	$ans04=shift @entry;	if($enq01ans==4){$enq01sel=$ans04};
	$ans05=shift @entry;	if($enq01ans==5){$enq01sel=$ans05};
	$ans06=shift @entry;	if($enq01ans==6){$enq01sel=$ans06};
	$ans07=shift @entry;	if($enq01ans==7){$enq01sel=$ans07};
	$ans08=shift @entry;	if($enq01ans==8){$enq01sel=$ans08};
	$ans09=shift @entry;	if($enq01ans==9){$enq01sel=$ans09};
	$ans10=shift @entry;	if($enq01ans==10){$enq01sel=$ans10};
	$enq01sel=~s/\r//g;	$enq01sel=~s/\n//g;	$enq01sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\n//g;	$qes=~s/\r//g;	$qes=~s/,/./g;
	close(ENQ1);
	if($sel_type>0){
		$enq01qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq01text==0 ){						$err_enq01="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq01ans==0 )||($enq01ans==0) ){				$err_enq01="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq01ans==0 )||($enq01ans==0))&&(length $enq01text==0 ) ){	$err_enq01="<font color=red>←この設問は必須項目です。</font>";	}}
			&jcode::euc2sjis(\$err_enq01);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq01ans',$enq01ans,'1');
		enq_radio($ans02,'enq01ans',$enq01ans,'2');

		enq_radio($ans03,'enq01ans',$enq01ans,'3');
		enq_radio($ans04,'enq01ans',$enq01ans,'4');
		enq_radio($ans05,'enq01ans',$enq01ans,'5');
		enq_radio($ans06,'enq01ans',$enq01ans,'6');
		enq_radio($ans07,'enq01ans',$enq01ans,'7');
		enq_radio($ans08,'enq01ans',$enq01ans,'8');
		enq_radio($ans09,'enq01ans',$enq01ans,'9');
		enq_radio($ans10,'enq01ans',$enq01ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq01text>".$enq01text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq=$tmp;
	}
	$enq01.=$enq;

	$enq02ans=$in{'enq02ans'};	$enq02ans=~s/\r//g;	$enq02ans=~s/\n//g;	$enq02ans=~s/,/./g;
	$enq02text=$in{'enq02text'};	$enq02text=~s/\r//g;	$enq02text=~s/\n//g;	$enq02text=~s/,/./g;
	open(ENQ2,$enq2_path);		@entry=<ENQ2>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq02ans==1){$enq02sel=$ans01};
	$ans02=shift @entry;	if($enq02ans==2){$enq02sel=$ans02};
	$ans03=shift @entry;	if($enq02ans==3){$enq02sel=$ans03};
	$ans04=shift @entry;	if($enq02ans==4){$enq02sel=$ans04};
	$ans05=shift @entry;	if($enq02ans==5){$enq02sel=$ans05};
	$ans06=shift @entry;	if($enq02ans==6){$enq02sel=$ans06};
	$ans07=shift @entry;	if($enq02ans==7){$enq02sel=$ans07};
	$ans08=shift @entry;	if($enq02ans==8){$enq02sel=$ans08};
	$ans09=shift @entry;	if($enq02ans==9){$enq02sel=$ans09};
	$ans10=shift @entry;	if($enq02ans==10){$enq02sel=$ans10};

	$enq02sel=~s/\r//g;	$enq02sel=~s/\n//g;	$enq02sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\r//g;	$qes=~s/\n//g;	$qes=~s/,/./g;
	close(ENQ2);
	if($sel_type>0){
		$enq02qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq02text==0 ){						$err_enq02="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq02ans==0 )||($enq02ans==0) ){				$err_enq02="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq02ans==0 )||($enq02ans==0))&&(length $enq02text==0 ) ){	$err_enq02="<font color=red>←この設問は必須項目です。</font>";	}}
		&jcode::euc2sjis(\$err_enq02);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq02ans',$enq02ans,'1');
		enq_radio($ans02,'enq02ans',$enq02ans,'2');
		enq_radio($ans03,'enq02ans',$enq02ans,'3');
		enq_radio($ans04,'enq02ans',$enq02ans,'4');
		enq_radio($ans05,'enq02ans',$enq02ans,'5');
		enq_radio($ans06,'enq02ans',$enq02ans,'6');
		enq_radio($ans07,'enq02ans',$enq02ans,'7');
		enq_radio($ans08,'enq02ans',$enq02ans,'8');
		enq_radio($ans09,'enq02ans',$enq02ans,'9');
		enq_radio($ans10,'enq02ans',$enq02ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq02text>".$enq02text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq.=$tmp;
	}
	$enq02.=$enq;

	$enq03ans=$in{'enq03ans'};	$enq03ans=~s/\r//g;	$enq03ans=~s/\n//g;	$enq03ans=~s/,/./g;
	$enq03text=$in{'enq03text'};	$enq03text=~s/\r//g;	$enq03text=~s/\n//g;	$enq03text=~s/,/./g;
	open(ENQ3,$enq3_path);		@entry=<ENQ3>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq03ans==1){$enq03sel=$ans01};
	$ans02=shift @entry;	if($enq03ans==2){$enq03sel=$ans02};
	$ans03=shift @entry;	if($enq03ans==3){$enq03sel=$ans03};
	$ans04=shift @entry;	if($enq03ans==4){$enq03sel=$ans04};
	$ans05=shift @entry;	if($enq03ans==5){$enq03sel=$ans05};
	$ans06=shift @entry;	if($enq03ans==6){$enq03sel=$ans06};
	$ans07=shift @entry;	if($enq03ans==7){$enq03sel=$ans07};
	$ans08=shift @entry;	if($enq03ans==8){$enq03sel=$ans08};
	$ans09=shift @entry;	if($enq03ans==9){$enq03sel=$ans09};
	$ans10=shift @entry;	if($enq03ans==10){$enq03sel=$ans10};
	$enq03sel=~s/\r\n//g;	$enq03sel=~s/\n//g;	$enq03sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\n//g;	$qes=~s/\r//g;	$qes=~s/,/./g;
	close(ENQ3);
	if($sel_type>0){
		$enq03qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq03text==0 ){						$err_enq03="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq03ans==0 )||($enq03ans==0) ){				$err_enq03="<font color=red>←この設問は必須項目です。</font>";	}}

			if(($sel_type==3)){	if(((length $enq03ans==0 )||($enq03ans==0))&&(length $enq03text==0 ) ){	$err_enq03="<font color=red>←この設問は必須項目です。</font>";	}}
			&jcode::euc2sjis(\$err_enq03);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq03ans',$enq03ans,'1');
		enq_radio($ans02,'enq03ans',$enq03ans,'2');
		enq_radio($ans03,'enq03ans',$enq03ans,'3');
		enq_radio($ans04,'enq03ans',$enq03ans,'4');
		enq_radio($ans05,'enq03ans',$enq03ans,'5');
		enq_radio($ans06,'enq03ans',$enq03ans,'6');
		enq_radio($ans07,'enq03ans',$enq03ans,'7');
		enq_radio($ans08,'enq03ans',$enq03ans,'8');
		enq_radio($ans09,'enq03ans',$enq03ans,'9');
		enq_radio($ans10,'enq03ans',$enq03ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq03text>".$enq03text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq.=$tmp;
	}
	$enq03=$enq;

	$enq04ans=$in{'enq04ans'};	$enq04ans=~s/\r//g;	$enq04ans=~s/\n//g;	$enq04ans=~s/,/./g;
	$enq04text=$in{'enq04text'};	$enq04text=~s/\r//g;	$enq04text=~s/\n//g;	$enq04text=~s/,/./g;
	open(ENQ4,$enq4_path);		@entry=<ENQ4>;
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;	if($enq04ans==1){$enq04sel=$ans01};
	$ans02=shift @entry;	if($enq04ans==2){$enq04sel=$ans02};
	$ans03=shift @entry;	if($enq04ans==3){$enq04sel=$ans03};
	$ans04=shift @entry;	if($enq04ans==4){$enq04sel=$ans04};
	$ans05=shift @entry;	if($enq04ans==5){$enq04sel=$ans05};
	$ans06=shift @entry;	if($enq04ans==6){$enq04sel=$ans06};
	$ans07=shift @entry;	if($enq04ans==7){$enq04sel=$ans07};
	$ans08=shift @entry;	if($enq04ans==8){$enq04sel=$ans08};
	$ans09=shift @entry;	if($enq04ans==9){$enq04sel=$ans09};
	$ans10=shift @entry;	if($enq04ans==10){$enq04sel=$ans10};
	$enq04sel=~s/\n//g;$enq04sel=~s/\r\n//g;	$enq04sel=~s/,/./g;
	$qes="";
	do{$qes.=shift @entry;}while($#entry>0);
	$qes=~s/\r//g;	$qes=~s/\n//g;		$qes=~s/,/./g;
	close(ENQ4);
	if($sel_type>0){
		$enq04qes.=$qes;
		if($req_type!=0){
			if(($sel_type==1)){	if(length $enq04text==0 ){						$err_enq04="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==2)){	if((length $enq04ans==0 )||($enq04ans==0) ){				$err_enq04="<font color=red>←この設問は必須項目です。</font>";	}}
			if(($sel_type==3)){	if(((length $enq04ans==0 )||($enq04ans==0))&&(length $enq04text==0 ) ){	$err_enq04="<font color=red>←この設問は必須項目です。</font>";	}}
			&jcode::euc2sjis(\$err_enq04);
		}
	}

	$tmp="";
	$enq="";
	if(($sel_type==2)||($sel_type==3)){
		enq_radio($ans01,'enq04ans',$enq04ans,'1');
		enq_radio($ans02,'enq04ans',$enq04ans,'2');
		enq_radio($ans03,'enq04ans',$enq04ans,'3');
		enq_radio($ans04,'enq04ans',$enq04ans,'4');
		enq_radio($ans05,'enq04ans',$enq04ans,'5');

		enq_radio($ans06,'enq04ans',$enq04ans,'6');
		enq_radio($ans07,'enq04ans',$enq04ans,'7');
		enq_radio($ans08,'enq04ans',$enq04ans,'8');
		enq_radio($ans09,'enq04ans',$enq04ans,'9');
		enq_radio($ans10,'enq04ans',$enq04ans,'10');
	}
	if(($sel_type==1)||($sel_type==3)){
		$tmp.="<br>[自由記入]<br><textarea name=enq04text>".$enq04text."</textarea>";
		&jcode::euc2sjis(\$tmp);
		$enq.=$tmp;
	}
	$enq04=$enq;



if(length $in{'cmd'} eq 0){
	$hakensaki_gyousyu=$hakensaki_gyousyu_def;
	$other_school1=$other_school_def;
	$other_school2=$other_school_def;
	$other_school3=$other_school_def;
	$other_school4=$other_school_def;
}else{

$job_id=$in{'job_id'};
$name_A=$in{'name_A'};
$name_B=$in{'name_B'};
$name_A2=$in{'name_A2'};
$name_B2=$in{'name_B2'};
if($in{'visa'} eq '1'){$visa='就労ビザあり';}
&jcode::euc2sjis(\$visa);
$visa_term=$in{'visa_term'};
if($in{'visa_type'} eq '1'){$visa_type='配偶者ビザ';}
&jcode::euc2sjis(\$visa_type);

$birthday_yy=$in{'birthday_yy'};
$birthday_mm=$in{'birthday_mm'};
$birthday_dd=$in{'birthday_dd'};
$sex=$in{'sex'};
$zip1=$in{'zip1'};
$zip2=$in{'zip2'};
$pref_num=$in{'pref_num'};
if($in{'pref'}  == '0')	{$err_pref='<font color=red>都道府県は必須項目です</font>';}
&jcode::euc2sjis(\$err_pref);
open ( FH_PREF , $pref_txt_path ) ;for ( $a = 0 ; $a <= $in{'pref'} ; $a++ ){$pref=<FH_PREF>;$pref=~s/\n//g;}close(FH_PREF);


$address1=$in{'address1'};
$address2=$in{'address2'};
$address3=$in{'address3'};
$moyori1_rosen=$in{'moyori1_rosen'};
$moyori1_eki=$in{'moyori1_eki'};
$moyori1_walk=$in{'moyori1_walk'};
$moyori1_bus=$in{'moyori1_bus'};
$moyori1_bike=$in{'moyori1_bike'};
$moyori1_other=$in{'moyori1_other'};
$moyori2_rosen=$in{'moyori2_rosen'};
$moyori2_eki=$in{'moyori2_eki'};
$moyori2_walk=$in{'moyori2_walk'};
$moyori2_bus=$in{'moyori2_bus'};
$moyori2_bike=$in{'moyori2_bike'};
$moyori2_other=$in{'moyori2_other'};
$tel1_1=$in{'tel1_1'};
$tel1_2=$in{'tel1_2'};
$tel1_3=$in{'tel1_3'};
open ( FH_TEL_RUSU, $rusu_txt_path ) ;for ( $a = 0 ; $a <= $in{'tel1_rusu'} ; $a++ ){$tel1_rusu=<FH_TEL_RUSU>;$tel1_rusu=~s/\n//g;}close(FH_TEL_RUSU);	

$tel2_1=$in{'tel2_1'};
$tel2_2=$in{'tel2_2'};
$tel2_3=$in{'tel2_3'};
open ( FH_TEL_RUSU, $rusu_txt_path ) ;for ( $a = 0 ; $a <= $in{'tel2_rusu'} ; $a++ ){$tel2_rusu=<FH_TEL_RUSU>;$tel2_rusu=~s/\n//g;}close(FH_TEL_RUSU);	

$fax_1=$in{'fax_1'};
$fax_2=$in{'fax_2'};
$fax_3=$in{'fax_3'};
if($in{'renraku_any'} eq '1'){	$renraku_any='○'; }else{	$renraku_any='×'; }
&jcode::euc2sjis(\$renraku_any);
if($in{'renraku_home'} eq '1'){	$renraku_home='○'; }else{	$renraku_home='×'; } 
&jcode::euc2sjis(\$renraku_home);
if($in{'renraku_mob'} eq '1'){	$renraku_mob='○'; }else{	$renraku_mob='×'; }
&jcode::euc2sjis(\$renraku_mob);
if($in{'renraku_pcmail'} eq '1'){	$renraku_pcmail='○'; }else{	$renraku_pcmail='×'; }
&jcode::euc2sjis(\$renraku_pcmail);
if($in{'renraku_mobmail'} eq '1'){	$renraku_mobmail='○'; }else{	$renraku_mobmail='×'; }
&jcode::euc2sjis(\$renraku_mobmail);
$mail1_A=$in{'mail1_A'};
$mail1_B=$in{'mail1_B'};
$mail2_A=$in{'mail2_A'};
$mail2_B=$in{'mail2_B'};
$gakureki=$in{'gakureki_num'};
if(length $in{'gakureki'}  eq 0)	{$err_gakureki='<font color=red>最終学歴は必須項目です</font>';}
&jcode::euc2sjis(\$err_gakureki);
open ( FH_GAKUREKI ,$gakureki_txt_path ) ;for ( $a = 0 ; $a <= $in{'gakureki'} ; $a++ ){$gakureki=<FH_GAKUREKI>;$gakureki=~ s/\n//g;}close(FH_GAKUREKI);

$gakureki_school=$in{'gakurekischool'};
$gakureki_year=$in{'gakureki_year'};

open ( FH_LAND ,$land_txt_path ) ;for ( $a = 0 ; $a <= $in{'ryugaku1_kokumei'} ; $a++ ){$ryugaku1_kokumei=<FH_LAND>;$ryugaku1_kokumei=~ s/\n//g;}close(FH_LAND);
$ryugaku1_kokumei_other=$in{'ryugaku1_kokumei_other'};
$ryugaku1_school=$in{'ryugaku1_school'};
$ryugaku1_start_yy=$in{'ryugaku1_start_yy'};
$ryugaku1_start_mm=$in{'ryugaku1_start_mm'};
$ryugaku1_end_yy=$in{'ryugaku1_end_yy'};
$ryugaku1_end_mm=$in{'ryugaku1_end_mm'};
open ( FH_LAND ,$land_txt_path ) ;for ( $a = 0 ; $a <= $in{'ryugaku2_kokumei'} ; $a++ ){$ryugaku2_kokumei=<FH_LAND>;$ryugaku2_kokumei=~ s/\n//g;}close(FH_LAND);
$ryugaku2_kokumei_other=$in{'ryugaku2_kokumei_other'};

$ryugaku2_school=$in{'ryugaku2_school'};
$ryugaku2_start_yy=$in{'ryugaku2_start_yy'};
$ryugaku2_start_mm=$in{'ryugaku2_start_mm'};
$ryugaku2_end_yy=$in{'ryugaku2_end_yy'};
$ryugaku2_end_mm=$in{'ryugaku2_end_mm'};
open ( FH_LAND ,$land_txt_path ) ;for ( $a = 0 ; $a <= $in{'ryugaku3_kokumei'} ; $a++ ){$ryugaku3_kokumei=<FH_LAND>;$ryugaku3_kokumei=~ s/\n//g;}close(FH_LAND);
$ryugaku3_kokumei_other=$in{'ryugaku3_kokumei_other'};
$ryugaku3_school=$in{'ryugaku3_school'};
$ryugaku3_start_yy=$in{'ryugaku3_start_yy'};
$ryugaku3_start_mm=$in{'ryugaku3_start_mm'};
$ryugaku3_end_yy=$in{'ryugaku3_end_yy'};
$ryugaku3_end_mm=$in{'ryugaku3_end_mm'};
$taizai1_kokumei=$in{'taizai1_kokumei'};
$taizai1_memo=$in{'taizai1_memo'};
$taizai1_term=$in{'taizai1_term'};
$taizai2_kokumei=$in{'taizai2_kokumei'};
$taizai2_memo=$in{'taizai2_memo'};
$taizai2_term=$in{'taizai2_term'};

open ( FH_STARTYEAR ,$start_year_txt_path ) ;for ( $a = 0 ; $a <= $in{'start_year'} ; $a++ ){$start_year=<FH_STARTYEAR>;$start_year=~ s/\n//g;}close(FH_STARTYEAR);
$start_month=$in{'start_month'};
$start_day=$in{'start_day'};
if(length $in{'nowstatus'}  eq 0)	{$err_nowstatus='<font color=red>現在の就業状況は必須項目です</font>';}
&jcode::euc2sjis(\$err_nowstatus);
if($in{'nowstatus'} eq '1'){
	$nowstatus='就業中';
}elsif($in{'nowstatus'} eq '2'){
	$nowstatus='現在は就業していない';
}
&jcode::euc2sjis(\$nowstatus);

open ( FH_WORKSTYLE ,$work_style_txt_path ) ;for ( $a = 0 ; $a <= $in{'work_style1'} ; $a++ ){$work_style1=<FH_WORKSTYLE>;$work_style1=~ s/\n//g;}close(FH_WORKSTYLE);
open ( FH_WORKSTYLE ,$work_style_txt_path ) ;for ( $a = 0 ; $a <= $in{'work_style2'} ; $a++ ){$work_style2=<FH_WORKSTYLE>;$work_style2=~ s/\n//g;}close(FH_WORKSTYLE);
open ( FH_WORKSTYLE ,$work_style_txt_path ) ;for ( $a = 0 ; $a <= $in{'work_style3'} ; $a++ ){$work_style3=<FH_WORKSTYLE>;$work_style3=~ s/\n//g;}close(FH_WORKSTYLE);


$time1=$in{'time1'};
$time2=$in{'time2'};
$time3=$in{'time3'};
$time4=$in{'time4'};

if($in{'work_time'} eq '1'){
	$work_time='可能';
}elsif($in{'work_time'} eq '2'){
	$work_time='不可能';
}
&jcode::euc2sjis(\$work_time);

$wday_mon=$in{'wday_mon'};
$wday_tue=$in{'wday_tue'};
$wday_wed=$in{'wday_wed'};
$wday_thu=$in{'wday_thu'};
$wday_fri=$in{'wday_fri'};

if($in{'overtime'} eq '1'){
	$overtime='可能';
}elsif($in{'overtime'} eq '2'){
	$overtime='不可能';
}
&jcode::euc2sjis(\$overtime);

$overtime_hour=$in{'overtime_hour'};

#$sat_type=$in{'sat_type'};
if($in{'sat_type'} eq '1'){
	$sat_type='可能';
}elsif($in{'sat_type'} eq '2'){
	$sat_type='不可能';
}elsif($in{'sat_type'} eq '3'){
	$sat_type='日にちによって可能';
}
&jcode::euc2sjis(\$sat_type);

if($in{'smoke'} eq '1'){
	$smoke='希望する';
}elsif($in{'smoke'} eq '2'){
	$smoke='問わない';
}
&jcode::euc2sjis(\$smoke);

#$smoke=$in{'smoke'};

$jikyu=$in{'jikyu'};

if($in{'jikyu_any'} eq '1'){
	$jikyu_any='問わない';
}
&jcode::euc2sjis(\$jikyu_any);

#$jikyu_any=$in{'jikyu_any'};

$work_memo=$in{'work_memo'};
$shokushu1=$in{'shokushu1'};
$shokushu2=$in{'shokushu2'};
$hakensaki_type=$in{'hakensaki_type'};
$hakensaki_kibo=$in{'hakensaki_kibo'};
$hakensaki_gyousyu=$in{'hakensaki_gyousyu'};
$saketai_gyousyu=$in{'saketai_gyousyu'};
$kinmuchi=$in{'kinmuchi'};
$shoyouzikan=$in{'shoyouzikan'};
$soft_word=$in{'soft_word'};
$soft_excel=$in{'soft_excel'};
$soft_excel1=$in{'soft_excel1'};
$soft_excel2=$in{'soft_excel2'};
$soft_excel3=$in{'soft_excel3'};
$soft_excel4=$in{'soft_excel4'};
$soft_excel5=$in{'soft_excel5'};
$soft_powerpoint=$in{'soft_powerpoint'};
$soft_access=$in{'soft_access'};

if($in{'soft_outlook'} eq '1'){
	$soft_outlook='Outlook';
}
&jcode::euc2sjis(\$soft_outlook);
#$soft_outlook=$in{'soft_outlook'};

if($in{'soft_lotus'} eq '1'){
	$soft_lotus='Lotus Notes';
}
&jcode::euc2sjis(\$soft_lotus);
#$soft_lotus=$in{'soft_lotus'};

$soft_other=$in{'soft_other'};
$typespeed_j=$in{'typespeed_j'};
$typespeed_e=$in{'typespeed_e'};
$os_win=$in{'os_win'};
$os_mac=$in{'os_mac'};
$os_unix=$in{'os_unix'};
$os_other=$in{'os_other'};
$os_other_name=$in{'os_other_name'};
$boki_score=$in{'boki_score'};
$boki_year=$in{'boki_year'};

$hisho_score=$in{'hisho_score'};
$hisho_year=$in{'hisho_year'};
$eiken_score=$in{'eiken_score'};
$eiken_year=$in{'eiken_year'};
$toeic_score=$in{'toeic_score'};
$toeic_year=$in{'toeic_year'};
#$toeic_ip=$in{'toeic_ip'};
if($in{'toeic_ip'} eq '1'){
	$toeic_ip='IPテスト';
}
&jcode::euc2sjis(\$toeic_ip);

$toefl_score=$in{'toefl_score'};
$toefl_year=$in{'toefl_year'};
$tsuyaku_score=$in{'tsuyaku_score'};
$tsuyaku_year=$in{'tsuyaku_year'};
$honyaku_score=$in{'honyaku_score'};
$honyaku_year=$in{'honyaku_year'};
$kokuren_score=$in{'kokuren_score'};
$kokuren_year=$in{'kokuren_year'};
#$cambridge1=$in{'cambridge1'};
if($in{'cambridge1'} eq '1'){
	$cambridge1='CPE';
}
&jcode::euc2sjis(\$cambridge1);

#$cambridge2=$in{'cambridge2'};
if($in{'cambridge2'} eq '1'){
	$cambridge2='CAE';
}
&jcode::euc2sjis(\$cambridge2);

#$cambridge3=$in{'cambridge3'};
if($in{'cambridge3'} eq '1'){
	$cambridge3='FCE';
}
&jcode::euc2sjis(\$cambridge3);

#$cambridge4=$in{'cambridge4'};
if($in{'cambridge4'} eq '1'){
	$cambridge4='PET';
}
&jcode::euc2sjis(\$cambridge4);

#$cambridge5=$in{'cambridge5'};
if($in{'cambridge5'} eq '1'){
	$cambridge5='KET';
}
&jcode::euc2sjis(\$cambridge5);

$cambridge_year=$in{'cambridge_year'};
$annai_lang=$in{'annai_lang'};
$annai_year=$in{'annai_year'};
#$other_gogaku1=$in{'other_gogaku1'};
open ( FH_WORKSTYLE ,$language_txt_path ) ;for ( $a = 0 ; $a <= $in{'other_gogaku1'} ; $a++ ){$other_gogaku1=<FH_WORKSTYLE>;$other_gogaku1=~ s/\n//g;}close(FH_WORKSTYLE);

$other_gogaku1_other=$in{'other_gogaku1_other'};

#$other_gogaku1_lv=$in{'other_gogaku1_lv'};
open ( FH_WORKSTYLE ,$lang_lv_txt_path ) ;for ( $a = 0 ; $a <= $in{'other_gogaku1_lv'} ; $a++ ){$other_gogaku1_lv=<FH_WORKSTYLE>;$other_gogaku1_lv=~ s/\n//g;}close(FH_WORKSTYLE);

#$other_gogaku2=$in{'other_gogaku2'};
open ( FH_WORKSTYLE ,$language_txt_path ) ;for ( $a = 0 ; $a <= $in{'other_gogaku2'} ; $a++ ){$other_gogaku2=<FH_WORKSTYLE>;$other_gogaku2=~ s/\n//g;}close(FH_WORKSTYLE);

$other_gogaku2_other=$in{'other_gogaku2_other'};
#$other_gogaku2_lv=$in{'other_gogaku2_lv'};
open ( FH_WORKSTYLE ,$lang_lv_txt_path ) ;for ( $a = 0 ; $a <= $in{'other_gogaku2_lv'} ; $a++ ){$other_gogaku2_lv=<FH_WORKSTYLE>;$other_gogaku2_lv=~ s/\n//g;}close(FH_WORKSTYLE);

#$other_gogaku3=$in{'other_gogaku3'};
open ( FH_WORKSTYLE ,$language_txt_path ) ;for ( $a = 0 ; $a <= $in{'other_gogaku3'} ; $a++ ){$other_gogaku3=<FH_WORKSTYLE>;$other_gogaku3=~ s/\n//g;}close(FH_WORKSTYLE);

$other_gogaku3_other=$in{'other_gogaku3_other'};

#$other_gogaku3_lv=$in{'other_gogaku3_lv'};
open ( FH_WORKSTYLE ,$lang_lv_txt_path ) ;for ( $a = 0 ; $a <= $in{'other_gogaku3_lv'} ; $a++ ){$other_gogaku3_lv=<FH_WORKSTYLE>;$other_gogaku3_lv=~ s/\n//g;}close(FH_WORKSTYLE);


$other_gogaku_shikaku=$in{'other_gogaku_shikaku'};

#$iss_school=$in{'iss_school'};
if($in{'iss_school'} eq '1'){
	$iss_school='通学歴あり';
}elsif($in{'iss_school'} eq '2'){
	$iss_school='通学歴なし';
}
&jcode::euc2sjis(\$iss_school);

$iss_school_name=$in{'iss_school_name'};
if($in{'iss_school_name'} eq '1'){
	$iss_school_name='東京校';
}elsif($in{'iss_school_name'} eq '2'){
	$iss_school_name='横浜校';
}elsif($in{'iss_school_name'} eq '3'){
	$iss_school_name='名古屋校';
}elsif($in{'iss_school_name'} eq '4'){
	$iss_school_name='ロサンゼルス校';
}
&jcode::euc2sjis(\$iss_school_name);

$iss_school_course=$in{'iss_school_course'};
$iss_school_class=$in{'iss_school_class'};
$iss_school_term=$in{'iss_school_term'};
$other_school1=$in{'other_school1'};
$other_school2=$in{'other_school2'};
$other_school3=$in{'other_school3'};
$other_school4=$in{'other_school4'};
$otoiawase=$in{'otoiawase'};

$job_id=~s/\r\n/ /g;	$job_id=~s/,/./g;
$name_A=~s/\r\n/ /g;	$name_A=~s/,/./g;
$name_B=~s/\r\n/ /g;	$name_B=~s/,/./g;
$name_A2=~s/\r\n/ /g;	$name_A2=~s/,/./g;
$name_B2=~s/\r\n/ /g;	$name_B2=~s/,/./g;
$visa=~s/\r\n/ /g;	$visa=~s/,/./g;
$visa_term=~s/\r\n/ /g;	$visa_term=~s/,/./g;
$visa_type=~s/\r\n/ /g;	$visa_type=~s/,/./g;
$birthday_yy=~s/\r\n/ /g;	$birthday_yy=~s/,/./g;
$birthday_mm=~s/\r\n/ /g;	$birthday_mm=~s/,/./g;
$birthday_dd=~s/\r\n/ /g;	$birthday_dd=~s/,/./g;

$sex=~s/\r\n/ /g;	$sex=~s/,/./g;
$zip1=~s/\r\n/ /g;	$zip1=~s/,/./g;
$zip2=~s/\r\n/ /g;	$zip2=~s/,/./g;
$pref=~s/\r\n/ /g;	$pref_num=~s/,/./g;
$address1=~s/\r\n/ /g;	$address1=~s/,/./g;
$address2=~s/\r\n/ /g;	$address2=~s/,/./g;
$address3=~s/\r\n/ /g;	$address3=~s/,/./g;
$moyori1_rosen=~s/\r\n/ /g;	$moyori1_rosen=~s/,/./g;
$moyori1_eki=~s/\r\n/ /g;	$moyori1_eki=~s/,/./g;
$moyori1_walk=~s/\r\n/ /g;	$moyori1_walk=~s/,/./g;
$moyori1_bus=~s/\r\n/ /g;	$moyori1_bus=~s/,/./g;
$moyori1_bike=~s/\r\n/ /g;	$moyori1_bike=~s/,/./g;
$moyori1_other=~s/\r\n/ /g;	$moyori1_other=~s/,/./g;
$moyori2_rosen=~s/\r\n/ /g;	$moyori2_rosen=~s/,/./g;
$moyori2_eki=~s/\r\n/ /g;	$moyori2_eki=~s/,/./g;
$moyori2_walk=~s/\r\n/ /g;	$moyori2_walk=~s/,/./g;
$moyori2_bus=~s/\r\n/ /g;	$moyori2_bus=~s/,/./g;
$moyori2_bike=~s/\r\n/ /g;	$moyori2_bike=~s/,/./g;
$moyori2_other=~s/\r\n/ /g;	$moyori2_other=~s/,/./g;
$tel1_1=~s/\r\n/ /g;	$tel1_1=~s/,/./g;
$tel1_2=~s/\r\n/ /g;	$tel1_2=~s/,/./g;
$tel1_3=~s/\r\n/ /g;	$tel1_3=~s/,/./g;
$tel1_rusu=~s/\r\n/ /g;	$tel1_rusu=~s/,/./g;
$tel2_1=~s/\r\n/ /g;	$tel2_1=~s/,/./g;
$tel2_2=~s/\r\n/ /g;	$tel2_2=~s/,/./g;
$tel2_3=~s/\r\n/ /g;	$tel2_3=~s/,/./g;
$tel2_rusu=~s/\r\n/ /g;	$tel2_rusu=~s/,/./g;
$fax_1=~s/\r\n/ /g;	$fax_1=~s/,/./g;
$fax_2=~s/\r\n/ /g;	$fax_2=~s/,/./g;
$fax_3=~s/\r\n/ /g;	$fax_3=~s/,/./g;
$renraku_any=~s/\r\n/ /g;	$renraku_any=~s/,/./g;
$renraku_home=~s/\r\n/ /g;	$renraku_home=~s/,/./g;
$renraku_mob=~s/\r\n/ /g;	$renraku_mob=~s/,/./g;
$renraku_pcmail=~s/\r\n/ /g;	$renraku_pcmail=~s/,/./g;
$renraku_mobmail=~s/\r\n/ /g;	$renraku_mobmail=~s/,/./g;
$mail1_A=~s/\r\n/ /g;	$mail1_A=~s/,/./g;
$mail1_B=~s/\r\n/ /g;	$mail1_B=~s/,/./g;
$mail2_A=~s/\r\n/ /g;	$mail2_A=~s/,/./g;
$mail2_B=~s/\r\n/ /g;	$mail2_B=~s/,/./g;
$gakureki=~s/\r\n/ /g;	$gakureki_num=~s/,/./g;
$gakureki_school=~s/\r\n/ /g;	$gakurekischool=~s/,/./g;
$gakureki_year=~s/\r\n/ /g;	$gakureki_year=~s/,/./g;
$ryugaku1_kokumei=~s/\r\n/ /g;	$ryugaku1_kokumei=~s/,/./g;
$ryugaku1_kokumei_other=~s/\r\n/ /g;	$ryugaku1_kokumei_other=~s/,/./g;
$ryugaku1_school=~s/\r\n/ /g;	$ryugaku1_school=~s/,/./g;
$ryugaku1_start_yy=~s/\r\n/ /g;	$ryugaku1_start_yy=~s/,/./g;
$ryugaku1_start_mm=~s/\r\n/ /g;	$ryugaku1_start_mm=~s/,/./g;
$ryugaku1_end_yy=~s/\r\n/ /g;	$ryugaku1_end_yy=~s/,/./g;
$ryugaku1_end_mm=~s/\r\n/ /g;	$ryugaku1_end_mm=~s/,/./g;
$ryugaku2_kokumei=~s/\r\n/ /g;	$ryugaku2_kokumei=~s/,/./g;
$ryugaku2_kokumei_other=~s/\r\n/ /g;	$ryugaku2_kokumei_other=~s/,/./g;
$ryugaku2_school=~s/\r\n/ /g;	$ryugaku2_school=~s/,/./g;
$ryugaku2_start_yy=~s/\r\n/ /g;	$ryugaku2_start_yy=~s/,/./g;
$ryugaku2_start_mm=~s/\r\n/ /g;	$ryugaku2_start_mm=~s/,/./g;
$ryugaku2_end_yy=~s/\r\n/ /g;	$ryugaku2_end_yy=~s/,/./g;
$ryugaku2_end_mm=~s/\r\n/ /g;	$ryugaku2_end_mm=~s/,/./g;
$ryugaku3_kokumei=~s/\r\n/ /g;	$ryugaku3_kokumei=~s/,/./g;
$ryugaku3_kokumei_other=~s/\r\n/ /g;	$ryugaku3_kokumei_other=~s/,/./g;
$ryugaku3_school=~s/\r\n/ /g;	$ryugaku3_school=~s/,/./g;
$ryugaku3_start_yy=~s/\r\n/ /g;	$ryugaku3_start_yy=~s/,/./g;
$ryugaku3_start_mm=~s/\r\n/ /g;	$ryugaku3_start_mm=~s/,/./g;
$ryugaku3_end_yy=~s/\r\n/ /g;	$ryugaku3_end_yy=~s/,/./g;
$ryugaku3_end_mm=~s/\r\n/ /g;	$ryugaku3_end_mm=~s/,/./g;
$taizai1_kokumei=~s/\r\n/ /g;	$taizai1_kokumei=~s/,/./g;
$taizai1_memo=~s/\r\n/ /g;	$taizai1_memo=~s/,/./g;
$taizai1_term=~s/\r\n/ /g;	$taizai1_term=~s/,/./g;
$taizai2_kokumei=~s/\r\n/ /g;	$taizai2_kokumei=~s/,/./g;
$taizai2_memo=~s/\r\n/ /g;	$taizai2_memo=~s/,/./g;
$taizai2_term=~s/\r\n/ /g;	$taizai2_term=~s/,/./g;
$start_year=~s/\r\n/ /g;	$start_year=~s/,/./g;
$start_month=~s/\r\n/ /g;	$start_month=~s/,/./g;
$start_day=~s/\r\n/ /g;	$start_day=~s/,/./g;
$nowstatus=~s/\r\n/ /g;	$nowstatus=~s/,/./g;
$work_style1=~s/\r\n/ /g;	$work_style1_num=~s/,/./g;
$work_style2=~s/\r\n/ /g;	$work_style2_num=~s/,/./g;
$work_style3=~s/\r\n/ /g;	$work_style3_num=~s/,/./g;
$time1=~s/\r\n/ /g;	$time1=~s/,/./g;
$time2=~s/\r\n/ /g;	$time2=~s/,/./g;
$time3=~s/\r\n/ /g;	$time3=~s/,/./g;
$time4=~s/\r\n/ /g;	$time4=~s/,/./g;
$work_time=~s/\r\n/ /g;	$work_time=~s/,/./g;
$wday_mon=~s/\r\n/ /g;	$wday_mon=~s/,/./g;
$wday_tue=~s/\r\n/ /g;	$wday_tue=~s/,/./g;
$wday_wed=~s/\r\n/ /g;	$wday_wed=~s/,/./g;
$wday_thu=~s/\r\n/ /g;	$wday_thu=~s/,/./g;
$wday_fri=~s/\r\n/ /g;	$wday_fri=~s/,/./g;
$overtime=~s/\r\n/ /g;	$overtime=~s/,/./g;
$overtime_hour=~s/\r\n/ /g;	$overtime_hour=~s/,/./g;
$sat_type=~s/\r\n/ /g;	$sat_type=~s/,/./g;
$smoke=~s/\r\n/ /g;	$smoke=~s/,/./g;
$jikyu=~s/\r\n/ /g;	$jikyu=~s/,/./g;
$jikyu_any=~s/\r\n/ /g;	$jikyu_any=~s/,/./g;
$work_memo=~s/\r\n/ /g;	$work_memo=~s/,/./g;
$shokushu1=~s/\r\n/ /g;	$shokushu1=~s/,/./g;
$shokushu2=~s/\r\n/ /g;	$shokushu2=~s/,/./g;
$hakensaki_type=~s/\r\n/ /g;	$hakensaki_type=~s/,/./g;
$hakensaki_kibo=~s/\r\n/ /g;	$hakensaki_kibo=~s/,/./g;
$hakensaki_gyousyu=~s/\r\n/ /g;	$hakensaki_gyousyu=~s/,/./g;
$saketai_gyousyu=~s/\r\n/ /g;	$saketai_gyousyu=~s/,/./g;
$kinmuchi=~s/\r\n/ /g;	$kinmuchi=~s/,/./g;
$shoyouzikan=~s/\r\n/ /g;	$shoyouzikan=~s/,/./g;
$soft_word=~s/\r\n/ /g;	$soft_word=~s/,/./g;
$soft_excel=~s/\r\n/ /g;	$soft_excel=~s/,/./g;
$soft_excel1=~s/\r\n/ /g;	$soft_excel1=~s/,/./g;
$soft_excel2=~s/\r\n/ /g;	$soft_excel2=~s/,/./g;
$soft_excel3=~s/\r\n/ /g;	$soft_excel3=~s/,/./g;
$soft_excel4=~s/\r\n/ /g;	$soft_excel4=~s/,/./g;
$soft_excel5=~s/\r\n/ /g;	$soft_excel5=~s/,/./g;
$soft_powerpoint=~s/\r\n/ /g;	$soft_powerpoint=~s/,/./g;
$soft_access=~s/\r\n/ /g;	$soft_access=~s/,/./g;
$soft_outlook=~s/\r\n/ /g;	$soft_outlook=~s/,/./g;
$soft_lotus=~s/\r\n/ /g;	$soft_lotus=~s/,/./g;

$soft_other=~s/\r\n/ /g;	$soft_other=~s/,/./g;
$typespeed_j=~s/\r\n/ /g;	$typespeed_j=~s/,/./g;
$typespeed_e=~s/\r\n/ /g;	$typespeed_e=~s/,/./g;
$os_win=~s/\r\n/ /g;	$os_win_win=~s/,/./g;
$os_mac=~s/\r\n/ /g;	$os_mac_mac=~s/,/./g;
$os_unix=~s/\r\n/ /g;	$os_unix_unix=~s/,/./g;
$os_other=~s/\r\n/ /g;	$os_other_other=~s/,/./g;
$os_other_name=~s/\r\n/ /g;	$os_other_name=~s/,/./g;
$boki_score=~s/\r\n/ /g;	$boki_score=~s/,/./g;
$boki_year=~s/\r\n/ /g;	$boki_year=~s/,/./g;
$hisho_score=~s/\r\n/ /g;	$hisho_score=~s/,/./g;
$hisho_year=~s/\r\n/ /g;	$hisho_year=~s/,/./g;
$eiken_score=~s/\r\n/ /g;	$eiken_score=~s/,/./g;
$eiken_year=~s/\r\n/ /g;	$eiken_year=~s/,/./g;
$toeic_score=~s/\r\n/ /g;	$toeic_score=~s/,/./g;
$toeic_year=~s/\r\n/ /g;	$toeic_year=~s/,/./g;
$toeic_ip=~s/\r\n/ /g;	$toeic_ip=~s/,/./g;
$toefl_score=~s/\r\n/ /g;	$toefl_score=~s/,/./g;
$toefl_year=~s/\r\n/ /g;	$toefl_year=~s/,/./g;
$tsuyaku_score=~s/\r\n/ /g;	$tsuyaku_score=~s/,/./g;
$tsuyaku_year=~s/\r\n/ /g;	$tsuyaku_year=~s/,/./g;
$honyaku_score=~s/\r\n/ /g;	$honyaku_score=~s/,/./g;
$honyaku_year=~s/\r\n/ /g;	$honyaku_year=~s/,/./g;
$kokuren_score=~s/\r\n/ /g;	$kokuren_score=~s/,/./g;
$kokuren_year=~s/\r\n/ /g;	$kokuren_year=~s/,/./g;
$cambridge1=~s/\r\n/ /g;	$cambridge1=~s/,/./g;
$cambridge2=~s/\r\n/ /g;	$cambridge2=~s/,/./g;
$cambridge3=~s/\r\n/ /g;	$cambridge3=~s/,/./g;

$cambridge4=~s/\r\n/ /g;	$cambridge4=~s/,/./g;
$cambridge5=~s/\r\n/ /g;	$cambridge5=~s/,/./g;
$cambridge_year=~s/\r\n/ /g;	$cambridge_year=~s/,/./g;
$annai_lang=~s/\r\n/ /g;	$annai_lang=~s/,/./g;
$annai_year=~s/\r\n/ /g;	$annai_year=~s/,/./g;
$other_gogaku1=~s/\r\n/ /g;	$other_gogaku1=~s/,/./g;
$other_gogaku1_other=~s/\r\n/ /g;	$other_gogaku1_other=~s/,/./g;
$other_gogaku1_lv=~s/\r\n/ /g;	$other_gogaku1_lv=~s/,/./g;
$other_gogaku2=~s/\r\n/ /g;	$other_gogaku2=~s/,/./g;
$other_gogaku2_other=~s/\r\n/ /g;	$other_gogaku2_other=~s/,/./g;
$other_gogaku2_lv=~s/\r\n/ /g;	$other_gogaku2_lv=~s/,/./g;
$other_gogaku3=~s/\r\n/ /g;	$other_gogaku3=~s/,/./g;
$other_gogaku3_other=~s/\r\n/ /g;	$other_gogaku3_other=~s/,/./g;
$other_gogaku3_lv=~s/\r\n/ /g;	$other_gogaku3_lv=~s/,/./g;
$other_gogaku_shikaku=~s/\r\n/ /g;	$other_gogaku_shikaku=~s/,/./g;
$iss_school=~s/\r\n/ /g;	$iss_school=~s/,/./g;
$iss_school_name=~s/\r\n/ /g;	$iss_school_name=~s/,/./g;
$iss_school_course=~s/\r\n/ /g;	$iss_school_course=~s/,/./g;
$iss_school_class=~s/\r\n/ /g;	$iss_school_class=~s/,/./g;
$iss_school_term=~s/\r\n/ /g;	$iss_school_term=~s/,/./g;
$other_school1=~s/\r\n/ /g;	$other_school1=~s/,/./g;
$other_school2=~s/\r\n/ /g;	$other_school2=~s/,/./g;
$other_school3=~s/\r\n/ /g;	$other_school3=~s/,/./g;
$other_school4=~s/\r\n/ /g;	$other_school4=~s/,/./g;







	if(length $name_A eq 0)	{$err_name='<font color=red>←名前(姓)は必須項目です</font>' ;}
	elsif(!is_zenkaku($name_A))	{$err_name='<font color=red>←名前(姓)は全角でお願いします</font>' ;}
	elsif(length $name_B eq 0)	{$err_name='<font color=red>←名前(名)は必須項目です</font>' ;}
	elsif(!is_zenkaku($name_B))	{$err_name='<font color=red>←名前(名)は全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name);

	if(length $name_A2 eq 0)	{$err_name2='<font color=red>←ふりがなは必須項目です</font>' ;}
	elsif(!is_zenkaku($name_A2))	{$err_name2='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	elsif(length $name_B2 eq 0)	{$err_name2='<font color=red>←ふりがなは必須項目です</font>' ;}
	elsif(!is_zenkaku($name_B2))	{$err_name2='<font color=red>←ふりがなは全角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_name2);

	if(length $birthday_yy eq 0)		{$err_birthday='<font color=red>←生年月日(年)は必須項目です</font>' ;}
	elsif(!is_hankaku($birthday_yy))	{$err_birthday='<font color=red>←生年月日(年)は半角でお願いします</font>' ;}
	elsif(length $birthday_mm eq 0)		{$err_birthday='<font color=red>←生年月日(月)は必須項目です</font>' ;}
	elsif(!is_hankaku($birthday_mm))	{$err_birthday='<font color=red>←生年月日(月)は半角でお願いします</font>' ;}
	elsif(length $birthday_dd eq 0)		{$err_birthday='<font color=red>←生年月日(日)は必須項目です</font>' ;}
	elsif(!is_hankaku($birthday_dd))	{$err_birthday='<font color=red>←生年月日(日)は半角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_birthday);

	( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( time )  ;
	$age=($year+1900)-(1900+ $birthday_yy);
	if($mon<$birthday_mm){
		$age=$age-1;
	}elsif($mon==$birthday_mm){
		if($mday<birthday_dd){
			$age=$age-1;
		}
	}

	if	(length $sex eq 0)	{$err_sex='<font color=red>性別は必須項目です</font>';}
	if	($sex eq 'man')		{$sex_kanji='男性';$sex_eng='man';}
	elsif	($sex eq 'woman')	{$sex_kanji='女性';$sex_eng='woman';}
	&jcode::euc2sjis(\$sex_kanji);
	&jcode::euc2sjis(\$err_sex);

	if(length $zip1 eq 0)		{$err_zip='<font color=red>←郵便番号は必須項目です</font>' ;}
	elsif(length $zip2 eq 0)	{$err_zip='<font color=red>←郵便番号は必須項目です</font>' ;}
	elsif(!is_hankaku($zip1))	{$err_zip='<font color=red>←郵便番号は半角でお願いします</font>' ;}
	elsif(!is_hankaku($zip2))	{$err_zip='<font color=red>←郵便番号は半角でお願いします</font>' ;}
	&jcode::euc2sjis(\$err_zip);

	$address1	=$in{'address1'};	$address1=~s/\r\n/ /g;	$address1=~s/,/./g;
	if(length $address1 == 0)	{$err_address1='<font color=red>←市区町村は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_address1);

	$address2	=$in{'address2'};	$address2=~s/\r\n/ /g;	$address2=~s/,/./g;
	if(length $address2 == 0)	{$err_address2='<font color=red>←番地は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_address2);

#	$address3	=$in{'address3'};	$address3=~s/\r\n/ /g;	$address3=~s/,/./g;
#	if(length $address3 == 0)	{$err_address3='<font color=red>←建物・部屋番号は必須項目です</font>' ;}
#	&jcode::euc2sjis(\$err_address3);

	if(length $moyori1_rosen eq 0)	{$err_moyori1='<font color=red>←最寄り駅（路線）は必須項目です</font>' ;}
	if(length $moyori1_eki eq 0)	{$err_moyori1='<font color=red>←最寄り駅（駅）は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_moyori1);

	if(length $in{'moyori_type'} eq 0)	{$err_moyori_type='<font color=red>←最寄り駅へのアクセス方法は必須項目です</font>' ;}
	open ( FH_MOYORI_TYPE, $moyori_type_txt_path ) ;for ( $a = 0 ; $a <= $in{'moyori_type'} ; $a++ ){$moyori_type=<FH_MOYORI_TYPE>;$moyori_type=~s/\n//g;}close(FH_MOYORI_TYPE);

	if(length $tel1_1 eq 0)		{$err_tel1='<font color=red>←市外局番は必須項目です</font>' ;}
	elsif(!is_hankaku($tel1_1))	{$err_tel1='<font color=red>←市外局番は半角でお願いします</font>' ;}
	elsif(length $tel1_2 eq 0)	{$err_tel1='<font color=red>←市内局番は必須項目です/font>' ;}
	elsif(!is_hankaku($tel1_2))	{$err_tel1='<font color=red>←市内局番は半角でお願いします</font>' ;}
	elsif(length $tel1_3 eq 0)	{$err_tel1='<font color=red>←自宅電話番号は必須項目です</font>' ;}
	elsif(!is_hankaku($tel1_3))	{$err_tel1='<font color=red>←自宅電話番号は半角でお願いします</font>' ;}
	elsif(length $tel1_rusu eq 0)	{$err_tel1='<font color=red>←留守電の有無は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_tel1);

	$fax1		=$in{'fax1'};	$fax1=~s/\r\n/ /g;		$fax1=~s/,/./g;
	$fax2		=$in{'fax2'};	$fax2=~s/\r\n/ /g;		$fax2=~s/,/./g;
	$fax3		=$in{'fax3'};	$fax3=~s/\r\n/ /g;		$fax3=~s/,/./g;

	if(	(length $renraku_any eq 0) &&
		(length $renraku_home eq 0) &&
		(length $renraku_mob eq 0) &&
		(length $renraku_pcmail eq 0) &&
		(length $renraku_mobmail eq 0))
	{$err_renraku='<font color=red>←希望連絡先は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_renraku);

	if(length $mail1_A eq 0)	{$err_mail1_A='<font color=red>←メールアドレスは必須項目です</font>' ;}
	elsif(!is_hankaku($mail1_A))	{$err_mail1_A='<font color=red>←メールアドレスは半角でお願いします</font>' ;}
	elsif($mail1_A ne $mail1_B)	{$err_mail1_A='<font color=red>←メールアドレスが一致しません<font>' ;	$err_mail1_B='<font color=red>←メールアドレスが一致しません<font>' ;}
	&jcode::euc2sjis(\$err_mail1_A);
	&jcode::euc2sjis(\$err_mail1_B);
	$TO_user=$mail1_A;

	if(!is_hankaku($mail2_A))	{$err_mail2_A='<font color=red>←メールアドレスは半角でお願いします</font>' ;}
	elsif($mail2_A ne $mail2_B)	{$err_mail2_A='<font color=red>←メールアドレスが一致しません<font>' ;	$err_mail2_B='<font color=red>←メールアドレスが一致しません<font>' ;}
	&jcode::euc2sjis(\$err_mail1_A);
	&jcode::euc2sjis(\$err_mail1_B);

	if($in{'gakureki'} eq '0')	{$err_gakureki='<font color=red>←最終学歴は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_gakureki);

	if(length $gakureki_year == 0)	{$err_gakureki_year='<font color=red>←卒業年は必須項目です</font>';}
	&jcode::euc2sjis(\$err_gakureki_year);

	if(($in{'ryugaku1_kokumei'} eq '0') && (length $ryugaku1_kokumei_other == 0))
	{$err_ryugaku1='<font color=red>←留学経験１は必須項目です</font>' ;}
	&jcode::euc2sjis(\$err_ryugaku1);

	$start_month=$in{'start_month'};
	$start_month='未定' if($start_month eq 13);
	&jcode::euc2sjis(\$start_month);
	$start_day=$in{'start_day'};
	$start_day='未定' if($start_day eq 32);
	&jcode::euc2sjis(\$start_day);

	if($in{'start_year'} eq 0)	{$err_start='<font color=red>←希望勤務開始日(年)は必須です。</font>' ;}
	elsif($in{'start_month'} eq 0)	{$err_start='<font color=red>←希望勤務開始日(月)は必須です。</font>' ;}
	elsif($in{'start_day'} eq 0)	{$err_start='<font color=red>←希望勤務開始日(日)は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_start);


	if( $in{'work_style1'} == 0 	){
		$err_work_style='<font color=red>←就業形態(第一希望)は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_work_style);

	if(length $time1 eq 0)		{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	elsif(length $time2 eq 0)	{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}

	elsif(length $time3 eq 0)	{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	elsif(length $time4 eq 0)	{$err_time='<font color=red>←希望勤務時間は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_time);

	if( length $work_time == 0 	){
		$err_work_time='<font color=red>←勤務時間相談は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_work_time);


	if(	(length $wday_mon == 0) && (length $wday_tue == 0) && (length $wday_wed == 0) && (length $wday_thu == 0) && 
		(length $wday_fri == 0)	){
		$err_wday='<font color=red>←希望勤務曜日は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_wday);

	if( length $overtime == 0 	){
		$err_overtime='<font color=red>←残業は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_overtime);


	open ( FH_SATTYPE , $sat_type_txt_path ) ;for ( $a = 0 ; $a <= $in{'sat_type'} ; $a++ ){$sat_type=<FH_SATTYPE>;$sat_type=~ s/\n//g;}close(FH_SATTYPE);
	open ( FH_SMOKE , $smoke_txt_path ) ;for ( $a = 0 ; $a <= $in{'smoke'} ; $a++ ){$smoke=<FH_SMOKE>;$smoke=~ s/\n//g;}close(FH_SMOKE);

	if((length $jikyu eq 0) && (length $jikyu_any eq 0)) 	{$err_jikyu='<font color=red>←希望時給は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_jikyu);

	if(length $kinmuchi eq 0 ) 	{$err_kinmuchi='<font color=red>←希望勤務地は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_kinmuchi);

	open ( FH_SHOKUSHU1 , $shokushu_txt_path ) ;for ( $a = 0 ; $a <= $in{'shokushu1'} ; $a++ ){$shokushu1=<FH_SHOKUSHU1>;$shokushu1=~ s/\n//g;}close(FH_SHOKUSHU1);
	if($in{'shokushu1'} eq '0' )	{$err_shokushu1='<font color=red>←希望職種は必須です。</font>' ;}
	&jcode::euc2sjis(\$err_shokushu1);

	open ( FH_SHOKUSHU2 , $shokushu_txt_path ) ;for ( $a = 0 ; $a <= $in{'shokushu2'} ; $a++ ){$shokushu2=<FH_SHOKUSHU2>;$shokushu2=~ s/\n//g;}close(FH_SHOKUSHU2);

	open ( FH_HAKENSAKITYPE , $hakensaki_type_txt_path ) ;for ( $a = 0 ; $a <= $in{'hakensaki_type'} ; $a++ ){$hakensaki_type=<FH_SHOKUSHU2>;$hakensaki_type=~ s/\n//g;}close(FH_HAKENSAKITYPE);

	open ( FH_HAKENSAKIKIBO, $hakensaki_kibo_txt_path ) ;for ( $a = 0 ; $a <= $in{'hakensaki_kibo'} ; $a++ ){$hakensaki_kibo=<FH_GAKUREKI>;$hakensaki_kibo=~ s/\n//g;}close(FH_HAKENSAKIKIBO);

	open ( FH_WORD , $word_txt_path) ;for ( $a = 0 ; $a <= $in{'soft_word'} ; $a++ ){$soft_word=<FH_WORD>;$soft_word=~ s/\n//g;}close(FH_WORD);
	open ( FH_EXCEL , $excel_txt_path ) ;for ( $a = 0 ; $a <= $in{'soft_excel'} ; $a++ ){$soft_excel=<FH_EXCEL>;$soft_excel=~ s/\n//g;}close(FH_EXCEL);
	open ( FH_POWERPOINT , $powerpoint_txt_path ) ;for ( $a = 0 ; $a <= $in{'soft_powerpoint'} ; $a++ ){$soft_powerpoint=<FH_POWERPOINT>;$soft_powerpoint=~ s/\n//g;}close(FH_POWERPOINT);
	open ( FH_ACCESS , $access_txt_path ) ;for ( $a = 0 ; $a <= $in{'soft_access'} ; $a++ ){$soft_access=<FH_ACCESS>;$soft_access=~ s/\n//g;}close(FH_ACCESS);

	if( $in{'soft_word'} eq '0' ){
		$err_soft_word='<font color=red>←Wordの使用経験は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_soft_word);

	if( $in{'soft_excel'} eq '0' ){
		$err_soft_excel='<font color=red>←Excelの使用経験は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_soft_excel);

	if( $in{'soft_powerpoint'} eq '0' ){
		$err_soft_powerpoint='<font color=red>←Powerpointの使用経験は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_soft_powerpoint);

	if( $in{'soft_access'} eq '0' ){
		$err_soft_access='<font color=red>←Accessの使用経験は必須です。</font>' ;
	}
	&jcode::euc2sjis(\$err_soft_access);

}

if($in{'cmd'} eq 'send'){
	use lib "./";
	use Lite;
#	use MIME::Lite;

	open(TEMPLATE,"../template/counseling/mail.txt");
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;

	&jcode::sjis2jis(\$strHtml);

	$type = 'multipart/mixed';
	$msg = MIME::Lite->new( From    =>$FROM,To      =>$TO,Subject =>$Subject,Type    =>$type);
	$msg->attach(Type     =>'TEXT',   Data     =>$strHtml);
	$msg->send;

#ichino
#	$msg1 = MIME::Lite->new( From=>$FROM,To=>$TO1,Subject=>$Subject,Type=>$type);
#	$msg1->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg1->send;

#inoue
#	$msg2 = MIME::Lite->new( From=>$FROM,To=>$TO2,Subject=>$Subject,Type=>$type);
#	$msg2->attach(Type=>'TEXT',Data=>$strHtml);
#	$msg2->send;

	open(TEMPLATE,"../template/counseling/mail_user.txt");
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	&replace;

	$msg_user = MIME::Lite->new( From=>$FROM,To=>$TO_user,Subject=>$Subject,Type=>$type);
	$msg_user->attach(Type=>'TEXT',Data=>$strHtml);
	$msg_user->send;
	if( length $in{'file'} >0){
		$tmp=$incfn{'file'};
		$tmp =~ /(.*)\.([^\.]+$)/;
		$ext=$2;
		if(length $ext == 0 ){
			$file2=time.$mail1_A.".doc";
		}else{
			$file2=time.$mail1_A.".".$ext;
		}
		open(ATTACH,">".$resume_dir.$file2);
		print ATTACH $in{file};
		close(ATTACH);
		$attach='1';
		print "Content-type: text/html\n\n";

		open(CSV_TEMPLATE,$csv_txt_path);
		@entry=<CSV_TEMPLATE>;
		close(CSV_TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		&replace;
		$strHtml.="\n";

		open(CSV,$csv_path);
		print CSV $strHtml;
		close(CSV);

		open(TEMPLATE,$thanks_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		&replace;
		print $strHtml;

		exit;
	}else{
		open(TEMPLATE,$confirm_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){	$strHtml.=$i;}
		$err_attach="<font color=red >←レジュメの添付は必須です。</font>";
		&jcode::euc2sjis(\$err_attach);
		&replace;
		print "Content-type: text/html\n\n";

		print $strHtml;
		exit;
	}


}elsif($in{'cmd'} eq 'confirm'){
	if((
(length	$err_name)+
(length	$err_name2)+
(length $err_birthday)+
(length	$err_sex)+
(length	$err_zip)+
(length	$err_pref)+
(length	$err_address1)+
(length	$err_address2)+
(length	$err_address3)+
(length	$err_moyori1)+
(length	$err_tel1)+
(length	$err_mail1_A)+
(length	$err_mail1_B)+
(length	$err_mail2_A)+
(length	$err_mail2_B)+
(length	$err_renraku)+
(length	$err_start)+
(length	$err_work_style)+
(length	$err_time)+
(length	$err_work_time)+
(length	$err_wday)+
(length	$err_jikyu)+
(length	$err_shokushu1)+
(length	$err_kinmuchi)+
(length	$err_soft_word)+
(length	$err_soft_excel)+
(length	$err_soft_powerpoint)+
(length	$err_soft_access)+
(length	$err_enq01)+
(length	$err_enq02)+
(length	$err_enq03)+
(length	$err_enq04)
) == 0 ){
#確認画面へ
		open(TEMPLATE,$confirm_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){	$strHtml.=$i;}
		&replace;
		print "Content-type: text/html\n\n";
		print $strHtml;
	}else{
		print "Content-type: text/html\n\n";
		$err='<br><font color=red>入力事項に不十分な部分がございます。ご確認の上、再度エントリーしてください。</font>';
		&jcode::euc2sjis(\$err);
		open(TEMPLATE,$entry_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){
			$strHtml.=$i;
		}
		&replace;

		$strHtml=~s/__entry_cgi_path__/$entry_cgi_path/g;
		print $strHtml;
	}
}elsif($in{'cmd'} eq 'edit'){
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print "Content-type: text/html\n\n";
	print $strHtml;
}else{
#init open
	$err_name="";
	$err_name2="";
	$err_birthday="";
	$err_sex="";
	$err_zip="";
	$err_pref="";
	$err_address1="";
	$err_address2="";
	$err_address3="";
	$err_moyori1="";
	$err_tel1="";
	$err_mail1_A="";
	$err_mail1_B="";
	$err_mail2_A="";
	$err_mail2_B="";
	$err_ryugaku1="";
	$err_gakureki="";
	$err_gakureki_year="";
	$err_start="";
	$err_nowstatus="";
	$err_work_style="";
	$err_time="";
	$err_work_time="";
	$err_overtime="";
	$err_wday="";
	$err_jikyu="";
	$err_shokushu1="";
	$err_kinmuchi="";
	$err_soft_word="";
	$err_soft_excel="";
	$err_soft_powerpoint="";
	$err_soft_access="";
	$err_enq01="";
	$err_enq02="";
	$err_enq03="";
	$err_enq04="";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$entry_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){
		$strHtml.=$i;
	}
	&replace;
	print $strHtml;
}

sub replace{
	$strHtml=~s/__epoch__/$epoch/g;
	$strHtml=~s/__entry_cgi_path__/$entry_cgi_path/g;
	$strHtml=~s/__err__/$err/g;

	$strHtml=~s/__age__/$age/g;

	$strHtml=~s/__visa__/$visa/g;
	$strHtml=~s/__visa_num__/$in{visa}/g;
	&replace_radio('visa','1');

	$strHtml=~s/__visa_type__/$visa_type/g;
	$strHtml=~s/__visa_type_num__/$in{visa_type}/g;
	&replace_radio('visa_type','1');

	&replace_radio('sex','man');
	&replace_radio('sex','woman');
	$strHtml=~s/__sex__/$sex/g;
	$strHtml=~s/__sex_kanji__/$sex_kanji/g;

	&replace_select02d('pref',47);
	$strHtml=~s/__pref__/$pref/g;
	$strHtml=~s/__pref_num__/$in{'pref'}/g;


	&replace_radio('tel1_rusu','1');
	&replace_radio('tel1_rusu','2');
	$strHtml=~s/__tel1_rusu__/$tel1_rusu/g;
	$strHtml=~s/__tel1_rusu_num__/$in{tel1_rusu}/g;

	&replace_radio('tel2_rusu','1');
	&replace_radio('tel2_rusu','2');
	$strHtml=~s/__tel2_rusu__/$tel2_rusu/g;
	$strHtml=~s/__tel2_rusu_num__/$in{tel2_rusu}/g;

	&replace_radio('renraku_any','1');
	$strHtml=~s/__renraku_any_num__/$in{'renraku_any'}/g;
	&replace_radio('renraku_home','1');
	$strHtml=~s/__renraku_home_num__/$in{'renraku_home'}/g;
	&replace_radio('renraku_mob','1');
	$strHtml=~s/__renraku_mob_num__/$in{'renraku_mob'}/g;
	&replace_radio('renraku_pcmail','1');
	$strHtml=~s/__renraku_pcmail_num__/$in{'renraku_pcmail'}/g;
	&replace_radio('renraku_mobmail','1');
	$strHtml=~s/__renraku_mobmail_num__/$in{'renraku_mobmail'}/g;

	&replace_select02d(start_year,11);
	$strHtml=~s/__start_year__/$start_year/g;
	$strHtml=~s/__start_year_num__/$in{'start_year'}/g;
	&replace_select02d(start_month,13);
	$strHtml=~s/__start_month__/$start_month/g;
	$strHtml=~s/__start_month_num__/$in{'start_month'}/g;
	&replace_select02d(start_day,32);
	$strHtml=~s/__start_day__/$start_day/g;
	$strHtml=~s/__start_day_num__/$in{'start_day'}/g;

	&replace_radio('nowstatus','1');
	&replace_radio('nowstatus','2');
	$strHtml=~s/__nowstatus__/$nowstatus/g;
	$strHtml=~s/__nowstatus_num__/$in{'nowstatus'}/g;

	&replace_select01d(work_style1,5);
	$strHtml=~s/__work_style1__/$work_style1/g;
	$strHtml=~s/__work_style1_num__/$in{'work_style1'}/g;

	&replace_select01d(work_style2,5);
	$strHtml=~s/__work_style2__/$work_style2/g;
	$strHtml=~s/__work_style2_num__/$in{'work_style2'}/g;

	&replace_select01d(work_style3,5);
	$strHtml=~s/__work_style3__/$work_style3/g;
	$strHtml=~s/__work_style3_num__/$in{'work_style3'}/g;

	&replace_radio('work_time','1');
	&replace_radio('work_time','2');
	$strHtml=~s/__work_time__/$work_time/g;
	$strHtml=~s/__work_time_num__/$in{'work_time'}/g;

	&replace_radio('time_type','1');
	$strHtml=~s/__time_type__/$time_type/g;

	&replace_radio('wday_mon','月');
	&replace_radio('wday_tue','火');
	&replace_radio('wday_wed','水');
	&replace_radio('wday_thu','木');
	&replace_radio('wday_fri','金');

	$strHtml=~s/__wday_mon__/$wday_mon/g;
	$strHtml=~s/__wday_tue__/$wday_tue/g;

	$strHtml=~s/__wday_wed__/$wday_wed/g;
	$strHtml=~s/__wday_thu__/$wday_thu/g;
	$strHtml=~s/__wday_fri__/$wday_fri/g;

	&replace_radio('overtime','1');
	&replace_radio('overtime','2');
	$strHtml=~s/__overtime__/$overtime/g;
	$strHtml=~s/__overtime_num__/$in{'overtime'}/g;
	$strHtml=~s/__overtime_hour__/$overtime_hour/g;

	&replace_radio('sat_type','1');
	&replace_radio('sat_type','2');
	&replace_radio('sat_type','3');
	$strHtml=~s/__sat_type__/$sat_type/g;
	$strHtml=~s/__sat_type_num__/$in{sat_type}/g;

	&replace_radio('smoke','1');
	&replace_radio('smoke','2');
	$strHtml=~s/__smoke__/$smoke/g;
	$strHtml=~s/__smoke_num__/$in{smoke}/g;

	$strHtml=~s/__jikyu__/$jikyu/g;

	&replace_radio('jikyu_any','1');
	$strHtml=~s/__jikyu_any__/$jikyu_any/g;
	$strHtml=~s/__jikyu_any_num__/$in{jikyu_any}/g;

	&replace_select02d(shokushu1,34);
	$strHtml=~s/__shokushu1__/$shokushu1/g;
	$strHtml=~s/__shokushu1_num__/$in{'shokushu1'}/g;
	&replace_select02d(shokushu2,34);
	$strHtml=~s/__shokushu2__/$shokushu2/g;
	$strHtml=~s/__shokushu2_num__/$in{'shokushu2'}/g;

	&replace_radio('hakensaki_type','1');
	&replace_radio('hakensaki_type','2');
	&replace_radio('hakensaki_type','3');
	$strHtml=~s/__hakensaki_type__/$hakensaki_type/g;
	$strHtml=~s/__hakensaki_type_num__/$in{hakensaki_type}/g;

	&replace_radio('hakensaki_kibo','1');
	&replace_radio('hakensaki_kibo','2');
	&replace_radio('hakensaki_kibo','3');
	$strHtml=~s/__hakensaki_kibo__/$hakensaki_kibo/g;
	$strHtml=~s/__hakensaki_kibo_num__/$in{hakensaki_kibo}/g;

	&replace_select01d('gakureki',6);
	$strHtml=~s/__gakureki__/$gakureki/g;
	$strHtml=~s/__gakureki_num__/$in{'gakureki'}/g;

	&replace_select02d('ryugaku1_kokumei',25);
	$strHtml=~s/__ryugaku1_kokumei__/$ryugaku1_kokumei/g;
	$strHtml=~s/__ryugaku1_kokumei_num__/$in{'ryugaku1_kokumei'}/g;

	&replace_select02d('ryugaku2_kokumei',25);
	$strHtml=~s/__ryugaku2_kokumei__/$ryugaku2_kokumei/g;
	$strHtml=~s/__ryugaku2_kokumei_num__/$in{'ryugaku2_kokumei'}/g;

	&replace_select02d('ryugaku3_kokumei',25);
	$strHtml=~s/__ryugaku3_kokumei__/$ryugaku3_kokumei/g;
	$strHtml=~s/__ryugaku3_kokumei_num__/$in{'ryugaku3_kokumei'}/g;

	&replace_radio('os_win','win');
	$strHtml=~s/__os_win__/$os_win/g;
	&replace_radio('os_mac','mac');
	$strHtml=~s/__os_mac__/$os_mac/g;
	&replace_radio('os_unix','unix');
	$strHtml=~s/__os_unix__/$os_unix/g;
	&replace_radio('os_other','other');
	$strHtml=~s/__os_other__/$os_other/g;

	&replace_select01d('soft_word',4);
	&replace_select01d('soft_excel',4);
	&replace_select01d('soft_powerpoint',4);
	&replace_select01d('soft_access',4);
	$strHtml=~s/__soft_word__/$soft_word/g;
	$strHtml=~s/__soft_excel__/$soft_excel/g;
	$strHtml=~s/__soft_powerpoint__/$soft_powerpoint/g;
	$strHtml=~s/__soft_access__/$soft_access/g;
	$strHtml=~s/__soft_word_num__/$in{'soft_word'}/g;
	$strHtml=~s/__soft_excel_num__/$in{'soft_excel'}/g;
	$strHtml=~s/__soft_powerpoint_num__/$in{'soft_powerpoint'}/g;
	$strHtml=~s/__soft_access_num__/$in{'soft_access'}/g;

	&replace_radio('soft_excel1','SUM');
	$strHtml=~s/__soft_excel1__/$soft_excel1/g;
	&replace_radio('soft_excel2','AVERAGE');
	$strHtml=~s/__soft_excel2__/$soft_excel2/g;
	&replace_radio('soft_excel3','IF');
	$strHtml=~s/__soft_excel3__/$soft_excel3/g;
	&replace_radio('soft_excel4','VLOOKUP');
	$strHtml=~s/__soft_excel4__/$soft_excel4/g;
	&replace_radio('soft_excel5','PIBOT');
	$strHtml=~s/__soft_excel5__/$soft_excel5/g;

	&replace_radio('toeic_ip','1');
	$strHtml=~s/__toeic_ip__/$toeic_ip/g;
	$strHtml=~s/__toeic_ip_num__/$in{'toeic_ip'}/g;

	&replace_radio('cambridge1','1');
	$strHtml=~s/__cambridge1__/$cambridge1/g;
	$strHtml=~s/__cambridge1_num__/$in{'cambridge1'}/g;

	&replace_radio('cambridge2','1');
	$strHtml=~s/__cambridge2__/$cambridge2/g;
	$strHtml=~s/__cambridge2_num__/$in{'cambridge2'}/g;

	&replace_radio('cambridge3','1');
	$strHtml=~s/__cambridge3__/$cambridge3/g;
	$strHtml=~s/__cambridge3_num__/$in{'cambridge3'}/g;

	&replace_radio('cambridge4','1');
	$strHtml=~s/__cambridge4__/$cambridge4/g;
	$strHtml=~s/__cambridge4_num__/$in{'cambridge4'}/g;

	&replace_radio('cambridge5','1');
	$strHtml=~s/__cambridge5__/$cambridge5/g;
	$strHtml=~s/__cambridge5_num__/$in{'cambridge5'}/g;

	&replace_select02d('other_gogaku1',21);
	$strHtml=~s/__other_gogaku1__/$other_gogaku1/g;
	$strHtml=~s/__other_gogaku1_num__/$in{'other_gogaku1'}/g;

	&replace_radio('other_gogaku1_lv','1');
	&replace_radio('other_gogaku1_lv','2');
	&replace_radio('other_gogaku1_lv','3');
	$strHtml=~s/__other_gogaku1_lv__/$other_gogaku1_lv/g;
	$strHtml=~s/__other_gogaku1_lv_num__/$in{'other_gogaku1_lv'}/g;

	&replace_select02d('other_gogaku2',21);
	$strHtml=~s/__other_gogaku2__/$other_gogaku2/g;
	$strHtml=~s/__other_gogaku2_num__/$in{'other_gogaku2'}/g;

	&replace_radio('other_gogaku2_lv','1');
	&replace_radio('other_gogaku2_lv','2');
	&replace_radio('other_gogaku2_lv','3');
	$strHtml=~s/__other_gogaku2_lv__/$other_gogaku2_lv/g;
	$strHtml=~s/__other_gogaku2_lv_num__/$in{'other_gogaku2_lv'}/g;

	&replace_select02d('other_gogaku3',21);
	$strHtml=~s/__other_gogaku3__/$other_gogaku3/g;
	$strHtml=~s/__other_gogaku3_num__/$in{'other_gogaku3'}/g;

	&replace_radio('other_gogaku3_lv','1');
	&replace_radio('other_gogaku3_lv','2');
	&replace_radio('other_gogaku3_lv','3');
	$strHtml=~s/__other_gogaku3_lv__/$other_gogaku3_lv/g;
	$strHtml=~s/__other_gogaku3_lv_num__/$in{'other_gogaku3_lv'}/g;

	$strHtml=~s/__other_shikaku1__/$other_shikaku1/g;
	$strHtml=~s/__other_shikaku1_shutoku__/$other_shikaku1_shutoku/g;
	$strHtml=~s/__other_shikaku2__/$other_shikaku2/g;
	$strHtml=~s/__other_shikaku2_shutoku__/$other_shikaku2_shutoku/g;
	$strHtml=~s/__other_shikaku3__/$other_shikaku3/g;
	$strHtml=~s/__other_shikaku3_shutoku__/$other_shikaku3_shutoku/g;
	$strHtml=~s/__other_shikaku4__/$other_shikaku4/g;
	$strHtml=~s/__other_shikaku4_shutoku__/$other_shikaku4_shutoku/g;

	&replace_radio('iss_school','1');
	&replace_radio('iss_school','2');
	$strHtml=~s/__iss_school__/$iss_school/g;
	$strHtml=~s/__iss_school_num__/$in{'iss_school'}/g;

	&replace_select01d('iss_school_name',4);
	$strHtml=~s/__iss_school_name__/$iss_school_name/g;
	$strHtml=~s/__iss_school_name_num__/$in{'iss_school_name'}/g;

$strHtml=~s/__job_id__/$job_id/g;
$strHtml=~s/__name_A__/$name_A/g;
$strHtml=~s/__name_B__/$name_B/g;
$strHtml=~s/__name_A2__/$name_A2/g;
$strHtml=~s/__name_B2__/$name_B2/g;
$strHtml=~s/__visa__/$visa/g;
$strHtml=~s/__visa_term__/$visa_term/g;
$strHtml=~s/__visa_type__/$visa_type/g;
$strHtml=~s/__birthday_yy__/$birthday_yy/g;
$strHtml=~s/__birthday_mm__/$birthday_mm/g;
$strHtml=~s/__birthday_dd__/$birthday_dd/g;
$strHtml=~s/__sex__/$sex/g;
$strHtml=~s/__zip1__/$zip1/g;
$strHtml=~s/__zip2__/$zip2/g;
$strHtml=~s/__pref__/$pref_num/g;
$strHtml=~s/__address1__/$address1/g;
$strHtml=~s/__address2__/$address2/g;
$strHtml=~s/__address3__/$address3/g;

$strHtml=~s/__moyori1_rosen__/$moyori1_rosen/g;
$strHtml=~s/__moyori1_eki__/$moyori1_eki/g;
$strHtml=~s/__moyori1_walk__/$moyori1_walk/g;
$strHtml=~s/__moyori1_bus__/$moyori1_bus/g;
$strHtml=~s/__moyori1_bike__/$moyori1_bike/g;
$strHtml=~s/__moyori1_other__/$moyori1_other/g;
$strHtml=~s/__moyori2_rosen__/$moyori2_rosen/g;
$strHtml=~s/__moyori2_eki__/$moyori2_eki/g;
$strHtml=~s/__moyori2_walk__/$moyori2_walk/g;
$strHtml=~s/__moyori2_bus__/$moyori2_bus/g;
$strHtml=~s/__moyori2_bike__/$moyori2_bike/g;
$strHtml=~s/__moyori2_other__/$moyori2_other/g;
$strHtml=~s/__tel1_1__/$tel1_1/g;
$strHtml=~s/__tel1_2__/$tel1_2/g;
$strHtml=~s/__tel1_3__/$tel1_3/g;
$strHtml=~s/__tel1_rusu__/$tel1_rusu/g;
$strHtml=~s/__tel1_rusu_num__/$in{'tel1_rusu'}/g;
$strHtml=~s/__tel2_1__/$tel2_1/g;
$strHtml=~s/__tel2_2__/$tel2_2/g;
$strHtml=~s/__tel2_3__/$tel2_3/g;
$strHtml=~s/__tel2_rusu__/$tel2_rusu/g;
$strHtml=~s/__tel2_rusu_num__/$in{'tel2_rusu'}/g;
$strHtml=~s/__fax_1__/$fax_1/g;
$strHtml=~s/__fax_2__/$fax_2/g;
$strHtml=~s/__fax_3__/$fax_3/g;
$strHtml=~s/__renraku_any__/$renraku_any/g;
$strHtml=~s/__renraku_home__/$renraku_home/g;
$strHtml=~s/__renraku_mob__/$renraku_mob/g;
$strHtml=~s/__renraku_pcmail__/$renraku_pcmail/g;
$strHtml=~s/__renraku_mobmail__/$renraku_mobmail/g;
$strHtml=~s/__mail1_A__/$mail1_A/g;
$strHtml=~s/__mail1_B__/$mail1_B/g;
$strHtml=~s/__mail2_A__/$mail2_A/g;

$strHtml=~s/__mail2_B__/$mail2_B/g;
$strHtml=~s/__gakureki__/$gakureki_num/g;
$strHtml=~s/__gakureki_school__/$gakurekischool/g;
$strHtml=~s/__gakureki_year__/$gakureki_year/g;
$strHtml=~s/__ryugaku1_kokumei__/$ryugaku1_kokumei_num/g;
$strHtml=~s/__ryugaku1_kokumei_other__/$ryugaku1_kokumei_other/g;
$strHtml=~s/__ryugaku1_school__/$ryugaku1_school/g;
$strHtml=~s/__ryugaku1_start_yy__/$ryugaku1_start_yy/g;
$strHtml=~s/__ryugaku1_start_mm__/$ryugaku1_start_mm/g;
$strHtml=~s/__ryugaku1_end_yy__/$ryugaku1_end_yy/g;
$strHtml=~s/__ryugaku1_end_mm__/$ryugaku1_end_mm/g;
$strHtml=~s/__ryugaku2_kokumei__/$ryugaku1_kokumei_num/g;
$strHtml=~s/__ryugaku2_kokumei_other__/$ryugaku2_kokumei_other/g;
$strHtml=~s/__ryugaku2_school__/$ryugaku2_school/g;
$strHtml=~s/__ryugaku2_start_yy__/$ryugaku2_start_yy/g;
$strHtml=~s/__ryugaku2_start_mm__/$ryugaku2_start_mm/g;
$strHtml=~s/__ryugaku2_end_yy__/$ryugaku2_end_yy/g;
$strHtml=~s/__ryugaku2_end_mm__/$ryugaku2_end_mm/g;
$strHtml=~s/__ryugaku3_kokumei__/$ryugaku3_kokumei_num/g;
$strHtml=~s/__ryugaku3_kokumei_other__/$ryugaku3_kokumei_other/g;
$strHtml=~s/__ryugaku3_school__/$ryugaku3_school/g;
$strHtml=~s/__ryugaku3_start_yy__/$ryugaku3_start_yy/g;
$strHtml=~s/__ryugaku3_start_mm__/$ryugaku3_start_mm/g;
$strHtml=~s/__ryugaku3_end_yy__/$ryugaku3_end_yy/g;
$strHtml=~s/__ryugaku3_end_mm__/$ryugaku3_end_mm/g;
$strHtml=~s/__taizai1_kokumei__/$taizai1_kokumei/g;
$strHtml=~s/__taizai1_memo__/$taizai1_memo/g;
$strHtml=~s/__taizai1_term__/$taizai1_term/g;
$strHtml=~s/__taizai2_kokumei__/$taizai2_kokumei/g;
$strHtml=~s/__taizai2_memo__/$taizai2_memo/g;
$strHtml=~s/__taizai2_term__/$taizai2_term/g;
$strHtml=~s/__start_year__/$start_year/g;
$strHtml=~s/__start_month__/$start_month/g;
$strHtml=~s/__start_day__/$start_day/g;
$strHtml=~s/__nowstatus__/$nowstatus/g;
$strHtml=~s/__work_style1__/$work_style1_num/g;
$strHtml=~s/__work_style2__/$work_style2_num/g;
$strHtml=~s/__work_style3__/$work_style3_num/g;
$strHtml=~s/__time1__/$time1/g;
$strHtml=~s/__time2__/$time2/g;
$strHtml=~s/__time3__/$time3/g;
$strHtml=~s/__time4__/$time4/g;
$strHtml=~s/__work_time__/$work_time/g;
$strHtml=~s/__wday_mon__/$wday_mon_月/g;
$strHtml=~s/__wday_tue__/$wday_tue_火/g;
$strHtml=~s/__wday_wed__/$wday_wed_水/g;
$strHtml=~s/__wday_thu__/$wday_thu_木/g;
$strHtml=~s/__wday_fri__/$wday_fri_金/g;
$strHtml=~s/__overtime__/$overtime/g;
$strHtml=~s/__sat_type__/$sat_type/g;
$strHtml=~s/__smoke__/$smoke/g;
$strHtml=~s/__jikyu__/$jikyu/g;
$strHtml=~s/__jikyu_any__/$jikyu_any/g;
$strHtml=~s/__work_memo__/$work_memo/g;
$strHtml=~s/__shokushu1__/$shokushu1/g;
$strHtml=~s/__shokushu2__/$shokushu2/g;
$strHtml=~s/__hakensaki_type__/$hakensaki_type/g;
$strHtml=~s/__hakensaki_kibo__/$hakensaki_kibo/g;
$strHtml=~s/__hakensaki_gyousyu__/$hakensaki_gyousyu/g;
$strHtml=~s/__saketai_gyousyu__/$saketai_gyousyu/g;
$strHtml=~s/__kinmuchi__/$kinmuchi/g;
$strHtml=~s/__shoyouzikan__/$shoyouzikan/g;
$strHtml=~s/__soft_word__/$soft_word/g;
$strHtml=~s/__soft_excel__/$soft_excel/g;
$strHtml=~s/__soft_excel1__/$in{'soft_excel1'}/g;
$strHtml=~s/__soft_excel2__/$in{'soft_excel2'}/g;
$strHtml=~s/__soft_excel3__/$in{'soft_excel3'}/g;
$strHtml=~s/__soft_excel4__/$in{'soft_excel4'}/g;
$strHtml=~s/__soft_excel5__/$in{'soft_excel5'}/g;
$strHtml=~s/__soft_powerpoint__/$soft_powerpoint/g;
$strHtml=~s/__soft_access__/$soft_access/g;

&replace_radio('soft_outlook','1');
$strHtml=~s/__soft_outlook__/$soft_outlook/g;
$strHtml=~s/__soft_outlook_num__/$in{'soft_outlook'}/g;

&replace_radio('soft_lotus','1');
$strHtml=~s/__soft_lotus__/$soft_lotus/g;
$strHtml=~s/__soft_lotus_num__/$in{'soft_lotus'}/g;

$strHtml=~s/__soft_other__/$soft_other/g;
$strHtml=~s/__typespeed_j__/$typespeed_j/g;
$strHtml=~s/__typespeed_e__/$typespeed_e/g;
$strHtml=~s/__os_win__/$os_win_win/g;
$strHtml=~s/__os_mac__/$os_mac_mac/g;
$strHtml=~s/__os_unix__/$os_unix_unix/g;
$strHtml=~s/__os_other__/$os_other_other/g;
$strHtml=~s/__os_other_name__/$os_other_name/g;
$strHtml=~s/__boki_score__/$boki_score/g;
$strHtml=~s/__boki_year__/$boki_year/g;
$strHtml=~s/__hisho_score__/$hisho_score/g;
$strHtml=~s/__hisho_year__/$hisho_year/g;

$strHtml=~s/__eiken_score__/$eiken_score/g;
$strHtml=~s/__eiken_year__/$eiken_year/g;
$strHtml=~s/__toeic_score__/$toeic_score/g;
$strHtml=~s/__toeic_year__/$toeic_year/g;
$strHtml=~s/__toeic_ip__/$toeic_ip/g;
$strHtml=~s/__toefl_score__/$toefl_score/g;
$strHtml=~s/__toefl_year__/$toefl_year/g;
$strHtml=~s/__tsuyaku_score__/$tsuyaku_score/g;
$strHtml=~s/__tsuyaku_year__/$tsuyaku_year/g;
$strHtml=~s/__honyaku_score__/$honyaku_score/g;
$strHtml=~s/__honyaku_year__/$honyaku_year/g;
$strHtml=~s/__kokuren_score__/$kokuren_score/g;
$strHtml=~s/__kokuren_year__/$kokuren_year/g;
$strHtml=~s/__cambridge1__/$cambridge1/g;
$strHtml=~s/__cambridge2__/$cambridge2/g;
$strHtml=~s/__cambridge3__/$cambridge3/g;
$strHtml=~s/__cambridge4__/$cambridge4/g;
$strHtml=~s/__cambridge5__/$cambridge5/g;
$strHtml=~s/__cambridge_year__/$cambridge_year/g;
$strHtml=~s/__annai_lang__/$annai_lang/g;
$strHtml=~s/__annai_year__/$annai_year/g;
$strHtml=~s/__other_gogaku1__/$other_gogaku1/g;
$strHtml=~s/__other_gogaku1_other__/$other_gogaku1_other/g;
$strHtml=~s/__other_gogaku1_lv__/$other_gogaku1_lv/g;
$strHtml=~s/__other_gogaku2__/$other_gogaku2/g;
$strHtml=~s/__other_gogaku2_other__/$other_gogaku2_other/g;
$strHtml=~s/__other_gogaku2_lv__/$other_gogaku2_lv/g;
$strHtml=~s/__other_gogaku3__/$other_gogaku3/g;
$strHtml=~s/__other_gogaku3_other__/$other_gogaku3_other/g;
$strHtml=~s/__other_gogaku3_lv__/$other_gogaku3_lv/g;
$strHtml=~s/__other_gogaku_shikaku__/$other_gogaku_shikaku/g;
$strHtml=~s/__iss_school__/$iss_school/g;
$strHtml=~s/__iss_school_name__/$iss_school_name/g;
$strHtml=~s/__iss_school_course__/$iss_school_course/g;
$strHtml=~s/__iss_school_class__/$iss_school_class/g;
$strHtml=~s/__iss_school_term__/$iss_school_term/g;
$strHtml=~s/__other_school1__/$other_school1/g;
$strHtml=~s/__other_school2__/$other_school2/g;
$strHtml=~s/__other_school3__/$other_school3/g;
$strHtml=~s/__other_school4__/$other_school4/g;
$strHtml=~s/__otoiawase__/$otoiawase/g;
$strHtml=~s/__attach__/$attach/g;



	$strHtml=~s/__enq01__/$enq01/g;
	$strHtml=~s/__enq01qes__/$enq01qes/g;
	$strHtml=~s/__enq01ans__/$enq01ans/g;
	$strHtml=~s/__enq01sel__/$enq01sel/g;
	$strHtml=~s/__enq01text__/$enq01text/g;

	$strHtml=~s/__enq02__/$enq02/g;
	$strHtml=~s/__enq02qes__/$enq02qes/g;
	$strHtml=~s/__enq02ans__/$enq02ans/g;
	$strHtml=~s/__enq02sel__/$enq02sel/g;
	$strHtml=~s/__enq02text__/$enq02text/g;

	$strHtml=~s/__enq03__/$enq03/g;
	$strHtml=~s/__enq03qes__/$enq03qes/g;
	$strHtml=~s/__enq03ans__/$enq03ans/g;
	$strHtml=~s/__enq03sel__/$enq03sel/g;
	$strHtml=~s/__enq03text__/$enq03text/g;

	$strHtml=~s/__enq04__/$enq04/g;
	$strHtml=~s/__enq04qes__/$enq04qes/g;
	$strHtml=~s/__enq04ans__/$enq04ans/g;
	$strHtml=~s/__enq04sel__/$enq04sel/g;
	$strHtml=~s/__enq04text__/$enq04text/g;

	$strHtml=~s/__err__/$err/g;
	$strHtml=~s/__err_attach__/$err_attach/g;
	$strHtml=~s/__err_name__/$err_name/g;
	$strHtml=~s/__err_name2__/$err_name2/g;
	$strHtml=~s/__err_birthday__/$err_birthday/g;
	$strHtml=~s/__err_age__/$err_age/g;
	$strHtml=~s/__err_sex__/$err_sex/g;
	$strHtml=~s/__err_zip__/$err_zip/g;
	$strHtml=~s/__err_pref__/$err_pref/g;
	$strHtml=~s/__err_address1__/$err_address1/g;
	$strHtml=~s/__err_address2__/$err_address2/g;
	$strHtml=~s/__err_address3__/$err_address3/g;
	$strHtml=~s/__err_moyori1__/$err_moyori1/g;
	$strHtml=~s/__err_tel1__/$err_tel1/g;
	$strHtml=~s/__err_mail1_A__/$err_mail1_A/g;
	$strHtml=~s/__err_mail1_B__/$err_mail1_B/g;
	$strHtml=~s/__err_mail2_A__/$err_mail2_A/g;
	$strHtml=~s/__err_mail2_B__/$err_mail2_B/g;
	$strHtml=~s/__err_renraku__/$err_renraku/g;
	$strHtml=~s/__err_ryugaku1__/$err_ryugaku1/g;
	$strHtml=~s/__err_gakureki__/$err_gakureki/g;
	$strHtml=~s/__err_gakureki_year__/$err_gakureki_year/g;
	$strHtml=~s/__err_start__/$err_start/g;

	$strHtml=~s/__err_nowstatus__/$err_nowstatus/g;
	$strHtml=~s/__err_work_style__/$err_work_style/g;
	$strHtml=~s/__err_time__/$err_time/g;
	$strHtml=~s/__err_work_time__/$err_work_time/g;
	$strHtml=~s/__err_overtime__/$err_overtime/g;
	$strHtml=~s/__err_wday__/$err_wday/g;
	$strHtml=~s/__err_jikyu__/$err_jikyu/g;
	$strHtml=~s/__err_shokushu1__/$err_shokushu1/g;
	$strHtml=~s/__err_kinmuchi__/$err_kinmuchi/g;
	$strHtml=~s/__err_soft_word__/$err_soft_word/g;
	$strHtml=~s/__err_soft_excel__/$err_soft_excel/g;
	$strHtml=~s/__err_soft_powerpoint__/$err_soft_powerpoint/g;
	$strHtml=~s/__err_soft_access__/$err_soft_access/g;

	$strHtml=~s/__err_enq01__/$err_enq01/g;
	$strHtml=~s/__err_enq02__/$err_enq02/g;
	$strHtml=~s/__err_enq03__/$err_enq03/g;
	$strHtml=~s/__err_enq04__/$err_enq04/g;
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

sub is_zenkaku{
	$tmp = $_[0];
	&jcode::sjis2euc(\$tmp);
	if($tmp !~ /[\x00-\x7f]/){
		return 1;
	}else{
		return 0;
	}
}

sub is_hankaku{
	$tmp = $_[0];
	if($tmp !~ /[\x80-\xff]/){
		return 1;
	}else{
		return 0;
	}
}

sub enq_radio{
	$temp=$_[0];
	$temp=~s/\n//g;
	if(length $temp > 0){
		if($_[3]==$_[2]){
			$enq.="<input type=radio name=".$_[1]." value=".$_[3]." checked>".$temp;
		}else{
			$enq.="<input type=radio name=".$_[1]." value=".$_[3].">".$temp;
		}
	};
}

sub wtime {
	($sec,$min,$hour,$mday,$mon,$year,$weekday) = localtime;
	
	$year += 1900;
	$mon++;
	$mon = sprintf("%.2d",$mon);
	$mday = sprintf("%.2d",$mday);
	$hour = sprintf("%.2d",$hour);
	$min = sprintf("%.2d",$min);
	$sec = sprintf("%.2d",$sec);
	
	@week = ("日","月","火","水","木","金","土");
	$weekday = $week[$wday];
	&jcode::euc2sjis(\$weekday);
}

