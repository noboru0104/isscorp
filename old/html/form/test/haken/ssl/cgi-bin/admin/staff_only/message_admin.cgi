#!/usr/bin/perl
# MW 20080807 改修 メッセージ管理詳細画面

#require '../../../perl_lib/jcode.pl';
#require '../../../perl_lib/cgi-lib.pl';

require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
&ReadParse;

#$entry_cgi_path     ="/haken/ssl/cgi-bin/admin/staff_only/upload_admin.cgi";
$entry_cgi_path     ="./upload_admin.cgi";

$resume_cgi_path   =$entry_cgi_path."?type=resume";
$timesheet_cgi_path   =$entry_cgi_path."?type=timesheet";
$message_cgi_path   =$entry_cgi_path."?type=message";

$resume_dir     ="../../secret/staff_only/upload/";

$csv_dir        ="../../secret/staff_only/upload/";
$csv_mes_path       =$csv_dir . "message.csv";

$template_dir       ="../../template/staff_only/upload/";
$index_html_path    =$template_dir . "message_admin.html";

$id=$ENV{'QUERY_STRING'};

open(CSV,$csv_mes_path);
@list=<CSV>;
close(CSV);

foreach $i (@list){
#    &jcode::sjis2euc(\$i);
    $i=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
    if($1 eq $id){
        $name=$3;
        $title=$4;
        $mes=$5;
        print "Content-type: text/html\n\n";
        open(TEMPLATE,$index_html_path);
        @entry=<TEMPLATE>;
        close(TEMPLATE);
        $strHtml="";
        foreach $e (@entry){$strHtml.=$e;}
        $strHtml=~s/__RESUME__ADMIN__LINK__/$resume_cgi_path/g;
        $strHtml=~s/__TIME__ADMIN__LINK__/$timesheet_cgi_path/g;
        $strHtml=~s/__MES__ADMIN__LINK__/$message_cgi_path/g;
        $strHtml=~s/__NAME__/$name/g;
        $strHtml=~s/__TITLE__/$title/g;
        $strHtml=~s/__MESSAGE__/$mes/g;
#        &jcode::euc2sjis(\$strHtml);
        print $strHtml;
    }
}