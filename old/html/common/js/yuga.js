/*
 * yuga.js 0.5.1 - 優雅なWeb制作のためのJS
 *
 * Copyright (c) 2007 Kyosuke Nakamura (kyosuke.jp)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Since:     2006-10-30
 * Modified:  2008-02-26
 *
 * jQuery 1.2.3
 * ThickBox 3.1
 */

/*
 * [使用方法] XHTMLのhead要素内で次のように読み込みます。
 
<link rel="stylesheet" href="css/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/thickbox.js"></script>
<script type="text/javascript" src="js/yuga.js" charset="utf-8"></script>

 */

(function($) {

	$(function() {
		$.yuga.selflink({
			hoverClass:'btn'  //除去するロールオーバーのクラス名
		});
		$.yuga.rollover({
			hoverClass: '.btn',		//イメージにつけるロールオーバーのクラスセレクタ
			groupClass: '.btngroup',//グループ化したロールオーバーのクラスセレクタ
			postfix: '_on'			//ファイル名の末尾につける文字列
		});
		$.yuga.externalLink({
			windowOpen: true,				//外部リンクは別ウインドウで開く
			externalClass: 'externalLink'	//外部リンクにつけるクラス名
		});
		$.yuga.thickbox();
		$.yuga.stripe({
			oddClass:'odd',		//奇数行につくクラス
			evenClass:'even'	//偶数行につくクラス
		});
		$.yuga.css3class();
		$('a[href^=#], area[href^=#]').setYugaScroll();
	});

	//---------------------------------------------------------------------

	$.yuga = {
		// URIを解析したオブジェクトを返すfunction
		Uri: function(s){
			this.originalPath = s;
			//絶対パスを取得
			this.getAbsolutePath = function(path){
				if (!path.match(/^(mailto:)|(javascript:)/)) {
					var img = new Image();
					img.src = path;
					path = img.src;
					img.src = '#';
				}
				return path;
			};
			this.absolutePath = this.getAbsolutePath(s);
			//同じ文書にリンクしているかどうか
			this.isSelfLink = (this.absolutePath == location.href);
			//絶対パスを分解
			var fields = {'schema' : 2, 'username' : 5, 'password' : 6, 'host' : 7, 'path' : 9, 'query' : 10, 'fragment' : 11};
			var r = /^((\w+):)?(\/\/)?((\w+):?(\w+)?@)?([^\/\?:]+):?(\d+)?(\/?[^\?#]+)?\??([^#]+)?#?(\w*)/.exec(this.absolutePath);
			for (var field in fields) {
				this[field] = r[fields[field]]; 
			}
		},
		//現在のページへのリンク
		selflink: function (options) {
			var c = $.extend({
				hoverClass:'btn'
			}, options);
			$('a[href]').each(function(){
				var href = new $.yuga.Uri(this.getAttribute('href'));
				if (href.isSelfLink && !href.fragment) {
					$(this).addClass('current');
					//img要素が含まれていたら現在用画像（_cr）に設定
					$(this).find('img').each(function(){
						//ロールオーバークラスが設定されていたら削除
						$(this).removeClass(c.hoverClass);
						this.originalSrc = $(this).attr('src');
						this.currentSrc = this.getAttribute('src').replace(/(\.gif|\.jpg|\.png)/, "_cr$1");
						$(this).attr('src',this.currentSrc);
					});
				}
			});
		},
		//ロールオーバー
		rollover: function(options) {
			var c = $.extend({
				hoverClass: '.roll',
				groupClass: '.rollgroup',
				postfix: '_on'
			}, options);
			//ロールオーバーするノードの初期化
			$(c.hoverClass).each(function(){
				this.originalSrc = $(this).attr('src');
				this.rolloverSrc = this.originalSrc.replace(/(\.gif|\.jpg|\.png)$/, c.postfix+"$1");
				this.rolloverImg = new Image;
				this.rolloverImg.src = this.rolloverSrc;
			});
			//通常ロールオーバー
			$(c.hoverClass).not($(c.groupClass+' '+c.hoverClass)).hover(function(){
				$(this).attr('src',this.rolloverSrc);
			},function(){
				$(this).attr('src',this.originalSrc);
			});
			//グループ化されたロールオーバー
			$(c.groupClass).hover(function(){
				$(this).find(c.hoverClass).each(function(){
					$(this).attr('src',this.rolloverSrc);
				});
			},function(){
				$(this).find(c.hoverClass).each(function(){
					$(this).attr('src',this.originalSrc);
				});
			});
		},
		//外部リンクは別ウインドウを設定
		externalLink: function(options) {
			var c = $.extend({
				windowOpen:true,
				externalClass: 'externalLink'
			}, options);
			var e = $('a[href^="http://"]');
			if (c.windowOpen) {
				e.click(function(){
					window.open(this.href, '_blank');
					return false;
				});
			}
			e.addClass(c.externalClass);
		},
		//ドキュメントのスクロールを制御するオブジェクト
		scroll: (function() {
			var c;
			var TimerId;
			var stepCount = 0;
			var lastY = -1;
			//スクロール中に実行されるfunction
			function move() {
				var currentX = getCurrentX();
				var currentY = getCurrentY();
				if (stepCount >= c.step) {
					//スクロール終了時
					window.scrollTo(currentX, c.endY);
					stepCount = 0;
				} else if (lastY != currentY) {
					//スクロール操作時
					stepCount = 0;
				} else {
					//通常スクロール時
					stepCount++;
					window.scrollTo(currentX, getEasingY());
					lastY = getEasingY();
					TimerId = setTimeout(move, Math.floor(300/c.fps)); 
				}

			};
			function getCurrentY() {
				return document.body.scrollTop  || document.documentElement.scrollTop;
			}
			function getCurrentX() {
				return document.body.scrollLeft  || document.documentElement.scrollLeft;
			}
			function getEasingY() {
				return Math.floor(getEasing(c.startY, c.endY, stepCount, c.step, c.easing));
			}
			function getEasing(start, end, stepCount, step, easing) {
				var s = stepCount/step;
				return (end-start)*(s+easing/(100*Math.PI)*Math.sin(Math.PI*s))+start;
			}
			return {
				set: function(options) {
					c = $.extend({
						startY:getCurrentY(),
						endY:0,
						easing:100,
						step:30,
						fps:60
					}, options);
					lastY = c.startY;
					TimerId = setTimeout(move, Math.floor(1000/c.fps)); 
				}
			};
		})(),
		//画像へ直リンクするとthickboxで表示(thickbox.js利用)
		thickbox: function() {
			try {
				tb_init('a[@href$=".jpg"], a[@href$=".gif"], a[@href$=".png"]');
			} catch(e) {
			}	
		},
		//奇数、偶数を自動追加
		stripe: function(options) {
			var c = $.extend({
				oddClass:'odd',
				evenClass:'even'
			}, options);
			$('ul, ol').each(function(){
				$(this).children('li:odd').addClass(c.evenClass);
				$(this).children('li:even').addClass(c.oddClass);
			});
			$('table').each(function(){
				$(this).children('tr:odd').addClass(c.evenClass);
				$(this).children('tr:even').addClass(c.oddClass);
			});
		},
		//css3のクラスを追加
		css3class: function() {
			//:first-child, :last-childをクラスとして追加
			$('body :first-child').addClass('firstChild');
			$('body :last-child').addClass('lastChild');
			//css3の:emptyをクラスとして追加
			$('body :empty').addClass('empty');
		}
	};
	$.fn.setYugaScroll = function() {
		return this.each(function(){
			$(this).each(function(){
				this.hrefdata = new $.yuga.Uri(this.getAttribute('href'));
			}).click(function(){
				var target = $('#'+this.hrefdata.fragment);
				if (target.length) {
					$.yuga.scroll.set({
						endY: target.offset().top
					});
					return false;
				}
			});
		});
	};

})(jQuery);


