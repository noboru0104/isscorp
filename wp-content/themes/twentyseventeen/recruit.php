<?php
/*
Template Name: recruit
*/
?>
<?php get_header(); ?>
<div id="interpretation" class="l-wrapper">
	<div class="l-pankuzu">
    	<div class="l-inner">
    		<p><a href="<?php echo home_url();?>/">ホーム</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<a href="<?php echo home_url();?>/recruit/">採用情報</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;キャリア採用</p>
        </div>
    </div>
    
    <section>
        <div class="l-mainVisual">
            <div class="l-mainVisual-inner">
                <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/recruit/img_mv_pc.png" alt="採用情報 recruit">
            </div>
        </div>
    </section>
    
    <div class="l-container"> 
    	<div class="l-main">
            <div class="l-recruitBlock">
                <h3><p>キャリア採用</p></h3>
            </div>            
            <div class="l-recruitBlock01">
                <p>
                	アイ・エス・エスは、言葉を通して「企業」と「人」をつなぎ、付加価値の高いサービスを通して国際社会に貢献することを目指しています。
                </p>
                <p>
                	新たな技術革新やビジネス、文化、アイデア等が国際コミュニケーションを介して共有される機会が多い昨今では、現場の業務で、国際社会の動きを実感し、やりがいとして感じられる場面が多くあります。<br>
                    私たちと一緒に、お客様のご満足を第一に、国際社会に貢献しませんか。
                </p>
                
                <div class="l-recruit-button">
                    <p><a href="<?php echo home_url();?>/kokusai/conference/support.html"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/recruit/btn_recruit_05_pc.png" alt="開催・運営のトータルサポート"></a><a href="<?php echo home_url();?>/kokusai/conference/system.html"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/recruit/btn_recruit_06_pc.png" alt="会議運営体制"></a></p>
                </div>
                              
            </div>
            <div class="l-recruitBlock03">
            	<h4>募集職種</h4>
                <div class="l-recruitBlock03-list">
                
                	<?php
						$fields0 = CFS()->get('career');
						
						foreach (array_reverse($fields0) as $field0) {						
												
					?>
                
                		<a href="#<?php echo $field0['number']; ?>">・<?php echo $field0['employ']; ?></a>
                    
                    <?php
						}
					?>
                    
                </div>
            </div>
            
            <?php
				$fields = CFS()->get('career');
				
				foreach (array_reverse($fields) as $field) {						
										
			?>
            
            <div id="<?php echo $field['number']; ?>" class="l-recruitBlock04">
            	<p class="p-title"><?php echo $field['employ']; ?></p>
                <table>
                	<tr>
                        <th>職種</th>
                        <td><?php echo $field['job']; ?></td>
                    </tr>
                    <tr>
                        <th>業務内容</th>
                        <td><?php echo $field['detail']; ?></td>
                    </tr>
                    <tr>
                        <th>ポイント</th>
                        <td><?php echo $field['point']; ?></td>
                    </tr>
                    <tr>
                        <th>資格・スキル</th>
                        <td><?php echo $field['skill']; ?></td>
                    </tr>
                    <tr>
                        <th>勤務時間</th>
                        <td><?php echo $field['time']; ?></td>
                    </tr>
                    <tr>
                        <th>勤務地</th>
                        <td><?php echo $field['location']; ?></td>
                    </tr>
                    <tr>
                        <th>待遇</th>
                        <td><?php echo $field['treatment']; ?></td>
                    </tr>
                    <tr>
                        <th>応募方法</th>
                        <td><?php echo $field['method']; ?></td>
                    </tr>
                </table>
            </div>
            <?php
            	}
            ?>
        </div>
        <div class="l-side">
        	<div class="l-side-menu">
            	<div class="p-side-menu-title">
                	<h3>採用情報</h3>
                </div>
                <div class="p-side-menu-list">
                	<p><a href="<?php echo home_url();?>/recruit/career/">キャリア採用</a></p>
                	<p><a href="<?php echo home_url();?>/recruit/career/">人材育成プログラム</a></p>
                    <p><a href="<?php echo home_url();?>/recruit/career/">転職者インタビュー</a></p>
                    <p><a href="<?php echo home_url();?>/recruit/career/">通訳者・翻訳者の登録</a></p>
                </div>
            </div>
            <div class="l-side-office">
            	<div class="p-side-office-title">
                	<h3>営業拠点</h3>
                </div>
                <div class="p-side-office-photo">
                	<p><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_side_photo.png" alt=""></p>
                </div>
                <div class="p-side-office-list">
                	<p><a href="<?php echo home_url();?>/">&gt;&nbsp;東京本社</a></p>
                	<p><a href="<?php echo home_url();?>/">&gt;&nbsp;関西支店</a></p>
                    <p><a href="<?php echo home_url();?>/">&gt;&nbsp;名古屋支店</a></p>
                    <p><a href="<?php echo home_url();?>/">&gt;&nbsp;福岡支店</a></p>
                </div>
            </div>
            <div class="l-side-banner">
            	<div class="p-side-banner-list">
                	<p><a href=""><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_side_registration.png" alt=""></a></p>
                </div>
            </div>
        </div>
    </div>
	
</div>
<?php get_footer(); ?>