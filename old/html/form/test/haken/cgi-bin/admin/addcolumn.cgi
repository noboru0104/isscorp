#!/usr/bin/perl

#各種設定開始

#トップページの更新の有無の設定（0は更新しない。１は更新する。デフォルトは更新しない）

my $RenewalTop = 0;

#ディレクトリ情報ここから
my $TextFileDir = "../secret/trend";
my $TemplateDir = "../template/trend";
my $ImageDir = "../../home/trend/image";
#ディレクトリ情報ここまで

#モジュール使用宣言

require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#テンプレートファイルリストここから
my $CompHtml = "addcomplete.html";
my $CorrectForm = "correctinputdata.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#保存ファイル名用変数（これを変更することで保存ファイル名が変更される。複数のfaqを設けるときに使用。デフォルトは"faq"）
my $SaveFileHead = "column";

#入力情報の過不足チェック用変数
my $ErrorCheck = "0";#0はエラーなし。その他はエラーあり。

#エラーメッセージ変数
my $ErrorMessage = "入力情報に不足があります。不足情報を入力し直して情報を再送信してください。";

#正常終了時Htmlソース出力用文字列(unix)
my $HtmlSrc = "";
open SAFEENDHTML, "$TemplateDir/$CompHtml";
while (<SAFEENDHTML>){
	$HtmlSrc .= $_;
}
close SAFEENDHTML;

#不正終了時Htmlソース出力用文字列(unix)
my $ErrorHtmlSrc = "";
open ERRORHTML, "$TemplateDir/$Failure";
while (<ERRORHTML>){
	$ErrorHtmlSrc .= $_;
}
close ERRORHTML;

#再入力用フォーム出力用文字列(unix)
my $ReInputForm = "";
open INPUTFORM, "$TemplateDir/$CorrectForm";
while (<INPUTFORM>){
	$ReInputForm .= $_;
}
close INPUTFORM;


#各種設定終了


#フォームからのデータ受け取り開始

print ("Content-type:text/html\n\n");

&ReadParse(*in);

my $QCategory = $in{'Category'};
my $Question = $in{'TextQuestion'};
my $Answer = $in{'TextAnswer'};
my $Respondent = $in{'Respondent'};
my $file = $in{'image'};


#フォームからのデータ受け取り終了

#受け取り情報が充分であるかどうかの判定
if(length ($Question) == 0){
	$ErrorCheck = 1;
}
else{}
if(length ($Answer) == 0){
	$ErrorCheck = 1;
}
else{}
if(length ($Respondent) == 0){
	$ErrorCheck = 1;
}
else{}

#正常時の書き込み作業
if($ErrorCheck == 0)
{
	#ファイルデータ受け取り開始
	my $ImageFile = "";
	
	if(length($file) == 0){
	}
	else{
	$ImageFile = sprintf("%10d",time());
	$ImageFile = "$ImageFile" . "\.jpeg";
	
	open OUTPUT, ">$ImageDir/$ImageFile";
	binmode(OUTPUT);
	print OUTPUT $file;
	close OUTPUT;
	chmod 0750,"$ImageDir/$ImageFile";
	}
	#ファイル生成完了

	#取得したデータから改行コードを削除
	#受け取ったデータの中に改行コードがあると、リスト表示に問題が発生するため
	$Question =~ s/\r\n/<br>/g;
	$Answer =~ s/\r\n/<br>/g;
	$Respondent =~ s/\r\n/<br>/g;

	$Question =~ s/\n/<br>/g;
	$Answer =~ s/\n/<br>/g;
	$Respondent =~ s/\n/<br>/g;
	
	#一意なファイル名を生成するためにエポック秒取得
	my $date = sprintf("%10d",time());
	
	#保存ファイル名生成
	my $SaveFileName = "$SaveFileHead" . "$date" . "\.txt";
	
	#ファイル保存
	open FAQ, ">$TextFileDir/$SaveFileName" or die "Error!";
	print FAQ ("$SaveFileName\n");
	print FAQ ("$QCategory\n");
	print FAQ ("$Question\n");
	print FAQ ("$Answer\n");
	print FAQ ("$Respondent\n");
	print FAQ ("$ImageFile\n");
	close FAQ;
	
	print ("$HtmlSrc");
}
#情報の不足時の再入力を促す処理
else
{
	$ReInputForm =~ s/__MESSAGE__TO__USER__/$ErrorMessage/;
	$ReInputForm =~ s/__QUESTION__MESSAGE__/$Question/;
	$ReInputForm =~ s/__ANSWER__MESSAGE__/$Answer/;
	$ReInputForm =~ s/__RESPONDENT__/$Respondent/;
	
	print ("$ReInputForm");
}

#以下サブルーチン

