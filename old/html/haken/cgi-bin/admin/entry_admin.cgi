#!/usr/bin/perl

#require '../../../perl_lib/jcode.pl';
#require '../../../perl_lib/cgi-lib.pl';

require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';
&ReadParse;

$entry_cgi_path		="/cgi-bin/admin/entry_admin.cgi";
#$entry_cgi_path		="/test/cgi-bin/admin/entry_admin.cgi";

#$resume_dir		="../../../secret/resume/";
$resume_dir		="../secret/resume/";

#$csv_dir		="../../../secret/entry/";
$csv_dir		="../secret/entry/";
$csv_path		=$csv_dir . "registered.csv";
$csv_tmp_path		=$csv_dir . "tmp.csv";
$csv_write_path		=">>".$csv_dir . "registered.csv";

#$template_dir		="../../../template/entry/";
$template_dir		="../template/entry/";
$index_html_path	=$template_dir . "entry_admin.html";
$detail_html_path	=$template_dir . "entry_detail.html";
$csv_head_txt_path	=$template_dir . "csv_head.txt";
$enq_edit_html_path	=$template_dir . "entry_enq_edit.html";
$enq_update_html_path	=$template_dir . "entry_enq_update.html";



if($in{'cmd'} eq 'enq_edit'){

	print "Content-type: text/html\n\n";
	$enq_path=$csv_dir."enq".$in{'id'}.".txt";
	open(ENQ,$enq_path);
	@entry=<ENQ>;
	$id=$in{'id'};
	$sel_type=shift @entry;
	$req_type=shift @entry;
	$ans01=shift @entry;
	$ans02=shift @entry;
	$ans03=shift @entry;
	$ans04=shift @entry;
	$ans05=shift @entry;
	$ans06=shift @entry;
	$ans07=shift @entry;
	$ans08=shift @entry;
	$ans09=shift @entry;
	$ans10=shift @entry;
#	foreach $i (@entty){$qes.=$i;}
	do{$qes.=shift @entry;}while($#entry>0);
	close(ENQ);

	open(TEMPLATE,$enq_edit_html_path);
	$strHtml="";
	@entry=<TEMPLATE>;
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	close(TEMPLATE);
	print $strHtml;

}elsif($in{'cmd'} eq 'enq_update'){
	$id=$in{'id'};
	$sel_type=$in{sel_type};
	$req_type=$in{req_type};
	$ans01=$in{ans01};
	$ans02=$in{ans02};
	$ans03=$in{ans03};
	$ans04=$in{ans04};
	$ans05=$in{ans05};
	$ans06=$in{ans06};
	$ans07=$in{ans07};
	$ans08=$in{ans08};
	$ans09=$in{ans09};
	$ans10=$in{ans10};
	$qes=$in{'qes'};

	$enq_path=$csv_dir."enq".$in{'id'}.".txt";
	open(ENQ,">".$enq_path);
	print ENQ $sel_type."\n";
	print ENQ $req_type."\n";
	print ENQ $ans01."\n";
	print ENQ $ans02."\n";
	print ENQ $ans03."\n";
	print ENQ $ans04."\n";
	print ENQ $ans05."\n";
	print ENQ $ans06."\n";
	print ENQ $ans07."\n";
	print ENQ $ans08."\n";
	print ENQ $ans09."\n";
	print ENQ $ans10."\n";
	print ENQ $qes."\n";
	close(ENQ);

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$enq_update_html_path);
	$strHtml="";
	@entry=<TEMPLATE>;
	foreach $i (@entry){$strHtml.=$i;}
	&replace;
	close(TEMPLATE);
	print $strHtml;

}elsif($in{'cmd'} eq 'download'){
	$filename = 'entry.csv';

print <<"EOL";
Content-type: text/comma-separated-values

Content-Disposition: attachment; filename=$filename

EOL
	open (HEAD,$csv_head_txt_path);
	foreach $data (<HEAD>) {	print $data."\n";}
	close(HEAD);
	open (IN,$csv_path);
	foreach $data (<IN>) {		print $data;}
	close(IN);
}elsif($in{'cmd'} eq 'resume'){
	$resume_path = $resume_dir.$in{'id'};
	$in{'id'} =~ /(.*)\.([^\.]+$)/;
	if($2 eq "png"){
		print "Content-type: image/png\n";
	}elsif(($2 eq "jpg") || ($2 eq "jpeg")){
		print "Content-type: image/jpeg\n";
	}elsif($2 eq "pdf"){
		print "Content-type: application/pdf\n";
	}elsif($2 eq "sit"){
		print "Content-type: application/x-stuffit\n";
	}elsif($2 eq "zip"){
		print "Content-type: application/x-zip-compressed\n";
	}elsif($2 eq "doc"){
		print "Content-type: application/msword\n";
	}else{
		print "Content-type: application/msword\n";
	}

print <<"EOL";
Content-Disposition: attachment; filename=$filename

EOL
	open (IN,$resume_path);
	foreach $data (<IN>) {		print $data;}
	close(IN);
}elsif($in{'cmd'} eq 'detail'){
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="";
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),/;
		if(($1 eq $in{'id'}) && ($33 eq $in{'email'}) ){
			$strList=$i;
		}
	}
	if(length $strList > 0){
		
		print "Content-type: text/html\n\n";
		open(TEMPLATE,$detail_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
$strList=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?)/;
	$name_A1=$4;
	$name_B1=$5;
	$name_A2=$6;
	$name_B2=$7;
	$name_A3=$8;
	$name_B3=$9;
	$name_A4=$10;
	$name_B4=$11;
	$name_A5=$12;
	$name_B5=$13;
	$name_A6=$14;
	$name_B6=$15;
	$birth_day=$16;
	$sex=$17;
	$age=$18;
	$zip=$19;
	$pref=$20;
	$address1=$21;
	$address2=$22;
	$address3=$23;
	$moyori_rosen=$24;
	$moyori_eki=$25;
	$moyori_type=$26;
	$moyori_min=$27;
	$tel=$28;
	$tel_rusu=$29;
	$mob=$30;
	$mob_rusu=$31;
	$fax=$32;
	$mail=$33;
	$renraku=$34;
	$start_year=$35;
	$start_month=$36;
	$start_day=$37;
	$style1=$38;
	$style2=$39;
	$style3=$40;
	$shoukai=$41;
	$time12=$42;
	$time34=$43;
	$time_type=$44;
	$wday_mon=$45;
	$wday_tue=$46;
	$wday_wed=$47;
	$wday_thu=$48;
	$wday_fri=$49;
	$wday_sat=$50;
	$wday_sun=$51;
	$sat_type=$52;
	$smoke=$53;
	$jikyu=$54;
	$koutsuhi=$55;
	$shokushu1=$56;
	$shokushu2=$57;
	$hakensaki_type=$58;
	$hakensaki_kibo=$59;
	$hakensaki_gyousyu=$60;
	$saketai_gyousyu=$61;
	$kinmuchi=$62;
	$shoyouzikan=$63;
	$gakureki=$64;
	$ryugaku1_kokumei=$65;
	$ryugaku1_school=$66;
	$ryugaku1_start_yy=$67;
	$ryugaku1_start_mm=$68;
	$ryugaku1_end_yy=$69;
	$ryugaku1_end_mm=$70;
	$ryugaku2_kokumei=$71;
	$ryugaku2_school=$72;
	$ryugaku2_start_yy=$73;
	$ryugaku2_start_mm=$74;
	$ryugaku2_end_yy=$75;
	$ryugaku2_end_mm=$76;
	$tsuyaku_gakureki=$77;
	$soft_word=$78;
	$soft_excel=$79;
	$soft_excel1=$80;
	$soft_excel2=$81;
	$soft_excel3=$82;
	$soft_excel4=$83;
	$soft_powerpoint=$84;
	$soft_access=$85;
	$soft_other=$86;
	$os_win=$87;
	$os_mac=$88;
	$os_unix=$89;
	$os_other=$90;
	$eiken_score=$91;
	$eiken_shutoku=$92;
	$toeic_score=$93;
	$toeic_shutoku=$94;
	$toefl_score=$95;
	$toefl_shutoku=$96;
	$tsuyaku_score=$97;
	$tsuyaku_shutoku=$98;
	$kokuren_score=$99;
	$kokuren_shutoku=$100;
	$menkyo_shutoku=$101;
	$boki_score=$102;
	$boki_shutoku=$103;
	$hisho_score=$104;
	$hisho_shutoku=$105;
	$other_gogaku=$106;
	$kaiwa=$107;
	$hoyuu_shikaku=$108;
	$PR_kibou=$109;
	$otoiawase=$110;
	$enq01qes=$111;
	$enq01sel=$112;
	$enq01text=$113;
	$enq02qes=$114;
	$enq02sel=$115;
	$enq02text=$116;
	$enq03qes=$117;
	$enq03sel=$118;
	$enq03text=$119;
	$enq04qes=$120;
	$enq04sel=$121;
	$enq04text=$122;

	$strHtml=~s/__name_A1__/$name_A1/g;
	$strHtml=~s/__name_B1__/$name_B1/g;
	$strHtml=~s/__name_A2__/$name_A2/g;
	$strHtml=~s/__name_B2__/$name_B2/g;
	$strHtml=~s/__name_A3__/$name_A3/g;
	$strHtml=~s/__name_B3__/$name_B3/g;
	$strHtml=~s/__name_A4__/$name_A4/g;
	$strHtml=~s/__name_B4__/$name_B4/g;
	$strHtml=~s/__name_A5__/$name_A5/g;
	$strHtml=~s/__name_B5__/$name_B5/g;
	$strHtml=~s/__name_A6__/$name_A6/g;
	$strHtml=~s/__name_B6__/$name_B6/g;
	$strHtml=~s/__birth_day__/$birth_day/g;
	$strHtml=~s/__sex__/$sex/g;
	$strHtml=~s/__age__/$age/g;
	$strHtml=~s/__zip__/$zip/g;
	$strHtml=~s/__pref__/$pref/g;
	$strHtml=~s/__address1__/$address1/g;
	$strHtml=~s/__address2__/$address2/g;
	$strHtml=~s/__address3__/$address3/g;
	$strHtml=~s/__moyori_rosen__/$moyori_rosen/g;
	$strHtml=~s/__moyori_eki__/$moyori_eki/g;
	$strHtml=~s/__moyori_type__/$moyori_type/g;
	$strHtml=~s/__moyori_min__/$moyori_min/g;
	$strHtml=~s/__tel__/$tel/g;
	$strHtml=~s/__tel_rusu__/$tel_rusu/g;
	$strHtml=~s/__mob__/$mob/g;
	$strHtml=~s/__mob_rusu__/$mob_rusu/g;
	$strHtml=~s/__fax__/$fax/g;
	$strHtml=~s/__mail__/$mail/g;
	$strHtml=~s/__renraku__/$renraku/g;
	$strHtml=~s/__start_year__/$start_year/g;
	$strHtml=~s/__start_month__/$start_month/g;
	$strHtml=~s/__start_day__/$start_day/g;
	$strHtml=~s/__style1__/$style1/g;
	$strHtml=~s/__style2__/$style2/g;
	$strHtml=~s/__style3__/$style3/g;
	$strHtml=~s/__shoukai__/$shoukai/g;
	$strHtml=~s/__time12__/$time12/g;
	$strHtml=~s/__time34__/$time34/g;
	$strHtml=~s/__time_type__/$time_type/g;
	$strHtml=~s/__wday_mon__/$wday_mon/g;
	$strHtml=~s/__wday_tue__/$wday_tue/g;
	$strHtml=~s/__wday_wed__/$wday_wed/g;
	$strHtml=~s/__wday_thu__/$wday_thu/g;
	$strHtml=~s/__wday_fri__/$wday_fri/g;
	$strHtml=~s/__wday_sat__/$wday_sat/g;
	$strHtml=~s/__wday_sun__/$wday_sun/g;
	$strHtml=~s/__sat_type__/$sat_type/g;
	$strHtml=~s/__smoke__/$smoke/g;
	$strHtml=~s/__jikyu__/$jikyu/g;
	$strHtml=~s/__koutsuhi__/$koutsuhi/g;
	$strHtml=~s/__shokushu1__/$shokushu1/g;
	$strHtml=~s/__shokushu2__/$shokushu2/g;
	$strHtml=~s/__hakensaki_type__/$hakensaki_type/g;
	$strHtml=~s/__hakensaki_kibo__/$hakensaki_kibo/g;
	$strHtml=~s/__hakensaki_gyousyu__/$hakensaki_gyousyu/g;
	$strHtml=~s/__saketai_gyousyu__/$saketai_gyousyu/g;
	$strHtml=~s/__kinmuchi__/$kinmuchi/g;
	$strHtml=~s/__shoyouzikan__/$shoyouzikan/g;
	$strHtml=~s/__gakureki__/$gakureki/g;
	$strHtml=~s/__ryugaku1_kokumei__/$ryugaku1_kokumei/g;
	$strHtml=~s/__ryugaku1_school__/$ryugaku1_school/g;
	$strHtml=~s/__ryugaku1_start_yy__/$ryugaku1_start_yy/g;
	$strHtml=~s/__ryugaku1_start_mm__/$ryugaku1_start_mm/g;
	$strHtml=~s/__ryugaku1_end_yy__/$ryugaku1_end_yy/g;
	$strHtml=~s/__ryugaku1_end_mm__/$ryugaku1_end_mm/g;
	$strHtml=~s/__ryugaku2_kokumei__/$ryugaku2_kokumei/g;
	$strHtml=~s/__ryugaku2_school__/$ryugaku2_school/g;
	$strHtml=~s/__ryugaku2_start_yy__/$ryugaku2_start_yy/g;
	$strHtml=~s/__ryugaku2_start_mm__/$ryugaku2_start_mm/g;
	$strHtml=~s/__ryugaku2_end_yy__/$ryugaku2_end_yy/g;
	$strHtml=~s/__ryugaku2_end_mm__/$ryugaku2_end_mm/g;
	$strHtml=~s/__tsuyaku_gakureki__/$tsuyaku_gakureki/g;
	$strHtml=~s/__soft_word__/$soft_word/g;
	$strHtml=~s/__soft_excel__/$soft_excel/g;
	$strHtml=~s/__soft_excel1__/$soft_excel1/g;
	$strHtml=~s/__soft_excel2__/$soft_excel2/g;
	$strHtml=~s/__soft_excel3__/$soft_excel3/g;
	$strHtml=~s/__soft_excel4__/$soft_excel4/g;
	$strHtml=~s/__soft_powerpoint__/$soft_powerpoint/g;
	$strHtml=~s/__soft_access__/$soft_access/g;
	$strHtml=~s/__soft_other__/$soft_other/g;
	$strHtml=~s/__os_win__/$os_win/g;
	$strHtml=~s/__os_mac__/$os_mac/g;
	$strHtml=~s/__os_unix__/$os_unix/g;
	$strHtml=~s/__os_other__/$os_other/g;
	$strHtml=~s/__eiken_score__/$eiken_score/g;
	$strHtml=~s/__eiken_shutoku__/$eiken_shutoku/g;
	$strHtml=~s/__toeic_score__/$toeic_score/g;
	$strHtml=~s/__toeic_shutoku__/$toeic_shutoku/g;
	$strHtml=~s/__toefl_score__/$toefl_score/g;
	$strHtml=~s/__toefl_shutoku__/$toefl_shutoku/g;
	$strHtml=~s/__tsuyaku_score__/$tsuyaku_score/g;
	$strHtml=~s/__tsuyaku_shutoku__/$tsuyaku_shutoku/g;
	$strHtml=~s/__kokuren_score__/$kokuren_score/g;
	$strHtml=~s/__kokuren_shutoku__/$kokuren_shutoku/g;
	$strHtml=~s/__menkyo_shutoku__/$menkyo_shutoku/g;
	$strHtml=~s/__boki_score__/$boki_score/g;
	$strHtml=~s/__boki_shutoku__/$boki_shutoku/g;
	$strHtml=~s/__hisho_score__/$hisho_score/g;
	$strHtml=~s/__hisho_shutoku__/$hisho_shutoku/g;
	$strHtml=~s/__other_gogaku__/$other_gogaku/g;
	$strHtml=~s/__kaiwa__/$kaiwa/g;
	$strHtml=~s/__hoyuu_shikaku__/$hoyuu_shikaku/g;
	$strHtml=~s/__PR_kibou__/$PR_kibou/g;
	$strHtml=~s/__otoiawase__/$otoiawase/g;
	$strHtml=~s/__enq01qes__/$enq01qes/g;
	$strHtml=~s/__enq01sel__/$enq01sel/g;
	$strHtml=~s/__enq01text__/$enq01text/g;
	$strHtml=~s/__enq02qes__/$enq02qes/g;
	$strHtml=~s/__enq02sel__/$enq02sel/g;
	$strHtml=~s/__enq02text__/$enq02text/g;
	$strHtml=~s/__enq03qes__/$enq03qes/g;
	$strHtml=~s/__enq03sel__/$enq03sel/g;
	$strHtml=~s/__enq03text__/$enq03text/g;
	$strHtml=~s/__enq04qes__/$enq04qes/g;
	$strHtml=~s/__enq04sel__/$enq04sel/g;
	$strHtml=~s/__enq04text__/$enq04text/g;

		print $strHtml;
	}else{
		$strList="お問い合わせの情報は存在しません。削除された可能性があります。";
		print "Content-type: text/html\n\n";

		open(TEMPLATE,$index_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		$strHtml=~s/__INDEX__/$strList/g;
		&jcode::euc2sjis(\$strHtml);
		print $strHtml;
	}
}elsif($in{'cmd'} eq "del_confirm"){
	$strList="本当に削除してもよろしいですか？";
	$strList.="<a href=".$entry_cgi_path ."?cmd=del&id=".$in{'id'}."&email=".$in{'email'}.">[OK]</a>/<a href=".$entry_cgi_path .">戻る</a>";
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
	
}elsif($in{'cmd'} eq "del"){
	rename $csv_path,$csv_tmp_path;
	open(CSV_WRITE,$csv_write_path);
	open(CSV_TMP,$csv_tmp_path);
	@entry=<CSV_TMP>;
	close(CSV_TMP);
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		if(($1 eq $in{'id'}) && ($33 eq $in{'email'}) ){
			unlink $resume_dir.$3;
		}else{
			print CSV_WRITE $i;
		}
	}
	close(CSV_WRITE);
	
	$strList="削除しました。";
	$strList.="<a href=".$entry_cgi_path .">戻る</a>";
	
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}else{
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="[<a href=".$entry_cgi_path ."?cmd=download >CSVファイルのダウンロード</a>]<br><br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=1 >アンケート設定1</a>]<br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=2 >アンケート設定2</a>]<br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=3 >アンケート設定3</a>]<br>";
	$strList.="[<a href=".$entry_cgi_path ."?cmd=enq_edit&id=4 >アンケート設定4</a>]<br>";
	$strList.="<table>";
	foreach $i (@entry){
		$strList.="<tr>";
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		$strList.="<td><a href=./entry_admin.cgi?cmd=detail&id=".$1. "&email=". $33 .">[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]". $4 . " " . $5 . "</a></td>";
		if(length $3 >0){
			$strList.="<td>[<a href=./entry_admin.cgi?cmd=resume&id=".$3.">レジュメ</a>]</td>";
		}else{
			$strList.="<td></td>";
		}
		$strList.="<td>[<a href=./entry_admin.cgi?cmd=del_confirm&id=".$1. "&email=". $33 .">削除</a>]</td></tr>";
	}
	$strList.="</table>";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}

sub replace{
	if($sel_type ==1){
		$sel_type_kanji="テキスト";
	}elsif($sel_type == 2){
		$sel_type_kanji="選択肢";
	}elsif($sel_type == 3){
		$sel_type_kanji="テキスト+選択肢";
	}else{
		$sel_type_kanji="使用しない";
		$sel_type=0;
	}
	&jcode::euc2sjis(\$sel_type_kanji);

	if($req_type == 1){
		$req_type_kanji="必須";
	}else{
		$req_type_kanji="任意";
		$req_type=0;
	}
	&jcode::euc2sjis(\$req_type_kanji);

	$strHtml=~s/__id__/$id/g;
	&replace_radio2('sel_type','0',$sel_type);
	&replace_radio2('sel_type','1',$sel_type);
	&replace_radio2('sel_type','2',$sel_type);
	&replace_radio2('sel_type','3',$sel_type);
	$strHtml=~s/__sel_type__/$sel_type/g;
	$strHtml=~s/__sel_type_kanji__/$sel_type_kanji/g;
	&replace_radio2('req_type','1',$req_type);
	$strHtml=~s/__req_type__/$req_type/g;
	$strHtml=~s/__req_type_kanji__/$req_type_kanji/g;
	$strHtml=~s/__qes__/$qes/g;
	$strHtml=~s/__ans01__/$ans01/g;
	$strHtml=~s/__ans02__/$ans02/g;
	$strHtml=~s/__ans03__/$ans03/g;
	$strHtml=~s/__ans04__/$ans04/g;
	$strHtml=~s/__ans05__/$ans05/g;
	$strHtml=~s/__ans06__/$ans06/g;
	$strHtml=~s/__ans07__/$ans07/g;
	$strHtml=~s/__ans08__/$ans08/g;
	$strHtml=~s/__ans09__/$ans09/g;
	$strHtml=~s/__ans10__/$ans10/g;
}

sub replace_radio2{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($_[2] == $tmp2){
		$strHtml=~s/$tmp/checked/g;
	}else{
		$strHtml=~s/$tmp//g;
	}
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp/$tmp/g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

