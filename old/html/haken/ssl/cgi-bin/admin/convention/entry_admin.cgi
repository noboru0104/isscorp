#!/usr/bin/perl

require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
&ReadParse;


$entry_cgi_path		="/cgi-bin/admin/convention/entry_admin.cgi";

$csv_dir		="../../secret/convention/";
$csv_path		=$csv_dir . "registered.csv";
$csv_tmp_path		=$csv_dir . "tmp.csv";
$csv_write_path		=">>".$csv_dir . "registered.csv";

$template_dir		="../../template/convention/";
$index_html_path	=$template_dir . "entry_admin.html";
$detail_html_path	=$template_dir . "entry_detail.html";
$csv_head_txt_path	=$template_dir . "csv_head.txt";



if($in{'cmd'} eq 'detail'){
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="";
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
		if(($1 eq $in{'id'}) && ($13 eq $in{'email'}) ){
			$strList=$i;
		}
	}
	if(length $strList > 0){
		
		print "Content-type: text/html\n\n";
		open(TEMPLATE,$detail_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
$strList=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
	$company_name=$2;
	$company_name2=$3;
	$name=$4;
	$name2=$5;
	$zip=$6;
	$pref=$7;
	$address1=$8;
	$address2=$9;
	$address3=$10;
	$tel=$11;
	$fax=$12;
	$mail=$13;
	$message=$14;

	$strHtml=~s/__company_name__/$company_name/g;
	$strHtml=~s/__company_name2__/$company_name2/g;
	$strHtml=~s/__name__/$name/g;
	$strHtml=~s/__name2__/$name2/g;
	$strHtml=~s/__zip__/$zip/g;
	$strHtml=~s/__pref__/$pref/g;
	$strHtml=~s/__address1__/$address1/g;
	$strHtml=~s/__address2__/$address2/g;
	$strHtml=~s/__address3__/$address3/g;
	$strHtml=~s/__tel__/$tel/g;
	$strHtml=~s/__fax__/$fax/g;
	$strHtml=~s/__mail__/$mail/g;
	$strHtml=~s/__message__/$message/g;

		print $strHtml;
	}else{
		$strList="お問い合わせの情報は存在しません。削除された可能性があります。";
		print "Content-type: text/html\n\n";
		open(TEMPLATE,$index_html_path);
		@entry=<TEMPLATE>;
		close(TEMPLATE);
		$strHtml="";
		foreach $i (@entry){$strHtml.=$i;}
		$strHtml=~s/__INDEX__/$strList/g;
		&jcode::euc2sjis(\$strHtml);
		print $strHtml;
	}
}elsif($in{'cmd'} eq "del_confirm"){
	$strList="本当に削除してもよろしいですか？";
	$strList.="<a href=".$entry_cgi_path ."?cmd=del&id=".$in{'id'}."&email=".$in{'email'}.">[OK]</a>/<a href=".$entry_cgi_path .">戻る</a>";
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
	
}elsif($in{'cmd'} eq "del"){
	rename $csv_path,$csv_tmp_path;
	open(CSV_WRITE,$csv_write_path);
	open(CSV_TMP,$csv_tmp_path);
	@entry=<CSV_TMP>;
	close(CSV_TMP);
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		if(($1 eq $in{'id'}) && ($13 eq $in{'email'}) ){
		}else{
			print CSV_WRITE $i;
		}
	}
	close(CSV_WRITE);
	
	$strList="削除しました。";
	$strList.="<a href=".$entry_cgi_path .">戻る</a>";
	
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}elsif($in{'cmd'} eq 'download'){
	$filename = 'entry.csv';

#Content-type: text/comma-separated-values
print <<"EOL";
Content-type: text/csv
Content-Disposition: attachment; filename=$filename

EOL
	open (HEAD,$csv_head_txt_path);
	foreach $data (<HEAD>) {	print $data."\n";}
	close(HEAD);
	open (IN,$csv_path);
	foreach $data (<IN>) {		print $data;}
	close(IN);

}else{
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	$strList="[<a href=".$entry_cgi_path ."?cmd=download >CSVファイルのダウンロード</a>]<br><br><table>";
	foreach $i (@entry){
		$strList.="<tr>";
		&jcode::sjis2euc(\$i);
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		$strList.="<td><a href=./entry_admin.cgi?cmd=detail&id=".$1. "&email=". $13 .">[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]". $2 . " " . $4 . "</a></td>";
		$strList.="<td>[<a href=./entry_admin.cgi?cmd=del_confirm&id=".$1. "&email=". $13 .">削除</a>]</td></tr>";
	}
	$strList.="</table>";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}

sub replace{
	if($sel_type ==1){
		$sel_type_kanji="テキスト";
	}elsif($sel_type == 2){
		$sel_type_kanji="選択肢";
	}elsif($sel_type == 3){
		$sel_type_kanji="テキスト+選択肢";
	}else{
		$sel_type_kanji="使用しない";
		$sel_type=0;
	}
	&jcode::euc2sjis(\$sel_type_kanji);

	if($req_type == 1){
		$req_type_kanji="必須";
	}else{
		$req_type_kanji="任意";
		$req_type=0;
	}
	&jcode::euc2sjis(\$req_type_kanji);

	$strHtml=~s/__id__/$id/g;
	&replace_radio2('sel_type','0',$sel_type);
	&replace_radio2('sel_type','1',$sel_type);
	&replace_radio2('sel_type','2',$sel_type);
	&replace_radio2('sel_type','3',$sel_type);
	$strHtml=~s/__sel_type__/$sel_type/g;
	$strHtml=~s/__sel_type_kanji__/$sel_type_kanji/g;
	&replace_radio2('req_type','1',$req_type);
	$strHtml=~s/__req_type__/$req_type/g;
	$strHtml=~s/__req_type_kanji__/$req_type_kanji/g;
	$strHtml=~s/__qes__/$qes/g;
	$strHtml=~s/__ans01__/$ans01/g;
	$strHtml=~s/__ans02__/$ans02/g;
	$strHtml=~s/__ans03__/$ans03/g;
	$strHtml=~s/__ans04__/$ans04/g;
	$strHtml=~s/__ans05__/$ans05/g;
	$strHtml=~s/__ans06__/$ans06/g;
	$strHtml=~s/__ans07__/$ans07/g;
	$strHtml=~s/__ans08__/$ans08/g;
	$strHtml=~s/__ans09__/$ans09/g;
	$strHtml=~s/__ans10__/$ans10/g;
}

sub replace_radio2{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($_[2] == $tmp2){
		$strHtml=~s/$tmp/checked/g;
	}else{
		$strHtml=~s/$tmp//g;
	}
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp/$tmp/g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

