<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="follow,index,noydir">
<meta http-equiv="keywords" content="ISS,アイ・エス・エス,お問い合わせ" />
<title>入力 | ISS総合お問い合わせフォーム </title>
<link href="common/form_style.css" rel="stylesheet" type="text/css" media="screen" />
<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8">
<script type="text/javascript" src="/_common/js/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="/_common/js/ajaxcodedisplay-or.js"></script>
<script type="text/javascript" src="/_common/js/DropDownMenu.js"></script>
<script type="text/javascript"><!--
function set_imemode(myobj, mymode){
    switch(mymode){
        case 0: myobj.style.imeMode = "inactive"; break;
        case 1: myobj.style.imeMode = "active"; break;
        default: break;
    }
}

function AutoCheck(checkname) {
      document.getElementById(checkname).checked = true;
}

--></script>
</head>
<body>

<!-- Header Begin -->
<div id="header">
<div class="contentform"><a href="../index.html"><img src="../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../index.html"><img src="../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣、法人研修／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi"></div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<div id="formcont">
<h2><img src="images/ind_h2.jpg" alt="お問い合わせフォーム" width="700" height="78" /></h2>
<form method="POST" action="contact.php">
<table cellspacing="2" id="confirm">
  <tr>
    <td colspan="2" class="t1">入力画面</td>
  </tr>
    <tr>
      <td colspan="2" class="hisu"><span class="importance">*</span>&nbsp;は必須入力です。</td>
    </tr>
      
      <tr>
        <th><p align="left">お問い合わせサービス <span class="importance">*</span></p>
（複数選択可）    </th>
    <td>
      <label>
        <input name="checkbox1" type="checkbox" id="checkbox1" value="・通訳" <?php if(hs($p['checkbox1']) != "") echo "checked"; ?>/> 
        通訳</label>
      <br />
      <label>
        <input name="checkbox2" type="checkbox" id="checkbox2" value="・国際会議企画・運営" <?php if(hs($p['checkbox2']) != "") echo "checked"; ?>/> 
        国際会議企画・運営</label>
      <br />
      <label><input name="checkbox3" type="checkbox" id="checkbox3" value="・翻訳" <?php if(hs($p['checkbox3']) != "") echo "checked"; ?>/> 
      翻訳</label>
      <br />
      <label>
        <input name="checkbox4" type="checkbox" id="checkbox4" value="・人材派遣・紹介予定派遣" <?php if(hs($p['checkbox4']) != "") echo "checked"; ?>/> 
        人材派遣・紹介予定派遣</label>
      <br />
      <label>
        <input name="checkbox5" type="checkbox" id="checkbox5" value="・法人向け語学研修" <?php if(hs($p['checkbox5']) != "") echo "checked"; ?>/> 
        法人向け語学研修</label>
      <br />
      <label>
        <input name="checkbox6" type="checkbox" id="checkbox6" value="・その他" <?php if(hs($p['checkbox6']) != "") echo "checked"; ?>/> 
        その他</label>
      <?php if(isset($e['checkbox'])){ ?>
      <p class="error">↑選択してください↑</p>
    <?php } ?></td>
    </tr>
      <tr>
        <th>お問い合わせ内容&nbsp;<span class="importance">*</span></th>
    <td><textarea name="toiawase" cols="45" id="toiawase" style="font-size:13px;"><?php echo hs($p['toiawase']); ?></textarea>
      <?php if(isset($e['toiawase'])){ ?>
      <p class="error">↑入力してください↑</p>
    <?php } ?></td>
    </tr>
      
      <tr>
        <th>会社（団体）名 <span class="importance">*</span></th>
    <td><input name="company" type="text" size="25" id="company" value="<?php echo hs($p['company']); ?>">
<p><span class="selectRequiredMsg">例：株式会社アイ・エス・エス</span></p>
      <?php if(isset($e['company'])){ ?>
      <p class="error">↑正しく入力してください↑</p>
    <?php } ?> </td>
    </tr>
      
      <tr>
        <th>会社（団体）名 
        （フリガナ） <span class="importance">*</span></th>
    <td><input name="company2" type="text" size="25" id="company2" value="<?php echo hs($p['company2']); ?>" />
<p><span class="selectRequiredMsg">例：カブシキガイシャアイ・エス・エス</span></p>
      <?php if(isset($e['company2'])){ ?>
      <p class="error">↑正しく入力してください↑</p>
    <?php } ?></td>
    </tr>
      
      <tr>
        <th>部署名</th>
    <td><input name="depart" type="text" size="25" value="<?php echo hs($p['depart']); ?>" />
	<p><span class="selectRequiredMsg">例：人事部</span></p>
</td>
    </tr>
      
      <tr>
        <th>業界</th>
    <td>
      <select name="gyoukai">
        <option value="" selected>選択してください</option>
        <option value="水産・農林業・鉱業" <?php if(hs($p['gyoukai']) == "水産・農林業・鉱業" ) echo "selected"; ?>>水産・農林業・鉱業</option>
        <option value="建設業" <?php if(hs($p['gyoukai']) == "建設業" ) echo "selected"; ?>>建設業</option>
        <option value="製造業(食品)" <?php if(hs($p['gyoukai']) == "製造業(食品)" ) echo "selected"; ?>>製造業(食品)</option>
        <option value="製造業(繊維・アパレル)" <?php if(hs($p['gyoukai']) == "製造業(繊維・アパレル)" ) echo "selected"; ?>>製造業(繊維・アパレル)</option>
        <option value="造業(化学・ガラス・セメント)" <?php if(hs($p['gyoukai']) == "造業(化学・ガラス・セメント)" ) echo "selected"; ?>>造業(化学・ガラス・セメント)</option>
        <option value="製造業(医薬品)" <?php if(hs($p['gyoukai']) == "製造業(医薬品)" ) echo "selected"; ?>>製造業(医薬品)</option>
        <option value="製造業(鉄鋼・非鉄・金属)" <?php if(hs($p['gyoukai']) == "製造業(鉄鋼・非鉄・金属)" ) echo "selected"; ?>>製造業(鉄鋼・非鉄・金属)</option>
        <option value="製造業(機械・電気機器・精密機器)" <?php if(hs($p['gyoukai']) == "製造業(機械・電気機器・精密機器)" ) echo "selected"; ?>>製造業(機械・電気機器・精密機器)</option>
        <option value="製造業(自動車・輸送用機器)" <?php if(hs($p['gyoukai']) == "製造業(自動車・輸送用機器)" ) echo "selected"; ?>>製造業(自動車・輸送用機器)</option>
        <option value="製造業(高級消費財)" <?php if(hs($p['gyoukai']) == "製造業(高級消費財)" ) echo "selected"; ?>>製造業(高級消費財)</option>
        <option value="製造業(化粧品)" <?php if(hs($p['gyoukai']) == "製造業(化粧品)" ) echo "selected"; ?>>製造業(化粧品)</option>
        <option value="製造業(その他)" <?php if(hs($p['gyoukai']) == "製造業(その他)" ) echo "selected"; ?>>製造業(その他)</option>
        <option value="資源・エネルギー" <?php if(hs($p['gyoukai']) == "資源・エネルギー" ) echo "selected"; ?>>資源・エネルギー</option>
        <option value="運輸業・郵便業" <?php if(hs($p['gyoukai']) == "運輸業・郵便業" ) echo "selected"; ?>>運輸業・郵便業</option>
        <option value="情報通信業(IT・通信)" <?php if(hs($p['gyoukai']) == "情報通信業(IT・通信)" ) echo "selected"; ?>>情報通信業(IT・通信)</option>
        <option value="情報通信業(マスメディア・広告業)" <?php if(hs($p['gyoukai']) == "情報通信業(マスメディア・広告業)" ) echo "selected"; ?>>情報通信業(マスメディア・広告業)</option>
        <option value="流通業" <?php if(hs($p['gyoukai']) == "流通業" ) echo "selected"; ?>>流通業</option>
        <option value="金融・保険・証券業" <?php if(hs($p['gyoukai']) == "金融・保険・証券業" ) echo "selected"; ?>>金融・保険・証券業</option>
        <option value="不動産業" <?php if(hs($p['gyoukai']) == "不動産業" ) echo "selected"; ?>>不動産業</option>
        <option value="サービス(宿泊業、飲食サービス業)" <?php if(hs($p['gyoukai']) == "サービス(宿泊業、飲食サービス業)" ) echo "selected"; ?>>サービス(宿泊業、飲食サービス業)</option>
        <option value="サービス(医療・福祉サービス)" <?php if(hs($p['gyoukai']) == "サービス(医療・福祉サービス)" ) echo "selected"; ?>>サービス(医療・福祉サービス)</option>
        <option value="サービス(生活関連・娯楽・レジャーサービス)" <?php if(hs($p['gyoukai']) == "サービス(生活関連・娯楽・レジャーサービス)" ) echo "selected"; ?>>サービス(生活関連・娯楽・レジャーサービス)</option>
        <option value="サービス(専門技術サービス業)" <?php if(hs($p['gyoukai']) == "サービス(専門技術サービス業)" ) echo "selected"; ?>>サービス(専門技術サービス業)</option>
        <option value="サービス(その他サービス業)" <?php if(hs($p['gyoukai']) == "サービス(その他サービス業)" ) echo "selected"; ?>>サービス(その他サービス業)</option>
        <option value="共(官公庁/日本政府機関/地方公共団体)" <?php if(hs($p['gyoukai']) == "共(官公庁/日本政府機関/地方公共団体)" ) echo "selected"; ?>>共(官公庁/日本政府機関/地方公共団体)</option>
        <option value="公共(国際機関・外国政府機関)" <?php if(hs($p['gyoukai']) == "公共(国際機関・外国政府機関)" ) echo "selected"; ?>>公共(国際機関・外国政府機関)</option>
        <option value="公共(学術・文化関係)" <?php if(hs($p['gyoukai']) == "公共(学術・文化関係)" ) echo "selected"; ?>>公共(学術・文化関係)</option>
        <option value="公共(公益法人)" <?php if(hs($p['gyoukai']) == "公共(公益法人)" ) echo "selected"; ?>>公共(公益法人)</option>
        <option value="公共(教育機関)" <?php if(hs($p['gyoukai']) == "公共(教育機関)" ) echo "selected"; ?>>公共(教育機関)業</option>
        <option value="公共(非営利法人/その他の団体)" <?php if(hs($p['gyoukai']) == "公共(非営利法人/その他の団体)" ) echo "selected"; ?>>公共(非営利法人/その他の団体)</option>
        </select></td>
    </tr>
      
      <tr>
        <th>お名前&nbsp;<span class="importance">*</span></th>
    <td><input name="name" type="text" size="25" id="name" value="<?php echo hs($p['name']); ?>" onFocus="set_imemode(this, 1)">
      <?php if(isset($e['name'])){ ?>
      <p class="error">↑正しく入力してください↑</p>
    <?php } ?></td>
    </tr>
      
      <tr>
        <th>お名前（フリガナ） <span class="importance">*</span></th>
    <td><input name="name2" type="text" size="25" id="name2" value="<?php echo hs($p['name2']); ?>" onfocus="set_imemode(this, 1)" />
      <?php if(isset($e['name2'])){ ?>
      <p class="error">↑正しく入力してください↑</p>
    <?php } ?></td>
    </tr>
      
      <tr>
        <th>電話番号<span class="importance">*</span>&nbsp;&nbsp;</th>
    <td>
      <input name="tel" type="text" size="30" id="tel" value="<?php echo hs($p['tel']); ?>" onfocus="set_imeMode(this, 0)"/>
      <?php if(isset($e['tel'])){ ?>
      <p class="error">↑正しく入力してください↑</p>
      <?php } ?>            </td>
    </tr>
      
      <tr>
        <th>e-mail アドレス&nbsp;<span class="importance">*</span></th>
    <td><input name="mail" type="text" size="30"  id="mail" value="<?php echo hs($p['mail']); ?>" onfocus="set_imeMode(this, 0)"/>
      <?php if(isset($e['mail'])){ ?>
      <p class="error">↑正しく入力してください↑</p>
    <?php } ?></td>
    </tr>
      
      <tr>
        <th>ご住所&nbsp;</th>
    <td>
      〒
      <input type="text" size="4" maxlength="3" class="ime" name="zip21" id="zip21" value="<?php echo hs($p['zip21']); ?>" onfocus="set_imeMode(this, 0)" />
      -
      <input type="text" name="zip22" id="zip22" size="5" maxlength="4" class="ime" value="<?php echo hs($p['zip22']); ?>" onkeyup="AjaxZip3.zip2addr('zip21','zip22','pref21','addr21','strt21');" />      <span class="supplement">（半角数字）　郵便番号を入力すると住所が自動入力されます</span>
      <br>
      <br>
      <input type="text" name="pref21" id="pref21" size="40" value="<?php echo hs($p['pref21']); ?>" /> 
      <span class="selectRequiredMsg">(都道府県)</span>
      <br /><br />

      <input  name="addr21" type="text" id="addr21" style="font-size:13px;" size="40" value="<?php echo hs($p['addr21']); ?>" />
      <span class="selectRequiredMsg">(市区町村)</span><br /><br />
      <input type="text" name="strt21" size="40" value="<?php echo hs($p['strt21']); ?>">
      <span class="selectRequiredMsg">(番地以下)<br />
        ※ビル名まで詳しくご入力ください</span></td>
    </tr>
      
      <tr>
        <th>当社をどのように<br />
        お知りになりましたか</th>
    <td><!--<br>
<input type="text" name="strt21" size="40">-->
      <label><input name="kikkake" type="radio" value="検索エンジン（Yahoo!）" <?php if(hs($p['kikkake']) == "検索エンジン（Yahoo!）") echo "checked"; ?>/>
        検索エンジン（Yahoo!）</label><br />
      <label><input name="kikkake" type="radio" value="検索エンジン（Google）" <?php if(hs($p['kikkake']) == "検索エンジン（Google）") echo "checked"; ?>/>
        検索エンジン（Google）</label><br />
      <label><input name="kikkake" type="radio" value="新聞・雑誌" <?php if(hs($p['kikkake']) == "新聞・雑誌") echo "checked"; ?>/>
        新聞・雑誌</label><br />
      <label><input name="kikkake" type="radio" value="取引先や知人からの紹介" <?php if(hs($p['kikkake']) == "取引先や知人からの紹介") echo "checked"; ?>/>
        取引先や知人からの紹介</label><br />
      <label><input name="kikkake" type="radio" value="以前から知っていた" <?php if(hs($p['kikkake']) == "以前から知っていた") echo "checked"; ?>/>
        以前から知っていた</label><br />
      <label><input name="kikkake"  id ="sonota" type="radio" value="その他" <?php if(hs($p['sonota']) != "") echo "checked"; ?>/>
        その他</label>
      <input type="text" name="sonota" id="sonota" size="40" value="<?php echo hs($p['sonota']); ?>" onfocus="AutoCheck('sonota');"/></td>
    </tr>
	<tr>
	<td colspan="2"><p align="center" class="ltx"> <strong> 個人情報の利用目的および取扱いについて </strong></p>
        <br />
        <ol class="dc">
          <li>個人情報の利用目的 <br />
            お問い合わせ、ご相談の対応及び見積書作成、資料の送付の目的</li>
          <li>個人情報の第三者への提供 </li>
          <p>当社は、法令の定める場合を除き、あらかじめご本人の同意を得ずに、個人情報を第三者に提供することは致しません。 </p>
          <li>個人情報の外部委託 </li>
          <p>個人情報の処理について、外部委託をすることがあります。</p>
          <li>個人情報提出の任意性 </li>
          <p>個人情報のご提出は任意です。但し、必要な情報を提供頂かない場合は、適切な対応ができない場合があります。</p>
          <li>個人情報の開示等の請求</li>
          <p>ご提出頂いた個人情報に関する苦情の申し出並びに個人情報の開示等請求（1.開示のご請求、2.利用目的の通知のご請求、3.訂正のご請求、4.追加のご請求、5.消去のご請求、6.利用停止または第三者への提供停止のご請求）の権利があります。<br />上記権利の行使をご希望の場合、下記の「個人情報の取扱いに関するお問い合わせ先」宛、お電話あるいは書面でご請求ください。</p>
        </ol>
        <br />
        <p align="right">株式会社アイ・エス・エス<br />
        個人情報保護管理者　白木原 孝次</p>
        <br />
        <br />
        <p><strong>■個人情報の取扱いに関するお問い合わせ先</strong></p>
        <p>株式会社 アイ・エス・エス　個人情報保護管理者  白木原 孝次<br />
          〒102-0083 東京都千代田区麹町3-1-1 麹町311ビル9階 <br />
          電話 03-3230-7731　　e-mail：pv_info@issjp.com<br />
          個人情報保護方針は、当社ホームページ<a href="http://www.issjp.com/" target="_parent">http://www.issjp.com</a>をご参照下さい。</p></td>
		  </tr>
	
      
      <tr>
        <td class="btnForm" colspan="2"><input type="submit" name="btn_confirm" value="　　確 認　　" />
        <!--
&nbsp;
<input name="リセット" type="reset" />
-->            </td>
    </tr>
    </table>


<div align="right"><br />
<table width="600"  border="0" cellpadding="0" cellspacing="2" class="formTable mtx"">
		<tr>
		  <td align="center" valign="top"><span id="ss_img_wrapper_100-50_flash_ja">
  <a href="http://jp.globalsign.com/" target=_blank>
  <img src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif" alt="SSL　グローバルサインのサイトシール" name="ss_img" border=0 id="ss_img"></a>
  </span>
  <script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gs_flash_100-50_ja.js"></script></td>
			  <td width="2" align="left" valign="top">&nbsp;</td>
			  <td align="left" valign="top"><p class="mtx">当サイトでは、実在性の証明とプライバシー保護のため、グローバルサインのSSLサーバ証明書を使用し、SSL暗号化通信を実現しています。サイトシールのクリックにより、サーバ証明書の検証結果をご確認ください。</p></td>
	    </tr>
  </table>
</div></form>


</div>
<!-- コンテンツ -->

<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../common/images/pixel_trans.gif" width="816" height="15" alt="" class="spacer" />
<div class="menuList">
<ul>
  <li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>
</div>
</div>
</div>
<!-- Footer End -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body></html>