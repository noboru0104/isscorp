------------------------------------------------------------------
この度はオンライン登録にエントリーありがとうございました。

このメールは自動配信です。
登録いただいた内容を御確認ください。
------------------------------------------------------------------

お名前: __name_A__ 
ふりがな: __name_B__ 
メールアドレス: __mail_A__ 
年齢: __age__ 
性別: __sex__ 
現住所: __address__ 
自宅の最寄り駅 路線:__moyori_rosen__ / 駅:__moyori_eki__ 
連絡先
自宅： __renraku_jitaku__ 
携帯： __renraku_keitai__ 
 
最終学歴: __gakureki__ 
希望勤務開始日: __start_year__年 __start_month__月 __start_day__日 
勤務可能曜日: __wday__ 
希望期間: __term__ 
希望職種　第１希望: __shokushu1__ 
希望職種　第2希望: __shokushu2__ 
希望勤務地: __kinmuchi__ 

職歴１（直近のご経験）
会社名: __shokureki1_kaisha__ 
勤務期間: __shokureki1_start1__年 __shokureki1_start2__月 開始 〜 __shokureki1_end1__年 __shokureki1_end2__月 終了 
職種: __shokureki1_shokushu__ 
雇用形態: __shokureki1_keitai__ 
職務内容: __shokureki1_naiyo__ 
 
職歴２
会社名: __shokureki2_kaisha__ 
勤務期間: __shokureki2_start1__年 __shokureki2_start2__月 開始 〜 __shokureki2_end1__年 __shokureki2_end2__月 終了 
職種: __shokureki2_shokushu__ 
雇用形態: __shokureki2_keitai__ 
職務内容: __shokureki2_naiyo__ 
 
使用ＯＳ __os__ 
使用可能ソフト Word __soft_word__ 
Excel __soft_excel__ 
PwerPoint __soft_powerpoint__ 
Access __soft_access__ 
その他 __soft_other__ 
 
英語資格
TOEIC __toeic_score__点（取得：__toeic_shutoku__年） 
TOEFL __toefl_score__点（取得：__toefl_shutoku__年） 
英検 __eiken_score__点（取得：__eiken_shutoku__年） 
 
その他保有資格:
__shikaku__ 

スキルに関する補足PR:
__hosokuPR__ 

自己PR・ご希望など:
__PR_kibou__ 

ご質問・お問い合わせ:
__otoiawase__ 


------------------------------------------------------------------
このメールにお心当たりのないお客様は、大変お手数ですが
【お問い合わせ先】まで　ご連絡頂きますようお願い申し上げます。
------------------------------------------------------------------
【お問い合わせ先】
株式会社アイ・エス・エス・サービスセンター
フリーダイヤル：0120-598-155
touroku@isssc.co.jp
http://www.isssc.co.jp

