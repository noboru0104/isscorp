#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $DataFileDir = "../secret/job";

my $TemplateDir = "../template/job";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "delfinish.html";
my $ConfirmHtml = "confirmdelworkinfo.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#データファイルリストここから
my $WorkInfo = "workinfo.csv";
my $NewWorkInfo = "newworkinfo.csv";
my $BackupFile = "workinfo.bak";
#データファイルリストここまで

#モジュール読み込み
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#出力HTMLソース一時格納用変数
my $HtmlSrc = "";

#データファイル一時保存用文字列
my @TempStr = ();

#一時ファイル作成用文字列
my @TempFile = ();

#削除対象情報入手
my $DeleteWorkNo = $ENV{'QUERY_STRING'};

#各種設定ここまで

print("Content-type:text/html\n\n");

#削除情報入手
&ReadParse(*in);
my $DeleteMode = $in{'DeleteMode'};

#削除モード動作
if ($DeleteMode == 1){
	#編集用ファイル読み込みと一時ファイル生成
	open DATAFILE, "$DataFileDir/$WorkInfo";
	@TempStr = <DATAFILE>;
	close DATAFILE;
	
	open TEMPDATA, ">$DataFileDir/$BackupFile";
	foreach my $DataLine (@TempStr){
		print TEMPDATA ($DataLine);
	}
	close TEMPDATA;
	
	#削除作業（削除する部分をファイルに書き直さないだけ
	open DATADELETE, ">$DataFileDir/$WorkInfo";
	foreach my $DataLine (@TempStr){
		my @DataLineElement = split(/,/,$DataLine);
		if ($DataLineElement[14] == $DeleteWorkNo){
		}
		else{
		print DATADELETE ($DataLine);
		}
	}
	close DATADELETE;
	
	#更新作業
	&CreateNewWorkInfo();
	&RenewTopPage();
	
	
	#編集終了メッセージテンプレート読み込み
	open BODYTEMP, "$TemplateDir/$HtmlBody";
	while (<BODYTEMP>){
		$HtmlSrc .= $_;
	}
	close BODYTEMP;	
	
	
}
#削除確認モード
else{
	#削除対象の情報入手
	my $AskNo = 0;
	my $JobNo = 0;
	my $Catch = 0;
	my $JobKind = 0;
	my $Wage = 0;
	my $WorkPlace = 0;
	my $PlaceData = 0;
	my $WorkDay = 0;
	my $AddWork = 0;
	my $StartTime = 0;
	my $EndTime = 0;
	my $WorkPeriod = 0;
	my $Bikou = 0;
	my $DayData = 0;
	
	open DATA, "$DataFileDir/$WorkInfo" or die "file not exist!";
	
	label1:while (<DATA>){
		my @TmpWorkInfo = split(/,/,$_);
		if ($TmpWorkInfo[14] == $DeleteWorkNo){
			$AskNo = $TmpWorkInfo[0];
			$JobNo = $TmpWorkInfo[1];
			$Catch = $TmpWorkInfo[2];
			$JobKind = $TmpWorkInfo[3];
			$Wage = $TmpWorkInfo[4];
			$WorkPlace = $TmpWorkInfo[6];
			$PlaceData = $TmpWorkInfo[13];
			$WorkDay = $TmpWorkInfo[7];
			$AddWork = $TmpWorkInfo[8];
			$StartTime = $TmpWorkInfo[9];
			$EndTime = $TmpWorkInfo[10];
			$WorkPeriod = $TmpWorkInfo[11];
			$Bikou = $TmpWorkInfo[12];
			$DayData = $TmpWorkInfo[15];
			

			$WorkDay = &ConvertDay($WorkDay,$DayData);
			$JobKind = &ConvertJob($JobKind);
			$WorkPlace = &ConvertPlace($WorkPlace,$PlaceData);
			last label1;
		}
		else{
		}
	}
	close DATA;
	
	#テンプレートファイル読み込み
	open BODYTEMP, "$TemplateDir/$ConfirmHtml";
	while (<BODYTEMP>){
		$HtmlSrc .= $_;
	}
	close BODYTEMP;
	
	#置換処理
	$HtmlSrc =~ s/__ASK__NO__/$JobNo/;
	$HtmlSrc =~ s/__CATCH__/$Catch/;
	$HtmlSrc =~ s/__JOB__TYPE__/$JobKind/;
	$HtmlSrc =~ s/__WAGE__/$Wage/;
	$HtmlSrc =~ s/__WORK__PLACE__/$WorkPlace/;
	$HtmlSrc =~ s/__WORK__DAY__/$WorkDay/;
	$HtmlSrc =~ s/__START__TIME__/$StartTime/;
	$HtmlSrc =~ s/__END__TIME__/$EndTime/;
	$HtmlSrc =~ s/__ADDITIONAL__WORK__/$AddWork/;
	$HtmlSrc =~ s/__WORK__PERIOD__/$WorkPeriod/;
	$HtmlSrc =~ s/__BIKOU__/$Bikou/;
	$HtmlSrc =~ s/__DELETE__NO__/$DeleteWorkNo/;
}

print ("$HtmlSrc");

#以下サブルーチン


sub ConvertJob{
	my $ConvertedJob = "";
	my @GetJobData = @_;
	#変換用仕事データ
	my @JobList = ("","通訳・翻訳・翻訳チェッカー","秘書・グループセクレタリー","英文事務・アシスタント・英文経理・貿易事務・人事・総務など","金融業界でのお仕事","IT・技術関連");
	$ConvertedJob = $JobList[$GetJobData[0]];
	return $ConvertedJob;
}

sub ConvertPlace{
	my $ConvertedPlace = "";
	my @GetPlaceData = @_;
	#変換用勤務場所データ
	my @PlaceList = ("","神谷町","赤坂見附","大手町","青山一丁目","茅場町","九段下","新宿","溜池山王","表参道","みなとみらい");
	if ($GetPlaceData[0] == -1){
		$ConvertedPlace = $GetPlaceData[1];
	}
	else{
		$ConvertedPlace = $PlaceList[$GetPlaceData[0]];
	}
	return $ConvertedPlace;
}

sub ConvertDay{
	my $ConvertedDay = "";
	my @GetDayData = @_;
	if ($GetDayData[0] == 1){
		$ConvertedDay = "平日";
	}
	elsif ($GetDayData[0] == 0){
		$ConvertedDay = "土・日";
	}
	elsif ($GetDayData[0] == -1){
		$ConvertedDay = $GetDayData[1];
	}
	else{
		if(($GetDayData[0] % 2) == 0){$ConvertedDay .= "月・";}
		if(($GetDayData[0] % 3) == 0){$ConvertedDay .= "火・";}
		if(($GetDayData[0] % 5) == 0){$ConvertedDay .= "水・";}
		if(($GetDayData[0] % 7) == 0){$ConvertedDay .= "木・";}
		if(($GetDayData[0] % 11) == 0){$ConvertedDay .= "金・";}
		if(($GetDayData[0] % 13) == 0){$ConvertedDay .= "土・";}
		if(($GetDayData[0] % 17) == 0){$ConvertedDay .= "日・";}
		chop ($ConvertedDay);
		chop ($ConvertedDay);
	}
	return $ConvertedDay;
}

sub CreateNewWorkInfo{
	#各種設定開始
	my $NumberOfData = 30;#書き出しデータ数。デフォルトは5
	#各種設定終了
	
	#データファイル読み込み
	my @TempWorkList = ();
	open CONVINFO, "$DataFileDir/$WorkInfo";
	@TempWorkList = <CONVINFO>;
	close CONVINFO;
	
	#データ書き込み

	open NEWINFO, ">$DataFileDir/$NewWorkInfo";

	$NumberOfData = @TempWorkList;#書き込みデータ数を無制限に

	for (my $count = 0;$count < $NumberOfData;$count++){
		print NEWINFO (pop(@TempWorkList));
	}
	close NEWINFO;
}

sub RenewTopPage{
	#各種設定開始
	my $NumberOfWorkData = 5;#仕事情報書き出しデータ数。デフォルトは5。CreateNewWorkInfo内の同名の変数の値も変える必要あり。
	my $NumberOfNewsData = 3;#最新ニュース書き出しデータ数。デフォルトは3
	my $CatchLength = 26;#最大表示表示キャッチフレーズ長
	
	#ファイル、ディレクトリ情報
	my $TemplateDirectory = "../template/toppage";
	my $UsingWorkDataDirectory = "../secret/job";
	my $UsingNewsDataDirectory = "../secret/news";
	my $TopPageDirectory = "../../home";
	my $HtmlTemplateFile = "toppage.html";
	my $WorkTableTemplateFile = "toppagetable.html";
	my $WorkRowTemplateFile = "toppagetableelement.html";
	my $NewsTableTemplateFile = "toppagetable2.html";
	my $NewsRowTemplateFile = "toppagetableelement2.html";
	my $WorkDataFileName = "newworkinfo.csv";
	my $NewsDataFileName = "newsinfo.csv";
	my $OutputFileName = "index.html";
	
	#各種設定終了
	
	#更新用情報生成開始
	my $OutputHtml = "";
	
	my $TempWorkRowSrc = "";
	my $TempWorkRow = "";
	my $OutputWorkRowSrc = "";
	my $OutputWorkRow = "";
	my $OutputWorkTable = "";
	
	my @Catch = ();
	my @DetailNo = ();
	
	my $TempNewsRowSrc = "";
	my $TempNewsRow = "";
	my $OutputNewsRowSrc = "";
	my $OutputNewsRow = "";
	my $OutputNewsTable = "";
	
	my @NewsTopic = ();
	my @NewsDetail = ();
	
	#仕事データ読み込み
	open WORKINFORMATION, "$UsingWorkDataDirectory/$WorkDataFileName";
	while (<WORKINFORMATION>){
		my @TempOutputInfo = split(/,/,$_);
		push (@Catch,"$TempOutputInfo[2]");
		push (@DetailNo,"$TempOutputInfo[14]");
	}
	close WORKINFORMATION;
	
	#仕事情報の行要素の整形
	open WORKROWDATA, "$TemplateDirectory/$WorkRowTemplateFile";
	while (<WORKROWDATA>){
		$TempWorkRowSrc .= $_;
	}
	close WORKROWDATA;
	
	for (my $count = 0;$count < $NumberOfWorkData;$count++){
		$OutputWorkRowSrc = "$TempWorkRowSrc";
		my $TempCatchData = shift @Catch;
		my $TempDetailNoData = shift @DetailNo;
		$OutputWorkRowSrc =~ s/__CATCH__/$TempCatchData/;
		$OutputWorkRowSrc =~ s/__DETAIL__NO__/$TempDetailNoData/g;
		$OutputWorkRow .= $OutputWorkRowSrc;
	}
	
	#仕事情報のテーブルの整形
	open WORKTABLEDATA, "$TemplateDirectory/$WorkTableTemplateFile";
	while (<WORKTABLEDATA>){
		$OutputWorkTable .= $_;
	}
	close WORKTABLEDATA;
	$OutputWorkTable =~ s/__TABLE__ELEMENT__/$OutputWorkRow/;
	
	
	#ニュースデータ読み込み
	open NEWSINFORMATION, "$UsingNewsDataDirectory/$NewsDataFileName";
	while (<NEWSINFORMATION>){
		my @TempOutputInfo = split(/,/,$_);
		push (@NewsTopic,"$TempOutputInfo[2]");
		push (@NewsDetail,"$TempOutputInfo[0]");
	}
	close NEWSINFORMATION;
	
	#ニュースの行要素の整形
	open NEWSROWDATA, "$TemplateDirectory/$NewsRowTemplateFile";
	while (<NEWSROWDATA>){
		$TempNewsRowSrc .= $_;
	}
	close NEWSROWDATA;
	
	for (my $count = 0;$count < $NumberOfNewsData;$count++){
		$OutputNewsRowSrc = "$TempNewsRowSrc";
		my $TempCatchData = shift @NewsTopic;
		my $TempDetailNoData = shift @NewsDetail;
		$OutputNewsRowSrc =~ s/__NEWS__MESSAGE__/$TempCatchData/;
		$OutputNewsRowSrc =~ s/__DETAIL__FILE__/$TempDetailNoData/g;
		$OutputNewsRow .= $OutputNewsRowSrc;
	}
	
	#ニュースのテーブルの整形
	open NEWSTABLEDATA, "$TemplateDirectory/$NewsTableTemplateFile";
	while (<NEWSTABLEDATA>){
		$OutputNewsTable .= $_;
	}
	close NEWSTABLEDATA;
	$OutputNewsTable =~ s/__TABLE__ELEMENT__/$OutputNewsRow/;
	
	
	#最終整形
	open FINAL, "$TemplateDirectory/$HtmlTemplateFile";
	while (<FINAL>){
		$OutputHtml .= $_;
	}
	close FINAL;
	
	$OutputHtml =~ s/__NEW__WORK__INFORMATION__/$OutputWorkTable/;
	$OutputHtml =~ s/__TOPPAGE__NEWS__/$OutputNewsTable/;
	
	#トップページ書き換え
	open OUTPUT, ">$TopPageDirectory/$OutputFileName";
	print OUTPUT ("$OutputHtml");
	close OUTPUT;
	

}