<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php
  $url = $_SERVER['REQUEST_URI'];
  $url2 = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<!doctype html>
<html>
<head>

<?php if((strstr($url2,'untitled.blue')==true)): ?>
<?php else: ?>
<script async src="https://s.yimg.jp/images/listing/tool/cv/ytag.js"></script>
<script>
window.yjDataLayer = window.yjDataLayer || [];
function ytag() { yjDataLayer.push(arguments); }
ytag({"type":"ycl_cookie"});
ytag({"type":"ycl_cookie_extended"});
</script>
<?php endif; ?>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<!--<meta name="format-detection" content="telephone=no">-->
<title>ISSコーポレート</title>
<!--テストサイトでは計測しない-->
<?php if((strstr($url2,'untitled.blue')==true)): ?>
<?php else: ?>
<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MFKZKFX');
</script>
<!-- End Google Tag Manager -->
<script>
var _bownow_trace_id_ = "UTC_59925d54ddc63";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>
<?php endif; ?>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/reset.css">
<!--english-->
<?php if(is_page('en') || is_parent_slug() === 'en' || strstr($url,'/en/')==true): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/common_en.css">
<?php else: ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/common.css">
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/jquery.bxslider.css">
<!-- ▲共通CSS▲ -->
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon.ico">
<!-- ▼個別CSS▼ -->
<!--ISSについて-->
<?php if(is_page('about') || is_parent_slug() === 'about' || is_parent_slug() === 'interview'): ?>
    <?php if(strstr($url,'/en/')==true): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/about.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/about/about.css">
    <?php endif; ?>
<!--通訳-->
<?php elseif(is_page('tsuyaku') || is_parent_slug() === 'tsuyaku'): ?>
    <?php if(strstr($url,'/en/')==true): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/interpretation.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/tsuyaku/interpretation.css">
    <?php endif; ?>
<!--コンベンション-->
<?php elseif(is_page('kokusai') || is_parent_slug() === 'kokusai' || is_parent_slug() === 'detail'): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/convention.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/kokusai/convention.css">
    <?php endif; ?>
<!--人材サービス-->
<?php elseif(is_page('personnel') || is_parent_slug() === 'personnel'): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/service.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/service/service.css">
    <?php endif; ?>
<!--通訳者・翻訳者養成スクール-->
<?php elseif(is_page('training') || is_parent_slug() === 'training'): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/training.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/training/training.css">
    <?php endif; ?>
<!--法人様向け語学研修-->
<?php elseif(is_page('language') || is_parent_slug() === 'language'): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/training.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/training/training.css">
    <?php endif; ?>
<!--翻訳-->
<?php elseif(is_page('honyaku')): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/honyaku.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/honyaku/honyaku.css">
    <?php endif; ?>
<!--お問い合わせ-->
<?php elseif(is_page('inquiry')):?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/inquiry.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/inquiry/inquiry.css">
    <?php endif; ?>
<!--入力フォーム-->
<?php elseif(is_page('form') ||is_page('form_complete') || is_parent_slug() === 'inquiry' || is_page('complete')): ?>
    <?php if(is_parent_slug() === 'inquiry' && is_page('form')): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/inquiry/inquiry2.css">
    <style type="text/css">
        .g-recaptcha { margin: 20px 0 15px; }
        .g-recaptcha > div {margin: 0 auto;}
    </style>
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/inquiry/inquiry.css">
    <style type="text/css">
        .g-recaptcha { margin: 20px 0 15px; }
        .g-recaptcha > div {margin: 0 auto;}
    </style>
<!--アクセスマップ-->
<?php elseif(is_page('access')): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/access/access.css">
<!--ニュース-->
<?php elseif(is_archive('news') || is_single()): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/news/news.css">
<!--サイトポリシー-->
<?php elseif(is_page('site')): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/site/site.css">
<!--個人情報保護方針-->
<?php elseif(is_page('policy')): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/policy/policy.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/policy/policy.css">
    <?php endif; ?>
<!--環境方針-->
<?php elseif(is_page('kankyo')): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/kankyo/kankyo.css">
<!--通訳者・翻訳者の登録について-->
<?php elseif(is_page('registration')): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/recruit/recruit.css">
<!--フリーランス通訳者の登録について-->
<?php elseif(is_page('interpreters') || is_page('staff')): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/recruit/recruit2.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/recruit/recruit.css">
<!--サイトマップ-->
<?php elseif(is_page('sitemap')): ?>
    <?php if(is_parent_slug() === 'en'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/sitemap.css">
    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/sitemap/sitemap.css">
    <?php endif; ?>

<!--英語関連ページかどうか-->
<?php elseif(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/english/english.css">
<?php else: ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/top.css">
<?php endif; ?>
<!--採用情報-->
<?php if(is_page('recruit') || is_parent_slug() === 'recruit' || is_parent_slug() === 'career' || is_parent_slug() === 'interview'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/recruit/recruit.css">
<?php endif; ?>
<!-- ▲個別CSS▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/script.js"></script>
<!-- ▲共通JS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/jquery.matchHeight.js"></script>
<!--コンベンション-->
<?php if(is_page('kokusai') || is_parent_slug() === 'kokusai' || is_parent_slug() === 'conference'): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/kokusai/convention.js"></script>
<!--ISSについて-->
<?php elseif(is_page('about') || is_parent_slug() === 'about' || is_parent_slug() === 'interview'): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/about/about.js"></script>
<!--通訳-->
<?php elseif(is_page('tsuyaku') || is_parent_slug() === 'tsuyaku'): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/tsuyaku/tsuyaku.js"></script>
<!--採用情報-->
<?php elseif(is_page('recruit') || is_parent_slug() === 'recruit' || is_page('registration')): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/recruit/recruit.js"></script>
<!--入力フォーム関係-->
<?php elseif(is_page('inquiry') || is_page('form')): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/inquiry/inquiry.js"></script>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<?php else: ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/bxslider.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/top.js"></script>
<?php endif; ?>
<!-- ▲個別JS▲ -->
<?php wp_head(); ?>
</head>
<body>
<?php if((strstr($url2,'untitled.blue')==true)): ?>
<?php else: ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFKZKFX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php endif; ?>
<header id="header">
    <div class="l-headerMenu">
        <div class="l-headerMenu-inner">
            <!--PCヘッダー左-->
            <div class="l-headerMenu-left">
                <div class="l-headerMenu-left-table">
                    <div class="l-headerMenu-left-table-cell">
                        <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                        <h1><a href="<?php echo home_url();?>/en/"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_header_logo_en_pc.png" alt="ISS,INC."></a></h1>
                        <?php else: ?>
                        <h1><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_header_logo_pc.png" alt="株式会社 アイ・エス・エス 通訳・翻訳・国際会議・人材サービス"></a></h1>
                        <?php endif; ?>
                    </div>
                    <div class="l-headerMenu-left-table-cell">
                        <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                        <a href="<?php echo home_url();?>/en/inquiry"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/english/btn_inquiry.png" alt="Contact"></a>
                        <?php else: ?>
                        <a href="<?php echo home_url();?>/inquiry"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_inquiry.png" alt="お問い合わせ"></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!--PCヘッダー右-->
            <div class="l-headerMenu-right">
                <p class="l-headerMenu-right-top pconly">
                    <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                    <a class="p-inquiry" href="<?php echo home_url();?>/en/inquiry"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/english/btn_inquiry_pc.png" alt="Contact"></a>
                    <?php else: ?>
                    <a class="p-inquiry" href="<?php echo home_url();?>/inquiry"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_inquiry_pc.png" alt="お問い合わせ"></a>
                    <?php endif; ?>
                </p>
                <p class="l-headerMenu-right-bottom pconly">
                    <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                    <a class="p-japanese" href="<?php echo home_url();?>">JAPANESE</a>
                    <?php else: ?>
                    <a class="p-access" href="<?php echo home_url();?>/access/">アクセスマップ</a>
                    <a class="p-recruit" href="<?php echo home_url();?>/recruit/">採用情報</a>
                    <a class="p-english" href="<?php echo home_url();?>/en/">ENGLISH</a>
                    <?php endif; ?>
                </p>
                <!--SPハンバーガーメニュー-->
                <div id="toggle_block" class="sponly0">

                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span>
                        </div>
                    </div><!--/#toggle-->
                    <nav>
                        <div id="global-nav-area">
                            <ul id="global-nav">
                                <!--英語メニュー-->
                                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                                <li class="title"><p><a href="<?php echo home_url();?>/en/">HOME</a></p></li>
                                <li class="title js-underlayer"><p>About ISS</p><p class="js-menu">＋</p></li>
                                <ul class="js-global-nav-detail">
                                    <li><a href="<?php echo home_url();?>/en/about/">About ISS</a></li>
                                    <li><a href="<?php echo home_url();?>/en/about/message">Top Message</a></li>
                                    <li><a href="<?php echo home_url();?>/en/about/company">Corporate Profile</a></li>
                                    <li><a href="<?php echo home_url();?>/en/about/history">History</a></li>
                                    <li><a href="<?php echo home_url();?>/en/about/mission">ISS Corporate Philosophy</a></li>
                                </ul>
                                <li class="title"><p><a href="<?php echo home_url();?>/en/tsuyaku/">Interpretation Services</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/en/kokusai/">Convention Services</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/en/personnel/">Personnel Services</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/en/training/">Interpreter and TranslatorTraining</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/en/inquiry/">Contact</a></p></li>
                                <!--日本語メニュー-->
                                <?php else: ?>
                                <li class="title"><p><a href="<?php echo home_url();?>">HOME</a></p></li>
                                <li class="title js-underlayer"><p>ISSについて</p><p class="js-menu">＋</p></li>
                                <ul class="js-global-nav-detail">
                                    <li><a href="<?php echo home_url();?>/about/">ISSについて</a></li>
                                    <li><a href="<?php echo home_url();?>/about/message">トップメッセージ</a></li>
                                    <li><a href="<?php echo home_url();?>/about/company">会社概要</a></li>
                                    <li><a href="<?php echo home_url();?>/about/pdf_dl">会社案内ダウンロード</a></li>
                                    <li><a href="<?php echo home_url();?>/about/history">沿革</a></li>
                                    <li><a href="<?php echo home_url();?>/about/mission">企業理念</a></li>
                                    <li><a href="<?php echo home_url();?>/about/csr">CSR</a></li>
                                    <!-- <li><a href="<?php echo home_url();?>/about/interview">創業50周年記念インタビュー</a></li> -->
                                </ul>
                                <li class="title js-underlayer"><p>通訳</p><p class="js-menu">＋</p></li>
                                <ul class="js-global-nav-detail">
                                    <li><a href="<?php echo home_url();?>/tsuyaku/">サービス概要</a></li>
                                    <!-- <li><a href="<?php echo home_url();?>/tsuyaku/audit">査察・監査における通訳</a></li> -->
                                    <li><a href="<?php echo home_url();?>/tsuyaku/online">オンライン通訳サービス</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/online_s">オンライン通訳 導入事例</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/audit">査察・監査における通訳</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/online_r">ISS遠隔通訳専用ルーム</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/flow">ご利用の流れ</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/system">通訳形態と通訳機材</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/service">対応業種と対応言語</a></li>
                                    <li><a href="<?php echo home_url();?>/tsuyaku/faq">よくあるご質問</a></li>
                                </ul>
                                <li class="title js-underlayer">
                                    <p>コンベンション</p>
                                    <p class="js-menu">＋</p>
                                </li>
                                <ul class="js-global-nav-detail">
                                    <li><a href="<?php echo home_url();?>/kokusai/">サービス概要</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/online">MICEオンライン開催・LIVE開催支援</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/detail">国際会議および学術会議企画・運営</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/detail/flow">開催・運営のトータルサポート</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/detail/system">会議運営体制</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/detail/works">国際会議と学術会議の主な実績紹介</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/corporate">企業ミーティング・イベント</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/event">式典・展示会・見本市・博覧会・スポーツイベント・その他</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/mice">MICEプロモーション・コンサルティング</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/organization">学会・団体事務局受託事業</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/facility">施設運営管理、施設運営サポート業務</a></li>
                                    <li><a href="<?php echo home_url();?>/kokusai/list">開催予定と実績一覧</a></li>
                                </ul>
                                <li class="title"><p><a href="<?php echo home_url();?>/personnel/">人材サービス</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/honyaku/">翻訳</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/recruit/">採用情報</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/access/">アクセスマップ</a></p></li>
                                <li class="title"><p><a href="<?php echo home_url();?>/inquiry/">お問い合わせ</a></p></li>
                                <?php endif; ?>

                            </ul>
                        </div>
                    </nav>
                </div><!--/#toggle_block-->
            </div>
        </div>
    </div>
    <div class="l-globalMenu">
        <div class="l-globalMenu-inner">
            <div>
                <!--グローバルメニュー-HOME-英語-->
                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                <a href="<?php echo home_url();?>/en/">HOME</a>
                <!--グローバルメニュー-HOME-日本語-->
                <?php else: ?>
                <a href="<?php echo home_url();?>">HOME</a>
                <?php endif; ?>
            </div>
            <div>
                <!--グローバルメニュー-ISSについて-英語-->
                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/en/about/">About ISS</a>
                <div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/en/about/">About ISS</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/en/about/message">Top Message</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/en/about/company">Corporate Profile</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/en/about/history">History</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/en/about/mission">ISS Corporate Philosophy</a></td></tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!--グローバルメニュー-ISSについて-日本語-->
                <?php else: ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/about/">ISSについて</a>
                <div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/about/">ISSについて</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/message">トップメッセージ</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/company">会社概要</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/pdf_dl">会社案内ダウンロード</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/history">沿革</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/mission">企業理念</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/about/csr">CSR</a></td></tr>
                                <!-- <tr><td><a href="<?php echo home_url();?>/about/interview">創業50周年記念インタビュー</a></td></tr> -->
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div>
                <!--グローバルメニュー-通訳-英語-->
                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/en/tsuyaku/">Interpretation Services</a>
                <!--グローバルメニュー-通訳-日本語-->
                <?php else: ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/tsuyaku/">通訳</a>
                <div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/">サービス概要</a></td></tr>
                                <!-- <tr><td><a href="<?php echo home_url();?>/tsuyaku/audit">査察・監査における通訳</a></td></tr> -->
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/online">オンライン通訳サービス</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/online_s">オンライン通訳 導入事例</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/online_r">ISS遠隔通訳専用ルーム</a></td></tr>                                
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/flow">ご利用の流れ</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/system">通訳形態と通訳機材</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/service">対応業種と対応言語</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/tsuyaku/faq">よくあるご質問</a></td></tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div>
                <!--グローバルメニュー-コンベンション-英語-->
                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/en/kokusai/">Convention Services</a>
                <!--グローバルメニュー-コンベンション-日本語-->
                <?php else: ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/kokusai/">コンベンション</a>
                <div class="header_courses_menu">
                    <div class="header_courses_list">
                        <div class="header_courses_list01 p-long">
                            <table>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/">サービス概要</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/online">MICEオンライン開催・LIVE開催支援</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/detail">国際会議および学術会議企画・運営</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/corporate">企業ミーティング・イベント</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/event">式典・展示会・見本市・博覧会・スポーツイベント・その他</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/mice">MICEプロモーション・コンサルティング</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/organization">学会・団体事務局受託事業</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/facility">施設運営管理、施設運営サポート業務</a></td></tr>
                                <tr><td><a href="<?php echo home_url();?>/kokusai/list">開催予定と実績一覧</a></td></tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div>
                <!--グローバルメニュー-人材サービス-英語-->
                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/en/personnel/">Personnel Services</a>
                <!--グローバルメニュー-人材サービス-日本語-->
                <?php else: ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/personnel/">人材サービス</a>
                <?php endif; ?>
            </div>
            <div>
                <!--グローバルメニュー-通訳者・翻訳者養成スクール-英語-->
                <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/en/training/">Interpreter and Translator<br>Training</a>
                <!--グローバルメニュー-通訳者・翻訳者養成スクール-日本語-->
                <?php else: ?>
                <a class="js-globalMenu" href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>
