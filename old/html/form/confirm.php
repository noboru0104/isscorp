<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="keywords" content="ISS,アイ・エス・エス,お問い合わせ" />
<title>確認 | ISS総合お問い合わせフォーム</title>
<link href="common/form_style.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="/_common/js/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="/_common/js/ajaxcodedisplay-or.js"></script>
<script type="text/javascript" src="/_common/js/DropDownMenu.js"></script>
</head>
<body>

<?php
$countflag	== "0";
 ?>

<!-- Header Begin -->
<div id="header">
<div class="contentform"><a href="../index.html"><img src="../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../index.html"><img src="../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣、法人研修／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi"></div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<div id="formcont">
<h2><img src="images/ind_h2.jpg" alt="お問い合わせフォーム" width="700" height="78" /></h2><form method="POST" action="contact.php">

<?php 
if(hs($p['checkbox1']) != "") echo "<input type=\"hidden\" name=\"checkbox1\" value=\"通訳\" />\n<input type=\"hidden\" name=\"checkbox1mail\" value=\"ask_conv@issjp.com\" />"; 

if(hs($p['checkbox2']) != "") echo "<input type=\"hidden\" name=\"checkbox2\" value=\"国際会議企画・運営\" />\n<input type=\"hidden\" name=\"checkbox2mail\" value=\"ask_conference@issjp.com\" />";

if(hs($p['checkbox3']) != "") echo "<input type=\"hidden\" name=\"checkbox3\" value=\"翻訳\" />\n<input type=\"hidden\" name=\"checkbox3mail\" value=\"ask_honyaku@issjp.com\" />";

if(hs($p['checkbox4']) != "") echo "<input type=\"hidden\" name=\"checkbox4\" value=\"人材派遣・紹介予定派遣\" />\n<input type=\"hidden\" name=\"checkbox4mail\" value=\"haken@issjp.com\" />"; 

if(hs($p['checkbox5']) != "") echo "<input type=\"hidden\" name=\"checkbox5\" value=\"法人向け語学研修\" />\n<input type=\"hidden\" name=\"checkbox5mail\" value=\"info@issjp.com\" />"; 

if(hs($p['checkbox6']) != "") echo "<input type=\"hidden\" name=\"checkbox6\" value=\"その他\" />\n<input type=\"hidden\" name=\"checkbox6mail\" value=\"info@issjp.com\" />";

 ?>
<input type="hidden" name="toiawase" value="<?php echo hs($p['toiawase']); ?>" />
<input type="hidden" name="company" value="<?php echo hs($p['company']); ?>" />
<input type="hidden" name="company2" value="<?php echo hs($p['company2']); ?>" />
<input type="hidden" name="depart" value="<?php echo hs($p['depart']); ?>" />
<input type="hidden" name="gyoukai" value="<?php echo hs($p['gyoukai']); ?>" />
<input type="hidden" name="name" value="<?php echo hs($p['name']); ?>" />
<input type="hidden" name="name2" value="<?php echo hs($p['name2']); ?>" />
<input type="hidden" name="zip21" value="<?php echo hs($p['zip21']); ?>" />
<input type="hidden" name="zip22" value="<?php echo hs($p['zip22']); ?>" />
<input type="hidden" name="pref21" value="<?php echo hs($p['pref21']); ?>" />
<input type="hidden" name="addr21" value="<?php echo hs($p['addr21']); ?>" />
<input type="hidden" name="strt21" value="<?php echo hs($p['strt21']); ?>" />
<input type="hidden" name="mail" value="<?php echo hs($p['mail']); ?>" />
<input type="hidden" name="tel" value="<?php echo hs($p['tel']); ?>" />
<input type="hidden" name="kikkake" value="<?php echo hs($p['kikkake']); ?>" />
<input type="hidden" name="sonota" value="<?php echo hs($p['sonota']); ?>" />
<input type="hidden" name="countflag" value="1" />


<table id="confirm">
  <tr>
    <td colspan="2" class="t1">入力内容確認</td>
  </tr>

<tr>
<th width="200">お問い合わせ先</th>
<td class="hisu">
<?php if(hs($p['checkbox1']) != "") echo hs($p['checkbox1'])."<br>"; ?>
<?php if(hs($p['checkbox2']) != "") echo hs($p['checkbox2'])."<br>"; ?>
<?php if(hs($p['checkbox3']) != "") echo hs($p['checkbox3'])."<br>"; ?>
<?php if(hs($p['checkbox4']) != "") echo hs($p['checkbox4'])."<br>"; ?>
<?php if(hs($p['checkbox5']) != "") echo hs($p['checkbox5'])."<br>"; ?>
<?php if(hs($p['checkbox6']) != "") echo hs($p['checkbox6'])."<br>"; ?></td>
</tr>

<tr>
<th>お問い合わせ内容</th>
<td class="hisu">
<?php echo hs($p['toiawase']); ?></td>
</tr>

<tr>
<th>会社（団体）名</th>
<td class="hisu">
<?php echo hs($p['company']); ?></td>
</tr>

<tr>
<th>会社（団体）名（フリガナ）</th>
<td class="hisu">
<?php echo hs($p['company2']); ?></td>
</tr>

<tr>
<th>部署名</th>
<td class="hisu">
<?php echo hs($p['depart']); ?></td>
</tr>

<tr>
<th>業界</th>
<td class="hisu">
<?php echo hs($p['gyoukai']); ?></td>
</tr>

<tr>
<th>お名前</th>
<td class="hisu"><?php echo hs($p['name']); ?></td>
</tr>

<tr>
<th>お名前（フリガナ）</th>
<td class="hisu"><?php echo hs($p['name2']); ?></td>
</tr>

<tr>
<th>電話番号</th>
<td class="hisu"><?php echo hs($p['tel']); ?></td>
</tr>

<tr>
<th>e-mai</th>
<td class="hisu"><?php echo hs($p['mail']); ?></td>
</tr>

<tr>
<th>ご住所</th>
<td class="hisu">〒<?php echo hs($p['zip21']); ?>-<?php echo hs($p['zip22']); ?><br />
<?php echo hs($p['pref21']);
echo hs($p['addr21']); 
echo hs($p['strt21']); 
?></td>
</tr>

<tr>
<th>当社をどのように<br>
お知りになりましたか</th>
<td class="hisu"><?php echo hs($p['kikkake']); ?>  <?php if(hs($p['kikkake']) == "その他") echo hs($p['sonota']); ?></td>
</tr>
<tr>
<td class="btnForm" colspan="2"><input type="submit" name="btn_send" value="　　送 信　　" /></td>
</tr>
</table>

</form>
</div>
    <!-- コンテンツ -->

	
<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../common/images/pixel_trans.gif" width="816" height="15" alt="" class="spacer" />
<div class="menuList">
<ul>
  <li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>
</div>
</div>
</div>
<!-- Footer End -->
	
    <!-- サイドバー
    <a href="/_common/inc/news.html" class="codeexample"></a>
    サイドバー -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874981274;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Target List -->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'PEOWJV0NTD';
var yahoo_retargeting_label = '';
var yahoo_retargeting_page_type = '';
var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
</body></html>
