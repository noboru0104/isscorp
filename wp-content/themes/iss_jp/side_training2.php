<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>通訳者・翻訳者養成</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="<?php echo home_url();?>/training">通訳者・翻訳者養成スクール</a></p>
        <p><a href="<?php echo home_url();?>/training/language">法人向け語学研修</a></p>
    </div>
</div>