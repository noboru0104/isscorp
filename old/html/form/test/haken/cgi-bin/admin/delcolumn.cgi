#!/usr/bin/perl

#各種設定開始
#ディレクトリ情報ここから
my $TextFileDir = "../secret/trend";

my $TemplateDir = "../template/trend";
my $LiblaryDir = "../perl_lib";
my $ImageDir = "../../home/trend/image";
my $ImageDir2 = "/trend/image";
#ディレクトリ情報ここまで

#windows上でのチェック用パス
#require 'cgi-lib.pl';
#require 'jcode.pl';

#unix上への実装時のパス
require '../perl_lib/jcode.pl';
require '../perl_lib/cgi-lib.pl';

#テンプレートファイルリストここから
my $CompHtml = "delcomplete.html";
my $DeleteCheck = "delcolumn.html";
my $Failure = "failure.html";
#テンプレートファイルリストここまで

#各種設定終了

print ("Content-type:text/html\n\n");

#削除対象の情報取得
my $DeleteFaqFile = $ENV{'QUERY_STRING'};

my @Tmp = ();
my $Question = "";
my $Answer = "";
my $Respondent = "";
my $ImageFile = "";

open FAQFILE, "$TextFileDir/$DeleteFaqFile" or die "Error!!";
while (<FAQFILE>){
	@Tmp = <FAQFILE>;
}
close FAQFILE;

$Question = $Tmp[1];
$Answer = $Tmp[2];
$Respondent = $Tmp[3];
$ImageFile = $Tmp[4];

$ImageFile =~ s/\r\n//;
$ImageFile =~ s/\n//;

my $ImageTag = '<img src="' . "$ImageDir2/$ImageFile" .'">';

#フォームからデータ取得
&ReadParse(*in);
my $DeleteMode = $in{'DeleteMode'};#動作モードに関する情報取得（"1"で削除動作開始）


#以下、動作モードに応じた処理

#削除動作
if($DeleteMode == 1){
	unlink ("$TextFileDir/$DeleteFaqFile");
	
	if(length($ImageFile) == 0){}
	else{
		unlink ("$ImageDir/$ImageFile");
	}
	
	my $OutputHtml = "";
	open COMPLETE, "$TemplateDir/$CompHtml";
	while (<COMPLETE>){
		$OutputHtml .= $_;
	}
	close COMPLETE;
	
	print ("$OutputHtml");
}
elsif(length ($DeleteFaqFile) == 0){
	print ('<html><head>');
	print ('<meta http-equiv="refresh" content="0;URL=listfaq.cgi">');
	print ('</head><body></body></html>');
}
else{
	my $ConfirmForm = "";
	open CONFIRM, "$TemplateDir/$DeleteCheck";
	while (<CONFIRM>){
		$ConfirmForm .= $_;
	}
	close CONFIRM;
	
	$ConfirmForm =~ s/__QUESTION__TEXT__/$Question/;
	$ConfirmForm =~ s/__ANSWER__TEXT__/$Answer/;
	$ConfirmForm =~ s/__RESPONDENT__NAME__/$Respondent/;
	$ConfirmForm =~ s/__COLUMN__IMAGE__/$ImageTag/;
	$ConfirmForm =~ s/__DELETE__FILE__NAME__/$DeleteFaqFile/;
	
	print ("$ConfirmForm");

}