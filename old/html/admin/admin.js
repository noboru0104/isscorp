var canFormat = 0;
if (document.selection)
    canFormat = 1;
var ua = navigator.userAgent;
if (ua.indexOf('Gecko') >= 0 && ua.indexOf('Safari') < 0)
    canFormat = 1;

function getSelection (e) {
    if (document.selection)
        return document.selection.createRange().text;
    else {
        var length = e.textLength;
        var start = e.selectionStart;
        var end = e.selectionEnd;
        if (end == 1 || end == 2) end = length;
        return e.value.substring(start, end);
    }
}

function setSelection (e, v) {
    if (document.selection)
        document.selection.createRange().text = v;
    else {
        var length = e.textLength;
        var start = e.selectionStart;
        var end = e.selectionEnd;
        if (end == 1 || end == 2) end = length;
        e.value = e.value.substring(0, start) + v + e.value.substr(end, length);
    }
}

function formatStr (e, v) {
    if (!canFormat) return;
    var str = getSelection(e);
    if (!str) return;
    setSelection(e, '<' + v + '>' + str + '</' + v + '>');
    return false;
}

function formatStr2(e) {
	if (!canFormat) return;
	var str = getSelection(e);
	if (!str)  return;
	setSelection(e, '<h5><strong>' + str + '</strong></h5>');
	return false;
}

function insertLink (e, isMail) {
    if (!canFormat) return;
    var str = getSelection(e);
    if (!str) return;
    var my_link = isMail ? prompt('Enter email address:') : prompt('Enter URL:', 'http://');
    if (isMail) my_link = 'mailto:' + my_link;
    if (my_link != null)
        setSelection(e, '<a href="' + my_link + '">' + str + '</a>');
    return false;
}

function doCheckAll (f, v) {
    var e = f.id;
    if (e.value)
        e.checked = v;
    else
        for (i=0; i<e.length; i++) 
            e[i].checked = v;
}
