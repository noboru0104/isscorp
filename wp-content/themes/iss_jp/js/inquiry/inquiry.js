jQuery(function( $ ) {
    jQuery( '#zip22' ).keyup( function() {
		
        AjaxZip3.zip2addr('zip21','zip22','addr21','addr21');
    } );
	jQuery( '#zip32' ).keyup( function() {
		//alert('a');
        AjaxZip3.zip2addr('zip31','zip32','pref31','addr31');
    } );
	
	$( '#mw_wp_form_mw-wp-form-418 select option[value=""]' )
	.html( '選択してください' );
	$( '#mw_wp_form_mw-wp-form-147 select option[value=""]' )
	.html( '選択してください' );
	
	$( '#mw_wp_form_mw-wp-form-435 select option[value=""]' )
	.html( '---' );
	$( '#mw_wp_form_mw-wp-form-148 select option[value=""]' )
	.html( '---' );
	
	$("select.p-renraku").change(function(){
		if($("select.p-renraku").val()==="その他"){
			$("#renrakusonota").removeAttr("disabled");
		}else{
			$("#renrakusonota").attr("disabled","disabled");			
		}
	});
	
	jQuery("#zip31").change(function () {
		if ($("#zip31").val() === '000'){
			if ($("#zip32").val() === '0000'){
				$("#pref31").val("海外");
				$("#addr31").val("");
				$("#banchi").val("");
			}
		}
		
	}).change();
	
	jQuery("#zip32").change(function () {
		if ($("#zip31").val() === '000'){
			if ($("#zip32").val() === '0000'){
				$("#pref31").val("海外");
				$("#addr31").val("");
				$("#banchi").val("");
			}
		}
		
	}).change();
    $('.l-recruitBlock04 td span.error').each(function () {
		if(!$(this).closest('td .alert').length){
$(this).closest('td').addClass('alert');
		}
    });
} );

$(function(){
	"use strict";	
	$(".registerBtn").attr("disabled", "disabled");
	$(".registerBtn").addClass("p-disabled");
	$(".l-recruitBlock05").hide();
	setDisabled();
	$(document).on("click", "[id='consent-1']", function(e) {
		setDisabled();
	});
	
	$(window).resize(function () {
		
		
		
	});
	
	$(window).load(function () {
		if($("form").length){
			$("form").attr("novalidate","true");
		}
		if($("#tel").length){
			$('#tel').get(0).type = 'tel';
		}
		if($("#keitai").length){
			$('#keitai').get(0).type = 'tel';
		}
		if($("#tel1").length){
			$('#tel1').get(0).type = 'tel';
		}
		if($("#tel2").length){
			$('#tel2').get(0).type = 'tel';
		}
		if($("#tel3").length){
			$('#tel3').get(0).type = 'tel';
		}
		if($("#fax1").length){
			$('#fax1').get(0).type = 'tel';
		}
		if($("#fax2").length){
			$('#fax2').get(0).type = 'tel';
		}
		if($("#fax3").length){
			$('#fax3').get(0).type = 'tel';
		}
		if($("#mail").length){
			$('#mail').get(0).type = 'url';
		}
		if($("#mail2").length){
			$('#mail2').get(0).type = 'url';
		}
		if($("#zip21").length){
			$('#zip21').get(0).type = 'tel';
		}
		if($("#zip22").length){
			$('#zip22').get(0).type = 'tel';
		}
		if($("#zip31").length){
			$('#zip31').get(0).type = 'tel';
		}
		if($("#zip32").length){
			$('#zip32').get(0).type = 'tel';
		}
		
		
		
	});
	
});

function setDisabled() {
	"use strict";
	if($("[id='consent-1']").length){
        if ($("[id='consent-1']").prop('checked')) {
			//チェックONの場合		
			//ボタンを押せるようにする
			$(".l-recruitBlock05").show();
			//$(".registerBtn").removeAttr("disabled");
			//$(".registerBtn").removeClass("p-disabled");
			if($("#renrakusonota").length){
				if($("select.p-renraku").val()==="その他"){
					$("#renrakusonota").removeAttr("disabled");
				}else{
					$("#renrakusonota").attr("disabled","disabled");			
				}
			}
		}
		//else if(!$("[id='consent-1']").prop('checked')) {
	//		//チェックOFFの場合
	//		$(":input").attr("disabled", "disabled");//同意するチェックは対象外
	//		$("[id='consent-1']").removeAttr("disabled");
	//		$(".registerBtn").addClass("p-disabled");
	//	}
		else {
			//チェックOFFの場合		
			//ボタンを押せないようにする
			$("[id='consent-1']").removeAttr("disabled");
			$(".l-recruitBlock05").hide();
			//$(".registerBtn").attr("disabled", "disabled");
			//$(".registerBtn").addClass("p-disabled");
			if($("#renrakusonota").length){
				if($("select.p-renraku").val()==="その他"){
					$("#renrakusonota").removeAttr("disabled");
				}else{
					$("#renrakusonota").attr("disabled","disabled");			
				}
			}
		}
	} else {
		if($(".mw_wp_form_confirm").length){
			//ボタンを押せるようにする
			$(".l-recruitBlock05").show();
			$(".registerBtn").removeAttr("disabled");
			$(".registerBtn").removeClass("p-disabled");
			if($("#renrakusonota").length){
				if($("select.p-renraku").val()==="その他"){
					$("#renrakusonota").removeAttr("disabled");
				}else{
					$("#renrakusonota").attr("disabled","disabled");			
				}
			}
		} else {
			
			//ボタンを押せないようにする
			$("[id='consent-1']").removeAttr("disabled");
			$(".l-recruitBlock05").hide();
			//$(".registerBtn").attr("disabled", "disabled");
			//$(".registerBtn").addClass("p-disabled");
			if($("#renrakusonota").length){
				if($("select.p-renraku").val()==="その他"){
					$("#renrakusonota").removeAttr("disabled");
				}else{
					$("#renrakusonota").attr("disabled","disabled");			
				}
			}
				
		}
	}
}

