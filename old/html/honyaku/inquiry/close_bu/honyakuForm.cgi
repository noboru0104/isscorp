#!/usr/bin/perl
############################################################
#
#    ISS �����Ԥ���Ͽ�ե�����E
#
############################################################
require './cgi-lib.pl';
require './jcode.pl';
use CGI::Carp qw(fatalsToBrowser);

############################################################
#    ��āE���
############################################################

## ������᡼��E��ɥ�E�
$toAdd = 'reg_honyaku@issjp.com';

## CC�᡼��E��ɥ�E�
$ccAdd = '';

## BCC�᡼��E��ɥ�E�
$bccAdd = '';

## �����ԥ᡼��E��ɥ�E�
$fromAdd = 'trans3@issjp.com';

## �᡼��E����ȥ�E
$subjectUser = 'ISS�������ȥ饤����EؤΤ����礢�꤬�Ȥ��������ޤ�';
$subjectAdmin = '�ȥ饤����Eα��礬����ޤ��� ';

## ��ǧ����HTML
$confirmHtmlFile = './honyaku_confirm.html';

## ���顼����HTML
$errorHtmlFile = './honyaku_error.html';

## ��λ����HTML
$endHtmlFile = './honyaku_complete.html';

## sendmail�ѥ�
$sendmail = "/usr/sbin/sendmail -t -f$fromAdd";

############################################################
#    �ץ�����೫��
############################################################
&decode();
&setTime();
&checkForm();

if($g_html{'f_conf'}){
	&outHtml($confirmHtmlFile);
}elsif($g_html{'f_exe'}){
	&sendMailUser();
	&sendMailAdmin();
	&outHtml($endHtmlFile);
}

############################################################
#    �ե��������
############################################################
sub decode{
	my ($key,$value);
	
	&ReadParse();
	while(($key,$value) = each %in){
		&jcode::convert(\$key,'euc');
		&jcode::convert(\$value,'euc','','z');
		$value =~ s/&/&amp;/gi;
		$value =~ s/\"/&quot;/gi;
		$value =~ s/\'/&#39;/gi;
		$value =~ s/</&lt;/gi;
		$value =~ s/>/&gt;/gi;
		$value =~ s/&lt;br&gt;/<br>/gi;
		$value =~ s/\r\n?/\n/g;
		$value =~ s/\n/<br>/g;
		$value =~ s/\0/ /g;
		
		if($key !~ /^f_/){
			$g_hidden .= qq(<input type="hidden" name="$key" value="$value">\n);
		}
		$g_html{$key} = $value;
	}
	$g_html{'hidden'} = $g_hidden;
	
}

############################################################
#    ƁE�����
############################################################
sub setTime{
	my ($sec,$min,$hour,$day,$mon,$year,$wday) = localtime();
	
	$g_time{'sec'} = sprintf("%02d",$sec);
	$g_time{'min'} = sprintf("%02d",$min);
	$g_time{'hour'} = sprintf("%02d",$hour);
	$g_time{'day'} = sprintf("%02d",$day);
	$g_time{'mon'} = sprintf("%02d",$mon+1);
	$g_time{'year'} = sprintf("%04d",$year+1900);
	$g_time{'all'} = "$g_time{'year'}/$g_time{'mon'}/$g_time{'day'} $g_time{'hour'}:$g_time{'min'}:$g_time{'sec'}";
}

############################################################
#    ���顼�����å�
############################################################
sub checkForm{
	my $flag = 1;
	
	if($g_html{'name1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�֤�̾�������פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'name2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�֤�̾����̾�פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'kana1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�֤�̾���������������ʡפ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}elsif($g_html{'kana1'} !~ /^(\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc])+$/){
		$g_html{'error'} .= qq(<li class="mtx">�֤�̾���������������ʡפ˥������ʰʳ������Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'kana2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�֤�̾����̾���������ʡפ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}elsif($g_html{'kana2'} !~ /^(\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc])+$/){
		$g_html{'error'} .= qq(<li class="mtx">�֤�̾����̾���������ʡפ˥������ʰʳ������Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
#	if($g_html{'birth_year'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">����ǯ���E�ǯ�פ����Ϥ���EƤ��ޤ���</li>);
#		$flag = 0;
#	}
#	if($g_html{'birth_month'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">����ǯ���E����פ����Ϥ���EƤ��ޤ���</li>);
#		$flag = 0;
#	}
#	if($g_html{'birth_day'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">����ǯ���E�ƁEפ����Ϥ���EƤ��ޤ���</li>);
#		$flag = 0;
#	}
	if($g_html{'sex'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�����̡פ����򤵤�EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'email'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�֥᡼��E��ɥ�E��פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}else{
		if($g_html{'email'} !~ /^[\w\-\~\.]+@[\w\-\~].[\w\-\~\.]+$/){
			$g_html{'error'} .= qq(<li class="mtx">�֥᡼��E��ɥ�E��פη����������Ǥ���</li>);
			$flag = 0;
		}
	}
	$zip = $g_html{'zip1'} . $g_html{'zip2'};
	if($g_html{'zip1'} eq '' or $g_html{'zip2'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">��͹���ֹ�פ����Ϥ���EƤ��ޤ���</li>);
#		$flag = 0;
	}elsif($zip !~ /^\d{7}$/){
		$g_html{'error'} .= qq(<li class="mtx">��͹���ֹ�פ����������Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'pref'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">����ƻ�ܸ��פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'address1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�ַ��Զ�Į¼ �פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'address2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�����ϡפ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'tel1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">��TEL���Գ����֡פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'tel2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">��TEL��������֡פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	if($g_html{'tel3'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">��TEL����4��פ����Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	$tel = $g_html{'tel1'} . $g_html{'tel2'} . $g_html{'tel3'};
	if($tel !~ /[0-9]/){
##	if($tel !~ /^\d{10}$/){
		$g_html{'error'} .= qq(<li class="mtx">��TEL�פ����������Ϥ���EƤ��ޤ���</li>);
		$flag = 0;
	}
	$fax = $g_html{'fax1'} . $g_html{'fax2'} . $g_html{'fax3'};
	if($fax ne ''){
		if($fax !~ /^\d{10}$/){
			$g_html{'error'} .= qq(<li class="mtx">��FAX�פ����������Ϥ���EƤ��ޤ���</li>);
			$flag = 0;
		}
	}
#	if($g_html{'agree'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">�֥����ӥ������ѵ���פ˥����å������äƤ��ޤ���</li>);
#		$flag = 0;
#	}
	
	if($flag == 0){
#		$g_html{'error'} .= qq(<li class="mtx">�֥饦�����ᤁEܥ���Ǥ����Τ���������E��Ǥ����⤦��E����Ϥ��ʤ����Ƥ��������� </li>);
		&outHtml($errorHtmlFile);
	}
	
	if($g_html{'lang1'} ne ''){
		$g_html{'lang1'} .= qq(����);
	}
	if($g_html{'lang2'} ne ''){
		$g_html{'lang2'} .= qq(����);
	}
	if($g_html{'lang3'} ne ''){
		$g_html{'lang3'} .= ' ' . qq(�ʸ���E� ) . $g_html{'lang3_name'} . qq(�ˡ���);
	}
	if($g_html{'lang4'} ne ''){
		$g_html{'lang4'} .= ' ' . qq(�ʸ���E� ) . $g_html{'lang4_name'} . qq(��);
	}

#	for($i=1;$i<=4;$i++){
#		$name = 'lang' . $i;
#	 if($g_html{$name}){
#		$g_html{'lang'} .= $g_html{$name} . ' ';
#	}


	}


#}

############################################################
#    HTML����
############################################################
sub outHtml{
	my $file = shift;
	my ($html,@htmlList,$htmlAll);
	my @tmp;
	
	open(HTML,"<$file") or die();
	read(HTML,$html,-s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	foreach (@htmlList){
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_."\n";
		}else{
			$htmlAll .= $_."\n";
		}
	}
	
#	&jcode::convert(\$htmlAll,'sjis','euc');
	print "Content-type:text/html; charset=euc-jp\n\n";
	print $htmlAll;
	
	exit;
}

############################################################
#    ����ɥ桼���᡼��E���
############################################################
sub sendMailUser{
	my $mailBody;

	foreach $key(keys %g_html){
		$value = $g_html{$key};
		$value =~ s/&amp;/&/gi;
		$value =~ s/&lt;/</gi;
		$value =~ s/&gt;/>/gi;
		$g_html{$key} = $value;
	}
	
	$mailBody = <<MAIL;
To: $g_html{'email'}
From: $fromAdd
Subject: $subjectUser
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

$g_html{'name1'} $g_html{'name2'} ��

���������ȥ饤����Eˤ����礤���������ˤ��꤬�Ȥ��������ޤ��� 
���Υ᡼��Eϥ����ƥ�褁E�ưŪ����������EƤ���ޤ��� 

����������Ƥ��Ҹ��������太�Ż��Ǥ���E�EǤ���E�ǽ���Τ���E��ˤϡ� 
���Ҥ褁Eȥ饤����E�������������Ƥ��������ޤ��� 

ˁE���E��Υ᡼��E˿������꤬�ʤ���E�ˤϡ����Ѥ���E��Ǥ��� 
�������ɥ�E��ޤǤ��Τ餻���������ޤ��褦���ꤤ�������ޤ��� 
��������E��ץ��ɥ�E���trans3\@issjp.com 

����Ȥ����������ꤤ�����夲�ޤ��� 

------------------------------------- 
������ҥ��������������� 
�Ķ�����������������E��� 
TEL: 03-6369-9994 
------------------------------------- 

MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}

############################################################
#    �����԰��᡼��E���
############################################################
sub sendMailAdmin{
	my $mailBody;
	
	$g_html{'career'} =~ s/<br>/\n/g;
	$g_html{'qualification'} =~ s/<br>/\n/g;
	$g_html{'achievement'} =~ s/<br>/\n/g;
	$g_html{'salespoint'} =~ s/<br>/\n/g;
	
	$mailBody = <<MAIL;
To: $toAdd
From: $g_html{'email'}
Cc: $ccAdd
Bcc: $bccAdd
Subject: $subjectAdmin
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

�����Ԥ���Ͽ������ޤ�����
����̾
$g_html{'name1'} $g_html{'name2'}
���ե�E���
$g_html{'kana1'} $g_html{'kana2'}
����ǯ���E
$g_html{'birth_year'}ǯ$g_html{'birth_month'}��Eg_html{'birth_day'}ƁE
������
$g_html{'sex'}
��E-mail���ɥ�E�
$g_html{'email'}
��͹���ֹ�E
$g_html{'zip1'}-$g_html{'zip2'}
����ƻ�ܸ�
$g_html{'pref'}
�����Զ�Į¼
$g_html{'address1'}
������
$g_html{'address2'}
��TEL
$g_html{'tel1'}-$g_html{'tel2'}-$g_html{'tel3'}
��FAX
$g_html{'fax1'}-$g_html{'fax2'}-$g_html{'fax3'}
����E�E�������
$g_html{'performance_word'}��E��ɡ�$g_html{'performance_page'}�ڡ���
������Eμ�E�E
$g_html{'lang1'}
$g_html{'lang2'}
$g_html{'lang3'}
$g_html{'lang4'}
���ǽ����򡢿���
$g_html{'career'}
����ʡ���������
$g_html{'qualification'}
��ľ��Eǯ�֤μ��פʼ���
$g_html{'achievement'}
�����������������������ȥ饤����E��Τä��а�
$g_html{'salespoint'}
MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}
