#!/usr/bin/perl
############################################################
#
#    ISS �����Ԥ���Ͽ�ե�����E
#
############################################################
require './cgi-lib.pl';
require './jcode.pl';
use CGI::Carp qw(fatalsToBrowser);

############################################################
#    ��āE���
############################################################

## ������᡼��E��ɥ�E�
$toAdd = 'm-aoyama@issjp.com';

## CC�E�᡼�E��E��E�ɥ�E�
$ccAdd = '';

## BCC�E�᡼�E��E��E�ɥ�E�
$bccAdd = '';

## �E��E��E��E��E�ԥ᡼�E��E��E�ɥ�E�
$fromAdd = 'm-aoyama@issjp.com';

## �E�᡼�E��E��E��E��E�ȥ�E�
$subjectUser = 'ISS�E��E��E��E��E��E��E�ȥ饤�E��E��E��E�ؤΤ�E��E��E��E�礢�E�꤬�E�Ȥ�E��E��E��E��E��E��E��E�ޤ�E�';
$subjectAdmin = '�E�ȥ饤�E��E��E��E�α�E��E�礬�E��E��E��E�ޤ�E��E��E� ';

## �E��E�ǧ�E��E��E��E�HTML
$confirmHtmlFile = './tsuyaku_confirm.html';

## �E��E��E�顼�E��E��E��E�HTML
$errorHtmlFile = './tsuyaku_error.html';

## �E��E�λ�E��E��E��E�HTML
$endHtmlFile = './tsuyaku_complete.html';

## sendmail�E�ѥ�E�
$sendmail = "/usr/sbin/sendmail -t -f$fromAdd";

############################################################
#    �E�ץ�E��E��E��E�೫�E��E�
############################################################
&decode();
&setTime();
&checkForm();

if($g_html{'f_conf'}){
	&outHtml($confirmHtmlFile);
}elsif($g_html{'f_exe'}){
	&sendMailUser();
	&sendMailAdmin();
	&outHtml($endHtmlFile);
}

############################################################
#    �E�ե�E��E��E��E��E��E��E��E�
############################################################
sub decode{
	my ($key,$value);
	
	&ReadParse();
	while(($key,$value) = each %in){
		&jcode::convert(\$key,'euc');
		&jcode::convert(\$value,'euc','','z');
		$value =~ s/&/&amp;/gi;
		$value =~ s/\"/&quot;/gi;
		$value =~ s/\'/&#39;/gi;
		$value =~ s/</&lt;/gi;
		$value =~ s/>/&gt;/gi;
		$value =~ s/&lt;br&gt;/<br>/gi;
		$value =~ s/\r\n?/\n/g;
		$value =~ s/\n/<br>/g;
		$value =~ s/\0/ /g;
		
		if($key !~ /^f_/){
			$g_hidden .= qq(<input type="hidden" name="$key" value="$value">\n);
		}
		$g_html{$key} = $value;
	}
	$g_html{'hidden'} = $g_hidden;
	
}

############################################################
#    �E��E��E��E��E��E��E��E�
############################################################
sub setTime{
	my ($sec,$min,$hour,$day,$mon,$year,$wday) = localtime();
	
	$g_time{'sec'} = sprintf("%02d",$sec);
	$g_time{'min'} = sprintf("%02d",$min);
	$g_time{'hour'} = sprintf("%02d",$hour);
	$g_time{'day'} = sprintf("%02d",$day);
	$g_time{'mon'} = sprintf("%02d",$mon+1);
	$g_time{'year'} = sprintf("%04d",$year+1900);
	$g_time{'all'} = "$g_time{'year'}/$g_time{'mon'}/$g_time{'day'} $g_time{'hour'}:$g_time{'min'}:$g_time{'sec'}";
}

############################################################
#    �E��E��E�顼�E��E��E��E��E�å�E�
############################################################
sub checkForm{
	my $flag = 1;
	
	if($g_html{'name1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E�֤�E�̾�E��E��E��E��E��E��E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'name2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E�֤�E�̾�E��E��E��E�̾�E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'kana1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E�֤�E�̾�E��E��E��E��E��E��E��E��E��E��E��E��E��E��E�ʡפ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}elsif($g_html{'kana1'} !~ /^(\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc])+$/){
		$g_html{'error'} .= qq(<li class="mtx">�E�֤�E�̾�E��E��E��E��E��E��E��E��E��E��E��E��E��E��E�ʡפ˥�E��E��E��E��E��E�ʰʳ�E��E��E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'kana2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E�֤�E�̾�E��E��E��E�̾�E��E��E��E��E��E��E��E��E�ʡפ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}elsif($g_html{'kana2'} !~ /^(\xa5[\xa1-\xf6]|\xa1[\xb3\xb4\xbc])+$/){
		$g_html{'error'} .= qq(<li class="mtx">�E�֤�E�̾�E��E��E��E�̾�E��E��E��E��E��E��E��E��E�ʡפ˥�E��E��E��E��E��E�ʰʳ�E��E��E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
#	if($g_html{'birth_year'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">�E��E��E��E�ǯ�E��E��E��E��E��E�ǯ�E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
#		$flag = 0;
#	}
#	if($g_html{'birth_month'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">�E��E��E��E�ǯ�E��E��E��E��E��E��E��E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
#		$flag = 0;
#	}
#	if($g_html{'birth_day'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">�E��E��E��E�ǯ�E��E��E��E��E��E��E��E��E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
#		$flag = 0;
#	}
	if($g_html{'sex'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E��E��E��E��E�̡פ�E��E��E��E�򤵤�E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'email'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E�֥᡼�E��E��E�ɥ�E��E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}else{
		if($g_html{'email'} !~ /^[\w\-\~\.]+@[\w\-\~].[\w\-\~\.]+$/){
			$g_html{'error'} .= qq(<li class="mtx">�E�֥᡼�E��E��E�ɥ�E��E�פη�E��E��E��E��E��E��E��E��E��E�Ǥ�E��E��E�</li>);
			$flag = 0;
		}
	}
	$zip = $g_html{'zip1'} . $g_html{'zip2'};
	if($g_html{'zip1'} eq '' or $g_html{'zip2'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">�E��E�͹�E��E��E�ֹ�E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
#		$flag = 0;
	}elsif($zip !~ /^\d{7}$/){
		$g_html{'error'} .= qq(<li class="mtx">�E��E�͹�E��E��E�ֹ�E�פ�E��E��E��E��E��E��E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'pref'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E��E��E��E�ƻ�E�ܸ�E��E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'address1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E�ַ�E��E�Զ�E�Į¼ �E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'address2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E��E��E��E��E�ϡפ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'tel1'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E��E�TEL�E��E��E�Գ�E��E��E��E�֡פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'tel2'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E��E�TEL�E��E��E��E��E��E��E��E�֡פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	if($g_html{'tel3'} eq ''){
		$g_html{'error'} .= qq(<li class="mtx">�E��E�TEL�E��E��E��E�4�E��E�פ�E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	$tel = $g_html{'tel1'} . $g_html{'tel2'} . $g_html{'tel3'};
	if($tel !~ /[0-9]/){
##	if($tel !~ /^\d{10}$/){
		$g_html{'error'} .= qq(<li class="mtx">�E��E�TEL�E�פ�E��E��E��E��E��E��E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
		$flag = 0;
	}
	$fax = $g_html{'fax1'} . $g_html{'fax2'} . $g_html{'fax3'};
	if($fax ne ''){
		if($fax !~ /^\d{10}$/){
			$g_html{'error'} .= qq(<li class="mtx">�E��E�FAX�E�פ�E��E��E��E��E��E��E��E��E��E�Ϥ�E��E��E�Ƥ�E��E�ޤ�E��E��E�</li>);
			$flag = 0;
		}
	}
#	if($g_html{'agree'} eq ''){
#		$g_html{'error'} .= qq(<li class="mtx">�E�֥�E��E��E��E�ӥ�E��E��E��E��E��E�ѵ�E��E��E�פ˥�E��E��E��E�å�E��E��E��E��E��E�äƤ�E��E�ޤ�E��E��E�</li>);
#		$flag = 0;
#	}
	
	if($flag == 0){
#		$g_html{'error'} .= qq(<li class="mtx">�E�֥饦�E��E��E��E��E��E��E�ܥ�E��E��E�Ǥ�E��E��E��E�Τ�E��E��E��E��E��E��E��E��E��E�Ǥ�E��E��E��E�⤦�E��E��E��E��E��E��E�Ϥ�E��E�ʤ�E��E��E��E�Ƥ�E��E��E��E��E��E��E��E��E� </li>);
		&outHtml($errorHtmlFile);
	}
	
	if($g_html{'lang1'} ne ''){
		$g_html{'lang1'} .= qq(�E��E��E��E�);
	}
	if($g_html{'lang2'} ne ''){
		$g_html{'lang2'} .= qq(�E��E��E��E�);
	}
	if($g_html{'lang3'} ne ''){
		$g_html{'lang3'} .= ' ' . qq(�E�ʸ�E��E��E� ) . $g_html{'lang3_name'} . qq(�E�ˡ�E��E��E�);
	}
	if($g_html{'lang4'} ne ''){
		$g_html{'lang4'} .= ' ' . qq(�E�ʸ�E��E��E� ) . $g_html{'lang4_name'} . qq(�E��E�);
	}

#	for($i=1;$i<=4;$i++){
#		$name = 'lang' . $i;
#	 if($g_html{$name}){
#		$g_html{'lang'} .= $g_html{$name} . ' ';
#	}


	}


#}

############################################################
#    HTML�E��E��E��E�
############################################################
sub outHtml{
	my $file = shift;
	my ($html,@htmlList,$htmlAll);
	my @tmp;
	
	open(HTML,"<$file") or die();
	read(HTML,$html,-s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	foreach (@htmlList){
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_."\n";
		}else{
			$htmlAll .= $_."\n";
		}
	}
	
#	&jcode::convert(\$htmlAll,'sjis','euc');
	print "Content-type:text/html; charset=euc-jp\n\n";
	print $htmlAll;
	
	exit;
}

############################################################
#    �E��E��E��E�ɥ桼�E��E��E�᡼�E��E��E��E��E��E�
############################################################
sub sendMailUser{
	my $mailBody;

	foreach $key(keys %g_html){
		$value = $g_html{$key};
		$value =~ s/&amp;/&/gi;
		$value =~ s/&lt;/</gi;
		$value =~ s/&gt;/>/gi;
		$g_html{$key} = $value;
	}
	
	$mailBody = <<MAIL;
To: $g_html{'email'}
From: $fromAdd
Subject: $subjectUser
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

$g_html{'name1'} $g_html{'name2'} �E��E�

�E��E��E��E��E��E��E��E��E�ȥ饤�E��E��E��E�ˤ�E��E��E��E�礤�E��E��E��E��E��E��E��E��E�ˤ�E��E�꤬�E�Ȥ�E��E��E��E��E��E��E��E�ޤ�E��E��E� 
�E��E��E�Υ᡼�E��E�ϥ�E��E��E��E�ƥ�E��E��E�ưŪ�E��E��E��E��E��E��E��E��E��E�Ƥ�E��E��E�ޤ�E��E��E� 

�E��E��E��E��E��E��E��E��E��E�Ƥ�E��E�Ҹ�E��E��E��E��E��E��E��E�太�E�Ż�E��E�Ǥ�E��E��E��E�Ǥ�E��E��E��E�ǽ�E��E��E�Τ�E��E��E��E��E��E�ˤϡ�E� 
�E��E��E�Ҥ�E��E�ȥ饤�E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E�Ƥ�E��E��E��E��E��E��E��E�ޤ�E��E��E� 

�E��E��E��E��E��E��E�Υ᡼�E��E�˿�E��E��E��E��E��E�꤬�E�ʤ�E��E��E��E�ˤϡ�E��E��E��E�Ѥ�E��E��E��E�Ǥ�E��E��E� 
�E��E��E��E��E��E��E�ɥ�E��E�ޤǤ�E��E�Τ餻�E��E��E��E��E��E��E��E��E�ޤ�E��E�褦�E��E��E�ꤤ�E��E��E��E��E��E��E�ޤ�E��E��E� 
�E��E��E��E��E��E��E��E��E�ץ�E��E�ɥ�E��E��E�trans3\@issjp.com 

�E��E��E��E�Ȥ�E��E��E��E��E��E��E��E��E�ꤤ�E��E��E��E��E�夲�E�ޤ�E��E��E� 

------------------------------------- 
�E��E��E��E��E�ҥ�E��E��E��E��E��E��E��E��E��E��E��E��E��E��E� 
�E�Ķ�E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E� 
TEL: 03-3230-2521 
------------------------------------- 

MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}

############################################################
#    �E��E��E��E��E�԰�E��E�᡼�E��E��E��E��E��E�
############################################################
sub sendMailAdmin{
	my $mailBody;
	
	$g_html{'career'} =~ s/<br>/\n/g;
	$g_html{'qualification'} =~ s/<br>/\n/g;
	$g_html{'achievement'} =~ s/<br>/\n/g;
	$g_html{'salespoint'} =~ s/<br>/\n/g;
	
	$mailBody = <<MAIL;
To: $toAdd
From: $g_html{'email'}
Cc: $ccAdd
Bcc: $bccAdd
Subject: $subjectAdmin
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

�E��E��E��E��E�Ԥ�E��E��E�Ͽ�E��E��E��E��E��E�ޤ�E��E��E��E��E�
�E��E��E��E�̾
$g_html{'name1'} $g_html{'name2'}
�E��E��E�ե�E��E��E�
$g_html{'kana1'} $g_html{'kana2'}
�E��E��E��E�ǯ�E��E��E��E�
$g_html{'birth_year'}ǯ$g_html{'birth_month'}�E��E�$g_html{'birth_day'}�E��E�
�E��E��E��E��E��E�
$g_html{'sex'}
�E��E�E-mail�E��E��E�ɥ�E�
$g_html{'email'}
�E��E�͹�E��E��E�ֹ�E�
$g_html{'zip1'}-$g_html{'zip2'}
�E��E��E��E�ƻ�E�ܸ�E�
$g_html{'pref'}
�E��E��E��E��E�Զ�E�Į¼
$g_html{'address1'}
�E��E��E��E��E��E�
$g_html{'address2'}
�E��E�TEL
$g_html{'tel1'}-$g_html{'tel2'}-$g_html{'tel3'}
�E��E�FAX
$g_html{'fax1'}-$g_html{'fax2'}-$g_html{'fax3'}
�E��E��E��E��E��E��E��E��E��E��E��E��E��E�
$g_html{'performance_word'}�E��E��E�ɡ�E�$g_html{'performance_page'}�E�ڡ�E��E��E�
�E��E��E��E��E��E�μ�E��E��E�
$g_html{'lang1'}
$g_html{'lang2'}
$g_html{'lang3'}
$g_html{'lang4'}
�E��E��E�ǽ�E��E��E��E�򡢿�E��E��E�
$g_html{'career'}
�E��E��E��E�ʡ�E��E��E��E��E��E��E��E��E�
$g_html{'qualification'}
�E��E�ľ�E��E�5ǯ�E�֤μ�E��E�פʼ�E��E��E�
$g_html{'achievement'}
�E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E��E�ȥ饤�E��E��E��E��E��E�Τä�E��E�а�E�
$g_html{'salespoint'}
MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail") or die();
	print M $mailBody;
	close(M);

}
