#!/usr/bin/perl
############################################################
#
#    ISS 管理画面
#
############################################################
use CGI qw(:standard);
use CGI::Carp qw(fatalsToBrowser);
use File::Copy;
require './jcode.pl';
require './init.pl';
require './subroutine.pl';
umask 0070;

############################################################
#    プログラム開始
############################################################

&formDecode();
&setTime();
&lock();

if($g_html{'f_preview'}){
	&getPreviewData();
	&outHtml($g_previewHtmlFile);
}elsif($g_html{'f_accept_exe'}){
	&acceptExe();
	&outHtml($g_endHtmlFile);
}elsif($g_html{'f_change'}){
	&changeCsvExe();
	&makeTopPage();
	&outHtml($g_endHtmlFile);
}elsif($g_html{'f_del'}){
	&delExe();
	&makeTopPage();
	&outHtml($g_endHtmlFile);
}elsif($g_html{'f_edit'}){
	&getEditData();
	&outHtml($g_editHtmlFile);
}elsif($g_html{'f_edit_conf'}){
	&checkForm();
	&editConf();
	&outHtml($g_editConfHtmlFile);
}elsif($g_html{'f_edit_exe'}){
	&editExe();
	&makeHtml() if($g_html{'detail'} eq '2');
	&makeTopPage();
	&outHtml($g_endHtmlFile);
}else{
	&getListData();
	&outHtml($g_topHtmlFile);
}