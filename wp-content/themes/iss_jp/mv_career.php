<div class="l-pankuzu">
    <div class="l-inner">
        <p><a href="<?php echo home_url();?>/">ホーム</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<a href="<?php echo home_url();?>/recruit">採用情報</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;<a href="<?php echo home_url();?>/recruit/registration/interpreters">フリーランス通訳者の登録について</a>&nbsp;&nbsp;&gt;&nbsp;&nbsp;フリーランス通訳者登録フォーム</p>
    </div>
</div>

<section>
    <div class="l-mainVisual">
        <div class="l-mainVisual-inner">
            <img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/recruit/img_mv_pc.png" alt="採用情報 Recruit">
        </div>
    </div>
</section>