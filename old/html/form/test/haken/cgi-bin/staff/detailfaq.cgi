#!/usr/bin/perl

#ディレクトリ情報(unix環境下実装用)ここから
my $TextFileDir = "../secret/staff";

my $TemplateDir = "../template/staff";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "detailfaq.html";
#テンプレートファイルリストここまで

#最終出力文字列
my $HtmlSrc = "";

#ここまで設定

print("Content-type:text/html\n\n");

#URLからオープンするファイルの名称を取得
my $OpenFileName = $ENV{'QUERY_STRING'};

#ファイルオープン、情報取得
my @FaqInfo = ();
open FAQFILE,("$TextFileDir/$OpenFileName");
@FaqInfo = <FAQFILE>;
close FAQFILE;

my $FileName = $FaqInfo[0];
my $Category = $FaqInfo[1];
my $Question = $FaqInfo[2];
my $Answer = $FaqInfo[3];
my $Respondent = $FaqInfo[4];

#テンプレート読み込み
open TEMPLATE,("$TemplateDir/$HtmlBody");
while(<TEMPLATE>){
	$HtmlSrc .= $_;
}
close TEMPLATE;

#置換処理
$HtmlSrc =~ s/__QUESTION__TEXT__/$Question/;
$HtmlSrc =~ s/__ANSWER__TEXT__/$Answer/;
$HtmlSrc =~ s/__RESPONDENT__NAME__/$Respondent/;

#HTML出力

print ("$HtmlSrc");

