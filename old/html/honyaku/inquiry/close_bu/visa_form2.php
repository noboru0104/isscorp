<?

$pagecheck = "0";

if(getenv("HTTP_REFERER") == "https://www.issjp.com/honyaku/inquiry/visa_form1.php"){$pagecheck = "1";}
if(getenv("HTTP_REFERER") == "https://202.212.213.210/honyaku/inquiry/visa_form1.php"){$pagecheck = "1";}

//テスト用
if(getenv("HTTP_REFERER") == "https://www.issjp.com/honyaku_test/inquiry/visa_form1.php"){$pagecheck = "1";}

if($pagecheck == "0"){
	header("Location: https://www.issjp.com/honyaku/inquiry/visa_form.html");
	exit;
}

$name_kanji = mb_convert_kana(htmlspecialchars($_POST['name_kanji']),"K","EUC-JP");
$name_kana = mb_convert_kana(htmlspecialchars($_POST['name_kana']),"K","EUC-JP");
$name_rome = mb_convert_kana(htmlspecialchars($_POST['name_rome']),"K","EUC-JP");

$sex = $_POST['sex'];

$email = mb_convert_kana(htmlspecialchars($_POST['email']),"K","EUC-JP");

$zip1 = mb_convert_kana(htmlspecialchars($_POST['zip1']),"K","EUC-JP");
$zip2 = mb_convert_kana(htmlspecialchars($_POST['zip2']),"K","EUC-JP");

$pref = $_POST['pref'];

$address1 = mb_convert_kana(htmlspecialchars($_POST['address1']),"K","EUC-JP");
$address2 = mb_convert_kana(htmlspecialchars($_POST['address2']),"K","EUC-JP");

$tel1 = mb_convert_kana(htmlspecialchars($_POST['tel1']),"K","EUC-JP");
$tel2 = mb_convert_kana(htmlspecialchars($_POST['tel2']),"K","EUC-JP");
$tel3 = mb_convert_kana(htmlspecialchars($_POST['tel3']),"K","EUC-JP");

$country = $_POST['country'];
$country_s = mb_convert_kana(htmlspecialchars($_POST['country_s']),"K","EUC-JP");

$language = $_POST['language'];
$language_s1 = mb_convert_kana(htmlspecialchars($_POST['language_s1']),"K","EUC-JP");
$language_s2 = mb_convert_kana(htmlspecialchars($_POST['language_s2']),"K","EUC-JP");

$suryo_tsucho = $_POST['suryo_tsucho'];
$suryo_koseki = $_POST['suryo_koseki'];
$suryo_zandaka = $_POST['suryo_zandaka'];
$suryo_shotoku = $_POST['suryo_shotoku'];
$suryo_zaishoku = $_POST['suryo_zaishoku'];
$suryo_kyuyo = $_POST['suryo_kyuyo'];
$suryo_konin = $_POST['suryo_konin'];
$suryo_gaikokujin = $_POST['suryo_gaikokujin'];
$suryo_sonota = mb_convert_kana(htmlspecialchars($_POST['suryo_sonota']),"K","EUC-JP");

$sending = $_POST['sending'];
$sending_sonota = $_POST['sending_sonota'];

$raisha_month = $_POST['raisha_month'];
$raisha_day = $_POST['raisha_day'];

$contact = $_POST['contact'];
$contact_fax1 = mb_convert_kana(htmlspecialchars($_POST['contact_fax1']),"K","EUC-JP");
$contact_fax2 = mb_convert_kana(htmlspecialchars($_POST['contact_fax2']),"K","EUC-JP");
$contact_fax3 = mb_convert_kana(htmlspecialchars($_POST['contact_fax3']),"K","EUC-JP");

$uketori_month = $_POST['uketori_month'];
$uketori_day = $_POST['uketori_day'];
$uketori_hoho = $_POST['uketori_hoho'];

$renraku = mb_convert_kana(htmlspecialchars($_POST['renraku']),"K","EUC-JP");

$upfile1_name = htmlspecialchars($_FILES['upfile1']['name']);
$upfile2_name = htmlspecialchars($_FILES['upfile2']['name']);
$upfile3_name = htmlspecialchars($_FILES['upfile3']['name']);

$err = "";

if ($name_kanji == ""){$err .= "・お名前<br>";}
if ($name_rome == ""){$err .= "・お名前（パスポートのローマ字表記）<br>";}

if ($sex == ""){$err .= "・性別<br>";}

if ($email == "" or !ereg("^[^@]+@[^.]+\..+", $email)){$err .= "・E-mailアドレス<br>";}

if ($pref == "" or $address1 == "" or $address2 == ""){$err .= "・ご住所<br>";}
if ($tel1 == "" or $tel2 == "" or $tel3 == ""){$err .= "・お電話番号<br>";}

if ($country == ""){
	$err .= "・ビザ申請先の国<br>";
}
elseif ($country == "その他の国" and $country_s == ""){
	$err .= "・ビザ申請先の国　その他の国名<br>";
} 

if ($language == ""){
	$err .= "・翻訳言語<br>";
}
elseif ($language == "その他" and ($language_s1 == "" or $language_s2 == "")){
	$err .= "・翻訳言語　その他の言語名<br>";
} 

if ($suryo_tsucho == "" and $suryo_koseki == "" and $suryo_zandaka == "" and $suryo_shotoku == "" and 

$suryo_zaishoku == "" and $suryo_kyuyo == "" and $suryo_konin == "" and $suryo_gaikokujin == "" and $suryo_sonota == ""){$err .= "・翻訳対象書類の種類と数量<br>";}


if ($sending == ""){
	$err .= "・翻訳原稿の送付<br>";
}
elseif ($sending == "フォーム"){
	if ($_FILES['upfile1']['size'] == 0 and $_FILES['upfile2']['size'] == 0 and $_FILES['upfile3']['size'] == 0){
		$err .= "・翻訳原稿の送付　添付ファイル<br>";
	}
}
else{
	if ($sending_sonota == ""){
		$err .= "・翻訳原稿の送付　その他の方法選択<br>";
	}
	elseif($sending_sonota == "持参"){
		if ($raisha_month == "" or $raisha_day == ""){
			$err .= "・翻訳原稿の送付　来社予定日時<br>";
		}
	}
}

if ($contact == ""){
	$err .= "・見積連絡方法のご希望<br>";
}
elseif ($contact == "FAX"){
	if ($contact_fax1 == "" or $contact_fax2 == "" or $contact_fax3 == ""){
		$err .= "・見積連絡方法のご希望　FAX番号<br>";
	}
}

if ($uketori_month == "" or $uketori_day == ""){
	$err .= "・受取希望日<br>";
}

if ($uketori_hoho == ""){
	$err .= "・受取方法<br>";
}

if ($err != ""){
	$err = "以下の項目の入力が正しくありません。<br>" . $err . "<br>戻るボタンでお戻りのうえ、入力しなおしてください。 ";
}
elseif ($sending == "フォーム"){
	//添付ファイルの処理

	//保存先パス基本
	$upfile_path_base = "/home/visa_data/";
	
	//保存先フォルダ名基本を日付時刻より作成する
	$folder_name_base = date("Ymd_His");
	//$folder_name_base = "2009612125851";
	
	//保存先フォルダ名
	$folder_name = $folder_name_base;
	
	//保存先パス
	$upfile_path = $upfile_path_base . $folder_name . "/";
	
	//フォルダ作成済フラグの初期化
	$is_make_folder = false;
	
	//フォルダ名の枝番用カウンターの初期化
	$folder_counter = 0;
	
	while($is_make_folder == false){
		if(mkdir($upfile_path) == true){
			chmod($upfile_path, 0775);
			//フォルダ作成済フラグをTrueにする
			$is_make_folder = true;
		}
		elseif($folder_counter > 99){
			//無限ループ回避用　100回以上フォルダ作成を行なったらループを抜ける
			$is_make_folder = true;
		}
		else{
			//枝番用カウンターのインクリメント
			$folder_counter++;
			//保存先フォルダ名の変更
			$folder_name = $folder_name_base . "_" . $folder_counter;
			//保存先パスの変更
			$upfile_path = $upfile_path_base . $folder_name . "/";		
		}
	}
	
	if($_FILES['upfile1']['size'] != 0){
		$result = move_uploaded_file($_FILES['upfile1']['tmp_name'], $upfile_path . $upfile1_name);
		chmod($upfile_path . $upfile1_name, 0775); 	
	}
	
	if($_FILES['upfile2']['size'] != 0){
		$result = move_uploaded_file($_FILES['upfile2']['tmp_name'], $upfile_path . $upfile2_name);
		chmod($upfile_path . $upfile2_name, 0775); 
	}
	
	if($_FILES['upfile3']['size'] != 0){
		$result = move_uploaded_file($_FILES['upfile3']['tmp_name'], $upfile_path . $upfile3_name);
		chmod($upfile_path . $upfile3_name, 0775); 
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
<meta name="description" content="ビザ申請書類の翻訳見積依頼を受け付けております。ISSは英国ビザ申請センターの翻訳会社リスト「Translation Agents List」に掲載されています。" />
<meta name="keywords" content="ビザ,翻訳,見積,証明書,翻訳会社,英国ビザ,ISS" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>ビザ申請書類の翻訳見積依頼 | ISS | 翻訳サービス内容詳細</title>
<link href="../../common/default.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../common/print.css" rel="stylesheet" type="text/css" media="print" />
<script type="text/javascript" src="../../common/script.js"></script>
<script type="text/javascript" src="../../form/_js/jquery.js"></script>
<script type="text/javascript" src="../../form/_js/sb/adapter/shadowbox-jquery.js"></script>
<script type="text/javascript" src="../../form/_js/sb/shadowbox.js"></script>
<script type="text/javascript">
<!--
Shadowbox.loadSkin('classic', '../../form/_js/sb/skin'); Shadowbox.loadLanguage('en', '../../form/_js/sb/lang');
 Shadowbox.loadPlayer(['img', 'swf', 'flv', 'qt', 'wmp', 'iframe', 'html'], '../../form/_js/sb/player'); 
 $(document).ready(function() {
 	Shadowbox.init();
	});
//-->
</script></head>
<body onload="initRollovers();">
<a name="pageTop" id="pageTop"></a>

<!-- Header Begin -->
<div id="header">
<div class="content"><a href="../../index.html"><img src="../../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../../index.html"><img src="../../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣、法人研修／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi">
<div class="searchBox">
<script type="text/javascript" src="../../search_s.js"></script>
</div>
<div class="hMenuBtn">
  <table cellspacing="0" summary="HeaderNavigation">
    <tr>
      <td><a href="http://www.issjp.com/index.html"><img src="../../common/images/hNavi_btn_05.gif" alt="HOME" width="57" height="20" /></a></td>
						<td><a href="http://www.issjp.com/inquiry/index.html"><img src="../../common/images/hNavi_btn_01.gif" alt="お問い合わせ" width="81" height="20" /></a></td>
      <td><a href="http://www.issjp.com/access/index.html"><img src="../../common/images/hNavi_btn_02.gif" alt="アクセスマップ" width="91" height="20" /></a></td>
      <td><a href="http://www.issjp.com/sitemap/index.html"><img src="../../common/images/hNavi_btn_03.gif" alt="サイトマップ" width="81" height="20" /></a></td>
      <td><a href="http://www.issjp.com/en/index.html"><img src="../../common/images/hNavi_btn_04.gif" alt="ENGLISH" width="65" height="20" border="0" /></a></td>
    </tr>
  </table>
</div>
</div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<!-- Global Navi Begin -->
<div id="globalNavi">
<div class="content">
<div class="menuList">
<ul>
<li class="bt01"><a href="http://www.issjp.com/about/index.html"><img src="../../common/images/gNavi_btn_01.jpg" alt="ISSグループとは" width="163" height="47" class="btn" /></a></li><li class="bt02"><a href="http://www.issjp.com/company/index.html"><img src="../../common/images/gNavi_btn_02.jpg" alt="アイ・エス・エス企業情報" width="163" height="47" class="btn" /></a></li>
<li class="bt03"><a href="http://www.issjp.com/service/index.html"><img src="../../common/images/gNavi_btn_03_current.jpg" alt="サービス案内" width="163" height="47" /></a></li><li class="bt04"><a href="http://www.issjp.com/recruit/index.html"><img src="../../common/images/gNavi_btn_04.jpg" alt="採用情報" width="163" height="47" class="btn" /></a></li><li class="bt05"><a href="http://www.issjp.com/publishing/index.html"><img src="../../common/images/gNavi_btn_05.jpg" alt="出版物紹介" width="164" height="47" class="btn" /></a></li>
</ul>
</div>
<h1><img src="../../service/images/h1.jpg" alt="サービス案内" width="816" height="90" /></h1>
</div>
</div>
<!-- Global Navi End -->

<!-- Main Content Begin -->
<div id="colum2">
<div class="content">

<!-- BreadList Begin -->
<div id="breadList">
<table cellspacing="0" summary="breadList"><tr>
<td><img src="../../common/images/breadListLeft.jpg" alt="" width="211" height="30" /></td>
<td nowrap="nowrap"><p class="s"><a href="http://www.issjp.com/index.html">HOME</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/service/index.html">サービス案内</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/honyaku/index.html">翻訳</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/honyaku/service.html">サービス内容詳細</a><span>&nbsp; &gt; &nbsp;</span>ビザ申請書類の翻訳見積依頼</p></td>
</tr></table>
</div>
<!-- BreadList End -->

<!-- Colum Left Begin -->
<div id="columLeft">
<div class="sMenu">
<ul>
<li><a href="http://www.issjp.com/tsuyaku/index.html"><img src="../../service/images/sMenu_btn_02.jpg" alt="通訳" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/kokusai/index.html"><img src="../../service/images/sMenu_btn_01.jpg" alt="国際会議企画・運営" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/honyaku/index.html"><img src="../../service/images/sMenu_btn_03_current.jpg" alt="翻訳" width="211" height="28" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/service.html"><img src="../images/sMenu_btn_04_02.jpg" alt="サービス内容詳細" width="211" height="18" class="btn" /></a></li>

<li class="third"><a href="http://www.issjp.com/honyaku/visa.html"><img src="../images/sMenu_btn_04_02_01_current.jpg" alt="英国ビザ申請書類の翻訳" width="211" height="18" /></a></li>

<li class="second"><a href="http://www.issjp.com/honyaku/flow.html"><img src="../images/sMenu_btn_04_03.jpg" alt="サービスの流れ" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/outline.html"><img src="../images/sMenu_btn_04_01.jpg" alt="サービスの体制" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/faq.html"><img src="../images/sMenu_btn_04_04.jpg" alt="FAQ" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/inquiry/index.html"><img src="../images/sMenu_btn_04_05.jpg" alt="問い合わせ先、地図" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="http://www.issjp.com/honyaku/inquiry/juridical.html"><img src="../images/sMenu_btn_04_05_01.jpg" alt="法人のお客様" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="http://www.issjp.com/honyaku/inquiry/individual.html"><img src="../images/sMenu_btn_04_05_04.jpg" alt="個人のお客様" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="https://www.issjp.com/honyaku/inquiry/honyaku_form.html"><img src="../images/sMenu_btn_04_05_02.jpg" alt="翻訳者の登録について" width="211" height="18" border="0" /></a></li>
<li class="space"><img src="../../common/images/pixel_white.gif" alt="" width="211" height="8" /></li>
<!-- li><a href="http://www.issjp.com/honyaku/localization.html"><img src="../../service/images/sMenu_btn_04.jpg" alt="ローカライゼーション" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/haken/index.html"><img src="../../service/images/sMenu_btn_05.jpg" alt="人材派遣・紹介予定派遣" width="211" height="28" class="btn" /></a></li>
<!-- li><a href="http://www.issjp.com/haken/referral.html"><img src="../../service/images/sMenu_btn_06.jpg" alt="紹介予定派遣" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/corptre/index.html"><img src="../../service/images/sMenu_btn_07.jpg" alt="法人向け語学研修" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/chinese/index.html"><img src="../../chinese/images/sMenu_btn_12.jpg" alt="中国語サービス" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/service/resources.html"><img src="../../service/images/sMenu_btn_08.jpg" alt="人材紹介" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/service/training.html"><img src="../../service/images/sMenu_btn_09.jpg" alt="通訳者・翻訳者養成" width="211" height="28" class="btn" /></a></li>
<!-- li><a href="http://www.issjp.com/service/com_training.html"><img src="../../service/images/sMenu_btn_10.jpg" alt="法人向け語学研修" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/service/network.html"><img src="../../service/images/sMenu_btn_11.jpg" alt="在ロサンゼルス米国現地法人" width="211" height="28" class="btn" /></a></li>
<li><img src="../../common/images/sMenu_bottom.jpg" alt="" width="211" height="30" /></li>
</ul>
</div>


<img src="../../common/images/pixel_trans.gif" alt="" width="211" height="20" class="spacer" />

<div class="infoBox">
<img src="../../common/images/infoBoxTitle.jpg" alt="お問い合わせ情報" width="193" height="26" />
<table class="fix"><tr>
<td class="ac"><a href="http://www.issjp.com/inquiry/index.html"><img src="../../common/images/infoBtn_01.jpg" alt="お問い合わせ窓口一覧" width="177" height="33" border="0" /></a></td>
</tr><tr>
<td class="ac"><a href="http://www.issjp.com/access/index.html"><img src="../../common/images/infoBtn_02.jpg" alt="アクセスマップ" width="177" height="33" border="0" /></a></td>
</tr></table>
</div>

<img src="../../common/images/pixel_trans.gif" alt="" width="211" height="20" class="spacer" />

</div>
<!-- Colum Left End -->

<!-- Colum Right Begin -->
<div id="columRight">


<!--　○○○○○○○○○○　ここから　○○○○○○○○○○　-->

<form action="visa_form3.php" method="post" name="myForm">

<div id="txtBody">

<h2><img src="images/visa_m__h2.jpg" alt="ビザ申請書類翻訳　見積依頼" width="525" height="60" /></h2>

<table border="0" cellpadding="0" cellspacing="0" width="525">

<tr>
<td>
<table border="0" cellpadding="0" cellpadding="0">
<tr>
<td colspan="2"><img src="../../common/images/h3_top.gif" alt="" width="525" height="5" /></td>
</tr>
<tr>
<td style="text-align:left; vertical-align:middle; height:40px; background-color:#DCF0F9; color:#326698"><strong>　ビザ申請書類翻訳　見積依頼フォーム</strong></td>
<td style="text-align:left; vertical-align:bottom; height:40px; background-color:#DCF0F9; color:#326698;"><strong>確認画面</strong></td>
</tr>
<tr>
<td colspan="2"><img src="../../common/images/h3_btm.gif" alt="" width="525" height="5" /></td>
</tr>
</table>
</td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr>

<? 
if ($err != ""){
	echo "<tr><td class=\"mtx\"><br><font color=\"#ff0000\">" . $err . "<br><br>※ご注意ください</font><br>前画面で原稿を添付いただいた場合、この画面から入力画面に戻りますと、<br>添付ファイルが削除されます。入力画面にお戻りの場合には、お手数ですが、<br>再度の添付のご対応をお願いいたします。</td></tr>";
}
?>

<tr>
<td>


<table border="0" cellpadding="5" cellspacing="3" width="525" class="formTable mtx">

<tr>
<th width="1%">お名前<span class="att">*</span></td>
<td><? echo $name_kanji; ?></td>
</tr>

<tr>
<th>お名前（フリガナ）</td>
<td><? echo $name_kana; ?></td>
</tr>

<tr>
<th><nobr>お名前（パスポート<br>のローマ字表記）<span class="att">*</span></nobr></td>
<td><? echo $name_rome; ?></td>
</tr>

<tr>
<th>性別<span class="att">*</span></td>
<td><? echo $sex; ?></td>
</tr>

<tr>
<th>E-mailアドレス<span class="att">*</span></td>
<td><? echo $email; ?></td>
</tr>

<tr>
<th>ご住所<span class="att">*</span></td>
<td><table border="0" cellpadding="0" cellspacing="1">
<tr>
<td><nobr>郵便番号</nobr></td>
<td><? echo $zip1 . "-" . $zip2; ?></td>
</tr>
<tr>
<td>都道府県</td>
<td><? echo $pref; ?></td>
</tr>
<tr>
<td>市区町村</td>
<td><? echo $address1; ?></td>
</tr>
<tr>
<td>番地以下</td>
<td><? echo $address2; ?></td>
</tr>
</table>
</td>
</tr>

<tr>
<th>お電話番号<span class="att">*</span></td>
<td><? echo $tel1 . "-" . $tel2 . "-" . $tel3; ?></td>
</tr>

<tr>
<th>ビザ申請先の国<span class="att">*</span></td>
<td>
<?
if ($country == "英国"){
	echo $country;
}
elseif ($country == "その他の国"){
	echo $country . " " . $country_s;
}
?>
</td>
</tr>

<tr>
<th>翻訳言語<span class="att">*</span></td>
<td>
<?
if ($language == "日本語→英語"){
	echo $language;
}
elseif ($language == "その他"){
	echo $language . " " . $language_s1 . "語→" . $language_s2 . "語";
}
?>
</td>
</tr>

<tr>
<th>翻訳対象書類の<br>種類と数量<span class="att">*</span></td>
<td>
<table border="0" cellpadding="0" cellspacing="1">

<?
if ($suryo_tsucho != ""){
	echo "<tr><td>通帳</td><td>" . $suryo_tsucho . "件</td></tr>";
}

if ($suryo_koseki != ""){
	echo "<tr><td>戸籍謄本</td><td>" . $suryo_koseki . "件</td></tr>";
}

if ($suryo_zandaka != ""){
	echo "<tr><td>残高証明書</td><td>" . $suryo_zandaka . "件</td></tr>";
}

if ($suryo_shotoku != ""){
	echo "<tr><td>所得証明書</td><td>" . $suryo_shotoku . "件</td></tr>";
}

if ($suryo_zaishoku != ""){
	echo "<tr><td>在職証明書</td><td>" . $suryo_zaishoku . "件</td></tr>";
}

if ($suryo_kyuyo != ""){
	echo "<tr><td>給与証明書</td><td>" . $suryo_kyuyo . "ヶ月分</td></tr>";
}

if ($suryo_konin != ""){
	echo "<tr><td>婚姻届受理証明書</td><td>" . $suryo_konin . "部</td></tr>";
}

if ($suryo_gaikokujin != ""){
	echo "<tr><td>外国人登録証明書</td><td>" . $suryo_gaikokujin . "部</td></tr>";
}

if ($suryo_sonota != ""){
	echo "<tr><td>その他の書類</td><td>" . $suryo_sonota . "</td></tr>"; 
}
?>
</table>


</td>
</tr>

<tr>
<th>翻訳原稿の送付<span class="att">*</span></td>
<td>
<?
if ($sending == "フォーム"){
	echo "このフォームに添付する<br>";	
	if ($_FILES['upfile1']['size'] != 0){
		echo "　" . $upfile1_name . "<br>";	
	}	
	if ($_FILES['upfile2']['size'] != 0){
		echo "　" . $upfile2_name . "<br>";	
	}	
	if ($_FILES['upfile3']['size'] != 0){
		echo "　" . $upfile3_name . "<br>";	
	}	
}
elseif ($sending == "その他"){
	if ($sending_sonota == "FAX"){
		echo "FAXで送付する";	
	}
	elseif ($sending_sonota =="郵送"){
		echo "郵送する";
	}
	elseif ($sending_sonota =="持参"){
		echo "持参する　（" . $raisha_month . "月" . $raisha_day . "日）";
	}
	elseif ($sending_sonota =="E-mail"){
		echo "E-mailに添付する";
	}
}
?>
</td>
</tr>

<tr>
<th>見積連絡方法の<br>ご希望<span class="att">*</span></td>
<td>
<?
if ($contact == "E-mail"){
	echo $contact;
}
elseif ($contact == "TEL"){
	echo $contact;
}
elseif ($contact == "FAX"){
	echo $contact . "　" . $contact_fax1 . "-" . $contact_fax2 . "-" . $contact_fax3;
}
?>
</td>
</tr>

<tr>
<th>受取希望日<span class="att">*</span></td>
<td>
<? echo $uketori_month . "月 " . $uketori_day . "日"; ?>
</td>
</tr>

<tr>
<th>受取方法<span class="att">*</span></td>
<td><? echo $uketori_hoho; ?></td>
</tr>

<tr>
<th>ご要望・ご連絡事項など</td>
<td><? echo ereg_replace("\r\n", "<br>", $renraku); ?></td>
</tr>


</table>
</td>
</tr>

<tr>
<td class="ac"><input name="submit" type="submit" style="font-size:10pt;" value="　送　信　" <? if($err != ""){echo " disabled";} ?>></td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" alt="" width="1" height="20" class="spacer" /></td>
</tr>

<tr>
<td class="ac"><input name="button" type="button" style="font-size:10pt;" onClick="history.back()" value="　戻　る　"></td>
</tr>

</table>

<img src="../../common/images/pixel_trans.gif" width="525" height="30" alt="" class="spacer" />
<a href="#pageTop"><img src="../../common/images/pageTop.gif" alt="このページの上へ" width="90" height="20" class="pageTop" /></a>
<img src="../../common/images/pixel_trans.gif" width="525" height="50" alt="" class="spacer" />

</div>

<input type="hidden" name="name_kanji" value="<? echo $name_kanji ?>">
<input type="hidden" name="name_kana" value="<? echo $name_kana ?>">
<input type="hidden" name="name_rome" value="<? echo $name_rome ?>">

<input type="hidden" name="sex" value="<? echo $sex ?>">

<input type="hidden" name="email" value="<? echo $email ?>">

<input type="hidden" name="zip1" value="<? echo $zip1 ?>">
<input type="hidden" name="zip2" value="<? echo $zip2 ?>">
<input type="hidden" name="pref" value="<? echo $pref ?>">
<input type="hidden" name="address1" value="<? echo $address1 ?>">
<input type="hidden" name="address2" value="<? echo $address2 ?>">

<input type="hidden" name="tel1" value="<? echo $tel1 ?>">
<input type="hidden" name="tel2" value="<? echo $tel2 ?>">
<input type="hidden" name="tel3" value="<? echo $tel3 ?>">

<input type="hidden" name="country" value="<? echo $country ?>">
<input type="hidden" name="country_s" value="<? echo $country_s ?>">

<input type="hidden" name="language" value="<? echo $language ?>">
<input type="hidden" name="language_s1" value="<? echo $language_s1 ?>">
<input type="hidden" name="language_s2" value="<? echo $language_s2 ?>">

<input type="hidden" name="suryo_tsucho" value="<? echo $suryo_tsucho ?>">
<input type="hidden" name="suryo_koseki" value="<? echo $suryo_koseki ?>">
<input type="hidden" name="suryo_zandaka" value="<? echo $suryo_zandaka ?>">
<input type="hidden" name="suryo_shotoku" value="<? echo $suryo_shotoku ?>">
<input type="hidden" name="suryo_zaishoku" value="<? echo $suryo_zaishoku ?>">
<input type="hidden" name="suryo_kyuyo" value="<? echo $suryo_kyuyo ?>">
<input type="hidden" name="suryo_konin" value="<? echo $suryo_konin ?>">
<input type="hidden" name="suryo_gaikokujin" value="<? echo $suryo_gaikokujin ?>">
<input type="hidden" name="suryo_sonota" value="<? echo $suryo_sonota ?>">

<input type="hidden" name="sending" value="<? echo $sending ?>">
<input type="hidden" name="sending_sonota" value="<? echo $sending_sonota ?>">
<input type="hidden" name="raisha_month" value="<? echo $raisha_month ?>">
<input type="hidden" name="raisha_day" value="<? echo $raisha_day ?>">

<input type="hidden" name="contact" value="<? echo $contact ?>">
<input type="hidden" name="contact_fax1" value="<? echo $contact_fax1 ?>">
<input type="hidden" name="contact_fax2" value="<? echo $contact_fax2 ?>">
<input type="hidden" name="contact_fax3" value="<? echo $contact_fax3 ?>">

<input type="hidden" name="uketori_month" value="<? echo $uketori_month ?>">
<input type="hidden" name="uketori_day" value="<? echo $uketori_day ?>">
<input type="hidden" name="uketori_hoho" value="<? echo $uketori_hoho ?>">

<input type="hidden" name="renraku" value="<? echo $renraku ?>">

<input type="hidden" name="folder_name" value="<? echo $folder_name ?>">
<input type="hidden" name="file_name1" value="<? echo $upfile1_name ?>">
<input type="hidden" name="file_name2" value="<? echo $upfile2_name ?>">
<input type="hidden" name="file_name3" value="<? echo $upfile3_name ?>">

</form>

<!--　○○○○○○○○○○　ここまで　○○○○○○○○○○　-->

</div>
<!-- Colum Right End -->

<br class="clr" />
</div>
</div>
<!-- Main Content End -->

<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../../common/images/pixel_trans.gif" width="816" height="35" alt="" class="spacer" />
<div class="menuList">
<ul>
<li class="bt01"><a href="http://www.issjp.com/tsuyaku/index.html">通訳</a>　|　<a href="http://www.issjp.com/kokusai/index.html">国際会議企画・運営</a>　|　<a href="http://www.issjp.com/honyaku/index.html">翻訳</a>　|　<a href="http://www.issjp.com/haken/index.html">人材派遣・紹介予定派遣</a>　|　<a href="http://www.issjp.com/corptre/index.html">法人向け語学研修</a></li>
<li class="bt02"><a href="http://www.issjp.com/policy/index.html"><img src="../../common/images/fNavi_btn_01.gif" alt="個人情報保護方針" width="99" height="20" /></a></li>
<li class="bt03"><a href="http://www.issjp.com/site/index.html"><img src="../../common/images/fNavi_btn_03.gif" alt="サイトポリシー" width="90" height="20" /></a></li>
</ul>
</div>
<div class="menuList">
<ul>
<li class="bt05">【ISSグループサイト】　<a href="http://www.issnet.co.jp/" target="_blank">通訳・翻訳者養成学校のISS</a>　|　<a href="http://haken.issjp.com/" target="_blank">通訳・翻訳のISS人材派遣</a>　|　<a href="http://www.isssc.com/" target="_blank">外資系転職のISS</a>　|　<a href="http://www.issla.net/" target="_blank">通訳・翻訳のISSロサンゼルス</a></li>
<li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>

</div>
</div>
</div>
<!-- Footer End -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>
</body>
</html>
