############################################################
#    ISS　更新ツール　投稿画面　サブルーチン
############################################################

############################################################
#    デコード処理
############################################################
sub formDecode {
	@paramList = param();
	foreach (@paramList){
		$param = param($_);
		&jcode::convert(\$param,'euc');
#		$param =~ s/</&lt;/g;
#		$param =~ s/>/&gt;/g;
#		$param =~ s/&lt;br&gt;/<br>/g;
		$param =~ s/\r\n/\n/g;
		$param =~ s/\r/\n/g;
		$param =~ s/\n/<br \/>/g;
		$param =~ s/"/'/g;
		$param =~ s/,/&sbquo;/g;
		if($_ !~ /^f_/ && $_ ne 'submit.x' && $_ ne 'submit.y'){
			$g_hidden .= "<input type=\"hidden\" name=\"$_\" value=\"$param\">\n";
		}
#		$param =~ s/"/&quot;/g;
		$g_param{$_} = $param;
		if($g_html{$_} eq ''){
			$g_html{$_} = $param;
		}
	}
	$g_html{'hidden'} = $g_hidden;
}

############################################################
#    フォームチェック
############################################################
sub checkForm{
	my $flag = 1;
	my $tmpFile = $g_time{'year'} . $g_time{'mon'} . $g_time{'day'} . $g_time{'hour'} . $g_time{'min'} . $g_time{'sec'};
	
	if($g_html{'detail'} eq '1'){
		if($g_html{'n_year'} eq '' or $g_html{'n_month'} eq '' or $g_html{'n_day'} eq ''){
			$g_html{'n_year_error'} = qq(<div class="error">エラー：投稿日を入力してください</div>);
			$flag = 0;
		}
		if($g_html{'title1'} eq ''){
			$g_html{'title1_error'} = qq(<div class="error">エラー：記事タイトルを入力してください</div>);
			$flag = 0;
		}
		if($g_html{'title_link'} eq '2'){
			if($g_html{'url'} eq ''){
				$g_html{'url_error'} = qq(<div class="error">エラー：URLを入力してください</div>);
				$flag = 0;
			}elsif($g_html{'url'} !~ /^https?:\/\/\w[\w\.\/\~\-\?\&\+\=\;\:\#]*$/ and $g_html{'url'} !~ /^mailto:[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]/){
				$g_html{'url_error'} = qq(<div class="error">エラー：URLはが不正です</div>);
				$flag = 0;
			}
		}elsif($g_html{'title_link'} eq '3'){
			if($g_html{'pdf'} eq ''){
				$g_html{'pdf_error'} = qq(<div class="error">エラー：PDFファイルを選択してください</div>);
				$flag = 0;
			}else{
				$tmpPdfFile = $tmpFile . '000';
				($extension1, $flag) = &uploadPdf(param('pdf'), $g_imageTmpDir, $tmpPdfFile, 'pdf');
			}
		}
	}elsif($g_html{'detail'} eq '2'){
		if($g_html{'p_year'} eq '' or $g_html{'p_month'} eq '' or $g_html{'p_day'} eq ''){
			$g_html{'p_year_error'} = qq(<div class="error">エラー：投稿日を入力してください</div>);
			$flag = 0;
		}
		if($g_html{'title2'} eq ''){
			$g_html{'title2_error'} = qq(<div class="error">エラー：記事タイトルを入力してください</div>);
			$flag = 0;
		}
		if($g_html{'text'} eq ''){
			$g_html{'text_error'} = qq(<div class="error">エラー：本文を入力してください</div>);
			$flag = 0;
		}
		if($g_html{'image1'} eq '' and $g_html{'image1_text'} ne ''){
			$g_html{'image1_error'} = qq(<div class="error">エラー：画像を選択してください</div>);
		}else{
			if($g_html{'image1'}){
				$tmpFile1 = $tmpFile . '001';
				($extension1, $flag) = uploadImage(param('image1'), $g_imageTmpDir, $tmpFile1, 'image1');
			}
		}
		if($g_html{'image2'} eq '' and $g_html{'image2_text'} ne ''){
			$g_html{'image2_error'} = qq(<div class="error">エラー：画像を選択してください</div>);
		}else{
			if($g_html{'image2'}){
				$tmpFile2 = $tmpFile . '002';
				($extension2, $flag) = uploadImage(param('image2'), $g_imageTmpDir, $tmpFile2, 'image2');
			}
		}
		if($g_html{'image3'} eq '' and $g_html{'image3_text'} ne ''){
			$g_html{'image3_error'} = qq(<div class="error">エラー：画像を選択してください</div>);
		}else{
			if($g_html{'image3'}){
				$tmpFile3 = $tmpFile . '003';
				($extension3, $flag) = uploadImage(param('image3'), $g_imageTmpDir, $tmpFile3, 'image3');
			}
		}
		if($g_html{'link1'} eq ''){
			if($g_html{'link1_text'} ne ''){
				$g_html{'link1_error'} = qq(<div class="error">エラー：リンク先を入力してください</div>);
				$flag = 0;
			}
		}else{
			if($g_html{'link1_text'} eq ''){
				$g_html{'link1_error'} = qq(<div class="error">エラー：リンクテキストを入力してください</div>);
				$flag = 0;
			}
			if($g_html{'link1'} !~ /^https?:\/\/\w[\w\.\/\~\-\?\&\+\=\;\:\#]*$/ and $g_html{'link1'} !~ /^mailto:[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]/){
				$g_html{'link1_error'} .= qq(<div class="error">エラー：URLは半角英数で入力してください</div>);
				$flag = 0;
			}
		}
		if($g_html{'link2'} eq ''){
			if($g_html{'link2_text'} ne ''){
				$g_html{'link2_error'} = qq(<div class="error">エラー：リンク先を入力してください</div>);
				$flag = 0;
			}
		}else{
			if($g_html{'link2_text'} eq ''){
				$g_html{'link2_error'} = qq(<div class="error">エラー：リンクテキストを入力してください</div>);
				$flag = 0;
			}
			if($g_html{'link2'} !~ /^https?:\/\/\w[\w\.\/\~\-\?\&\+\=\;\:\#]*$/ and $g_html{'link2'} !~ /^mailto:[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]/){
				$g_html{'link2_error'} .= qq(<div class="error">エラー：URLは半角英数で入力してください</div>);
				$flag = 0;
			}
		}
		if($g_html{'link3'} eq ''){
			if($g_html{'link3_text'} ne ''){
				$g_html{'link3_error'} = qq(<div class="error">エラー：リンク先を入力してください</div>);
				$flag = 0;
			}
		}else{
			if($g_html{'link3_text'} eq ''){
				$g_html{'link3_error'} = qq(<div class="error">エラー：リンクテキストを入力してください</div>);
				$flag = 0;
			}
			if($g_html{'link3'} !~ /^https?:\/\/\w[\w\.\/\~\-\?\&\+\=\;\:\#]*$/ and $g_html{'link3'} !~ /^mailto:[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]/){
				$g_html{'link3_error'} .= qq(<div class="error">エラー：URLは半角英数で入力してください</div>);
				$flag = 0;
			}
		}
		if($g_html{'pdf1'} eq ''){
			if($g_html{'pdf1_text'} ne ''){
				$g_html{'pdf1_error'} = qq(<div class="error">エラー：PDFを選択してください</div>);
				$flag = 0;
			}
		}else{
			if($g_html{'pdf1_text'} eq ''){
				$g_html{'pdf1_error'} = qq(<div class="error">エラー：リンクテキストを入力してください</div>);
				$flag = 0;
			}
			$tmpPdfFile1 = $tmpFile . '001';
			($extensionPdf1, $flag) = &uploadPdf(param('pdf1'), $g_imageTmpDir, $tmpPdfFile1, 'pdf1');
		}
		if($g_html{'pdf2'} eq ''){
			if($g_html{'pdf2_text'} ne ''){
				$g_html{'pdf2_error'} = qq(<div class="error">エラー：PDFを選択してください</div>);
				$flag = 0;
			}
		}else{
			if($g_html{'pdf2_text'} eq ''){
				$g_html{'pdf2_error'} = qq(<div class="error">エラー：リンクテキストを入力してください</div>);
				$flag = 0;
			}
			$tmpPdfFile2 = $tmpFile . '002';
			($extensionPdf2, $flag) = &uploadPdf(param('pdf2'), $g_imageTmpDir, $tmpPdfFile2, 'pdf2');
		}
		if($g_html{'pdf3'} eq ''){
			if($g_html{'pdf3_text'} ne ''){
				$g_html{'pdf3_error'} = qq(<div class="error">エラー：PDFを選択してください</div>);
				$flag = 0;
			}
		}else{
			if($g_html{'pdf3_text'} eq ''){
				$g_html{'pdf3_error'} = qq(<div class="error">エラー：リンクテキストを入力してください</div>);
				$flag = 0;
			}
			$tmpPdfFile3 = $tmpFile . '003';
			($extensionPdf3, $flag) = &uploadPdf(param('pdf3'), $g_imageTmpDir, $tmpPdfFile3, 'pdf3');
		}
	}
	
	if($g_html{'name'} eq ''){
		$g_html{'name_error'} = qq(<div class="error">エラー：氏名を入力してください</div>);
		$flag = 0;
	}
	if($g_html{'email'} eq ''){
		$g_html{'email_error'} = qq(<div class="error">エラー：E-mailを入力してください</div>);
		$flag = 0;
	}else{
		if($g_html{'email'} !~ /^[\w\.\-\&]+\@[\w\.\-\&]+\.[\w\.\-\&]+$/){
			$g_html{'email_error'} = qq(<div class="error">エラー：E-mailアドレスの形式が不正です</div>);
			$flag = 0;
		}
	}
	
	if($flag == 0){
		my $detail = $g_html{'detail'};
		my $name = 'detail_' . $detail . '_check';
		$g_html{$name} = 'checked';
		$g_html{'n_year_select'} = &setYear($g_html{'n_year'});
		$g_html{'n_month_select'} = &setMonth($g_html{'n_month'});
		$g_html{'n_day_select'} = &setDay($g_html{'n_day'});
		
		my $titleLink = $g_html{'title_link'};
		$name = 'title_link_' . $titleLink . '_check';
		$g_html{$name} = 'checked';

		$g_html{'p_year_select'} = &setYear($g_html{'p_year'});
		$g_html{'p_month_select'} = &setMonth($g_html{'p_month'});
		$g_html{'p_day_select'} = &setDay($g_html{'p_day'});

		$g_html{'text'} =~ s/<br \/>/\n/g;
		$g_html{'comment'} =~ s/<br \/>/\n/g;
		$g_html{'text'} =~ s/<br>/\n/g;
		$g_html{'comment'} =~ s/<br>/\n/g;
		
		&outHtml($g_errorHtmlFile);
	}
}

############################################################
#    確認画面設定（画像など）
############################################################
sub makeConfirmHtml {
	my $file = $g_time{'year'} . $g_time{'mon'} . $g_time{'day'} . $g_time{'hour'} . $g_time{'min'} . $g_time{'sec'};
	
	$g_html{'hidden'} .= qq(<input type="hidden" name="file_name" value="$file">\n);
	
	if($g_html{'pdf'}){
		$tmpPdfFile = $file;
		($extensionPdf, $flag) = &uploadPdf(param('pdf'), $g_imageTmpDir, $tmpPdfFile, 'pdf');
		$tmpPdfFile = $tmpPdfFile . '.' . $extensionPdf;
		$tmpPdfFilePath = $g_imageTmpPath . $tmpPdfFile;
		$g_html{'hidden'} .= qq(<input type="hidden" name="pdf_name" value="$tmpPdfFile">\n);
		$g_html{'pdf_src'} = qq(<a href="$tmpPdfFilePath" target="_blank">PDF</a><br>\n);
	}

	if($g_html{'image1'}){
		$tmpFile1 = $file . '001';
		($extension1, $flag) = uploadImage(param('image1'), $g_imageTmpDir, $tmpFile1, 'image1');
		$tmpFile1 = $tmpFile1 . '.' . $extension1;
		$tmpFilePath1 = $g_imageTmpDir . $tmpFile1;
		($format1, $width1, $height1) = &GetImageSize($tmpFilePath1);
		$tmpFileUrl1 = $g_imageTmpPath . $tmpFile1;

		$key = $g_width / $width1;
		$height1 = int($height1 * $key);
		$width1 = $g_width;
		$g_html{'width1'} = $width1;
		$g_html{'height1'} = $height1;
		$g_html{'image1_src'} = qq(<div class="clr conf-img"><a href="$tmpFileUrl1" target="_blank"><img src="$tmpFileUrl1" width="$width1" height="$height1" alt="クリックして拡大" align="left" border="0"></a>$g_html{'image1_text'}</div>\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="width1" value="$width1">\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="height1" value="$height1">\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="image1_name" value="$tmpFile1">\n);
	}
	if($g_html{'image2'}){
		$tmpFile2 = $file . '002';
		($extension2, $flag) = uploadImage(param('image2'), $g_imageTmpDir, $tmpFile2, 'image2');
		$tmpFile2 = $tmpFile2 . '.' . $extension2;
		$tmpFilePath2 = $g_imageTmpDir . $tmpFile2;
		($format2, $width2, $height2) = &GetImageSize($tmpFilePath2);
		$tmpFileUrl2 = $g_imageTmpPath . $tmpFile2;

		$key = $g_width / $width2;
		$height2 = int($height2 * $key);
		$width2 = $g_width;
		$g_html{'width2'} = $width2;
		$g_html{'height2'} = $height2;
		$g_html{'image2_src'} = qq(<div class="clr conf-img"><a href="$tmpFileUrl2" target="_blank"><img src="$tmpFileUrl2" width="$width2" height="$height2" alt="クリックして拡大" align="left" border="0"></a>$g_html{'image2_text'}</div>\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="width2" value="$width2">\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="height2" value="$height2">\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="image2_name" value="$tmpFile2">\n);
	}
	if($g_html{'image3'}){
		$tmpFile3 = $file . '003';
		($extension3, $flag) = uploadImage(param('image3'), $g_imageTmpDir, $tmpFile3, 'image3');
		$tmpFile3 = $tmpFile3 . '.' . $extension3;
		$tmpFilePath3 = $g_imageTmpDir . $tmpFile3;
		($format3, $width3, $height3) = &GetImageSize($tmpFilePath3);
		$tmpFileUrl3 = $g_imageTmpPath . $tmpFile3;

		$key = $g_width / $width3;
		$height3 = int($height3 * $key);
		$width3 = $g_width;
		$g_html{'width3'} = $width3;
		$g_html{'height3'} = $height3;
		$g_html{'image3_src'} = qq(<div class="clr conf-img"><a href="$tmpFileUrl3" target="_blank"><img src="$tmpFileUrl3" width="$width3" height="$height3" alt="クリックして拡大" align="left" border="0"></a>$g_html{'image3_text'}</div>\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="width3" value="$width3">\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="height3" value="$height3">\n);
		$g_html{'hidden'} .= qq(<input type="hidden" name="image3_name" value="$tmpFile3">\n);
	}
	
	if($g_html{'link1'}){
		if($g_html{'link1_popup'}){
			$link1Popup = '（別ウィンドウで表示する）';
		}
		$g_html{'link1_src'} = qq(<a href="$g_html{'link1'}" target="_blank">$g_html{'link1_text'}</a>$link1Popup<br>\n);
	}
	if($g_html{'link2'}){
		if($g_html{'link2_popup'}){
			$link2Popup = '（別ウィンドウで表示する）';
		}
		$g_html{'link2_src'} = qq(<a href="$g_html{'link2'}" target="_blank">$g_html{'link2_text'}</a>$link2Popup<br>\n);
	}
	if($g_html{'link3'}){
		if($g_html{'link3_popup'}){
			$link3Popup = '（別ウィンドウで表示する）';
		}
		$g_html{'link3_src'} = qq(<a href="$g_html{'link3'}" target="_blank">$g_html{'link3_text'}</a>$link3Popup<br>\n);
	}
	
	if($g_html{'pdf1'}){
		$tmpPdfFile1 = $file . '001';
		($extensionPdf1, $flag) = &uploadPdf(param('pdf1'), $g_imageTmpDir, $tmpPdfFile1, 'pdf1');
		$tmpPdfFile1 = $tmpPdfFile1 . '.' . $extensionPdf1;
		$tmpPdfFilePath1 = $g_imageTmpPath . $tmpPdfFile1;
		$g_html{'hidden'} .= qq(<input type="hidden" name="pdf1_name" value="$tmpPdfFile1">\n);
		$g_html{'pdf1_src'} = qq(<a href="$tmpPdfFilePath1" target="_blank">$g_html{'pdf1_text'}</a><br>\n);
	}
	if($g_html{'pdf2'}){
		$tmpPdfFile2 = $file . '002';
		($extensionPdf2, $flag) = &uploadPdf(param('pdf2'), $g_imageTmpDir, $tmpPdfFile2, 'pdf2');
		$tmpPdfFile2 = $tmpPdfFile2 . '.' . $extensionPdf2;
		$tmpPdfFilePath2 = $g_imageTmpPath . $tmpPdfFile2;
		$g_html{'hidden'} .= qq(<input type="hidden" name="pdf2_name" value="$tmpPdfFile2">\n);
		$g_html{'pdf2_src'} = qq(<a href="$tmpPdfFilePath2" target="_blank">$g_html{'pdf2_text'}</a><br>\n);
	}
	if($g_html{'pdf3'}){
		$tmpPdfFile3 = $file . '003';
		($extensionPdf3, $flag) = &uploadPdf(param('pdf3'), $g_imageTmpDir, $tmpPdfFile3, 'pdf3');
		$tmpPdfFile3 = $tmpPdfFile3 . '.' . $extensionPdf3;
		$tmpPdfFilePath3 = $g_imageTmpPath . $tmpPdfFile3;
		$g_html{'hidden'} .= qq(<input type="hidden" name="pdf3_name" value="$tmpPdfFile3">\n);
		$g_html{'pdf3_src'} = qq(<a href="$tmpPdfFilePath3" target="_blank">$g_html{'pdf3_text'}</a><br>\n);
	}

	if($g_html{'detail'} eq '1'){
		$titleLink = $g_html{'title_link'};
		if($g_html{'title_link'} eq '1'){
			$titleLinkSrc = $g_titleLinkText[$titleLink];
		}elsif($g_html{'title_link'} eq '2'){
			$titleLinkSrc = $g_titleLinkText[$titleLink] . qq(：<a href="$g_html{'url'}" target="_blank">$g_html{'url'}</a>);
		}elsif($g_html{'title_link'} eq '3'){
			$titleLinkSrc = $g_titleLinkText[$titleLink] . '：' . $g_html{'pdf_src'};
		}
		
		$g_html{'html'} = <<HTML;
  <table class="style_a">
   <tr>
    <th nowrap>投稿日</th>
    <td>$g_html{'n_year'}/$g_html{'n_month'}/$g_html{'n_day'}</td>
   </tr>
   <tr>
    <th nowrap>記事タイトル</th>
    <td>$g_html{'title1'}</td>
   </tr>
   <tr>
    <th nowrap>タイトルからのリンク</th>
    <td>$titleLinkSrc</td>
   </tr>
  </table>
HTML
	}elsif($g_html{'detail'} eq '2'){
		$g_html{'html'} = <<HTML;
  <table class="style_a">
   <tr>
    <th nowrap>投稿日</th>
    <td>$g_html{'p_year'}/$g_html{'p_month'}/$g_html{'p_day'}</td>
   </tr>
   <tr>
    <th nowrap>記事タイトル</th>
    <td>$g_html{'title2'}</td>
   </tr>
   <tr>
    <th nowrap>本文</th>
    <td>$g_html{'text'}
    </td>
   </tr>
   <tr>
    <th nowrap>画像</th>
    <td>$g_html{'image1_src'}
        $g_html{'image2_src'}
        $g_html{'image3_src'}</td>
   </tr>
   <tr>
    <th nowrap>関連リンク</th>
    <td>$g_html{'link1_src'}
        $g_html{'link2_src'}
        $g_html{'link3_src'}</td>
   </tr>
   <tr>
    <th nowrap>PDF</th>
    <td>$g_html{'pdf1_src'}
        $g_html{'pdf2_src'}
        $g_html{'pdf3_src'}</td>
   </tr>
  </table>
HTML
	}
}

############################################################
#    CSV作成
############################################################
sub makeCsv {
	my $num;
	my ($csv,$csvAll);

	$g_html{'text'} =~ s/&lt;/</g;
	$g_html{'text'} =~ s/&gt;/>/g;
	
	if(-e $g_numberCsv){
		open(NUM, "<$g_numberCsv") or &error("$g_numberCsvが開けませんでした");
		flock(NUM,2);
		read(NUM, $num, -s NUM);
		close(NUM);
		
		$num ++;
	}else{
		$num = 1;
	}
	
	if(-e $g_dataCsv){
		open(CSV,"<$g_dataCsv") or &error("$g_dataCsvが開けませんでした");
		flock(CSV,2);
		read(CSV,$csvAll,-s CSV);
		close(CSV);
		
		&jcode::convert(\$csvAll,'euc');
	}
	
	$time = qq($g_time{'year'}$g_time{'mon'}$g_time{'day'}$g_time{'hour'}$g_time{'min'}$g_time{'sec'});
	$g_html{'n_year'} = sprintf("%04d", $g_html{'n_year'});
	$g_html{'n_month'} = sprintf("%02d", $g_html{'n_month'});
	$g_html{'n_day'} = sprintf("%02d", $g_html{'n_day'});
	$g_html{'p_year'} = sprintf("%04d", $g_html{'p_year'});
	$g_html{'p_month'} = sprintf("%02d", $g_html{'p_month'});
	$g_html{'p_day'} = sprintf("%02d", $g_html{'p_day'});
	
	if($g_html{'detail'} eq '1'){
		$title = $g_html{'title1'};
		$upTime = $g_html{'n_year'} . $g_html{'n_month'} . $g_html{'n_day'};
	}elsif($g_html{'detail'} eq '2'){
		$title = $g_html{'title2'};
		$upTime = $g_html{'p_year'} . $g_html{'p_month'} . $g_html{'p_day'};
	}
	
	$csv = join(',', $num, $time, $upTime, $g_html{'detail'}, $g_html{'title_link'}, $title, $g_html{'url'}, $g_html{'position'}, $g_html{'name'}, $g_html{'email'}, $g_html{'file_name'}, '0', '0');
	
	$csvAll = $csv . "\n" . $csvAll;
	
	&jcode::convert(\$csvAll, 'sjis');
	open(CSV, ">$g_dataCsv") or &error("$g_dataCsvが開けませんでした");
	flock(CSV, 2);
	print CSV $csvAll;
	close(CSV);
	
	$file = $g_dataDir . $num . '.csv';
	foreach $key(keys %g_html){
		$value = $g_html{$key};
		if($key ne 'hidden'){
			$masterCsv .= qq($key,$value\n);
		}
	}
	&jcode::convert(\$masterCsv, 'sjis');
	open(CSV, ">$file") or &error("$fileが開けませんでした");
	print CSV $masterCsv;
	close(CSV);

	open(NUM,">$g_numberCsv") or &error("$g_numberCsvが開けませんでした");
	flock(NUM,2);
	print NUM $num;
	close(NUM);
	
	if($g_html{'pdf_name'} ne ''){
		$tmpPdf = $g_imageTmpDir . $g_html{'pdf_name'};
		$newPdf = $g_pdfDir . $g_html{'pdf_name'};
		if(-e $tmpPdf){
			move($tmpPdf, $newPdf);
		}
	}
	if($g_html{'image1_name'} ne ''){
		$tmpImage = $g_imageTmpDir . $g_html{'image1_name'};
		$newImage = $g_imageDir . $g_html{'image1_name'};
		if(-e $tmpImage){
			move($tmpImage, $newImage);
		}
	}
	if($g_html{'image2_name'} ne ''){
		$tmpImage = $g_imageTmpDir . $g_html{'image2_name'};
		$newImage = $g_imageDir . $g_html{'image2_name'};
		if(-e $tmpImage){
			move($tmpImage, $newImage);
		}
	}
	if($g_html{'image3_name'} ne ''){
		$tmpImage = $g_imageTmpDir . $g_html{'image3_name'};
		$newImage = $g_imageDir . $g_html{'image3_name'};
		if(-e $tmpImage){
			move($tmpImage, $newImage);
		}
	}
	if($g_html{'pdf1_name'} ne ''){
		$tmpPdf = $g_imageTmpDir . $g_html{'pdf1_name'};
		$newPdf = $g_pdfDir . $g_html{'pdf1_name'};
		if(-e $tmpPdf){
			move($tmpPdf, $newPdf);
		}
	}
	if($g_html{'pdf2_name'} ne ''){
		$tmpPdf = $g_imageTmpDir . $g_html{'pdf2_name'};
		$newPdf = $g_pdfDir . $g_html{'pdf2_name'};
		if(-e $tmpPdf){
			move($tmpPdf, $newPdf);
		}
	}
	if($g_html{'pdf3_name'} ne ''){
		$tmpPdf = $g_imageTmpDir . $g_html{'pdf3_name'};
		$newPdf = $g_pdfDir . $g_html{'pdf3_name'};
		if(-e $tmpPdf){
			move($tmpPdf, $newPdf);
		}
	}

}

############################################################
#    HTML生成
############################################################
sub makeHtml{
	my $file = $g_html{'file_name'} . '.html';
	
	$g_html{'date'} = "$g_html{'p_year'}/$g_html{'p_month'}/$g_html{'p_day'}";
	
	if($g_html{'image1_name'}){
		$g_html{'image_src'} .= qq(<img src="$g_imagePath$g_html{'image1_name'}" alt="$g_html{'image1_text'}" width="$g_html{'width1'}" height="$g_html{'height1'}" />);
	}
	if($g_html{'image2_name'}){
		$g_html{'image_src'} .= qq(<img src="$g_imagePath$g_html{'image2_name'}" alt="$g_html{'image2_text'}" width="$g_html{'width2'}" height="$g_html{'height2'}" />);
	}
	if($g_html{'image3_name'}){
		$g_html{'image_src'} .= qq(<img src="$g_imagePath$g_html{'image3_name'}" alt="$g_html{'image3_text'}" width="$g_html{'width3'}" height="$g_html{'height3'}" />);
	}

	if($g_html{'link1'}){
		if($g_html{'link1_popup'}){
			$target1 = 'target="_blank"';
		}
		$linkSrc .= qq(<li class="mtx"><a href="$g_html{'link1'}" $target1>$g_html{'link1_text'}</a></li>\n);
	}
	if($g_html{'link2'}){
		if($g_html{'link2_popup'}){
			$target2 = 'target="_blank"';
		}
		$linkSrc .= qq(<li class="mtx"><a href="$g_html{'link2'}" $target2>$g_html{'link2_text'}</a></li>\n);
	}
	if($g_html{'link3'}){
		if($g_html{'link3_popup'}){
			$target3 = 'target="_blank"';
		}
		$linkSrc .= qq(<li class="mtx"><a href="$g_html{'link3'}" $target3>$g_html{'link3_text'}</a></li>\n);
	}
	if($linkSrc){
		$g_html{'link_src'} = <<HTML;
<h4><strong>関連リンク</strong></h4>

<ul class="listIconArrow">
$linkSrc
</ul>
HTML
	}
	
	if($g_html{'pdf1_name'}){
		$pdfSrc .= qq(<li class="mtx"><a href="$g_pdfPath$g_html{'pdf1_name'}">$g_html{'pdf1_text'}</a></li>);
	}
	if($g_html{'pdf2_name'}){
		$pdfSrc .= qq(<li class="mtx"><a href="$g_pdfPath$g_html{'pdf2_name'}">$g_html{'pdf2_text'}</a></li>);
	}
	if($g_html{'pdf3_name'}){
		$pdfSrc .= qq(<li class="mtx"><a href="$g_pdfPath$g_html{'pdf3_name'}">$g_html{'pdf3_text'}</a></li>);
	}
	if($pdfSrc){
		$g_html{'pdf_src'} = <<HTML;
<h4><strong>PDF</strong></h4>

<ul class="listIconPDF">
$pdfSrc
</ul>
HTML
	}
	
	$g_html{'text'} =~ s/<h5>/<\/p><h5>/gi;
	$g_html{'text'} =~ s/<\/h5>/<\/h5><p class\=\"mtx\">/gi;

	open(HTML,"<$g_templateFile") or &error("$g_templateFileが開けませんでした");
	flock(HTML,2);
	read(HTML, $html, -s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	
	foreach (@htmlList) {
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_ . "\n";
		}else{
			$htmlAll .= $_ . "\n";
		}
	}
	
	&jcode::convert(\$htmlAll, 'sjis');
	$file = $g_htmlDir . $file;
	open(HTML, ">$file") or &error("$fileが開けませんでした");
	print HTML $htmlAll;
	close(HTML);


}

############################################################
#    メール送信管理者宛
############################################################
sub sendMailAdmin{
	my $mailBody;
	my $sender;
	my $category = $g_html{'category'};
	
	$mailBody = <<MAIL;
To: $toAdd
From: $g_html{'email'}
Cc: $ccAdd
Bcc: $bccAdd
Subject: $subject
Content-Transfer-Encoding:7bit
Mime-Version:1.0
Content-Type:text/plain;charset=iso-2022-jp

$g_time{'year'}$g_time{'mon'}$g_time{'day'}$g_time{'hour'}$g_time{'min'} :received

$subject
-----
記事タイトル：$g_html{'title'}
所属：$g_html{'position'}
名前：$g_html{'name'}
コメント：$g_html{'comment'}
-----

投稿内容を確認の上、承認作業を行ってください。
http://www.issjp.com/admin/index.cgi
MAIL

	&jcode::convert(\$mailBody,'jis');
	open(M,"|$sendmail");
	print M $mailBody;
	close(M);

}

############################################################
#    時刻設定
############################################################
sub setTime{
	my ($sec,$min,$hour,$day,$mon,$year,$wday) = localtime();
	
	$g_time{'sec'} = sprintf("%02d", $sec);
	$g_time{'min'} = sprintf("%02d", $min);
	$g_time{'hour'} = sprintf("%02d", $hour);
	$g_time{'day'} = sprintf("%02d", $day);
	$g_time{'mon'} = sprintf("%02d", $mon+1);
	$g_time{'year'} = sprintf("%04d", $year+1900);
	$g_time{'all'} = "$g_time{'year'}/$g_time{'mon'}/$g_time{'day'} $g_time{'hour'}:$g_time{'min'}:$g_time{'sec'}";
	$g_time{'utc'} = time;
}

############################################################
#    HTML出力
############################################################
sub outHtml{
	my $file = shift;
	my ($html,@htmlList,$htmlAll);
	my @tmp;
	
	open(HTML,"<$file") or &error("$fileが開けませんでした");
	read(HTML,$html,-s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	
	foreach (@htmlList) {
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_ . "\n";
		}else{
			$htmlAll .= $_ . "\n";
		}
	}
	
	&jcode::convert(\$htmlAll,'sjis');
	print "Content-type:text/html; charset=Shift_JIS\n\n";
	print $htmlAll;
	
	&fileunlock();
	exit;
}

############################################################
#    システムエラー出力
############################################################
sub error{
	$g_html{'error'} = shift;
	my ($html,@htmlList,$htmlAll);
	my @tmp;
	
	open(HTML,"<$g_sysErrorHtmlFile");
	read(HTML,$html,-s HTML);
	close(HTML);
	
	&jcode::convert(\$html,'euc');
	@htmlList = split(/\n/,$html);
	
	foreach (@htmlList) {
		if(/(<!--'(.+)'-->)/){
			$tmp[1] = $1;
			$tmp[2] = $g_html{$2};
			s/$tmp[1]/$tmp[2]/gi;
			$htmlAll .= $_ . "\n";
		}else{
			$htmlAll .= $_ . "\n";
		}
	}
	
	&jcode::convert(\$htmlAll,'sjis');
	print "Content-type:text/html; charset=Shift_JIS\n\n";
	print $htmlAll;
	
	unless($lockerr){
		&fileunlock();
	}
	exit;
}

############################################################
#    selectのyearをセット
############################################################
sub setYear{
	my $set = shift;
	my $optionYear;
	
	for($i=$g_time{'year'}-1;$i<=$g_time{'year'}+5;$i++){
		if($i eq $set){
			$optionYear .= qq(<option value="$i" selected>$i</option>\n);
		}else{
			$optionYear .= qq(<option value="$i">$i</option>\n);
		}
	}
	
	return $optionYear;
}

############################################################
#    selectのmonthをセット
############################################################
sub setMonth{
	my $set = shift;
	my $optionMonth;
	
	for($i=1;$i<=12;$i++){
		if($i eq $set){
			$optionMonth .= qq(<option value="$i" selected>$i</option>\n);
		}else{
			$optionMonth .= qq(<option value="$i">$i</option>\n);
		}
	}
	
	return $optionMonth;
}

############################################################
#    selectのdayをセット
############################################################
sub setDay{
	my $set = shift;
	my $optionDay;
	
	for($i=1;$i<=31;$i++){
		if($i eq $set){
			$optionDay .= qq(<option value="$i" selected>$i</option>\n);
		}else{
			$optionDay .= qq(<option value="$i">$i</option>\n);
		}
	}
	
	return $optionDay;
}

############################################################
#    ロック
############################################################
sub lock{
	my $count = 0;
	
	while( !symlink("$$", $g_lockFile) ){
		if( $count == 5 ){
			$lockerr = 1;
			&error("ただいまアクセスが集中しています。");
		}
		$count ++;
		sleep(1);
	}
}

############################################################
#    ロック解除
############################################################
sub fileunlock{
#	if(-e $g_lockFile){
		unlink($g_lockFile);
#	}
}

############################################################
#    画像アップロード
############################################################
sub uploadImage {
	my ($localPath,$imagePath,$fileName,$field) = @_;
	my (@imageList,$image,$imageData,$extension,$tmp);
	my $tmpImage;
	my $errorFile = $g_html{'file'};
	my $eFlag = 1;
	$errorFile = $g_errorHtmlFile{$errorFile};
	$field = $field . '_error';
	
	@imageList = split(/\\/, $localPath);
	$tmp = @imageList;
	$tmp--;
	$image = $imageList[$tmp];
	$image =~ s!\s!!g;
	@extensionTmp = split(/\./,$image);
	$extension = pop(@extensionTmp);
	
	if($extension =~ /gif/i or $extension =~ /jpe?g?/i or $extension =~ /png/i){
		$fileName = $imagePath . $fileName . '.' . $extension;
		$g_html{$field} = qq(<span class="must">画像アップロード失敗</span><br>);
		open(OUT,">$fileName") or &outHtml($errorFile);
		seek($localPath, 0, 0);
		while( read($localPath, $imageData, 1024, ) ){
			print OUT $imageData;
			$tmpImage .= $imageData;
		}
		close(OUT);
		if(length($tmpImage) > 2*1024*1024){
			$g_html{$field} .= qq(<div class="error">エラー：ファイルサイズは2MB以内にしてください</div>);
		}
		if(length($tmpImage) <= 0){
			$g_html{$field} = qq(<div class="error">エラー：ファイルタイプが不正です（アップロード可能：.gif/.jpg/.jpe/.png形式）</div>);
			$eFlag = 0;
		}
	}else{
		$g_html{$field} .= qq(<div class="error">エラー：ファイルタイプが不正です。（アップロード可能：.gif/.jpg/.jpe/.png形式）</div>);
		$eFlag = 0;
	}
	
	return($extension, $eFlag);
}

############################################################
#    PDFアップロード
############################################################
sub uploadPdf {
	my ($localPath,$imagePath,$fileName,$field) = @_;
	my (@imageList,$image,$imageData,$extension,$tmp);
	my $tmpImage;
	my $errorFile = $g_html{'file'};
	my $eFlag = 1;
	$errorFile = $g_errorHtmlFile{$errorFile};
	$field = $field . '_error';
	
	@imageList = split(/\\/, $localPath);
	$tmp = @imageList;
	$tmp--;
	$image = $imageList[$tmp];
	$image =~ s!\s!!g;
	@extensionTmp = split(/\./,$image);
	$extension = pop(@extensionTmp);
	
	if($extension =~ /pdf/i){
		$fileName = $imagePath . $fileName . '.' . $extension;
		$g_html{$field} = qq(<span class="must">PDFアップロード失敗</span><br>);
		open(OUT,">$fileName") or &outHtml($errorFile);
		seek($localPath, 0, 0);
		while( read($localPath, $imageData, 1024, ) ){
			print OUT $imageData;
			$tmpImage .= $imageData;
		}
		close(OUT);
		if(length($tmpImage) > 2*1024*1024){
			$g_html{$field} .= qq(<div class="error">エラー：ファイルサイズは2MB以内にしてください</div>);
		}
		if(length($tmpImage) <= 0){
			$g_html{$field} .= qq(<div class="error">エラー：ファイルタイプが不正です。（アップロード可能：.pdf形式）</div>);
			$eFlag = 0;
		}
	}else{
		$g_html{$field} .= qq(<div class="error">エラー：ファイルタイプが不正です。（アップロード可能：.pdf形式）</div>);
		$eFlag = 0;
	}
	
	return($extension, $eFlag);
}

############################################################
#    画像サイズ取得
############################################################
sub GetImageSize{
    my ( $IMG, $in ) = @_;
    my ( %SHT, %LNG );
    my ( $buf, $mark, $type, $f_size, $width, $height );
    my ( $TAG, $TYPE, $COUNT, $V_OFFSET, $PK, $ENTRY, $Exif_IFD );
    my ( $endian, $dummy1, $dummy2, $dummy, $EOI, $APP1, $length, $exif );
    my ( $format, $offset, $line, $CODE, $jfif );
    my @TGA;
    my $ntag;

    # 定数
    $mark = pack("C", 0xff);
    %SHT = ( 'II' => 'v', 'MM' => 'n' );
    %LNG = ( 'II' => 'V', 'MM' => 'N' );

    # 初期値
    $endian   = '';
    $width    = -1;
    $height   = -1;
    $format   = '';
    $Exif_IFD = -1;

    if( $in eq '' ){
	$in = 'IMG';
    }

    open( $in, $IMG ) || return( '', -1, -1 );

    binmode($in);
    seek( $in, 0, 0 );
    read( $in, $buf, 6 );

    # GIF 形式
    if($buf =~ /^GIF/i){
	$format = 'GIF';
	read( $in, $buf, 2 );
	$width  = unpack("v*", $buf);
	read( $in, $buf, 2);
	$height = unpack("v*", $buf);

    # Windows Bit Map 形式
    }elsif($buf =~ /BM/){
	$format = 'BMP';
	seek( $in, 12, 1 );
	read( $in, $buf, 8 );
	($width, $height) = unpack("VV", $buf);

    # TIFF 形式
    }elsif( $buf =~ /(II)/ || $buf =~ /(MM)/ ){
	$format = 'TIFF';
	$endian = $1;
	seek( $in, 0, 0 );
	read( $in, $buf, 8 );
	( $endian, $dummy1, $offset ) = 
	    unpack( "A2$SHT{$endian}$LNG{$endian}", $buf );

	seek( $in, $offset, 0 );
	read( $in, $buf, 2 );
	$ENTRY = unpack( $SHT{$endian}, $buf );

	for( $i = 0 ; $i < $ENTRY ; $i++ ){
	    read( $in, $buf, 8 );
	    $PK = "$SHT{$endian}$SHT{$endian}$LNG{$endian}";
	    ( $TAG, $TYPE, $COUNT ) = unpack( $PK, $buf );

	    read( $in, $buf, 4 );
	    ( $TAG != 256 && $TAG != 257 ) and next;
	    if( $TYPE == 3 ){
		$PK = "$SHT{$endian}";
	    }elsif( $TYPE == 4 ){
		$PK = "$LNG{$endian}";
	    }else{
		next;
	    }

	    $V_OFFSET = unpack( $PK, $buf );

	    # Image width and height
	    ( $TAG == 256   ) and ( $width  = $V_OFFSET   );
	    ( $TAG == 257   ) and ( $height = $V_OFFSET   );
	    ( $TAG == 34665 ) and ( $format .= '-EXIF'    );
	}

    # PPM 形式
    }elsif( $buf =~ /^(P[123456])\n/ ){
	if( $1 eq 'P1' || $1 eq 'P4' ){
	    $format = 'PBM';
	}elsif( $1 eq 'P2' || $1 eq 'P5' ){
	    $format = 'PGM';
	}else{
	    $format = 'PPM';
	}
	seek( $in, 0, 0 );
	<$in>;
	while( <$in> ){
	    next if ( /^\#/ );
	    chomp;
	    ( $width, $height ) = split( /\s+/, $_ );
	    last;
	}

    # PNG 形式
    }elsif( $buf =~ /PNG/){
	$format = 'PNG';
	seek( $in, 8, 0 );

	while(1){
	    read( $in, $buf, 8 );
	    ( $offset, $CODE ) = unpack( "NA4", $buf );

	    if( $CODE eq 'IHDR' ){
		read( $in, $buf, 8 );
		( $width, $height ) = unpack( "NN", $buf );
		seek( $in, $offset-8+4, 1 );
		last;

	    }elsif( $CODE eq 'IEND' ){
		last;

	    }else{
		seek( $in, $offset+4, 1 );
	    }
	}

    }else{
	# JPEG 形式
	seek( $in, 0, 0 );
	read( $in, $buf, 2 );
	( $buf, $type ) = unpack("C*", $buf );
	if( $buf == 0xFF && $type == 0xD8 ){
	    $format = 'JPEG';
	  JPEG:while(read( $in, $buf, 1 )){
	      if(($buf eq $mark) && read( $in, $buf, 3 )){
		  $type   = unpack("C*", substr($buf, 0, 1));
		  $f_size = unpack("n*", substr($buf, 1, 2));

		  ( $type == 0xD9 ) and ( last JPEG );
		  ( $type == 0xDA ) and ( last JPEG );

		  if($type == 0xC0 || $type == 0xC2){
		      read( $in, $buf, $f_size-2 );
		      $height = unpack("n*", substr($buf, 1, 2));
		      $width  = unpack("n*", substr($buf, 3, 2));
		      ( $format =~ /EXIF/ ) and ( last JPEG );

		  }elsif( $type == 0xE1 ){
		      read( $in, $buf, $f_size-2 );
		      $exif = unpack( "A4", substr( $buf, 0, 4 ) );
		      if( $exif =~ /exif/i ){
			  $format .= '-EXIF';
			  ( $width > 0 && $height > 0 ) and ( last JPEG );
		      }

		  }elsif( $type == 0xE0 ){
		      read( $in, $buf, $f_size-2 );
		      $jfif = unpack( "A4", substr( $buf, 0, 4 ) );
		      if( $jfif =~ /jfif/i ){
			  $format .= '-JFIF';
		      }

		  }elsif( $type2 == 0x01 || 
			  ( $type2 >= 0xD0 && $type2 < 0xD9 ) ){
		      seek( $in, -2, 1 );

		  }else{
		      read( $in, $buf, $f_size-2 );
		  }
	      }
	  }
	}

	if( $width > 0 && $height > 0 ){
	    close( $in );
	    return( $format, $width, $height );
	}

	# TGA 形式
	seek( $in, 0, 0 );
	read( $in, $buf, 18 );
	@TGA = unpack( "CCCvvCvvvvCC", $buf );
	if( $TGA[1] == 0 || $TGA[1] == 1 ){
	    if( $TGA[2] ==  0 || $TGA[2] == 1 || $TGA[2] ==  2 ||
		$TGA[2] ==  3 || $TGA[2] == 9 || $TGA[2] == 10 ||
		$TGA[1] == 11 ){
		$format = 'TGA';
		$width  = $TGA[8];
		$height = $TGA[9];
	    }
	}

    }

    close( $in );
    return( $format, $width, $height );
}

1;