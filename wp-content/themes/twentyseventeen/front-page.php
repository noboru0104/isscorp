<?php get_header(); ?>

<?php 
//var_dump(excerpt_more());
?>

<section>
	<div class="l-slider">
        <div class="l-slider-inner">
            <ul class="bxslider">
                <li><a href=""><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_01_pc.png" alt=""></a></li>
                <li><a href=""><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_02_pc.png" alt=""></a></li>
                <li><a href=""><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_03_pc.png" alt=""></a></li>
                <li><a href=""><img class="is-imgChange2" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_04_pc.png" alt=""></a></li>                
            </ul>
        </div>
    </div>
</section>

<!--<section>
    <div class="l-mainVisual">
        <div class="l-mainVisual-inner">
            <div>
            	<img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv_pc.png" alt="">
            </div>
        </div>
    </div>
</section>-->
  
<div class="l-wrapper">

	<div class="l-interpretationBlock">
    	<div class="l-inner">
            <div class="l-interpretationBlock02">
            	<div class="l-interpretationBlock02-table">
                	<div class="l-interpretationBlock02-cell">
                    	<div>
                            <p class="p-title pconly">通訳</p>
                            <p class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_01_pc.png" alt=""></p>
                            <p class="p-message">国際会議、社内会議、商談、展示会、各種イベント、電話会議など、グローバル企業のあらゆるビジネスシーンで必要となる同時または</p>
                            <p class="p-all"><a href="<?php echo home_url();?>/tsuyaku/">詳しく見る</a></p>
                    	</div>
                    </div>
                    <div class="l-interpretationBlock02-cell">
                    	<div>
                            <p class="p-title pconly">コンベンション</p>
                            <p class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_02_pc.png" alt=""></p>
                            <p class="p-message">国内外の数多くの国際会議の総合的企画・運営業務を主に、全般にわたり幅広いサポートサービスを展開しています。国内外の数多くの国際会議の総合的企画・運営業務を主に</p>
                            <p class="p-all"><a href="<?php echo home_url();?>/kokusai/">詳しく見る</a></p>
                    	</div>
                    </div>
                    <div class="l-interpretationBlock02-cell">
                    	<div>
                            <p class="p-title pconly">人材サービス</p>
                            <p class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_03_pc.png" alt=""></p>
                            <p class="p-message">語学スペシャリストを多く擁し、質の高い人材派遣・紹介予定派遣サービスを提供しております。通訳・翻訳を中心に、秘書や英文事務など高度な技能・経験を備えたスタッフを派遣しています。</p>
                            <p class="p-all"><a href="<?php echo home_url();?>/service/">詳しく見る</a></p>
                    	</div>
                    </div>
                </div>
            </div>
            <div class="l-interpretationBlock02">
            	<div class="l-interpretationBlock02-table">
                	<div class="l-interpretationBlock02-cell">
                    	<div>
                            <p class="p-title pconly">翻訳</p>
                            <p class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_04_pc.png" alt=""></p>
                            <p class="p-message">マニュアルなどの技術文書、法律、医薬、金融関連の専門文書など、お客様の様々な翻訳ニーズにお応えします。マニュアルなどの技術文書、法律、医薬、金融関連の専門文書など、お客様の様々な翻訳ニーズにお応えします。</p>
                            <p class="p-all"><a href="<?php echo home_url();?>/translation/">詳しく見る</a></p>
                    	</div>
                    </div>
                    <div class="l-interpretationBlock02-cell">
                    	<div>
                            <p class="p-title pconly">通訳者・翻訳者養成スクール</p>
                            <p class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_service_05_pc.png" alt=""></p>
                            <p class="p-message">ISSの通訳者・翻訳者養成コースの授業は、常に実践を視野に入れた訓練を主体としています。独自のカリキュラムと訓練方法は、国際的な実践の場で対応できる能力養成に重点を置いています。</p>
                            <p class="p-all"><a href="<?php echo home_url();?>/training/">詳しく見る</a></p>
                    	</div>
                    </div>
                    <div class="l-interpretationBlock02-cell p-middle">
                    	<p class="p-honyaku"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_honyaku_pc.png" alt=""></p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="l-newsBlock">
    	<div class="l-inner">
            <div class="l-newsBlock01">
            	<div class="l-newsBlock01-table">
                	<p>News&nbsp;&amp;&nbsp;Topics</p>
                    <p><a href="<?php echo home_url();?>/news/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_news_all_pc.png" alt=""></a></p>
                </div>
            </div>
            <div class="l-newsBlock02">
            	<a href="<?php echo home_url();?>/news/detail.html">
                    <div class="l-newsBlock02-table">                        
                        <div class="p-date">
                            <p>2016.10.27</p>
                        </div>
                        <div class="p-category">
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category01_pc.png" alt=""></p>
                        </div>
                        <div class="p-message">
                            <p>【ISSインスティテュート】2016[秋期]レギュラーコースのお申込み受付を開始しました!</p>
                        </div>
                    </div>
                </a>
                <a href="<?php echo home_url();?>/news/detail.html">
                    <div class="l-newsBlock02-table">                        
                        <div class="p-date">
                            <p>2016.10.01</p>
                        </div>
                        <div class="p-category">
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category02_pc.png" alt=""></p>
                        </div>
                        <div class="p-message">
                            <p>世界最大規模の国際会議「第99回ライオンズクラブ世界大会」の運営を担当しました</p>
                        </div>
                    </div>
                </a>
                <a href="<?php echo home_url();?>/news/detail.html">
                    <div class="l-newsBlock02-table">                        
                        <div class="p-date">
                            <p>2016.08.25</p>
                        </div>
                        <div class="p-category">
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category03_pc.png" alt=""></p>
                        </div>
                        <div class="p-message">
                            <p>【採用情報】通訳サービスのコーディネーター募集</p>
                        </div>
                    </div>
                </a>
                <a href="<?php echo home_url();?>/news/detail.html">
                    <div class="l-newsBlock02-table">                        
                        <div class="p-date">
                            <p>2016.08.06</p>
                        </div>
                        <div class="p-category">
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category04_pc.png" alt=""></p>
                        </div>
                        <div class="p-message">
                            <p>「オバマ米国大統領広島訪問接遇」一式の運営を担当しました</p>
                        </div>
                    </div>
                </a>
                <a href="<?php echo home_url();?>/news/detail.html">
                    <div class="l-newsBlock02-table">                        
                        <div class="p-date">
                            <p>2016.07.01</p>
                        </div>
                        <div class="p-category">
                            <p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_news_category05_pc.png" alt=""></p>
                        </div>
                        <div class="p-message">
                            <p>【ISSインスティテュート】2016[秋期]レギュラーコースのお申込み受付を開始しました!</p>
                        </div>
                    </div>
                </a>
            </div>
    	</div>
    </div>
    
    <div class="l-inquiryBlock">
    	<div class="l-inner">
            <div class="l-inquiryBlock-table">
                <div class="p-inquiry">
                    <div class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/bg_inquiry_02_pc.png" alt=""></div>
                    <div class="p-button"><a href="<?php echo home_url();?>"><img class="js-btn_inquiry is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_inquiry_01_pc.png" alt=""></a></div>
                </div>
                <div class="p-registration">
                    <div class="p-photo"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/bg_registration_01_pc.png" alt=""></div>
                    <div class="p-button"><a href="<?php echo home_url();?>"><img class="js-btn_registration is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_registration_01_pc.png" alt=""></a></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="l-bottomBlock">
    	<div class="l-inner">
        	<div class="l-bottomBlock01">
        		<p class="p-bottomTitle">加盟団体</p>
            </div>
            <ul>
            	<li><a href="http://www.iccaworld.org/" target="_blank"><img class="p-icca" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_icca.png" alt=""></a></li>
                <li><a href="http://www.jccb.or.jp/" target="_blank"><img class="p-jccb" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jccb.png" alt=""></a></li>
                <li><a href="http://www.jp-cma.org/" target="_blank"><img class="p-jcma" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jcma.png" alt=""></a></li>
                <li><a href="http://www.fukuoka-dc.jpn.com/" target="_blank"><img class="p-fdc" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_fdc.png" alt=""></a></li>
                <li><a href="http://www.jtf.jp/" target="_blank"><img class="p-jtc" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jtc.png" alt=""></a></li>
                <li><a href="http://www.jassa.jp/" target="_blank"><img class="p-jsa" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_bottom_jsa.png" alt=""></a></li>
            </ul>
            <div class="l-bottomBlock02">
                <a href="<?php echo home_url();?>">公益財団法人 東京観光財団</a><a href="<?php echo home_url();?>">公益財団法人 横浜観光コンベンション・ビューロー</a><br><a href="http://www.ccb.or.jp/mice/" target="_blank">公益財団法人 ちば国際コンベンションビューロー</a><a href="<?php echo home_url();?>">公益財団法人 高松観光コンベンション・ビューロー</a>
            </div>
        </div>
    </div>
    
</div>

<?php get_footer(); ?>