<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>アイ・エス・エスについて</h3>
    </div>
    <div class="p-side-menu-list">
        <p><a href="<?php echo home_url();?>/about/message">トップメッセージ</a></p>
        <p><a href="<?php echo home_url();?>/about/company">会社概要</a></p>
        <!-- <p><a href="<?php echo home_url();?>/about/pdf_dl">会社案内ダウンロード</a></p> -->
        <p><a href="<?php echo home_url();?>/about/history">沿革</a></p>
        <p><a href="<?php echo home_url();?>/about/mission">企業理念</a></p>
        <p><a href="<?php echo home_url();?>/about/csr">CSR</a></p>
        <!-- <p><a href="<?php echo home_url();?>/about/interview/">創業50周年記念インタビュー</a></p> -->
    </div>
</div>
