<?

$pagecheck = "0";

if(getenv("HTTP_REFERER") == "https://www.issjp.com/honyaku/inquiry/visa_form.html"){$pagecheck = "1";}
if(getenv("HTTP_REFERER") == "https://202.212.213.210/honyaku/inquiry/visa_form.html"){$pagecheck = "1";}

//テスト用
if(getenv("HTTP_REFERER") == "https://www.issjp.com/honyaku_test/inquiry/visa_form.html"){$pagecheck = "1";}

if($pagecheck == "0"){
	header("Location: https://www.issjp.com/honyaku/inquiry/visa_form.html");
	exit;
}

if($_POST['agree'] == ""){
	header("Location: https://www.issjp.com/honyaku/inquiry/visa_form.html");
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-JP" />
<meta name="description" content="ビザ申請書類の翻訳見積依頼を受け付けております。ISSは英国ビザ申請センターの翻訳会社リスト「Translation Agents List」に掲載されています。" />
<meta name="keywords" content="ビザ,翻訳,見積,証明書,翻訳会社,英国ビザ,ISS" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<title>ビザ申請書類の翻訳見積依頼 | ISS | 翻訳サービス内容詳細</title>
<link href="../../common/default.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../../common/print.css" rel="stylesheet" type="text/css" media="print" />
<script type="text/javascript" src="../../common/script.js"></script>
<script type="text/javascript" src="../../form/_js/jquery.js"></script>
<script type="text/javascript" src="../../form/_js/sb/adapter/shadowbox-jquery.js"></script>
<script type="text/javascript" src="../../form/_js/sb/shadowbox.js"></script>
<script type="text/javascript">
<!--
Shadowbox.loadSkin('classic', '../../form/_js/sb/skin'); Shadowbox.loadLanguage('en', '../../form/_js/sb/lang');
 Shadowbox.loadPlayer(['img', 'swf', 'flv', 'qt', 'wmp', 'iframe', 'html'], '../../form/_js/sb/player'); 
 $(document).ready(function() {
 	Shadowbox.init();
	});
//-->
</script></head>
<body onload="initRollovers();">
<a name="pageTop" id="pageTop"></a>

<!-- Header Begin -->
<div id="header">
<div class="content"><a href="../../index.html"><img src="../../common/images/h_logo_l.jpg" alt="ISS" name="logo" id="logo_l" width="55" height="50" /></a><a href="../../index.html"><img src="../../common/images/h_logo_r.jpg" name="logo" id="logo_r" alt="株式会社アイ・エス・エス" width="235" height="18" /></a><h1 class="logo_text">通訳、翻訳、国際会議、人材派遣、法人研修／ISS</h1>

<!-- Header Navigation Begin -->
<div id="headerNavi">
<div class="searchBox">
<script type="text/javascript" src="../../search_s.js"></script>
</div>
<div class="hMenuBtn">
  <table cellspacing="0" summary="HeaderNavigation">
    <tr>
      <td><a href="http://www.issjp.com/index.html"><img src="../../common/images/hNavi_btn_05.gif" alt="HOME" width="57" height="20" /></a></td>
						<td><a href="http://www.issjp.com/inquiry/index.html"><img src="../../common/images/hNavi_btn_01.gif" alt="お問い合わせ" width="81" height="20" /></a></td>
      <td><a href="http://www.issjp.com/access/index.html"><img src="../../common/images/hNavi_btn_02.gif" alt="アクセスマップ" width="91" height="20" /></a></td>
      <td><a href="http://www.issjp.com/sitemap/index.html"><img src="../../common/images/hNavi_btn_03.gif" alt="サイトマップ" width="81" height="20" /></a></td>
      <td><a href="http://www.issjp.com/en/index.html"><img src="../../common/images/hNavi_btn_04.gif" alt="ENGLISH" width="65" height="20" border="0" /></a></td>
    </tr>
  </table>
</div>
</div>
<!-- Header Navigation End -->
</div>
</div>
<!-- Header End -->

<!-- Global Navi Begin -->
<div id="globalNavi">
<div class="content">
<div class="menuList">
<ul>
<li class="bt01"><a href="http://www.issjp.com/about/index.html"><img src="../../common/images/gNavi_btn_01.jpg" alt="ISSグループとは" width="163" height="47" class="btn" /></a></li><li class="bt02"><a href="http://www.issjp.com/company/index.html"><img src="../../common/images/gNavi_btn_02.jpg" alt="アイ・エス・エス企業情報" width="163" height="47" class="btn" /></a></li>
<li class="bt03"><a href="http://www.issjp.com/service/index.html"><img src="../../common/images/gNavi_btn_03_current.jpg" alt="サービス案内" width="163" height="47" /></a></li><li class="bt04"><a href="http://www.issjp.com/recruit/index.html"><img src="../../common/images/gNavi_btn_04.jpg" alt="採用情報" width="163" height="47" class="btn" /></a></li><li class="bt05"><a href="http://www.issjp.com/publishing/index.html"><img src="../../common/images/gNavi_btn_05.jpg" alt="出版物紹介" width="164" height="47" class="btn" /></a></li>
</ul>
</div>
<h1><img src="../../service/images/h1.jpg" alt="サービス案内" width="816" height="90" /></h1>
</div>
</div>
<!-- Global Navi End -->

<!-- Main Content Begin -->
<div id="colum2">
<div class="content">

<!-- BreadList Begin -->
<div id="breadList">
<table cellspacing="0" summary="breadList"><tr>
<td><img src="../../common/images/breadListLeft.jpg" alt="" width="211" height="30" /></td>
<td nowrap="nowrap"><p class="s"><a href="http://www.issjp.com/index.html">HOME</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/service/index.html">サービス案内</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/honyaku/index.html">翻訳</a><span>&nbsp; &gt; &nbsp;</span><a href="http://www.issjp.com/honyaku/service.html">サービス内容詳細</a><span>&nbsp; &gt; &nbsp;</span>ビザ申請書類の翻訳見積依頼</p></td>
</tr></table>
</div>
<!-- BreadList End -->

<!-- Colum Left Begin -->
<div id="columLeft">
<div class="sMenu">
<ul>
<li><a href="http://www.issjp.com/tsuyaku/index.html"><img src="../../service/images/sMenu_btn_02.jpg" alt="通訳" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/kokusai/index.html"><img src="../../service/images/sMenu_btn_01.jpg" alt="国際会議企画・運営" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/honyaku/index.html"><img src="../../service/images/sMenu_btn_03_current.jpg" alt="翻訳" width="211" height="28" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/service.html"><img src="../images/sMenu_btn_04_02.jpg" alt="サービス内容詳細" width="211" height="18" class="btn" /></a></li>

<li class="third"><a href="http://www.issjp.com/honyaku/visa.html"><img src="../images/sMenu_btn_04_02_01_current.jpg" alt="英国ビザ申請書類の翻訳" width="211" height="18" /></a></li>

<li class="second"><a href="http://www.issjp.com/honyaku/flow.html"><img src="../images/sMenu_btn_04_03.jpg" alt="サービスの流れ" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/outline.html"><img src="../images/sMenu_btn_04_01.jpg" alt="サービスの体制" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/faq.html"><img src="../images/sMenu_btn_04_04.jpg" alt="FAQ" width="211" height="18" class="btn" /></a></li>
<li class="second"><a href="http://www.issjp.com/honyaku/inquiry/index.html"><img src="../images/sMenu_btn_04_05.jpg" alt="問い合わせ先、地図" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="http://www.issjp.com/honyaku/inquiry/juridical.html"><img src="../images/sMenu_btn_04_05_01.jpg" alt="法人のお客様" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="http://www.issjp.com/honyaku/inquiry/individual.html"><img src="../images/sMenu_btn_04_05_04.jpg" alt="個人のお客様" width="211" height="18" border="0" class="btn" /></a></li>
<li class="third"><a href="https://www.issjp.com/honyaku/inquiry/honyaku_form.html"><img src="../images/sMenu_btn_04_05_02.jpg" alt="翻訳者の登録について" width="211" height="18" border="0" /></a></li>
<li class="space"><img src="../../common/images/pixel_white.gif" alt="" width="211" height="8" /></li>
<!-- li><a href="http://www.issjp.com/honyaku/localization.html"><img src="../../service/images/sMenu_btn_04.jpg" alt="ローカライゼーション" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/haken/index.html"><img src="../../service/images/sMenu_btn_05.jpg" alt="人材派遣・紹介予定派遣" width="211" height="28" class="btn" /></a></li>
<!-- li><a href="http://www.issjp.com/haken/referral.html"><img src="../../service/images/sMenu_btn_06.jpg" alt="紹介予定派遣" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/corptre/index.html"><img src="../../service/images/sMenu_btn_07.jpg" alt="法人向け語学研修" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/chinese/index.html"><img src="../../chinese/images/sMenu_btn_12.jpg" alt="中国語サービス" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/service/resources.html"><img src="../../service/images/sMenu_btn_08.jpg" alt="人材紹介" width="211" height="28" class="btn" /></a></li>
<li><a href="http://www.issjp.com/service/training.html"><img src="../../service/images/sMenu_btn_09.jpg" alt="通訳者・翻訳者養成" width="211" height="28" class="btn" /></a></li>
<!-- li><a href="http://www.issjp.com/service/com_training.html"><img src="../../service/images/sMenu_btn_10.jpg" alt="法人向け語学研修" width="211" height="28" class="btn" /></a></li -->
<li><a href="http://www.issjp.com/service/network.html"><img src="../../service/images/sMenu_btn_11.jpg" alt="在ロサンゼルス米国現地法人" width="211" height="28" class="btn" /></a></li>
<li><img src="../../common/images/sMenu_bottom.jpg" alt="" width="211" height="30" /></li>
</ul>
</div>


<img src="../../common/images/pixel_trans.gif" alt="" width="211" height="20" class="spacer" />

<div class="infoBox">
<img src="../../common/images/infoBoxTitle.jpg" alt="お問い合わせ情報" width="193" height="26" />
<table class="fix"><tr>
<td class="ac"><a href="http://www.issjp.com/inquiry/index.html"><img src="../../common/images/infoBtn_01.jpg" alt="お問い合わせ窓口一覧" width="177" height="33" border="0" /></a></td>
</tr><tr>
<td class="ac"><a href="http://www.issjp.com/access/index.html"><img src="../../common/images/infoBtn_02.jpg" alt="アクセスマップ" width="177" height="33" border="0" /></a></td>
</tr></table>
</div>

<img src="../../common/images/pixel_trans.gif" alt="" width="211" height="20" class="spacer" /></div>
<!-- Colum Left End -->

<!-- Colum Right Begin -->
<div id="columRight">


<!--　○○○○○○○○○○　ここから　○○○○○○○○○○　-->

<form action="visa_form2.php" method="post" enctype="multipart/form-data">

<div id="txtBody">

<h2><img src="images/visa_m__h2.jpg" alt="ビザ申請書類翻訳　見積依頼" width="525" height="60" /></h2>

<table border="0" cellpadding="0" cellspacing="0" width="525">

<tr>
<td>
<table border="0" cellpadding="0" cellpadding="0">
<tr>
<td colspan="2"><img src="../../common/images/h3_top.gif" alt="" width="525" height="5" /></td>
</tr>
<tr>
<td style="text-align:left; vertical-align:middle; height:40px; background-color:#DCF0F9; color:#326698"><strong>　ビザ申請書類翻訳　見積依頼フォーム</strong></td>
<td style="text-align:left; vertical-align:bottom; height:40px; background-color:#DCF0F9; color:#326698;"><strong>入力画面</strong></td>
</tr>
<tr>
<td colspan="2"><img src="../../common/images/h3_btm.gif" alt="" width="525" height="5" /></td>
</tr>
</table>
</td>
</tr>


<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr>

<!--tr>
<td class="mtx">ビザ申請に必要な書類や、各種証明書類の翻訳を、ISSがお手伝いいたします。<br>戸籍謄本や預貯金通帳など必要とされる書類の翻訳に翻訳証明をおつけして、ご自宅までお届けいたします。</td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr>

<tr>
<td class="mtx">ビザ申請書類翻訳　見積依頼フォーム</td>
</tr>

<tr>
<td><img src="../../common/images/pixel_trans.gif" width="525" height="20" alt="" class="spacer" /></td>
</tr -->

<tr>
<td class="mtx"><font color="#ff0000">*</font>は入力必須項目です</td>
</tr>

<tr>
<td>


<table border="0" cellpadding="5" cellspacing="3" width="525" class="formTable mtx">

<tr>
<th width="1%">お名前<span class="att">*</span></td>
<td><input type="text" name="name_kanji" style="ime-mode: active;" maxlength="50" size="50" /></td>
</tr>

<tr>
<th>お名前（フリガナ）</td>
<td><input type="text" name="name_kana" style="ime-mode: active;" maxlength="50" size="50" /><br>※全角カタカナでご入力、姓と名の間にスペースを入れてください</td>
</tr>

<tr>
<th><nobr>お名前（パスポート<br>のローマ字表記）<span class="att">*</span></nobr></td>
<td><input type="text" name="name_rome" style="ime-mode: disabled;" maxlength="50" size="50" /><br>※姓と名の間にスペースを入れてください</td>
</tr>


<tr>
<th>性別<span class="att">*</span></td>
<td><input type="radio" name="sex" value="男性" />男性　　<input type="radio" name="sex" value="女性" />女性</td>
</tr>


<tr>
<th>E-mailアドレス<span class="att">*</span></td>
<td><input type="text" name="email" style="ime-mode: disabled;" maxlength="50" size="50" /></td>
</tr>

<tr>
<th>ご住所<span class="att">*</span></td>
<td><table border="0" cellpadding="0" cellspacing="1">
<tr>
<td><nobr>郵便番号</nobr></td>
<td><input type="text" name="zip1" style="ime-mode: disabled;" maxlength="10" size="10" />−<input type="text" name="zip2" style="ime-mode: disabled;" maxlength="10" size="10" /></td>
</tr>
<tr>
<td>都道府県</td>
<td>
<select name="pref">
<option value="">選択して下さい</option>
<option value="北海道">北海道</option>
<option value="青森県">青森県</option>
<option value="岩手県">岩手県</option>
<option value="秋田県">秋田県</option>
<option value="宮城県">宮城県</option>
<option value="山形県">山形県</option>
<option value="福島県">福島県</option>
<option value="新潟県">新潟県</option>
<option value="茨城県">茨城県</option>
<option value="栃木県">栃木県</option>
<option value="群馬県">群馬県</option>
<option value="長野県">長野県</option>
<option value="埼玉県">埼玉県</option>
<option value="千葉県">千葉県</option>
<option value="東京都">東京都</option>
<option value="神奈川県">神奈川県</option>
<option value="山梨県">山梨県</option>
<option value="静岡県">静岡県</option>
<option value="富山県">富山県</option>
<option value="石川県">石川県</option>
<option value="岐阜県">岐阜県</option>
<option value="愛知県">愛知県</option>
<option value="福井県">福井県</option>
<option value="滋賀県">滋賀県</option>
<option value="三重県">三重県</option>
<option value="和歌山県">和歌山県</option>
<option value="奈良県">奈良県</option>
<option value="大阪府">大阪府</option>
<option value="京都府">京都府</option>
<option value="兵庫県">兵庫県</option>
<option value="岡山県">岡山県</option>
<option value="鳥取県">鳥取県</option>
<option value="広島県">広島県</option>
<option value="島根県">島根県</option>
<option value="山口県">山口県</option>
<option value="香川県">香川県</option>
<option value="徳島県">徳島県</option>
<option value="高知県">高知県</option>
<option value="愛媛県">愛媛県</option>
<option value="福岡県">福岡県</option>
<option value="大分県">大分県</option>
<option value="佐賀県">佐賀県</option>
<option value="長崎県">長崎県</option>
<option value="熊本県">熊本県</option>
<option value="宮崎県">宮崎県</option>
<option value="鹿児島県">鹿児島県</option>
<option value="沖縄県">沖縄県</option>
<option value="海外">海外</option>
</select></td>
</tr>
<tr>
<td>市区町村</td>
<td><input type="text" name="address1" style="ime-mode: active;" maxlength="100" size="55" /></td>
</tr>
<tr>
<td>番地以下</td>
<td><input type="text" name="address2" style="ime-mode: active;" maxlength="100" size="55" /></td>
</tr>
</table>
※ご自宅以外の場合には「○○様方」等をご入力ください<br />
※ご住所はアパート名、ビル名まで詳しくご入力ください</td>
</tr>

<tr>
<th>お電話番号<span class="att">*</span></td>
<td><input type="text" name="tel1" style="ime-mode: disabled;" maxlength="10" size="10" />−<input type="text" name="tel2" style="ime-mode: disabled;" maxlength="10" size="10" />−<input type="text" name="tel3" style="ime-mode: disabled;" maxlength="10" size="10" /><br>※日中のご連絡が可能なお電話番号</td>
</tr>

<tr>
<th>ビザ申請先の国<span class="att">*</span></td>
<td><input type="radio" name="country" value="英国">英国<br><input type="radio" name="country" value="その他の国">その他の国　<input type="text" name="country_s" style="ime-mode: active;" maxlength="30" size="20" /></td>
</tr>

<tr>
<th>翻訳言語<span class="att">*</span></td>
<td><input type="radio" name="language" value="日本語→英語">日本語→英語<br><input type="radio" name="language" value="その他">その他　<input type="text" name="language_s1" style="ime-mode: active;" maxlength="30" size="15" />語→<input type="text" name="language_s2" style="ime-mode: active;" maxlength="30" size="15" />語</td>
</tr>

<tr>
<th>翻訳対象書類の<br>種類と数量<span class="att">*</span></td>
<td><table border="0" cellpadding="0" cellspacing="1">

<tr>
<td>通帳</td>
<td><select name="suryo_tsucho">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 件</td>
</tr>

<tr>
<td>戸籍謄本</td>
<td><select name="suryo_koseki">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 件</td>
</tr>

<tr>
<td>残高証明書</td>
<td><select name="suryo_zandaka">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 件</td>
</tr>

<tr>
<td>所得証明書</td>
<td><select name="suryo_shotoku">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 件</td>
</tr>

<tr>
<td>在職証明書</td>
<td><select name="suryo_zaishoku">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 件</td>
</tr>

<tr>
<td>給与証明書</td>
<td><select name="suryo_kyuyo">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> ヶ月分</td>
</tr>

<tr>
<td>婚姻届受理証明書</td>
<td><select name="suryo_konin">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 部</td>
</tr>

<tr>
<td>外国人登録証明書</td>
<td><select name="suryo_gaikokujin">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
</select> 部</td>
</tr>

<tr>
<td>その他の書類</td>
<td><input type="text" name="suryo_sonota" style="ime-mode: active;" maxlength="50" size="40" /></td>
</tr>

</table>
※必要な書類については、提出先機関にお問い合わせください<br>
※翻訳証明書は、翻訳書類の点数に関わらず、<br>
　一式につき１部を発行いたします</td>
</tr>



<tr>
<th>翻訳原稿の送付<span class="att">*</span></td>
<td><table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="radio" name="sending" value="フォーム" /></td>
<td>このフォームに添付する</td>
</tr>

<tr>
<td></td>
<td><table border="0" cellpadding="0" cellspacing="1">
<tr>
<td>添付ファイル１<input type="file" name="upfile1" size="35"></td>
</tr>
<tr>
<td>添付ファイル２<input type="file" name="upfile2" size="35"></td>
</tr>
<tr>
<td>添付ファイル３<input type="file" name="upfile3" size="35"></td>
</tr>
</table></td>
</tr>
<tr>
<td><img src="../../common/images/pixel_trans.gif" width="1" height="10" alt="" class="spacer" /></td>
<td></td>
</tr>
<tr>
<td><input type="radio" name="sending" value="その他" /></td>
<td>その他の方法で送付する<br />送付方法を下記より選択してください</td>
</tr>

<tr>
<td></td>
<td><table border="0" cellpadding="0" cellspacing="0">

<tr>
<td><input type="radio" name="sending_sonota" value="FAX" /></td>
<td>FAX 03-3262-6633</td>
</tr>

<tr>
<td><input type="radio" name="sending_sonota" value="郵送" /></td>
<td>郵送　〒102-0082<br>
　東京都千代田区一番町23-3日本生命一番町ビル4F<br>
　株式会社アイ・エス・エス<br>
　ランゲージ事業部　翻訳グループ宛</td>
</tr>

<tr>
<td><input type="radio" name="sending_sonota" value="持参" /></td>
<td>持参（ISS本社に来社）<br>
　来社予定日
<select name="raisha_month">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select> 月　

<select name="raisha_day">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select> 日<br>
　※ご来社の予定が変更になる場合はご連絡ください。<br>
　　（受付時間:平日9:00〜17:00、土日祝日は休業）</td>
</tr>

<tr>
<td><input type="radio" name="sending_sonota" value="E-mail" /></td>
<td>E-mail添付　<a href="mailto:visa_application@issjp.com">visa_application@issjp.com</a></td>
</tr>

</table></td>
</tr>

</table></td>
</tr>






<tr>
<th>見積連絡方法の<br>ご希望<span class="att">*</span></td>
<td><input type="radio" name="contact" value="E-mail">E-mail<br><input type="radio" name="contact" value="TEL">TEL<br><input type="radio" name="contact" value="FAX">FAX　<input type="text" name="contact_fax1" style="ime-mode: disabled;" maxlength="10" size="10" />−<input type="text" name="contact_fax2" style="ime-mode: disabled;" maxlength="10" size="10" />−<input type="text" name="contact_fax3" style="ime-mode: disabled;" maxlength="10" size="10" /></td>
</tr>




<tr>
<th>受取希望日<span class="att">*</span></td>
<td>
<select name="uketori_month">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select> 月　

<select name="uketori_day">
<option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select> 日 までに受取希望<br>
※翻訳代金のお振込みを確認してから納品まで<br>
　５営業日（宅配便ご利用の方は＋１日）程度かかります。<br>
　お急ぎの方はご相談となります。</td>
</tr>

<tr>
<th>受取方法<span class="att">*</span></td>
<td><input type="radio" name="uketori_hoho" value="クロネコヤマト着払便">クロネコヤマト着払便<br><input type="radio" name="uketori_hoho" value="直接ISS本社に来社">直接ISS本社に来社</td>
</tr>

<tr>
<th>ご要望・ご連絡事項など</td>
<td><textarea name="renraku" cols="50" rows="8" style="ime-mode: active;"></textarea></td>
</tr>
</table>
</td>
</tr>


<tr>
<td class="ac"><input type="submit" name="submit" style="font-size:10pt;" value="確認画面へ" /></td>
</tr>



</table>

<img src="../../common/images/pixel_trans.gif" width="525" height="30" alt="" class="spacer" />
<a href="#pageTop"><img src="../../common/images/pageTop.gif" alt="このページの上へ" width="90" height="20" class="pageTop" /></a>
<img src="../../common/images/pixel_trans.gif" width="525" height="50" alt="" class="spacer" /></div>
</form>

<!--　○○○○○○○○○○　ここまで　○○○○○○○○○○　-->
</div>
<!-- Colum Right End -->

<br class="clr" />
</div>
</div>
<!-- Main Content End -->

<!-- Footer Begin -->
<div id="footer">
<div class="content">
<img src="../../common/images/pixel_trans.gif" width="816" height="35" alt="" class="spacer" />
<div class="menuList">
<ul>
<li class="bt01"><a href="http://www.issjp.com/tsuyaku/index.html">通訳</a>　|　<a href="http://www.issjp.com/kokusai/index.html">国際会議企画・運営</a>　|　<a href="http://www.issjp.com/honyaku/index.html">翻訳</a>　|　<a href="http://www.issjp.com/haken/index.html">人材派遣・紹介予定派遣</a>　|　<a href="http://www.issjp.com/corptre/index.html">法人向け語学研修</a></li>
<li class="bt02"><a href="http://www.issjp.com/policy/index.html"><img src="../../common/images/fNavi_btn_01.gif" alt="個人情報保護方針" width="99" height="20" /></a></li>
<li class="bt03"><a href="http://www.issjp.com/site/index.html"><img src="../../common/images/fNavi_btn_03.gif" alt="サイトポリシー" width="90" height="20" /></a></li>
</ul>
</div>
<div class="menuList">
<ul>
<li class="bt05">【ISSグループサイト】　<a href="http://www.issnet.co.jp/" target="_blank">通訳・翻訳者養成学校のISS</a>　|　<a href="http://haken.issjp.com/" target="_blank">通訳・翻訳のISS人材派遣</a>　|　<a href="http://www.isssc.com/" target="_blank">外資系転職のISS</a>　|　<a href="http://www.issla.net/" target="_blank">通訳・翻訳のISSロサンゼルス</a></li>
<li class="bt04">&copy; ISS, INC. ALL RIGHTS RESERVED.</li>
</ul>

</div>
</div>
</div>
<!-- Footer End -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7347406-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<script>
var _bownow_trace_id_ = "UTC_574636b9466a7";
var hm = document.createElement("script");
hm.src = "https://contents.bownow.jp/js/trace.js";
document.getElementsByTagName("head")[0].appendChild(hm);
</script>
</body>
</html>
