#!/usr/bin/perl

#require '../../../perl_lib/jcode.pl';
#require '../../../perl_lib/cgi-lib.pl';

require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
&ReadParse;

$entry_cgi_path		="/cgi-bin/admin/staff_only/upload_admin.cgi";

$resume_dir		="../../secret/staff_only/upload/";

$csv_dir		="../../secret/staff_only/upload/";
$csv_path		=$csv_dir . "registered.csv";
$csv_tmp_path		=$csv_dir . "tmp.csv";
$csv_write_path		=">>".$csv_dir . "registered.csv";

$template_dir		="../../template/staff_only/upload/";
$index_html_path	=$template_dir . "upload_admin.html";

if($in{'cmd'} eq 'file'){
	$resume_path = $resume_dir.$in{'id'};
	$in{'id'} =~ /(.*)\.([^\.]+$)/;
	if($2 eq "png"){
		print "Content-Type: image/png\n";
	}elsif(($2 eq "jpg") || ($2 eq "jpeg")){
		print "Content-Type: image/jpeg\n";
	}elsif($2 eq "pdf"){
		print "Content-Type: application/pdf\n";
	}elsif($2 eq "sit"){
		print "Content-Type: application/x-stuffit\n";
	}elsif($2 eq "zip"){
		print "Content-Type: application/x-zip-compressed\n";
	}elsif($2 eq "doc"){
		print "Content-Type: application/msword\n";
	}elsif($2 eq "xls"){
		print "Content-Type: application/vnd.ms-excel\n";
	}elsif($2 eq "ppt"){
		print "Content-Type: application/vnd.ms-powerpoint\n";
	}else{
		print "Content-Type: application/msword\n";
	}
#	print "Content-Type: application/octet-stream\r\n";
	$filename=$in{'date'}."_".$in{'name'}."_".$in{'num'}.".".$2;
#	$filename=$in{'date'}."_".$in{'name'}."_".$in{'num'}.".xls";
##	$filename=~s/(\W)/'%'.unpack("H2", $1)/ego;
##	$filename=~tr/ /+/;
	open (IN,$resume_path);
	($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size,
	    $atime, $mtime, $ctime, $blksize, $blocks) = stat($resume_path);
	print "Content-Length: " . $size ."\r\n";
	print "Content-Disposition: attachment; filename=".$filename."\r\n\r\n";
#	print "Content-Disposition: attachment; filename=1.xls\r\n\r\n";
#	foreach $data (<IN>) {		print $data;}
	read(IN,$data,$size);
	print $data;
#	syswrite(STDOUT,$data,$size);
	close(IN);
}elsif($in{'cmd'} eq "del_confirm"){
	$strList="本当に削除してもよろしいですか？";
	$strList.="<a href=".$entry_cgi_path ."?cmd=del&name=".$in{'name'}."&epoch=".$in{'epoch'}."&type=".$in{'type'}.">[OK]</a>/<a href=".$entry_cgi_path .">戻る</a>";
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
	
}elsif($in{'cmd'} eq "del"){
	rename $csv_path,$csv_tmp_path;
	open(CSV_WRITE,$csv_write_path);
	open(CSV_TMP,$csv_tmp_path);
	@entry=<CSV_TMP>;
	close(CSV_TMP);
	foreach $i (@entry){
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),/;
		( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
		if(($1 eq $in{'epoch'}) && ($2 eq $in{'name'}) && ($3 eq $in{'type'})){
			unlink $resume_dir.$4;
			if(( length $5 )>0) {
				unlink $resume_dir.$5;
			}
		}else{
			print CSV_WRITE $i;
		}
	}
	close(CSV_WRITE);
	
	$strList="削除しました。";
	$strList.="<a href=".$entry_cgi_path ."?type=".$in{'type'}.">戻る</a>";
	
	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}else{
	open(CSV,$csv_path);
	@entry=<CSV>;
	close(CSV);
	if($in{'type'} eq "resume"){
		$strList.="<H1>職務経歴書一覧</H1><br>";
		$strList.="<a href=".$entry_cgi_path ."?type=timesheet >タイムシートへ</a><br><hr>";
	}else{
		$strList.="<H1>タイムシート一覧</H1><br>";
		$strList.="<a href=".$entry_cgi_path ."?type=resume >職務経歴書へ</a><br><hr>";
	}
	$strList.="<table>";
	$strList.="<tr><td>日付</td><td>名前</td><td>ダウンロード1</td><td>ダウンロード2</td><td>削除</td><td>メモ</td></tr>";
	foreach $i (@entry){
		&jcode::sjis2euc(\$i);
		$i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
		if($in{'type'} eq $3){
			( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;

			$strList.="<tr>";
			$strList.="<td>[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]";
			$strList.="</td><td>";
			$strList.=$2;
			$strList.="</td>";
			$strList.="<td>[<a href=".$entry_cgi_path ."?cmd=file&num=1&id=".$4."&name=".$2."&type=".$3."&date=".
				(($year + 1900) * 10000 + ($mon + 1) * 100 + $mday) . "_" . ($hour*10000 + $min * 100 + $sec)." >ファイル１</a>]</td>";
			if( length($5)>0 ){
				$strList.="<td>[<a href=".$entry_cgi_path ."?cmd=file&num=2&id=".$5."&name=".$2."&type=".$3."&date=".
				(($year + 1900) * 10000 + ($mon + 1) * 100 + $mday) . "_" . ($hour*10000 + $min * 100 + $sec)." >ファイル２</a>]</td>";
			}else{
				$strList.="</td><td>";
			}
			$strList.="<td>[<a href=".$entry_cgi_path ."?cmd=del_confirm&epoch=".$1. "&name=". $2 ."&type=".$3.">削除</a>]</td>";
			$strList.="<td>".$6."</td></tr>";
		}
	}
	$strList.="</table>";

	print "Content-type: text/html\n\n";
	open(TEMPLATE,$index_html_path);
	@entry=<TEMPLATE>;
	close(TEMPLATE);
	$strHtml="";
	foreach $i (@entry){$strHtml.=$i;}
	$strHtml=~s/__INDEX__/$strList/g;
	&jcode::euc2sjis(\$strHtml);
	print $strHtml;
}

sub replace{
	if($sel_type ==1){
		$sel_type_kanji="テキスト";
	}elsif($sel_type == 2){
		$sel_type_kanji="選択肢";
	}elsif($sel_type == 3){
		$sel_type_kanji="テキスト+選択肢";
	}else{
		$sel_type_kanji="使用しない";
		$sel_type=0;
	}
	&jcode::euc2sjis(\$sel_type_kanji);

	if($req_type == 1){
		$req_type_kanji="必須";
	}else{
		$req_type_kanji="任意";
		$req_type=0;
	}
	&jcode::euc2sjis(\$req_type_kanji);

	$strHtml=~s/__id__/$id/g;
	&replace_radio2('sel_type','0',$sel_type);
	&replace_radio2('sel_type','1',$sel_type);
	&replace_radio2('sel_type','2',$sel_type);
	&replace_radio2('sel_type','3',$sel_type);
	$strHtml=~s/__sel_type__/$sel_type/g;
	$strHtml=~s/__sel_type_kanji__/$sel_type_kanji/g;
	&replace_radio2('req_type','1',$req_type);
	$strHtml=~s/__req_type__/$req_type/g;
	$strHtml=~s/__req_type_kanji__/$req_type_kanji/g;
	$strHtml=~s/__qes__/$qes/g;
	$strHtml=~s/__ans01__/$ans01/g;
	$strHtml=~s/__ans02__/$ans02/g;
	$strHtml=~s/__ans03__/$ans03/g;
	$strHtml=~s/__ans04__/$ans04/g;
	$strHtml=~s/__ans05__/$ans05/g;
	$strHtml=~s/__ans06__/$ans06/g;
	$strHtml=~s/__ans07__/$ans07/g;
	$strHtml=~s/__ans08__/$ans08/g;
	$strHtml=~s/__ans09__/$ans09/g;
	$strHtml=~s/__ans10__/$ans10/g;
}

sub replace_radio2{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($_[2] == $tmp2){
		$strHtml=~s/$tmp/checked/g;
	}else{
		$strHtml=~s/$tmp//g;
	}
}

sub replace_radio{
	$tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
	&jcode::euc2sjis(\$tmp);
	$tmp2=$_[1];
	&jcode::euc2sjis(\$tmp2);
	if($in{$_[0]}){
		if($in{$_[0]} eq $tmp2){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp/$tmp/g;
		}
	}else{
		$strHtml=~s/$tmp//g;
	}
}
sub replace_select01d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}

sub replace_select02d{
	for ( $a = 0 ; $a <= $_[1] ; $a++ ){
		$tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
		if($in{$_[0]}){
			if($in{$_[0]} eq $a){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}else{
			if($a eq 0){
				$strHtml=~s/$tmp/selected/g;
			}else{
				$strHtml=~s/$tmp//g;
			}
		}
	}
}
sub replace_checkbox{
		$tmp = sprintf("__checkbox_%s__", $_[0]);
		if($in{$_[0]}){
			$strHtml=~s/$tmp/checked/g;
		}else{
			$strHtml=~s/$tmp//g;
		}
}

