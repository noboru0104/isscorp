<div class="l-side-menu">
    <div class="p-side-menu-title">
        <h3>通訳について</h3>
    </div>
    <div class="p-side-menu-list">
    <p><a href="<?php echo home_url();?>/tsuyaku">サービス概要</a></p>
        <p><a href="<?php echo home_url();?>/tsuyaku/online">オンライン通訳サービス</a></p>

        <p><a href="<?php echo home_url();?>/tsuyaku/online_s">オンライン通訳 導入事例</a></p>
        <!-- <p class="p-conference"><a href="<?php echo home_url();?>/tsuyaku/online_s">オンライン通訳 導入事例</a></p> -->
        <div class="p-conference-list">
            <!-- <p><a href="<?php echo home_url();?>/tsuyaku/online">オンライン通訳サービス</a></p> -->
            <!-- <p><a href="<?php echo home_url();?>/tsuyaku/online_s">導入事例</a></p> -->
            <!-- <p><a href="<?php echo home_url();?>/tsuyaku/online_r">ISS遠隔通訳専用ルーム</a></p> -->
            <p><a href="<?php echo home_url();?>/tsuyaku/audit">査察・監査における通訳</a></p>
            <p><a href="<?php echo home_url();?>/tsuyaku/attend">病院訪問や観光同行にける通訳</a></p>
        </div>

        <p><a href="<?php echo home_url();?>/tsuyaku/online_r">ISS遠隔通訳専用ルーム</a></p>
        <p><a href="<?php echo home_url();?>/tsuyaku/flow">ご利用の流れ</a></p>
        <p><a href="<?php echo home_url();?>/tsuyaku/system">通訳形態と通訳機材</a></p>
        <p><a href="<?php echo home_url();?>/tsuyaku/service">対応業種と対応言語</a></p>
        <p><a href="<?php echo home_url();?>/tsuyaku/faq">よくあるご質問</a></p>
    </div>
</div>
