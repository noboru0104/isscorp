var vNum = navigator.appVersion.charAt(0);
var bName = navigator.appName.charAt(0);
var vNum2 = navigator.userAgent;

document.writeln("<STYLE TYPE='text/css'><!--");
if(navigator.appVersion.indexOf("Mac") > -1)
{
  if (vNum2.indexOf("Safari") > -1)
   {
     // MAC Safari
     document.writeln("h1{font-size:130%;}");
     document.writeln("h2{font-size:130%;}");
     document.writeln("h3{font-size:130%;}");
     document.writeln("h4{font-size:120%;}");
     document.writeln(".ltx{font-size:130%; line-height:180%}");
     document.writeln(".mtx{font-size:110%; line-height:180%; letter-spacing: -0.15em;}");
     document.writeln(".stx{font-size:90%; line-height:180%; letter-spacing: -0.1em;}");
     document.writeln(".l{font-size:130%}");
     document.writeln(".m{font-size:100%}");
     document.writeln(".s{font-size:90%}");
   }
  else if( bName == "M")
   {
   if( vNum2.match(/MSIE 5.2/) )
    {
      // MAC IE5.2
     document.writeln("h1{font-size:110%;}");
     document.writeln("h2{font-size:85%;}");
     document.writeln("h3{font-size:85%;}");
     document.writeln("h4{font-size:80%;}");
     document.writeln(".ltx{font-size:110%; line-height:160%}");
     document.writeln(".mtx{font-size:80%; line-height:160%}");
     document.writeln(".stx{font-size:70%; line-height:160%}");
     document.writeln(".l{font-size:110%}");
     document.writeln(".m{font-size:80%}");
     document.writeln(".s{font-size:70%}");
    }
    else
    {
    // MAC IE
    document.writeln("h1{font-size:110%;}");
    document.writeln("h2{font-size:90%;}");
    document.writeln("h3{font-size:90%;}");
    document.writeln("h4{font-size:80%;}");
    document.writeln(".ltx{font-size:110%; line-height:150%}");
    document.writeln(".mtx{font-size:80%; line-height:150%}");
    document.writeln(".stx{font-size:70%; line-height:150%}");
    document.writeln(".l{font-size:110%}");
    document.writeln(".m{font-size:80%}");
    document.writeln(".s{font-size:70%}");
    }
   }
    else
    {
    if( vNum < 5 )
    {
      // MAC NETSCAPE 4.x
    document.writeln("h1{font-size:130%;}");
    document.writeln("h2{font-size:130%;}");
    document.writeln("h3{font-size:130%;}");
    document.writeln("h4{font-size:120%;}");
    document.writeln(".ltx{font-size:140%; line-height:170%}");
    document.writeln(".mtx{font-size:120%; line-height:150%}");
    document.writeln(".stx{font-size:100%; line-height:150%}");
    document.writeln(".l{font-size:140%}");
    document.writeln(".m{font-size:120%}");
    document.writeln(".s{font-size:100%}");
    }
    else
    {
      // MAC NETSCAPE 6.x~
     document.writeln("h1{font-size:110%;}");
     document.writeln("h2{font-size:100%;}");
     document.writeln("h3{font-size:100%;}");
     document.writeln("h4{font-size:90%;}");
     document.writeln(".ltx{font-size:110%; line-height:160%}");
     document.writeln(".mtx{font-size:80%; line-height:160%}");
     document.writeln(".stx{font-size:70%; line-height:160%}");
     document.writeln(".l{font-size:110%}");
     document.writeln(".m{font-size:80%}");
     document.writeln(".s{font-size:70%}");
    }
   }

}
else
{
  if( bName == "M")
  {
    // WIN IE
    document.writeln("h1{font-size:100%;}");
    document.writeln("h2{font-size:100%;}");
    document.writeln("h3{font-size:90%;}");
    document.writeln("h4{font-size:80%;}");
    document.writeln(".ltx{font-size:90%; line-height:180%;}");
    document.writeln(".mtx{font-size:75%; line-height:180%;}");
    document.writeln(".stx{font-size:70%; line-height:170%;}");
    document.writeln(".l{font-size:90%}");
    document.writeln(".m{font-size:75%}");
    document.writeln(".s{font-size:70%}");
  }
  else
  {
		 if(navigator.userAgent.indexOf("Gecko/") != -1)
    {
      // Gecko
    document.writeln("h1{font-size:100%;}");
    document.writeln("h2{font-size:100%;}");
    document.writeln("h3{font-size:90%;}");
    document.writeln("h4{font-size:80%;}");
    document.writeln(".ltx{font-size:90%; line-height:168%; letter-spacing:0.01em;}");
    document.writeln(".mtx{font-size:80%; line-height:160%; letter-spacing:-0.06em;}");
    document.writeln(".stx{font-size:65%; line-height:160%; letter-spacing:0.09em;}");
    document.writeln(".l{font-size:90%}");
    document.writeln(".m{font-size:80%}");
    document.writeln(".s{font-size:65%}");
	document.writeln("caption{font-size:100%;}");
    
    }
  }
}
 document.writeln("--></STYLE>");

