#!/usr/bin/perl

#require '../../../perl_lib/jcode.pl';
#require '../../../perl_lib/cgi-lib.pl';

require '../../perl_lib/jcode.pl';
require '../../perl_lib/cgi-lib.pl';
&ReadParse;

#$entry_cgi_path     ="/haken/ssl/cgi-bin/admin/staff_only/upload_admin.cgi";
# MW 20080807 パス修正　message_cgi追加
$entry_cgi_path     ="./upload_admin.cgi";
$message_cgi_path   ="./message_admin.cgi";

$resume_dir     ="../../secret/staff_only/upload/";

$csv_dir        ="../../secret/staff_only/upload/";
$csv_path       =$csv_dir . "registered.csv";
$csv_tmp_path       =$csv_dir . "tmp.csv";
$csv_write_path     =">>".$csv_dir . "registered.csv";
$csv_mes_path       =$csv_dir . "message.csv";
$csv_mes_tmp_path       =$csv_dir . "mes_tmp.csv";
$csv_mes_write_path     =">>".$csv_dir . "message.csv";

$template_dir       ="../../template/staff_only/upload/";
$index_html_path    =$template_dir . "upload_admin.html";

if($in{'cmd'} eq 'file'){
    $resume_path = $resume_dir.$in{'id'};
    $in{'id'} =~ /(.*)\.([^\.]+$)/;
    if($2 eq "png"){
        print "Content-Type: image/png\n";
    }elsif(($2 eq "jpg") || ($2 eq "jpeg")){
        print "Content-Type: image/jpeg\n";
    }elsif($2 eq "pdf"){
        print "Content-Type: application/pdf\n";
    }elsif($2 eq "sit"){
        print "Content-Type: application/x-stuffit\n";
    }elsif($2 eq "zip"){
        print "Content-Type: application/x-zip-compressed\n";
    }elsif($2 eq "doc"){
        print "Content-Type: application/msword\n";
    }elsif($2 eq "xls"){
        print "Content-Type: application/vnd.ms-excel\n";
    }elsif($2 eq "ppt"){
        print "Content-Type: application/vnd.ms-powerpoint\n";
    }else{
        print "Content-Type: application/msword\n";
    }
#   print "Content-Type: application/octet-stream\r\n";
    $filename=$in{'date'}."_".$in{'name'}."_".$in{'num'}.".".$2;
#   $filename=$in{'date'}."_".$in{'name'}."_".$in{'num'}.".xls";
##  $filename=~s/(\W)/'%'.unpack("H2", $1)/ego;
##  $filename=~tr/ /+/;
    ($dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size,
        $atime, $mtime, $ctime, $blksize, $blocks) = stat($resume_path);
    if(length($size) >0){ 
        print "Content-Length: " . $size ."\r\n";
    }

    print "Content-Disposition: attachment; filename=".$filename."\r\n\r\n";
#   print "Content-Disposition: attachment; filename=1.xls\r\n\r\n";

    open (IN,$resume_path);
#   foreach $data (<IN>) {      print $data;}
    read(IN,$data,$size);
    print $data;
#   syswrite(STDOUT,$data,$size);
    close(IN);
}elsif($in{'cmd'} eq "del_confirm"){
    $strList="本当に削除してもよろしいですか？";
    &jcode::euc2sjis(\$strList);
    $strList.="<a href=".$entry_cgi_path ."?cmd=del&amp;name=".$in{'name'}."&amp;epoch=".$in{'epoch'}."&amp;type=".$in{'type'}.">[OK]</a>/<a href=".$entry_cgi_path ;
    $strTmp=">戻る</a>";
    &jcode::euc2sjis(\$strTmp);
    $strList.=$strTmp;
    print "Content-type: text/html\n\n";
    open(TEMPLATE,$index_html_path);
    @entry=<TEMPLATE>;
    close(TEMPLATE);
    $strHtml="";
    foreach $i (@entry){$strHtml.=$i;}
    $strHtml=~s/__INDEX__/$strList/g;
    print $strHtml;
    
}elsif($in{'cmd'} eq "del"){
# MW 20080807 改修 メッセージ一覧用削除処理
    if($in{'type'} eq "message"){
        rename $csv_mes_path,$csv_mes_tmp_path;
        open(CSV_MES_WRITE,$csv_mes_write_path);
        open(CSV_MES_TMP,$csv_mes_tmp_path);
        @entry=<CSV_MES_TMP>;
        close(CSV_MES_TMP);
    #   $name_euc=$in{'name'}
    #   &jcode::sjis2euc(\$name_euc);
        foreach $i (@entry){
    #       &jcode::sjis2euc(\$i);
            $i=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
            ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $2 )  ;
            if(($2 ne $in{'epoch'}) && ($3 ne $in{'name'})){
                print CSV_MES_WRITE $i;
            }
        }
        close(CSV_MES_WRITE);
    }else{
        rename $csv_path,$csv_tmp_path;
        open(CSV_WRITE,$csv_write_path);
        open(CSV_TMP,$csv_tmp_path);
        @entry=<CSV_TMP>;
        close(CSV_TMP);
    #   $name_euc=$in{'name'}
    #   &jcode::sjis2euc(\$name_euc);
        foreach $i (@entry){
    #       &jcode::sjis2euc(\$i);
            $i=~/(.*?),(.*?),(.*?),(.*?),(.*?),/;
            ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
    #       if(($1 eq $in{'epoch'}) && ($2 eq $name_euc) && ($3 eq $in{'type'})){
            if(($1 eq $in{'epoch'}) && ($2 eq $in{'name'}) && ($3 eq $in{'type'})){
                unlink $resume_dir.$4;
                if(( length $5 )>0) {
                    unlink $resume_dir.$5;
                }
            }else{
    #           &jcode::euc2sjis(\$i);
                print CSV_WRITE $i;
            }
        }
        close(CSV_WRITE);
    }
    
    $strList="削除しました。";
    $strList.="<a href=".$entry_cgi_path ."?type=".$in{'type'}.">戻る</a>";
    
    print "Content-type: text/html\n\n";
    open(TEMPLATE,$index_html_path);
    @entry=<TEMPLATE>;
    close(TEMPLATE);
    $strHtml="";
    foreach $i (@entry){$strHtml.=$i;}
    $strHtml=~s/__INDEX__/$strList/g;
    &jcode::euc2sjis(\$strHtml);
    print $strHtml;
}else{
    open(CSV,$csv_path);
    @entry=<CSV>;
    close(CSV);
# MW 20080807 改修 メッセージ一覧追加
    if($in{'type'} eq "resume"){
        $strList.="<H1>職務経歴書一覧</H1><br>\n";
        $strList.="<a href=\"".$entry_cgi_path ."?type=timesheet\">タイムシートへ</a><br>\n";
        $strList.="<a href=\"".$entry_cgi_path ."?type=message\">メッセージへ</a><br><hr>\n";
    }elsif($in{'type'} eq "message"){
        $strList.="<H1>メッセージ一覧</H1><br>\n";
        $strList.="<a href=\"".$entry_cgi_path ."?type=timesheet\">タイムシートへ</a><br>\n";
        $strList.="<a href=\"".$entry_cgi_path ."?type=resume\">職務経歴書へ</a><br><hr>\n";
    }else{
        $strList.="<H1>タイムシート一覧</H1><br>\n";
        $strList.="<a href=\"".$entry_cgi_path ."?type=resume\">職務経歴書へ</a><br>\n";
        $strList.="<a href=\"".$entry_cgi_path ."?type=message\">メッセージへ</a><br><hr>\n";
    }
    $strList.="<table>";

# MW 20080807 改修 文字化け修正（文字コード変換処理の変更)
    if($in{'type'} eq "message"){
        open(CSV_MS,$csv_mes_path);
        @messagelist=<CSV_MS>;
        close(CSV_MS);
        $strList.="<tr><td>日付</td><td>名前</td><td>タイトル</td><td>削除</td></tr>\n";
        &jcode::euc2sjis(\$strList);
        foreach $i (@messagelist){
#            &jcode::sjis2euc(\$i);
            $i=~/(.*?),(.*?),(.*?),(.*?),(.*)/;
            ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $2 )  ;
            my $name = $3;
            my $title = $4;
            $strList.="<tr>\n";
            $strList.="<td>[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]</td>\n";
            $strList.="<td>__NAME__</td>\n";
            $strList.="<td><a href=\"".$message_cgi_path."?".$1."\">__TITLE__</a></td>\n";
            $strList.="<td>[<a href=".$entry_cgi_path ."?cmd=del_confirm&amp;epoch=".$2. "&amp;name=__NAME__&amp;type=message>削除</a>]</td>\n";
            $strList.="</tr>\n";
            &jcode::euc2sjis(\$strList);
            $strList=~s/__NAME__/$name/g;
            $strList=~s/__TITLE__/$title/g;
        }
    }else{
        $strList.="<tr><td>日付</td><td>名前</td><td>ダウンロード1</td><td>ダウンロード2</td><td>削除</td><td>メモ</td></tr>\n";
        &jcode::euc2sjis(\$strList);
        foreach $i (@entry){
#            &jcode::sjis2euc(\$i);
            $i=~/(.*?),(.*?),(.*?),(.*?),(.*?),(.*)/;
            if($in{'type'} eq $3){
                ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst )  =  localtime ( $1 )  ;
                my $name = $2;
                $strList.="<tr>\n";
                $strList.="<td>[". ($year + 1900) ."-". ($mon + 1) ."-". $mday ." " .$hour.":".$min.":".$sec ."]";
                $strList.="</td>\n<td>__NAME__</td>\n";
                $strList.="<td>[<a href=".$entry_cgi_path ."?cmd=file&amp;num=1&amp;id=".$4."&amp;name=__NAME__&amp;type=".$3."&amp;date=".
                    (($year + 1900) * 10000 + ($mon + 1) * 100 + $mday) ." >ファイル１</a>]</td>\n";
                if( length($5)>0 ){
                    $strList.="<td>[<a href=".$entry_cgi_path ."?cmd=file&amp;num=2&amp;id=".$5."&amp;name=__NAME__&amp;type=".$3."&amp;date=".
                    (($year + 1900) * 10000 + ($mon + 1) * 100 + $mday) ." >ファイル２</a>]</td>\n";
                }else{
                    $strList.="<td>&nbsp;</td>\n";
                }
                $strList.="<td>[<a href=".$entry_cgi_path ."?cmd=del_confirm&amp;epoch=".$1. "&amp;name=__NAME__&amp;type=".$3.">削除</a>]</td>\n";
                $strList.="<td>".$6."</td>\n</tr>\n";
                &jcode::euc2sjis(\$strList);
                $strList=~s/__NAME__/$name/g;
            }
        }
    }
    $strList.="</table>";

    print "Content-type: text/html\n\n";
    open(TEMPLATE,$index_html_path);
    @entry=<TEMPLATE>;
    close(TEMPLATE);
    $strHtml="";
    foreach $i (@entry){$strHtml.=$i;}
    $strHtml=~s/__INDEX__/$strList/g;
#    &jcode::euc2sjis(\$strHtml);
    print $strHtml;
}

sub replace{
    if($sel_type ==1){
        $sel_type_kanji="テキスト";
    }elsif($sel_type == 2){
        $sel_type_kanji="選択肢";
    }elsif($sel_type == 3){
        $sel_type_kanji="テキスト+選択肢";
    }else{
        $sel_type_kanji="使用しない";
        $sel_type=0;
    }
    &jcode::euc2sjis(\$sel_type_kanji);

    if($req_type == 1){
        $req_type_kanji="必須";
    }else{
        $req_type_kanji="任意";
        $req_type=0;
    }
    &jcode::euc2sjis(\$req_type_kanji);

    $strHtml=~s/__id__/$id/g;
    &replace_radio2('sel_type','0',$sel_type);
    &replace_radio2('sel_type','1',$sel_type);
    &replace_radio2('sel_type','2',$sel_type);
    &replace_radio2('sel_type','3',$sel_type);
    $strHtml=~s/__sel_type__/$sel_type/g;
    $strHtml=~s/__sel_type_kanji__/$sel_type_kanji/g;
    &replace_radio2('req_type','1',$req_type);
    $strHtml=~s/__req_type__/$req_type/g;
    $strHtml=~s/__req_type_kanji__/$req_type_kanji/g;
    $strHtml=~s/__qes__/$qes/g;
    $strHtml=~s/__ans01__/$ans01/g;
    $strHtml=~s/__ans02__/$ans02/g;
    $strHtml=~s/__ans03__/$ans03/g;
    $strHtml=~s/__ans04__/$ans04/g;
    $strHtml=~s/__ans05__/$ans05/g;
    $strHtml=~s/__ans06__/$ans06/g;
    $strHtml=~s/__ans07__/$ans07/g;
    $strHtml=~s/__ans08__/$ans08/g;
    $strHtml=~s/__ans09__/$ans09/g;
    $strHtml=~s/__ans10__/$ans10/g;
}

sub replace_radio2{
    $tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
    &jcode::euc2sjis(\$tmp);
    $tmp2=$_[1];
    &jcode::euc2sjis(\$tmp2);
    if($_[2] == $tmp2){
        $strHtml=~s/$tmp/checked/g;
    }else{
        $strHtml=~s/$tmp//g;
    }
}

sub replace_radio{
    $tmp = sprintf("__checked_%s_%s__", $_[0],$_[1]);
    &jcode::euc2sjis(\$tmp);
    $tmp2=$_[1];
    &jcode::euc2sjis(\$tmp2);
    if($in{$_[0]}){
        if($in{$_[0]} eq $tmp2){
            $strHtml=~s/$tmp/checked/g;
        }else{
            $strHtml=~s/$tmp/$tmp/g;
        }
    }else{
        $strHtml=~s/$tmp//g;
    }
}
sub replace_select01d{
    for ( $a = 0 ; $a <= $_[1] ; $a++ ){
        $tmp = sprintf("__selected_%s_%01d__", $_[0],$a);
        if($in{$_[0]}){
            if($in{$_[0]} eq $a){
                $strHtml=~s/$tmp/selected/g;
            }else{
                $strHtml=~s/$tmp//g;
            }
        }else{
            if($a eq 0){
                $strHtml=~s/$tmp/selected/g;
            }else{
                $strHtml=~s/$tmp//g;
            }
        }
    }
}

sub replace_select02d{
    for ( $a = 0 ; $a <= $_[1] ; $a++ ){
        $tmp = sprintf("__selected_%s_%02d__", $_[0],$a);
        if($in{$_[0]}){
            if($in{$_[0]} eq $a){
                $strHtml=~s/$tmp/selected/g;
            }else{
                $strHtml=~s/$tmp//g;
            }
        }else{
            if($a eq 0){
                $strHtml=~s/$tmp/selected/g;
            }else{
                $strHtml=~s/$tmp//g;
            }
        }
    }
}
sub replace_checkbox{
        $tmp = sprintf("__checkbox_%s__", $_[0]);
        if($in{$_[0]}){
            $strHtml=~s/$tmp/checked/g;
        }else{
            $strHtml=~s/$tmp//g;
        }
}

