function submitCheck(){
//未入力のチェック

	df = document.form1;

	if(!df.p_name_f.value){
		alert("お名前（漢字）姓をご入力下さい");
		df.p_name_f.focus();
		return(false);
	}
	if(!df.p_name_s.value){
		alert("お名前（漢字）名をご入力下さい");
		df.p_name_s.focus();
		return(false);
	}
	if(!df.prefecture.value){
		alert("都道府県を選択して下さい");
		df.prefecture.focus();
		return(false);
	}
	if(!df.address1.value){
		alert("市区町村名をご入力下さい");
		df.address1.focus();
		return(false);
	}
	if(!df.address2.value){
		alert("番地・マンション名をご入力下さい");
		df.address2.focus();
		return(false);
	}
	if(!df.tel1.value){
		alert("電話番号１をご入力下さい");
		df.tel1.focus();
		return(false);
	}
	if(!df.tel2.value){
		alert("電話番号２をご入力下さい");
		df.tel2.focus();
		return(false);
	}
	if(!df.tel3.value){
		alert("電話番号３をご入力下さい");
		df.tel3.focus();
		return(false);
	}
	if(df.tel1.value.length && !df.tel1.value.match (/^[\w\-@\.]+$/)){
		alert("電話番号１は半角数字をご入力下さい");
		df.tel1.focus();
		return (false);
	}
	if(df.tel2.value.length && !df.tel2.value.match (/^[\w\-@\.]+$/)){
		alert("電話番号２は半角数字をご入力下さい");
		df.tel2.focus();
		return (false);
	}
	if(df.tel3.value.length && !df.tel3.value.match (/^[\w\-@\.]+$/)){
		alert("電話番号３は半角数字をご入力下さい");
		df.tel3.focus();
		return (false);
	}
	if(!df.mail.value){
		alert("メールアドレスをご入力下さい");
		df.mail.focus();
		return(false);
	}
	if(df.mail.value.length && !df.mail.value.match (/^[\w\-@\.]+$/)) {
		alert("メールアドレスを正しくご入力下さい");
		df.mail.focus();
		return (false);
	}
	if(!df.c_name_f.value){//未入力のチェック
		alert("お子様の名前（漢字）姓をご入力下さい");
		df.c_name_f.focus();
		return(false);
	}
	if(!df.c_name_s.value){
		alert("お子様の名前（漢字）名をご入力下さい");
		df.c_name_s.focus();
		return(false);
	}
	if(!df.c_old.value){
		alert("お子様のご年齢を選択して下さい");
		df.c_old.focus();
		return(false);
	}
}

//070705 修正（IMEモードの切り替えを追加）

function set_imeMode(myobj, mymode) {

	switch(mymode){
		case 0: myobj.style.imeMode = "inactive"; break;	//英語
		case 1: myobj.style.imeMode = "active"; break;		//日本語
		default: break;
	}

}

