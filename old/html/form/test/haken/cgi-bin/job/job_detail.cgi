#!/usr/bin/perl

#各種設定ここから

#ディレクトリ情報(unix環境下実装用)ここから
my $DataFileDir = "../secret/job";

my $TemplateDir = "../template/job";
my $LiblaryDir = "../perl_lib";
#ディレクトリ情報(unix環境下実装用)ここまで

#テンプレートファイルリストここから
my $HtmlBody = "workdetail.html";
#テンプレートファイルリストここまで

#データファイルリストここから
my $WorkInfo = "workinfo.csv";
#データファイルリストここまで

#モジュール読み込み
#require '../perl_lib/jcode.pl';
#require '../perl_lib/cgi-lib.pl';

#出力HTMLソース一時格納用変数
my $HtmlSrc = "";


#各種設定ここまで

print("Content-type:text/html\n\n");

my $DisplayJobNo = $ENV{'QUERY_STRING'};

my $AskNo = 0; 
my $JobNo = 0;
my $Catch = 0;
my $JobKind = 0;
my $Wage = 0;
my $WorkPlace = 0;
my $PlaceData = 0;
my $WorkDay = 0;
my $AddWork = 0;
my $StartTime = 0; 
my $EndTime = 0;
my $WorkPeriod = 0;
my $Bikou = 0;
my $WorkInformation = 0;
my $EntryCondition = 0;


open DATA, "$DataFileDir/$WorkInfo" or die "file not exist!";

label1:while (<DATA>){
	my @TmpWorkInfo = split(/,/,$_);
	if ($TmpWorkInfo[14] == $DisplayJobNo){
		$AskNo = $TmpWorkInfo[0];
		$JobNo = $TmpWorkInfo[1];
		$Catch = $TmpWorkInfo[2];
		$JobKind = $TmpWorkInfo[3]; 
		$Wage = $TmpWorkInfo[4];
		$WorkPlace = $TmpWorkInfo[6];
		$PlaceData = $TmpWorkInfo[13]; 
		$WorkDay = $TmpWorkInfo[7];
		$AddWork = $TmpWorkInfo[8];
		$StartTime = $TmpWorkInfo[9]; 
		$EndTime = $TmpWorkInfo[10];
		$WorkPeriod = $TmpWorkInfo[11];
		$Bikou = $TmpWorkInfo[12];
		$DayData = $TmpWorkInfo[15];
		$WorkInformation = $TmpWorkInfo[16];
		$EntryCondition = $TmpWorkInfo[17];
		
		$WorkDay = &ConvertDay($WorkDay);
		$JobKind = &ConvertJob($JobKind);
		$WorkPlace = &ConvertPlace($WorkPlace,$PlaceData);
		last label1;
	}
}

close DATA;

#ソース読み込み
open BODYTEMP, "$TemplateDir/$HtmlBody";
while (<BODYTEMP>){
	$HtmlSrc .= $_;
}
close BODYTEMP;

#以下文字列置換
$HtmlSrc =~ s/__ASK__NO__/$JobNo/;
$HtmlSrc =~ s/__CATCH__/$Catch/;
$HtmlSrc =~ s/__JOB__TYPE__/$JobKind/;
$HtmlSrc =~ s/__JOB__NAME__/$AskNo/;
$HtmlSrc =~ s/__WAGE__/$Wage/;
$HtmlSrc =~ s/__WORK__PLACE__/$WorkPlace/;
$HtmlSrc =~ s/__WORK__DAY__/$WorkDay/;
$HtmlSrc =~ s/__START__TIME__/$StartTime/;
$HtmlSrc =~ s/__END__TIME__/$EndTime/;
$HtmlSrc =~ s/__ADDITIONAL__WORK__/$AddWork/;
$HtmlSrc =~ s/__WORK__PERIOD__/$WorkPeriod/;
$HtmlSrc =~ s/__BIKOU__/$Bikou/;
$HtmlSrc =~ s/__ENTRY__NO__/$JobNo/;
$HtmlSrc =~ s/__WORK__INFO__/$WorkInformation/;
$HtmlSrc =~ s/__ENTRY__CONDITION__/$EntryCondition/;

#ソース出力
print("$HtmlSrc");


#以下サブルーチン
sub ConvertJob{
	my $ConvertedJob = "";
	my @GetJobData = @_;
	#変換用仕事データ
	my @JobList = ('','通訳・翻訳・翻訳チェッカー','秘書・グループセクレタリー','英文事務・アシスタント・英文経理・貿易事務・人事・総務など','金融業界でのお仕事','IT・技術関連');
	$ConvertedJob = $JobList[$GetJobData[0]];
	return $ConvertedJob;
}

sub ConvertPlace{
	my $ConvertedPlace = "";
	my @GetPlaceData = @_;
	#変換用勤務場所データ
	my @PlaceList = ('','神谷町','赤坂見附','大手町','青山一丁目','茅場町','九段下','新宿','溜池山王','表参道','みなとみらい');
	if ($GetPlaceData[0] == -1){
		$ConvertedPlace = $GetPlaceData[1];
	}
	else{
		$ConvertedPlace = $PlaceList[$GetPlaceData[0]];
	}
	return $ConvertedPlace;
}

sub ConvertDay{
	my $ConvertedDay = "";
	my @GetDayData = @_;
	if ($GetDayData[0] == 1){
		$ConvertedDay = "平日";
	}
	elsif ($GetDayData[0] == 0){
		$ConvertedDay = "土・日";
	}
	elsif ($GetDayData[0] == -1){
		$ConvertedDay = $GetDayData[1];
	}
	else{
		if(($GetDayData[0] % 2) == 0){$ConvertedDay .= "月・";}
		if(($GetDayData[0] % 3) == 0){$ConvertedDay .= "火・";}
		if(($GetDayData[0] % 5) == 0){$ConvertedDay .= "水・";}
		if(($GetDayData[0] % 7) == 0){$ConvertedDay .= "木・";}
		if(($GetDayData[0] % 11) == 0){$ConvertedDay .= "金・";}
		if(($GetDayData[0] % 13) == 0){$ConvertedDay .= "土・";}
		if(($GetDayData[0] % 17) == 0){$ConvertedDay .= "日・";}
		chop ($ConvertedDay);
		chop ($ConvertedDay);
	}
	return $ConvertedDay;

}