<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php
  $url = $_SERVER['REQUEST_URI'];  
?>
<div id="page-top">
    <div><a href="#header"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_top.png" alt="TOPに戻る"></a></div>
</div>
<footer>
	<div class="l-footerBlock">
    	<div class="l-inner">
        	<!--フッターメニュー上段-英語-->
            <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
            <div class="l-footerBlock01">
            	<a href="<?php echo home_url();?>/en/">HOME</a>|<a href="<?php echo home_url();?>/en/about/">About ISS</a>|<a href="<?php echo home_url();?>/en/tsuyaku/">Interpretation Services</a><!--|<a href="<?php echo home_url();?>/en/kokusai/">Convention Services</a>-->|<a href="<?php echo home_url();?>/en/personnel/">Personnel Services</a>|<a href="<?php echo home_url();?>/en/training/">Interpreter and Translator Training</a>
            </div>
            <!--フッターメニュー上段-日本語-->       
			<?php else: ?>
            <div class="l-footerBlock01">
            	<a href="<?php echo home_url();?>">HOME</a>|<a href="<?php echo home_url();?>/about/">ISSについて</a>|<a href="<?php echo home_url();?>/tsuyaku/">通訳</a>|<a href="<?php echo home_url();?>/kokusai/">会議・イベント運営</a>|<a href="<?php echo home_url();?>/personnel/">人材サービス</a>|<a href="<?php echo home_url();?>/training/">通訳者・翻訳者養成スクール</a>
            </div>
            <?php endif; ?>            
            
            <!--フッターメニュー中段-英語-->
            <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
            <div class="l-footerBlock02">
            	<div class="l-footerBlock02-01">
            		<a href="<?php echo home_url();?>/en/sitemap">Sitemap</a>|<a href="<?php echo home_url();?>/en/policy">Policy on the Protection of Personal Information</a>|<a href="<?php echo home_url();?>">JAPANESE</a>
                </div>
                <div class="l-footerBlock02-02">
            		<a href="<?php echo home_url();?>/en/inquiry"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/english/btn_inquiry_pc.png" alt="Contact"></a>
                </div>
            </div>
            <!--フッターメニュー中段-日本語-->
            <?php else: ?>
            <div class="l-footerBlock02">
            	<div class="l-footerBlock02-01">
                	<div class="l-footerBlock02-01-table">
            			<div class="l-footerBlock02-01-table-cell">
                       		<a href="<?php echo home_url();?>/sitemap">サイトマップ</a><a href="<?php echo home_url();?>/site">サイトポリシー</a><a href="<?php echo home_url();?>/kankyo">環境方針</a><br class="sponly0"><a href="<?php echo home_url();?>/policy">個人情報保護方針</a><br><a href="<?php echo home_url();?>/en/">ENGLISH</a>
                        </div>
                        <div class="l-footerBlock02-01-table-cell">
                       		<a href="https://privacymark.jp/" target="_blank"><img class="p-image04 footer_privacy" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_privacy_pc.png" alt="たいせつにしますプライバシー"></a>
                        </div>
                        <!-- <div class="l-footerBlock02-01-table-cell">
                       		<a href="http://ea21.jp/" target="_blank"><img class="p-image04 footer_eco" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_eco_pc.png" alt="エコアクション21"></a>
                        </div> -->
                        <div class="l-footerBlock02-01-table-cell">
                       		<img class="p-image04 footer_eco" src="<?php echo get_stylesheet_directory_uri();?>/images/common/eruboshi3.png" alt="えるぼし">
                        </div>
                    </div>
                </div>
                <!--<div class="l-footerBlock02-01">
            		<a href="<?php echo home_url();?>/sitemap">サイトマップ</a><a href="<?php echo home_url();?>/site">サイトポリシー</a><a href="<?php echo home_url();?>/policy">個人情報保護方針</a><a href="https://privacymark.jp/" target="_blank"><img class="p-image04 footer_privacy" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_privacy_pc.png" alt=""></a><span><a href="<?php echo home_url();?>/en/">ENGLISH</a></span>
                </div>-->
                <div class="l-footerBlock02-02">
            		<a href="<?php echo home_url();?>/inquiry"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_inquiry_pc.png" alt="お問い合わせ"></a>
                </div>
            </div>
            <?php endif; ?>
            
            <!--フッターメニュー下段-英語-->
            <?php if(is_page('en') || is_parent_slug() === 'en' || (strstr($url,'/en/')==true)): ?>
            
            <!--フッターメニュー下段-日本語-->
            <?php else: ?>
            <div class="l-footerBlock03">
            	<p>グループサイト</p>
            	<a href="https://haken.issjp.com/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_haken_pc.png" alt="株式会社アイ・エス・エス 通訳・翻訳派遣 グローバル人材サービス"></a><a href="https://www.issnet.co.jp/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_institute_pc.png" alt="アイ・エス・エス・インスティテュート 通訳者・翻訳者養成スクール"></a><a class="p-footer-honyaku" href="https://www.honyakuctr.com/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_honyaku_pc.png" alt="翻訳センター"></a>
              <!-- <a class="" href="https://www.hcls.com/" target="_blank"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_hcls_pc.png" alt="HC Language Solutions, Inc."></a> -->
            </div>
            <?php endif; ?>
        </div>
    </div>
    <!--＝＝＝＝＝＝＝＝＝＝ここから＝＝＝＝＝＝＝＝＝＝-->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 874981274;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="0" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/874981274/?guid=ON&amp;script=0"/>
    </div>
    </noscript>
    <!--＝＝＝＝＝＝＝＝＝＝ここまで＝＝＝＝＝＝＝＝＝＝-->
    <!--＝＝＝＝＝＝＝＝＝＝ここから＝＝＝＝＝＝＝＝＝＝-->
    <!-- Yahoo Code for your Target List -->
    <script type="text/javascript" language="javascript">
    /* <![CDATA[ */
    var yahoo_retargeting_id = 'PEOWJV0NTD';
    var yahoo_retargeting_label = '';
    var yahoo_retargeting_page_type = '';
    var yahoo_retargeting_items = [{item_id: '', category_id: '', price: '', quantity: ''}];
    /* ]]> */
    </script>
    <script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
    <!--＝＝＝＝＝＝＝＝＝＝ここまで＝＝＝＝＝＝＝＝＝＝-->
	<p>© ISS, INC.</p>
</footer>
<?php wp_footer(); ?>
<script type="text/javascript" language="javascript">
function syncerRecaptchaCallback( code ){
	"use strict";
	if(code !== "")
	{
		//$( '#mw_wp_form form' ).removeAttr( 'disabled' ) ;
		//alert('a');
		$(".registerBtn").removeAttr("disabled");
		$(".registerBtn").removeClass("p-disabled");
	}
}
</script>
</body>
</html>
